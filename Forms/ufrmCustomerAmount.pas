unit ufrmCustomerAmount;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, RzButton, RzPanel, Vcl.ExtCtrls, Datasnap.DBClient;

type
  TfrmCustomerAmount = class(TForm)
    tvView: TcxGridDBTableView;
    Lv: TcxGridLevel;
    Grid: TcxGrid;
    RzToolbar10: TRzToolbar;
    RzSpacer77: TRzSpacer;
    RzBut2: TRzToolButton;
    RzSpacer78: TRzSpacer;
    RzBut4: TRzToolButton;
    RzSpacer79: TRzSpacer;
    RzBut5: TRzToolButton;
    RzBut8: TRzToolButton;
    RzSpacer5: TRzSpacer;
    RzBut3: TRzToolButton;
    RzSpacer6: TRzSpacer;
    RzSpacer7: TRzSpacer;
    RzBut6: TRzToolButton;
    RzSpacer1: TRzSpacer;
    RzToolButton1: TRzToolButton;
    RzSpacer3: TRzSpacer;
    tvViewColumn1: TcxGridDBColumn;
    tvViewColumn2: TcxGridDBColumn;
    tvViewColumn3: TcxGridDBColumn;
    tvViewColumn4: TcxGridDBColumn;
    tvViewColumn5: TcxGridDBColumn;
    tvViewColumn6: TcxGridDBColumn;
    tvViewColumn7: TcxGridDBColumn;
    tvViewColumn8: TcxGridDBColumn;
    dsMaster: TClientDataSet;
    dsMasterSounce: TDataSource;
    procedure RzBut2Click(Sender: TObject);
    procedure tvViewGetCellHeight(Sender: TcxCustomGridTableView;
      ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem;
      ACellViewInfo: TcxGridTableDataCellViewInfo; var AHeight: Integer);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCustomerAmount: TfrmCustomerAmount;

implementation

{$R *.dfm}

uses
  ufrmCustomerPost , uDataModule;

procedure TfrmCustomerAmount.FormActivate(Sender: TObject);
begin
//  sk_Decorate_OfferAmount

end;

procedure TfrmCustomerAmount.RzBut2Click(Sender: TObject);
var
  lvCode : string;
  s : string;
  lvExIst : Boolean;
  bm:TBookmark;
  Child : TfrmCustomerPost;
begin
  Child := TfrmCustomerPost.Create(nil);
//  Application.CreateForm(TfrmCustomerPost,frmCustomerPost);
  try
    Child.ShowModal;
    s := Child.cxTextEdit1.Text;
  finally
    Child.Free;
  end;

  with Self.dsMaster do
  begin

    if State <> dsInactive then
    begin
      {
      First;
      lvExIst := False;
      while not Eof do
      begin
        if FieldByName(tvGroupName.DataBinding.FieldName).AsString = Trim( s ) then
        begin
          ShowMessage('相同房间已存在');
          lvExIst := True;
          Break;
        end;
        Next;
      end;
      First;
      if not lvExIst then
      begin
        Insert;
        bm := GetBookmark;
        Post;
        GotoBookmark(bm);
      end;
      if Trim(s) <> '' then
      begin
        GetGroupName(True);
        N15.Click;
      end;
      }
    end;

  end;

end;

procedure TfrmCustomerAmount.tvViewGetCellHeight(Sender: TcxCustomGridTableView;
  ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem;
  ACellViewInfo: TcxGridTableDataCellViewInfo; var AHeight: Integer);
begin
  if AHeight < 23 then  AHeight := 23;
  
end;

end.
