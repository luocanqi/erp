unit ufrmGoodsPrice;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxCustomData, cxStyles, cxTL,
  cxTLdxBarBuiltInMenu, dxSkinsCore, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, cxInplaceContainer, cxTLData, cxDBTL, Vcl.ComCtrls,
  Vcl.ToolWin, cxMaskEdit, System.ImageList, Vcl.ImgList, RzButton, RzPanel,
  dxSkinscxPCPainter, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  Data.DB, cxDBData, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Datasnap.DBClient, cxTextEdit;

type
  TfrmGoodsPrice = class(TForm)
    pnl1: TPanel;
    StatusBar1: TStatusBar;
    cxImageList1: TcxImageList;
    bvl1: TBevel;
    pnl2: TPanel;
    Panel1: TPanel;
    dsTreeList: TClientDataSet;
    DataTreeList: TDataSource;
    tmr1: TTimer;
    tv: TTreeView;
    RzToolbar6: TRzToolbar;
    RzSpacer16: TRzSpacer;
    RzToolButton18: TRzToolButton;
    RzSpacer17: TRzSpacer;
    RzToolButton20: TRzToolButton;
    RzSpacer21: TRzSpacer;
    RzToolButton23: TRzToolButton;
    RzSpacer22: TRzSpacer;
    RzToolButton24: TRzToolButton;
    RzSpacer26: TRzSpacer;
    RzToolButton19: TRzToolButton;
    RzToolbar5: TRzToolbar;
    RzSpacer13: TRzSpacer;
    RzToolButton15: TRzToolButton;
    RzToolButton16: TRzToolButton;
    RzSpacer14: TRzSpacer;
    RzToolButton17: TRzToolButton;
    RzToolButton1: TRzToolButton;
    RzSpacer1: TRzSpacer;
    RzSpacer2: TRzSpacer;
    RzToolButton2: TRzToolButton;
    RzSpacer4: TRzSpacer;
    RzSpacer5: TRzSpacer;
    RzSpacer6: TRzSpacer;
    RzToolButton3: TRzToolButton;
    RzSpacer7: TRzSpacer;
    RzToolButton4: TRzToolButton;
    RzToolButton5: TRzToolButton;
    RzToolButton6: TRzToolButton;
    RzSpacer8: TRzSpacer;
    RzSpacer9: TRzSpacer;
    RzSpacer10: TRzSpacer;
    Grid: TcxGrid;
    tvView: TcxGridDBTableView;
    tvViewColumn1: TcxGridDBColumn;
    tvViewColumn6: TcxGridDBColumn;
    tvViewColumn7: TcxGridDBColumn;
    tvViewColumn2: TcxGridDBColumn;
    tvViewColumn4: TcxGridDBColumn;
    tvViewColumn3: TcxGridDBColumn;
    tvViewColumn5: TcxGridDBColumn;
    tvId: TcxGridDBColumn;
    tvViewColumn8: TcxGridDBColumn;
    Lv: TcxGridLevel;
    procedure tmr1Timer(Sender: TObject);
    procedure RzToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    dwTableName : string;
  end;

var
  frmGoodsPrice: TfrmGoodsPrice;

implementation

{$R *.dfm}

uses
   uDataModule;

procedure TfrmGoodsPrice.RzToolButton1Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmGoodsPrice.tmr1Timer(Sender: TObject);
var
  szSQLText : string;
begin
  dwTableName := 'sk_Maintain_GoodsPriceTree';
  tmr1.Enabled := False;
  szSQLText := 'Select * from '+ dwTableName;
  with DM do
  begin
    ADOQuery1.Close;
    ADOQuery1.SQL.Text := szSQLText;
    ADOQuery1.Open;
    Self.dsTreeList.Data := DataSetProvider1.Data;
    Self.dsTreeList.Open;
  end;
end;

end.
