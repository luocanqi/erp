unit ufrmPayAccounts;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, RzButton, StdCtrls, Mask, RzEdit,ComCtrls, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  cxTextEdit, cxCurrencyEdit, cxMemo, dxSkinBlueprint,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinHighContrast,
  dxSkinMetropolis, dxSkinMetropolisDark, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinSevenClassic, dxSkinSharpPlus, dxSkinTheAsphaltWorld,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint;

type
  TfrmPayAccounts = class(TForm)
    procedure RzBitBtn3Click(Sender: TObject);
    procedure RzBitBtn2Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cxCurrencyEdit1PropertiesChange(Sender: TObject);
    procedure cxCurrencyEdit2PropertiesChange(Sender: TObject);
    procedure cxCurrencyEdit3PropertiesChange(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    g_PostCode : string;
  end;

var
  frmPayAccounts: TfrmPayAccounts;

implementation

uses
  uDataModule,global,ufrmBranch;


{$R *.dfm}

procedure TfrmPayAccounts.cxCurrencyEdit1PropertiesChange(Sender: TObject);
begin
  Self.cxTextEdit1.Text := MoneyConvert(Self.cxCurrencyEdit1.Value);
end;

procedure TfrmPayAccounts.cxCurrencyEdit2PropertiesChange(Sender: TObject);
begin
  Self.cxTextEdit2.Text := MoneyConvert(Self.cxCurrencyEdit2.Value);
end;

procedure TfrmPayAccounts.cxCurrencyEdit3PropertiesChange(Sender: TObject);
begin
  Self.cxTextEdit3.Text := MoneyConvert(Self.cxCurrencyEdit3.Value);
end;

procedure TfrmPayAccounts.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmPayAccounts.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 27 then Close;
  
end;

procedure TfrmPayAccounts.RzBitBtn2Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmPayAccounts.RzBitBtn3Click(Sender: TObject);
var
  sqlstr : string;
  time : string;
  t1,t2 : Currency;
begin
  time := FormatDateTime('c',Now);
  t1 := Self.cxCurrencyEdit2.Value;
  t2 := Self.cxCurrencyEdit3.Value;

  sqlstr := Format('Update t_paymoney as p,t_payoutlist as i set i.status=''1'',p.accountmoney=''%f'',p.paymoney=''%f'',p.accountmoney_time=''%s'',p.remarks=''%s'' where p.code="%s" and i.code=p.code',[
  t1,
  t2,
  time,
  Self.cxMemo1.Text,
  g_PostCode
  ]) ;

  OutputLog(sqlstr);

  with DM.Qry do
  begin
    Close;
    SQL.Clear;
    SQL.Text := sqlstr;
    if 0 <> ExecSQL then
    begin
      Application.MessageBox('����ɹ�!',m_title,MB_OK + MB_ICONQUESTION);
      m_IsModify := True;
    end
    else
    begin
      Application.MessageBox('����ʧ��!',m_title,MB_OK + MB_ICONQUESTION)
    end;

  end;

end;

end.
