unit ufrmManage;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, RzButton, ExtCtrls, DBGridEhGrouping,
  ToolCtrlsEh, DBGridEhToolCtrls, DynVarsEh, MemTableDataEh, Db, ADODB,
  MemTableEh, PrnDbgeh, Menus, RzPanel, GridsEh, DBAxisGridsEh,
  DBGridEh, RzTabs,TreeUtils,TreeFillThrd, Mask, RzEdit, RzLabel, FileCtrl,
  RzFilSys, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, dxSkinsCore, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxClasses, cxGridCustomView, cxGrid, cxMemo,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore,
  dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxPageControlProducer, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxSkinsdxBarPainter, dxSkinsdxRibbonPainter, dxPSCore, dxPScxGridLnk,
  dxPScxGridLayoutViewLnk, dxPScxCommon, cxGridBandedTableView,
  cxGridDBBandedTableView, cxContainer, dxCore, cxDateUtils, cxButtons,
  cxMaskEdit, cxDropDownEdit, cxCalendar, cxTextEdit, cxGroupBox, cxCurrencyEdit,ufrmBaseController,
  EhLibVCL, cxSpinEdit, frxClass, frxDBSet, dxLayoutContainer,
  cxGridCustomLayoutView, cxGridLayoutView, cxGridDBLayoutView,
  cxGridViewLayoutContainer, cxGridCardView, cxGridDBCardView, cxGridChartView,
  cxGridDBChartView, BMDThread, cxLabel, cxSplitter,UtilsTree, FillThrdTree,
  Datasnap.DBClient,UnitADO, cxCheckBox, dxmdaset;

type
  TfrmManage = class(TfrmBaseController)
    pcSys: TRzPageControl;
    TabSheet1: TRzTabSheet;
    TabSheet3: TRzTabSheet;
    Splitter1: TSplitter;
    DataRecord: TDataSource;
    DataMaster: TDataSource;
    Left: TPanel;
    TreeView1: TTreeView;
    RzToolbar1: TRzToolbar;
    RzSpacer1: TRzSpacer;
    RzToolButton1: TRzToolButton;
    RzSpacer2: TRzSpacer;
    RzToolButton2: TRzToolButton;
    RzSpacer3: TRzSpacer;
    RzToolButton3: TRzToolButton;
    RzSpacer4: TRzSpacer;
    RzToolButton4: TRzToolButton;
    RzToolButton5: TRzToolButton;
    RzToolButton6: TRzToolButton;
    RzSpacer7: TRzSpacer;
    tvDealGrid: TcxGridDBTableView;
    Level1: TcxGridLevel;
    cxGrid2: TcxGrid;
    tvDealGridColumn1: TcxGridDBColumn;
    tvDealGridColumn2: TcxGridDBColumn;
    tvDealGridColumn3: TcxGridDBColumn;
    tvDealGridColumn4: TcxGridDBColumn;
    tvDealGridColumn5: TcxGridDBColumn;
    tvDealGridColumn6: TcxGridDBColumn;
    tvDealGridColumn7: TcxGridDBColumn;
    tvDealGridColumn8: TcxGridDBColumn;
    tvDealGridColumn9: TcxGridDBColumn;
    tvDealGridColumn10: TcxGridDBColumn;
    tvDealGridColumn11: TcxGridDBColumn;
    tvDealGridColumn12: TcxGridDBColumn;
    tvDealGridColumn13: TcxGridDBColumn;
    tvDealGridColumn14: TcxGridDBColumn;
    tvDealGridColumn15: TcxGridDBColumn;
    tvDealGridColumn16: TcxGridDBColumn;
    Print: TPopupMenu;
    N1: TMenuItem;
    RzToolbar2: TRzToolbar;
    RzSpacer5: TRzSpacer;
    RzToolButton7: TRzToolButton;
    RzSpacer8: TRzSpacer;
    RzToolButton8: TRzToolButton;
    RzSpacer9: TRzSpacer;
    RzToolButton9: TRzToolButton;
    RzSpacer10: TRzSpacer;
    RzToolButton10: TRzToolButton;
    RzToolButton11: TRzToolButton;
    RzToolButton12: TRzToolButton;
    RzSpacer12: TRzSpacer;
    RzToolButton13: TRzToolButton;
    RzSpacer13: TRzSpacer;
    RzToolButton14: TRzToolButton;
    N5: TMenuItem;
    Execl1: TMenuItem;
    DataView: TDataSource;
    DataGroup: TDataSource;
    RzSpacer15: TRzSpacer;
    RzToolButton16: TRzToolButton;
    RzPanel3: TRzPanel;
    RzToolbar3: TRzToolbar;
    RzSpacer16: TRzSpacer;
    RzToolButton17: TRzToolButton;
    RzSpacer17: TRzSpacer;
    RzToolButton18: TRzToolButton;
    RzSpacer18: TRzSpacer;
    RzToolButton19: TRzToolButton;
    RzPanel2: TRzPanel;
    RzPanel5: TRzPanel;
    RzPanel6: TRzPanel;
    RzPanel7: TRzPanel;
    RzPanel8: TRzPanel;
    RzPanel9: TRzPanel;
    RzPanel10: TRzPanel;
    RzPanel11: TRzPanel;
    RzPanel12: TRzPanel;
    RzPanel13: TRzPanel;
    RzPanel14: TRzPanel;
    RzPanel15: TRzPanel;
    RzPanel16: TRzPanel;
    RzPanel17: TRzPanel;
    RzPanel18: TRzPanel;
    BMDThread1: TBMDThread;
    tvDealGridColumn17: TcxGridDBColumn;
    RzSpacer14: TRzSpacer;
    RzSpacer19: TRzSpacer;
    cxLabel1: TcxLabel;
    cxDateEdit1: TcxDateEdit;
    RzSpacer20: TRzSpacer;
    cxLabel2: TcxLabel;
    cxDateEdit2: TcxDateEdit;
    RzSpacer21: TRzSpacer;
    cxLabel3: TcxLabel;
    cxDateEdit3: TcxDateEdit;
    RzSpacer6: TRzSpacer;
    cxLabel4: TcxLabel;
    cxDateEdit4: TcxDateEdit;
    RzSpacer22: TRzSpacer;
    RzSpacer11: TRzSpacer;
    RzToolButton15: TRzToolButton;
    RzSpacer23: TRzSpacer;
    RzToolButton20: TRzToolButton;
    RzPanel19: TRzPanel;
    cxSplitter3: TcxSplitter;
    tvDealGridColumn18: TcxGridDBColumn;
    RzSpacer24: TRzSpacer;
    RzToolButton21: TRzToolButton;
    RzSpacer25: TRzSpacer;
    RzToolButton22: TRzToolButton;
    RzSpacer26: TRzSpacer;
    RzToolButton23: TRzToolButton;
    Master: TClientDataSet;
    RzPanel1: TRzPanel;
    RzPanel4: TRzPanel;
    RzPanel20: TRzPanel;
    Splitter2: TSplitter;
    Grid2: TcxGrid;
    tvGroup: TcxGridDBTableView;
    tvGroupColumn1: TcxGridDBColumn;
    tvGroupColumn2: TcxGridDBColumn;
    tvGroupColumn3: TcxGridDBColumn;
    tvGroupColumn4: TcxGridDBColumn;
    tvGroupColumn6: TcxGridDBColumn;
    lv3: TcxGridLevel;
    Grid3: TcxGrid;
    tvDebt: TcxGridDBTableView;
    cxGridDBColumn1: TcxGridDBColumn;
    cxGridDBColumn2: TcxGridDBColumn;
    cxGridDBColumn3: TcxGridDBColumn;
    cxGridDBColumn6: TcxGridDBColumn;
    lv4: TcxGridLevel;
    tvDealGridColumn19: TcxGridDBColumn;
    ADORecord: TClientDataSet;
    Grid1: TcxGrid;
    tvRecord: TcxGridDBTableView;
    tvRecordColumn1: TcxGridDBColumn;
    tvRecordColumn2: TcxGridDBColumn;
    tvRecordColumn3: TcxGridDBColumn;
    tvRecordColumn4: TcxGridDBColumn;
    tvRecordColumn5: TcxGridDBColumn;
    tvRecordColumn6: TcxGridDBColumn;
    tvRecordColumn7: TcxGridDBColumn;
    tvRecordColumn8: TcxGridDBColumn;
    tvRecordColumn9: TcxGridDBColumn;
    tvRecordColumn10: TcxGridDBColumn;
    tvRecordColumn11: TcxGridDBColumn;
    tvRecordColumn12: TcxGridDBColumn;
    tvRecordColumn13: TcxGridDBColumn;
    tvRecordColumn14: TcxGridDBColumn;
    tvRecordColumn15: TcxGridDBColumn;
    tvRecordColumn16: TcxGridDBColumn;
    tvRecordColumn17: TcxGridDBColumn;
    tvRecordColumn18: TcxGridDBColumn;
    lv1: TcxGridLevel;
    lv2: TcxGridLevel;
    tvView: TcxGridDBTableView;
    tvViewColumn1: TcxGridDBColumn;
    tvViewColumn2: TcxGridDBColumn;
    tvViewColumn3: TcxGridDBColumn;
    tvViewColumn4: TcxGridDBColumn;
    tvViewColumn5: TcxGridDBColumn;
    tvViewColumn6: TcxGridDBColumn;
    tvViewColumn7: TcxGridDBColumn;
    tvViewColumn8: TcxGridDBColumn;
    tvViewColumn9: TcxGridDBColumn;
    tvViewColumn10: TcxGridDBColumn;
    tvViewColumn11: TcxGridDBColumn;
    tvViewColumn12: TcxGridDBColumn;
    tvViewColumn13: TcxGridDBColumn;
    tvViewColumn14: TcxGridDBColumn;
    tvViewColumn15: TcxGridDBColumn;
    tvViewColumn16: TcxGridDBColumn;
    ADOGroup: TClientDataSet;
    ADOView: TADOQuery;
    tvDebtColumn1: TcxGridDBColumn;
    tvGroupColumn5: TcxGridDBColumn;
    DataDebt: TDataSource;
    ADODebt: TADOQuery;
    tvDebtColumn2: TcxGridDBColumn;
    tvDebtColumn3: TcxGridDBColumn;
    pm1: TPopupMenu;
    pm2: TPopupMenu;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N6: TMenuItem;
    N7: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    N10: TMenuItem;
    N11: TMenuItem;
    N12: TMenuItem;
    N13: TMenuItem;
    N14: TMenuItem;
    N15: TMenuItem;
    pmFrozen: TPopupMenu;
    MenuItem16: TMenuItem;
    MenuItem17: TMenuItem;
    RzSpacer27: TRzSpacer;
    RzToolButton24: TRzToolButton;
    N16: TMenuItem;
    N17: TMenuItem;
    N18: TMenuItem;
    N19: TMenuItem;
    N20: TMenuItem;
    N21: TMenuItem;
    N22: TMenuItem;
    N23: TMenuItem;
    RzSpacer28: TRzSpacer;
    RzToolButton25: TRzToolButton;
    RzToolbar4: TRzToolbar;
    RzSpacer29: TRzSpacer;
    RzToolButton26: TRzToolButton;
    PopupMenu1: TPopupMenu;
    Excel1: TMenuItem;
    MenuItem1: TMenuItem;
    Bevel1: TBevel;
    ds1: TDataSource;
    RzPageControl1: TRzPageControl;
    TabSheet2: TRzTabSheet;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1Column1: TcxGridDBColumn;
    cxGrid1DBTableView1Column2: TcxGridDBColumn;
    cxGrid1DBTableView1Column3: TcxGridDBColumn;
    Level2: TcxGridLevel;
    cxGrid1DBTableView1Column4: TcxGridDBColumn;
    dxMemData1: TdxMemData;
    dxMemData1c: TStringField;
    dxMemData1t: TCurrencyField;
    dxMemData1d: TStringField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure RzBitBtn27Click(Sender: TObject);
    procedure tvGrid1Column1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure RzToolButton1Click(Sender: TObject);
    procedure RzToolButton2Click(Sender: TObject);
    procedure tvDealGridColumn1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure RzToolButton7Click(Sender: TObject);
    procedure RzToolButton13Click(Sender: TObject);
    procedure RzToolButton6Click(Sender: TObject);
    procedure tvGrid3Column1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure tvDealGridEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure RzToolButton16Click(Sender: TObject);
    procedure RzToolButton17Click(Sender: TObject);
    procedure tvDealGridKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure tvGrid3Column18CustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure cxGrid1DBLayoutView1Item1GetDisplayText(
      Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
      var AText: string);
    procedure tvGroupColumn1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure tvView1Column1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure tvDealGridColumnSizeChanged(Sender: TcxGridTableView;
      AColumn: TcxGridColumn);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure RzToolButton14Click(Sender: TObject);
    procedure tvDealGridTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
      Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
      var AText: string);
    procedure tvView1ColumnSizeChanged(Sender: TcxGridTableView;
      AColumn: TcxGridColumn);
    procedure RzToolButton15Click(Sender: TObject);
    procedure RzToolButton20Click(Sender: TObject);
    procedure RzPanel3Resize(Sender: TObject);
    procedure RzToolButton21Click(Sender: TObject);
    procedure Execl1Click(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure RzToolButton22Click(Sender: TObject);
    procedure tvDealGridEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure RzToolButton23Click(Sender: TObject);
    procedure MasterAfterInsert(DataSet: TDataSet);
    procedure pcSysTabClick(Sender: TObject);
    procedure TreeView1DblClick(Sender: TObject);
    procedure RzPanel1Resize(Sender: TObject);
    procedure MasterAfterOpen(DataSet: TDataSet);
    procedure MasterAfterClose(DataSet: TDataSet);
    procedure ADORecordAfterInsert(DataSet: TDataSet);
    procedure tvRecordColumn1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure tvRecordColumn16PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure tvViewColumn1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure tvRecordColumnSizeChanged(Sender: TcxGridTableView;
      AColumn: TcxGridColumn);
    procedure tvRecordColumn16CustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure tvRecordTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
      Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
      var AText: string);
    procedure tvRecordEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure tvRecordStylesGetContentStyle(Sender: TcxCustomGridTableView;
      ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem;
      var AStyle: TcxStyle);
    procedure TreeView1Change(Sender: TObject; Node: TTreeNode);
    procedure ADORecordAfterOpen(DataSet: TDataSet);
    procedure ADORecordAfterClose(DataSet: TDataSet);
    procedure tvRecordEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure tvGroupTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
      Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
      var AText: string);
    procedure tvDealGridStylesGetContentStyle(Sender: TcxCustomGridTableView;
      ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem;
      var AStyle: TcxStyle);
    procedure tvDealGridColumn15PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure tvDealGridColumn12PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure tvDealGridColumn13PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure N6Click(Sender: TObject);
    procedure N7Click(Sender: TObject);
    procedure N8Click(Sender: TObject);
    procedure N10Click(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure tvDebtTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
      Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
      var AText: string);
    procedure N14Click(Sender: TObject);
    procedure N13Click(Sender: TObject);
    procedure N15Click(Sender: TObject);
    procedure MenuItem16Click(Sender: TObject);
    procedure N22Click(Sender: TObject);
    procedure N17Click(Sender: TObject);
    procedure N18Click(Sender: TObject);
    procedure N20Click(Sender: TObject);
    procedure N21Click(Sender: TObject);
    procedure Excel1Click(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure N23Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
  private
    { Private declarations }
    dwCompanyMoney : Currency;
    dwADO : TADO;
    dwMasterSQLText : string;
    dwRecordSQLText : string;
    g_GroupMoney  : Currency;
    g_sum_Money   : Currency;
    g_Paid        : Currency;
    g_ProjectList : string;
    g_ProjectName : string;
    g_PatTree: TNewUtilsTree;
    g_ProjectDir_1 : string; //明细目录
    g_ProjectDir_2 : string; //收款目录
    g_TableNameTree : string;
    g_Prefix : string;  //前缀
    g_Suffix : string;  //后缀
    g_TopLevelName : string; //顶级名称
    g_TopLevelCode : string; //顶用编号
    function GetSumCount(ADD1 : Double ; ADD2 : Currency):Boolean;
    procedure ShowSumMoney();
  public
    { Public declarations }
    g_PactCode: string;
    g_CodeList: string;
    procedure RefreshDetailTable();
    procedure ShowSettleAccounts(AIndex : Byte ; ACode : string); //显示结算
    procedure SetcxGrid(lpGridView : TcxGridDBTableView ; index : Byte ;lpSuffix:string);
  end;

var
  frmManage: TfrmManage;

implementation

uses
  ufrmRevenueProject,
  ufrmDetailed,
  ufrmIsViewGrid,
  cxGridExportLink,
  uDataModule,
  global,
  ufrmAccounts,
  ShellAPI,
  PrViewEh,
  Printers,
//  ufrmBranchInput,
  ufunctions,
  Math,
  ufrmPostPact,
  ufrmReceivablesAccount;

{$R *.dfm}

procedure TfrmManage.SetcxGrid(lpGridView : TcxGridDBTableView ; index : Byte ;lpSuffix:string);
var
  i : Integer;
  szFieldName : string;
  szCaption   : string;
  szName : string;

begin
  for I := 0 to lpGridView.ColumnCount-1 do
  begin
    Application.ProcessMessages;
    SetGrid(lpGridView.Columns[i],index,Self.Name + lpSuffix);
  end;
end;

procedure TfrmManage.ADORecordAfterClose(DataSet: TDataSet);
begin
  inherited;
  Self.RzToolButton7.Enabled := False;
  Self.RzToolButton23.Enabled:= False;
  Self.RzToolButton16.Enabled:= False;
  Self.RzToolButton8.Enabled := False;
  Self.RzToolButton9.Enabled := False;
  Self.RzToolButton10.Enabled:= False;
  Self.RzToolButton11.Enabled:= False;
  Self.RzToolButton13.Enabled:= False;
  Self.RzToolButton20.Enabled:= False;
  Self.RzToolButton25.Enabled:= False;

  Self.N23.Enabled := False;
  Self.N6.Enabled  := False;
  Self.N7.Enabled  := False;
  Self.N8.Enabled  := False;
  Self.N10.Enabled := False;
  Self.N15.Enabled := False;
  Self.N20.Enabled := False;
  Self.N21.Enabled := False;


  Self.cxDateEdit3.Enabled := False;
  Self.cxDateEdit4.Enabled := False;

end;

procedure TfrmManage.ADORecordAfterInsert(DataSet: TDataSet);
var
  szName : string;
  szCode : string;
  Prefix : string;
  Suffix : string;

begin
  inherited;
  Prefix := 'JY';
  Suffix := '100000';
  with DataSet do
  begin
    FieldByName('Code').Value := g_PactCode;
    Self.tvRecordColumn2.EditValue:= True;
    szName := Self.tvRecordColumn3.DataBinding.FieldName;
    szCode := Prefix + GetRowCode( g_Table_Income ,
                                        szName,
                                        Suffix,
                                        920000 + Self.tvRecord.DataController.RecordCount);
    FieldByName(szName).Value     := szCode;
    Self.tvRecordColumn3.EditValue := szCode;
    Self.tvRecordColumn4.EditValue := Date;
    Self.tvRecordColumn5.EditValue := g_ProjectName;
    Self.tvRecordColumn11.EditValue:= g_TopLevelName;
  end;
end;

procedure TfrmManage.ADORecordAfterOpen(DataSet: TDataSet);
begin
  inherited;
  with DataSet do
  begin
    Self.RzToolButton7.Enabled := True;
    Self.RzToolButton23.Enabled:= True;
    Self.RzToolButton16.Enabled:= True;
    Self.RzToolButton8.Enabled := True;
    Self.RzToolButton9.Enabled := True;
    Self.RzToolButton10.Enabled:= True;
    Self.RzToolButton11.Enabled:= True;
    Self.RzToolButton13.Enabled:= True;
    Self.RzToolButton20.Enabled:= True;
    Self.RzToolButton25.Enabled:= True;

    Self.N23.Enabled := True;
    Self.N6.Enabled  := True;
    Self.N7.Enabled  := True;
    Self.N8.Enabled  := True;
    Self.N10.Enabled := True;
    Self.N15.Enabled := True;
    Self.N20.Enabled := True;
    Self.N21.Enabled := True;

    Self.cxDateEdit3.Enabled := True;
    Self.cxDateEdit4.Enabled := True;

  end;
end;

procedure TfrmManage.cxGrid1DBLayoutView1Item1GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  inherited;

  AText := IntToStr(ARecord.Index + 1);
end;

procedure TfrmManage.Excel1Click(Sender: TObject);
begin
  inherited;
  //输出Excel
  CxGridToExcel(Self.Grid2,'交易记录明细表');
end;

procedure TfrmManage.Execl1Click(Sender: TObject);
begin
  inherited;
  case Self.pcSys.ActivePageIndex of
    0:  CxGridToExcel(Self.cxGrid2,'合同总价明细表');
    1:  CxGridToExcel(Self.Grid1,'交易记录明细表');
  end;
end;

procedure TfrmManage.FormActivate(Sender: TObject);
var
  List : TStringList;
  i : Integer;

begin
  inherited;
  Self.cxDateEdit1.Date:= Date;
  Self.cxDateEdit2.Date:= Date;
  Self.cxDateEdit3.Date:= Date;
  Self.cxDateEdit4.Date:= Date;
  {
  Self.RzToolButton1.Width := 70;
  Self.RzToolButton2.Width := 70;
  Self.RzToolButton3.Width := 70;
  Self.RzToolButton4.Width := 70;
  Self.RzToolButton5.Width := 70;
  Self.RzToolButton6.Width := 70;
  }
  Self.pcSys.TabIndex := 0;
  g_PatTree := TNewUtilsTree.Create(Self.TreeView1,DM.ADOconn,g_Table_RevenueTree,'收款管理');
  g_PatTree.GetTreeTable( g_ParentIndex  );

  g_ProjectDir_1 := Concat(g_Resources,'\' + g_CollectReceivables); //明细目录
  g_ProjectDir_2 := Concat(g_Resources,'\' + g_income); //收款目录

end;

procedure TfrmManage.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmManage.FormCreate(Sender: TObject);
var
  List : TStringList;
  I : Integer;
begin
  inherited;
  with (Self.tvDealGridColumn14.Properties as TcxComboBoxProperties ) do
  begin
    Items.Clear;
    List := TStringList.Create;
    try
      GetCompany(List,0);
      for I := 0 to List.Count-1 do Items.Add(List.Strings[i]);
    finally
      List.Free;
    end;

  end;
  SetcxGrid(Self.tvDealGrid,0,g_CollectReceivables);
  SetcxGrid(Self.tvRecord,0,g_income);
  SetcxGrid(Self.tvView,0,g_Dir_Collect_DetailTable);
end;

procedure TfrmManage.MasterAfterClose(DataSet: TDataSet);
begin
  inherited;
  Self.RzToolButton1.Enabled := False;
  Self.RzToolButton2.Enabled := False;
  Self.RzToolButton3.Enabled := False;
  Self.RzToolButton4.Enabled := False;
  Self.RzToolButton5.Enabled := False;
  Self.RzToolButton15.Enabled:= False;
  Self.RzToolButton22.Enabled:= False;
  Self.RzToolButton24.Enabled:= False;
  Self.cxDateEdit1.Enabled := False;
  Self.cxDateEdit2.Enabled := False;

  Self.N22.Enabled := False;
  Self.N2.Enabled  := False;
  Self.N3.Enabled  := False;
  Self.N4.Enabled  := False;
  Self.N13.Enabled := False;
  Self.N18.Enabled := False;
  Self.N17.Enabled := False;
end;

procedure TfrmManage.MasterAfterInsert(DataSet: TDataSet);
var
  szName : string;
  szCode : string;
  Prefix : string;
  Suffix : string;
begin
  Prefix := 'YS';
  Suffix := '000000';
  inherited;
  with DataSet do
  begin
    FieldByName('Code').Value := g_PactCode;
    Self.tvDealGridColumn19.EditValue := True;
    szName := Self.tvDealGridColumn2.DataBinding.FieldName;
    szCode := Prefix + GetRowCode( g_Table_Collect_Receivables ,
                                        szName,
                                        Suffix,
                                        900000 + Self.tvDealGrid.DataController.RecordCount);
    Self.tvDealGridColumn2.EditValue  := szCode;
    Self.tvDealGridColumn3.EditValue  := Date;
    Self.tvDealGridColumn4.EditValue  := g_ProjectName;
    Self.tvDealGridColumn9.EditValue  := g_TopLevelName;
    Self.tvDealGridColumn10.EditValue := Date;
    Self.tvDealGridColumn11.EditValue := Date;
    Self.tvDealGridColumn12.EditValue := 0;
    Self.tvDealGridColumn13.EditValue := 0;
    Self.tvDealGridColumn15.EditValue := 0;
  end;
end;

procedure TfrmManage.MasterAfterOpen(DataSet: TDataSet);
begin
  inherited;
  Self.RzToolButton1.Enabled := True;
  Self.RzToolButton2.Enabled := True;
  Self.RzToolButton3.Enabled := True;
  Self.RzToolButton4.Enabled := True;
  Self.RzToolButton5.Enabled := True;
  Self.RzToolButton15.Enabled:= True;
  Self.RzToolButton22.Enabled:= True;
  Self.RzToolButton24.Enabled:= True;

  Self.N22.Enabled := True;
  Self.N2.Enabled  := True;
  Self.N3.Enabled  := True;
  Self.N4.Enabled  := True;
  Self.N13.Enabled := True;
  Self.N18.Enabled := True;
  Self.N17.Enabled := True;

  Self.cxDateEdit1.Enabled := True;
  Self.cxDateEdit2.Enabled := True;
end;

procedure TfrmManage.MenuItem16Click(Sender: TObject);
var
  s : string;

begin
  inherited;
  //查看冻结
  if Sender = Self.MenuItem16 then
  begin
    s := 'no';
  end
  else
  if Sender = Self.MenuItem17 then
  begin
    s := 'yes'
  end;

  case Self.pcSys.ActivePageIndex of
    0:
    begin
      Self.Master.Close;
      with DM do
      begin
        ADOQuery1.Close;
        ADOQuery1.SQL.Clear;
        ADOQuery1.SQL.Text := 'Select * from ' + g_Table_Collect_Receivables +
                               ' where ' + g_CodeList + ' AND ' + Self.tvDealGridColumn19.DataBinding.FieldName +'=' + s ;
        ADOQuery1.Open;
        Self.Master.Data := DataSetProvider1.Data;
        Self.Master.Open;
      end;
    end;
    1:
    begin
      Self.ADORecord.Close;
      with DM do
      begin
        ADOQuery1.Close;
        ADOQuery1.SQL.Clear;
        ADOQuery1.SQL.Text := 'Select * from '+ g_Table_Income  + ' where ' + g_CodeList + ' AND ' + Self.tvRecordColumn2.DataBinding.FieldName + '=' + s;
        ADOQuery1.Open;
        Self.ADORecord.Data := DataSetProvider1.Data;
        Self.ADORecord.Open;
      end;

    end;
  end;
end;

procedure TfrmManage.MenuItem1Click(Sender: TObject);
begin
  inherited;
  DM.BasePrinterLink1.Component := Self.Grid2;
  DM.BasePrinterLink1.ReportTitleText := '项目名称：' + g_TopLevelName;
  DM.BasePrinter.Preview(True, nil);
end;

procedure TfrmManage.N10Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton13.Click;
end;

procedure TfrmManage.N13Click(Sender: TObject);
var
  szNumbers : string;
begin
  inherited;
  szNumbers := VarToStr( Self.tvDealGridColumn2.EditValue );
  CreateOpenDir(Handle,Concat(g_ProjectDir_1,'\' + szNumbers),True);
end;

procedure TfrmManage.N14Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton21.Click;
end;

procedure TfrmManage.N15Click(Sender: TObject);
var
  szNumbers : string;
begin
  inherited;
  szNumbers := VarToStr( Self.tvRecordColumn3.EditValue );
  CreateOpenDir(Handle,Concat(g_ProjectDir_2,'\' + szNumbers),True);
end;

procedure TfrmManage.N17Click(Sender: TObject);
begin
  inherited;
  //冻结
  SetGridStuat(Self.tvDealGrid,Self.tvDealGridColumn19.DataBinding.FieldName,False);
end;


procedure TfrmManage.N18Click(Sender: TObject);
begin
  inherited;
  //解冻
  SetGridStuat(Self.tvDealGrid, Self.tvDealGridColumn19.DataBinding.FieldName,True);
end;

procedure TfrmManage.N1Click(Sender: TObject);
begin
  inherited;
  case Self.pcSys.ActivePageIndex of
    0: DM.BasePrinterLink1.Component := Self.cxGrid2;
    1: DM.BasePrinterLink1.Component := Self.Grid1;
  end;
//  DM.BasePrinterLink1.ReportTitleText := '项目名称：' + g_Projectname;
  DM.BasePrinter.Preview(True, nil);
end;

procedure TfrmManage.N20Click(Sender: TObject);
begin
  inherited;
  SetGridStuat(Self.tvRecord,Self.tvRecordColumn2.DataBinding.FieldName,False);
end;

procedure TfrmManage.N21Click(Sender: TObject);
begin
  inherited;
  SetGridStuat(Self.tvRecord,Self.tvRecordColumn2.DataBinding.FieldName,True);
end;

procedure TfrmManage.N22Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton1.Click;
end;

procedure TfrmManage.N23Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton7.Click;
end;

procedure TfrmManage.N2Click(Sender: TObject);
begin
  inherited;
  RzToolButton22.Click;
end;

procedure TfrmManage.N3Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton2.Click;
end;

procedure TfrmManage.N4Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton3.Click;
end;

procedure TfrmManage.N6Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton23.Click;
end;

procedure TfrmManage.N7Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton8.Click;
end;

procedure TfrmManage.N8Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton9.Click;
end;

procedure TfrmManage.pcSysTabClick(Sender: TObject);
{
var
  Node : TTreeNode;
  szNode : PNodeData;
  CodeList : string;
  szColName  : string;
  szTableName: string;
  t : Integer;
  szSQLText : string;
  }
begin
  inherited;
//  Self.TreeView1DblClick(Sender);
//  Node := Self.TreeView1.Selected;
//  if Assigned(Node) then
//  begin
    {
    szNode :=  PNodeData(Node.Data);
    if Assigned(szNode) then
    begin
      g_ProjectName := Node.Text;// string;
      if Node.Count = 0 then
      begin
        g_PactCode := szNode.Code;
        szSQLText := 'code="'+ g_PactCode + '"';  //没有子节点

      end else
      begin
        treeCldnode(Node,CodeList);
        g_PactCode := CodeList;
        szSQLText := 'code in('''+ g_PactCode  +''')'; //取得所有子节点
        g_CodeList := szSQLText;
      end;

      case Self.pcSys.ActivePageIndex of
        0:
        begin
          t := GetTickCount;

          dwMasterSQLText := 'Select * from ' + g_Table_Collect_Receivables; //+ ' where ' + szSQLText;
          with DM do
          begin
            ADOQuery1.Close;
            ADOQuery1.SQL.Text := dwMasterSQLText;
            ADOQuery1.Open;
            Self.Master.Data := DataSetProvider1.Data;
            Self.Master.Open;
          end;
          ShowMessage(IntToStr(GetTickCount - t ));
        end;
        1:
        begin
          with Self.ADOQuery1 do
          begin
            Close;
            SQL.Clear;
            SQL.Text := 'Select * from '+ g_Table_Income  + ' where ' + szSQLText;// ' where code="' + g_PactCode + '"';
            open;
          end;

        end;

      end;
      }



          //Self.RefreshDetailTable;
          {
          //显示结算过信息
          with Self.ADOQuery4 do
          begin
            Close;
            SQL.Clear;
            SQL.Text := 'Select * from '+ g_Table_Pactmoney  +' where code="' + g_PactCode + '" and status=1';
            Open;
          end;

          with DM.Qry do
          begin
            Close;
            SQL.Clear;
            SQL.Text := 'Select * from '+ g_Table_Pactmoney +' where code="' +  g_PactCode + '"';
            Open;

            if RecordCount <> 0 then
               SQL.Text := Format('Update '+ g_Table_Pactmoney +' set sum_money=''%m'' where code="%s"',[ g_Sum_Money, g_PactCode ])
            else
               SQL.Text := 'Insert into '+ g_Table_Pactmoney +' (code,sum_money) values("'+ g_PactCode +'",'+ FloatToStr( g_Sum_Money ) +')';

            ExecSQL;

          end;
          }






        {
        g_TopLevelName:= GetTreeLevelName(Node);
        }

        {
        //取出项目名称
        szColName := Self.ProjectCol2.DataBinding.FieldName;
        szTableName := GetModuleTableName;
        if Length(szTableName) <> 0 then
        begin
          if g_ModuleIndex = 2 then
          begin

          end else
          begin

            s := 'SELECT '+ szColName + ' FROM '+ szTableName +
               ' where SignName="' + Node.Text +'" GROUP BY ' + szColName;

          end;

          with Self.ADOProjectName do
          begin
            Close;
            SQL.Clear;
            SQL.Text := s;
            Open;
          end;

        end;
        }
      {
      g_PostCode    := szNode.Code;
      g_CodeList := 'Code in('''+ g_CodeList + ''')';

      g_TopLevelName:= GetTreeLevelName(Node);
      with Self.final do
      begin
        Close;
        SQL.Text := 'Select * from '+ g_Table_Finance_ProjectFinal +' Where ' + g_CodeList;
        Open;
      end;

      with Self.Master do
      begin
        Close;
        SQL.Clear;
        SQL.Text := 'Select * from ' + g_Table_BranchMoney + ' where ' + g_CodeList;
        Open;
      end;
      Self.RefreshDetailTable;

      //g_Table_Maintain_CompanyInfoList //员工表
      //g_Table_Company_Staff_CheckWork  //考勤表

      s := 'select t3.SignName,t3.Parent from ('+ g_Table_Maintain_CompanyInfoList +
           ' AS t1 right join '+ g_Table_Company_Staff_CheckWork +
           ' AS t2 on t1.id=t2.Parent) left join ' + g_Table_Maintain_CompanyInfoTree +
           ' AS t3 on t1.TreeId=t3.Parent WHERE  t1.Contacts = "'+ g_SignName +'" GROUP BY  t3.SignName,t3.Parent';
      with Self.ADOYears do
      begin
        Close;
        SQL.Clear;
        SQL.Text := s;
        Open;
      end;
      }
  //  end;

//  end;
end;

procedure TfrmManage.RefreshDetailTable();
begin

  with Self.ADOView do
  begin
    Close;
    SQL.Clear;
    SQL.Text := 'Select * from ' + g_Table_DetailTable;
    Open;

  //  if RecordCount <> 0 then SetcxGrid(Self.tvView,0,g_DetailTable);
    
  end;

end;

procedure TfrmManage.RzBitBtn27Click(Sender: TObject);
var
  s : string;
  Node: TTreeNode;
  Code: string;
  szNode : PNodeData;
// Child : TfrmAppropriation;
begin
 {
  if m_user.dwType = 0 then
  begin
    Child := TfrmAppropriation.Create(Self);
    try
      Child.g_TableName := g_Table_DetailTable;
      Child.g_Prefix    := '';

        if Sender = RzBitBtn27 then
        begin
          //添加
          Node := Self.TreeView1.Selected;
          if Assigned(Node) then
          begin
            szNode := PNodeData(Node.Data);
            Child.g_IsModify := False;
            Child.cxDateEdit1.Date := Date;
            Child.g_PostNumbers := DM.getDataMaxDate(g_Table_DetailTable);
            Child.g_PostCode := szNode.Code;
            Child.ShowModal;
            Self.TreeView1Click(Sender);
          end
          else
          begin
            Application.MessageBox('请选择要添加的合同!!',m_title,MB_OK + MB_ICONQUESTION )
          end;

        end else
        if Sender = RzBitBtn28 then
        begin
          //编辑

          with cxGridView.DataController.DataSource.DataSet do
          begin
            if not IsEmpty then
            begin
              Child.g_PostNumbers := FieldByName('Numbers').AsString;
              Child.g_IsModify := True;
              Child.cxMemo1.Text := FieldByName('Appropriation').AsString;
              Child.cxMemo2.Text := FieldByName('fine').AsString;
              Child.cxMemo3.Text := FieldByName('Armourforwood').AsString;
              Child.cxMemo4.Text := FieldByName('Realmoney').AsString;
              Child.cxMemo5.Text := FieldByName('tax').AsString;
              Child.cxMemo6.Text := FieldByName('Invoice').AsString;
              Child.cxMemo7.Text := FieldByName('Hydropower').AsString;
              Child.cxMemo8.Text := FieldByName('LtIsa').AsString;
              Child.cxMemo9.Text := FieldByName('Remarks').AsString;
              Child.cxMemo10.Text:= FieldByName('Formula').AsString;
              Child.cxDateEdit1.Text := FieldByName('Dates').AsString;
              Child.cxCurrencyEdit1.Value := FieldByName('receiptsMoney').AsCurrency;
              Child.ShowModal;
              Self.TreeView1Click(Sender);
            end;

          end;

        end else
        if Sender = RzBitBtn29 then
        begin
          //删除
        //  cxGrid1DBTableView1.OptionsSelection设置MultiSelect:=True;
          cxGridView.DataController.DeleteSelection;
          Self.TreeView1Click(Sender);
        end;

    finally
      Child.Free;
    end;

  end;
   }
end;

procedure TfrmManage.RzPanel1Resize(Sender: TObject);
begin
  inherited;
  Self.RzPanel20.Width := Round(Self.RzPanel1.Width / 2);
end;

procedure TfrmManage.RzPanel3Resize(Sender: TObject);
var
  szWidth : Double;
begin
  inherited;
  szWidth    := Self.RzPanel3.Width / 3;
  Self.RzPanel2.Width:= Round(szWidth);
  Self.RzPanel5.Width:= Round(szWidth);
  Self.RzPanel6.Width:= Round(szWidth);
end;

procedure TfrmManage.RzToolButton13Click(Sender: TObject);
var
  Node : TTreeNode;
  sqlstr : string;
  Num , total: Currency;
  Child: TfrmAccounts;
  szNode : PNodeData;
  szSumMoney : Currency;
  i : Integer;
  szDebtMoney : Currency;

begin
  inherited;
  Node := Self.TreeView1.Selected;
  if Assigned(Node) then
  begin
    if Node.Count = 0 then
    begin
      Child := TfrmAccounts.Create(Application);
      try
    
        {
        case i of
          0:
          begin
            //支
            szSumMoney := g_Sum_Money - (g_Paid + g_GroupMoney); //剩余
          end;
          1:
          begin
            //收
            szSumMoney := g_Sum_Money - (g_Paid +  g_GroupMoney); //剩余
          end;
          2:
          begin
            //清
            szSumMoney := g_Sum_Money - (g_Paid + g_GroupMoney); //剩余
          end;
        else    
        end;
        }
        szSumMoney := g_Sum_Money - (g_Paid + g_GroupMoney); //剩余
        
        Child.g_ParentName   := g_TopLevelName;
        Child.g_ParentCode   := g_TopLevelCode;
        Child.g_ProjectName  := Node.Text;
        Child.g_ProjectCode  := g_PactCode;

        Child.g_tableName    := g_Table_pactDebtmoney;

        Child.cxCurrencyEdit1.Value := g_Sum_Money;  //合同总价
        Child.cxCurrencyEdit2.Value := g_Paid + g_GroupMoney; //交易总额
        Child.cxCurrencyEdit3.Value := szSumMoney;  //剩余
        Child.ShowModal;

        Self.TreeView1DblClick(Sender); //刷新

      finally
        Child.Free;
      end;

    end else
    begin
      MessageBox(handle, PChar('需要选择单项目工程，不可一次选择多项工程'),'提示:',
      MB_ICONQUESTION + MB_OK);
    end;

  end;
    {
    if MessageBox(handle, PChar(''),'提示',
         MB_ICONQUESTION + MB_YESNO) = IDYES then
    begin
    end;
    }
end;

procedure TfrmManage.RzToolButton14Click(Sender: TObject);
begin
  inherited;
  CreateOpenDir(Self.Handle, Concat( g_Resources ,'\PayAccountB' ) , True);
end;

procedure TfrmManage.RzToolButton15Click(Sender: TObject);
var
  szColName : string;
  sqltext   : string;

begin
  inherited;
//交易查询
  if Length(g_PactCode) <> 0 then
  begin
    szColName := Self.tvDealGridColumn3.DataBinding.FieldName;
    sqltext := 'select * from ' + g_Table_Collect_Receivables +
               ' where '+ g_CodeList + ' and ' + szColName +' between :t1 and :t2';
    dwADO.ADOSearchDateRange(Self.cxDateEdit1.Text,Self.cxDateEdit2.Text,sqltext,Self.Master);
  end;

end;


procedure TfrmManage.RzToolButton16Click(Sender: TObject);
var
  Child   : TfrmPayDetailed;
  szName  : string;
  szMoney : Currency;
  
begin
  inherited;
  with Self.tvRecord.DataController.DataSource.DataSet do
  begin
    if not IsEmpty then
    begin
      Child := TfrmPayDetailed.Create(Self);
      try

        Self.tvRecord.Controller.FocusedRow.Collapse(True);
        szName := Self.tvRecordColumn16.DataBinding.FieldName;
        szMoney:= FieldByName(szName).AsCurrency;
        Child.g_Money    := szMoney;
        Child.g_formName := Self.Name;
        Child.g_Detailed := g_Dir_Collect_DetailTable;
        szName := Self.tvRecordColumn3.DataBinding.FieldName;
        Child.g_PactCode   := g_PactCode;
        Child.g_TableName  := g_Table_DetailTable;
        Child.g_parentCode := FieldByName(szName).AsString;
        Child.ShowModal;
        Self.RefreshDetailTable;
        SetcxGrid(Self.tvView,0,g_Dir_Collect_DetailTable);
        Self.tvRecord.Controller.FocusedRow.Expand(True);
      finally
        Child.Free;
      end;

    end;
  end;  
  

end;

procedure TfrmManage.RzToolButton17Click(Sender: TObject);
var
   Node   : TTreeNode;
   szNode : PNodeData;
   Child  : TfrmPostPact;
   szCodeList  : string;
   szTableList : array[0..3] of TTableList;
begin
  Node := Self.TreeView1.Selected;
  if Assigned(Node) then
  begin
    if Node.Level > 0 then
    begin
       szNode :=  PNodeData(Node.Data);
       treeCldnode(Node,szCodeList);
       Child := TfrmPostPact.Create(Self);
       try

         Child.g_PatTree    := g_PatTree;
         Child.g_PostCode   := szNode.Code;
         Child.g_TableName  := g_Table_RevenueTree;
         Child.g_Prefix     := 'YS';
         Child.g_ProjectDir := ''; //附件目录
         Child.g_TreeNode   := Node;

         if Sender = Self.RzToolButton17 then
         begin
           //新增
           Child.cxDateEdit1.Date := Date;
           Child.g_IsModify := False;
           Child.ShowModal;
         end else
         if Sender = Self.RzToolButton18 then
         begin
           //编辑

           Child.cxDateEdit1.Date := szNode.InputDate;
           Child.cxLookupComboBox1.Text := szNode.SignName;
           Child.cxMemo1.Text := szNode.remarks;
           Child.g_IsModify   := True;
           Child.ShowModal;

         end else
         if Sender = Self.RzToolButton19 then
         begin

           if Node.Level <= 1 then
           begin
             Application.MessageBox('顶级帐目禁止删除!', ' ' ,MB_OK + MB_ICONQUESTION ) ;
           end
           else
           begin
             //可以删除

              szNode := PNodeData(Node.Data);
              if MessageBox(handle, PChar('确认要删除"' + szNode.SignName + '"'),
                       '提示', MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2) = IDYES then
              begin

                szTableList[0].ATableName := g_Table_Collect_Receivables;  //合同总价
                szTableList[0].ADirectory := g_CollectReceivables;

                szTableList[1].ATableName := g_Table_Income; //交易记录
                szTableList[1].ADirectory := g_dir_BranchMoney;

                szTableList[2].ATableName := g_Table_pactDebtmoney; //债务组合
                szTableList[2].ADirectory := '';

                szTableList[3].ATableName := g_Table_DetailTable; //交易记录明细
                szTableList[3].ADirectory := '';

                treeCldnode(Node,szCodeList);
                DeleteTableFile(szTableList,szCodeList);

                DM.InDeleteData(g_Table_Projects_Tree,szCodeList);
                g_PatTree.DeleteTree(Node);
                Self.TreeView1DblClick(Sender);

              end;

           end;

         end;

       finally
         Child.Free;
       end;

    end else
    begin
      Application.MessageBox('顶级节点禁止新增/编辑!','',MB_OK + MB_ICONQUESTION ) ;
    end;

  end else
  begin
    Application.MessageBox('请选择合同在点击新增/编辑/删除!','',MB_OK + MB_ICONQUESTION )
  end;

end;
{
var
  Node : TTreeNode;
  szCodeList  : string;
  szTableList : array[0..3] of TTableList;
  szNode : PNodeData;
  Child : TfrmRevenueProject;

begin
  inherited;

  if m_user.dwType = 0 then
  begin
    Node := Self.TreeView1.Selected;
    if Assigned(Node) then
    begin
      if Node.Level > 0 then
      begin
          szNode := PNodeData(Node.Data);

          Child := TfrmRevenueProject.Create(Self) ;
          try

            Child.g_PatTree    := g_PatTree;
            Child.g_PostCode   := szNode.Code;
            Child.g_TableName  := g_TableNameTree;
            Child.g_ProjectDir := ''; //附件目录

            if Sender = Self.RzToolButton17 then
            begin
              Child.cxDateEdit1.Date := Date;
              Child.g_IsModify := False;
              Child.ShowModal;
            end else
            begin
              if Sender = Self.RzToolButton18 then  //编辑
              begin
                if Node.Level > 1 then
                begin

                  Child.cxDateEdit1.Date    := szNode.Input_time;
                  Child.cxLookupComboBox1.Text := szNode.Caption;
                  Child.cxMemo1.Text := szNode.remarks;

                  Child.g_IsModify   := True;
                  Child.ShowModal;
                end else
                begin
                  Application.MessageBox('顶级节点禁止编辑!',m_title,MB_OK + MB_ICONINFORMATION ) ;
                end;

              end else
              begin
                if Sender = Self.RzToolButton19 then  //删除
                begin
                   if Node.Level > 1 then
                   begin

                      if MessageBox(handle, PChar('你确认要删除"' + szNode.Caption + '"'),
                               '提示', MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2) = IDYES then
                      begin

                        szTableList[0].ATableName := g_Table_Collect_Receivables;  //合同总价
                        szTableList[0].ADirectory := g_CollectReceivables;

                        szTableList[1].ATableName := g_Table_Income; //交易记录
                        szTableList[1].ADirectory := g_income;

                        szTableList[2].ATableName := g_Table_Pactmoney; //结算表
                        szTableList[2].ADirectory := '';

                        szTableList[3].ATableName := g_Table_DetailTable; //交易明细
                        szTableList[3].ADirectory := '';

                        treeCldnode(Node,szCodeList);
                        DeleteTableFile(szTableList,szCodeList);
                        DM.InDeleteData(g_TableName,szCodeList);
                        g_PatTree.DeleteTree(Node);
                        
                      end;

                   end else
                   begin
                     Application.MessageBox('顶级帐目禁止删除!',m_title,MB_OK + MB_ICONQUESTION )
                   end;

                end;

              end;

            end;


          finally
            Child.Free;
          end;

      end else begin
        Application.MessageBox('顶级节点禁止添加/编辑!',m_title,MB_OK + MB_ICONQUESTION )
      end;

    end else begin
      Application.MessageBox('请选择合同在点击添加!',m_title,MB_OK + MB_ICONQUESTION )
    end;

  end;

end;
}
procedure TfrmManage.RzToolButton1Click(Sender: TObject);
var
  Child : TfrmIsViewGrid;
  i : Integer;
  s , Code : string;
  Node: TTreeNode;
  szName : string;
  RowCount:Integer;
  szProjectName : string;

begin

  if Sender = Self.RzToolButton1 then
  begin
    //新增
    Node := Self.TreeView1.Selected;
    if Assigned(Node) then
    begin
      if Node.Level > 1 then
      begin
        dwADO.ADOAppend(Self.Master);
        Self.cxGrid2.SetFocus;
        SetGridFocus(Self.tvDealGrid);

      end else
      begin
        Application.MessageBox('该界面以限制，请新建一个工程项目，在进行操作！!!',m_title,MB_OK + MB_ICONQUESTION )
      end;

    end
    else
    begin
      Application.MessageBox('请选择要添加的合同!!',m_title,MB_OK + MB_ICONQUESTION )
    end;

  end else
  if Sender = Self.RzToolButton3 then
  begin
  //  cxGridDeleteData(Self.tvDealGrid,g_CollectReceivables,g_Table_Collect_Receivables);

    dwADO.ADODeleteSelection(Self.tvDealGrid,
                             g_CollectReceivables,
                             g_Table_Collect_Receivables,
                             Self.tvDealGridColumn2.DataBinding.FieldName);
  end else
  if Sender = Self.RzToolButton4 then
  begin
     Child := TfrmIsViewGrid.Create(Application);
     try
        Child.g_fromName := Self.Name + g_CollectReceivables;
        Child.ShowModal;
        SetcxGrid(Self.tvDealGrid,0,g_CollectReceivables);
     finally
        Child.Free;
     end;
  end;

end;

procedure TfrmManage.RzToolButton20Click(Sender: TObject);
var
  szColName : string;
  sqltext   : string;

begin
  inherited;
//交易查询
  if Length(g_PactCode) <> 0 then
  begin
    szColName := Self.tvRecordColumn4.DataBinding.FieldName;
    sqltext := 'select * from ' + g_Table_Income +
               ' where '+ g_CodeList + ' and ' + szColName +' between :t1 and :t2';
    dwADO.ADOSearchDateRange(Self.cxDateEdit3.Text,Self.cxDateEdit4.Text,sqltext,Self.ADORecord);
    Self.RefreshDetailTable;
  end;

end;

procedure TfrmManage.RzToolButton21Click(Sender: TObject);
begin
  inherited;
  GetMaintainInfo;
  ShowMessage('刷新完成!');
end;

procedure TfrmManage.RzToolButton22Click(Sender: TObject);
begin
  inherited;
  if Self.tvDealGridColumn19.EditValue then
  begin
    dwADO.ADOIsEdit(Self.Master);
  end else
  begin
    ShowMessage('冻结状态下无法进行编辑');
  end;
end;

procedure TfrmManage.RzToolButton23Click(Sender: TObject);
begin
  inherited;
  if Self.tvRecordColumn2.EditValue then
  begin
    dwADO.ADOIsEdit(Self.ADORecord);
  end else
  begin
    ShowMessage('冻结状态下无法进行编辑');
  end;
end;

procedure TfrmManage.RzToolButton2Click(Sender: TObject);
begin
  inherited;
  case Self.pcSys.ActivePageIndex of
    0: dwADO.ADOSaveData(Self.Master,dwMasterSQLText);
    1: dwADO.ADOSaveData(Self.ADORecord,dwRecordSQLText);
  end;
  ShowSumMoney();
  RefreshDetailTable; //明细表
end;

procedure TfrmManage.RzToolButton6Click(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TfrmManage.RzToolButton7Click(Sender: TObject);
var
  s , str : string;
  Node: TTreeNode;
  Code: string;
  szNode : PNodeData;
  szName : string;
  i , szIndex : Integer;
  Id , dir: string;
  Child : TfrmIsViewGrid;
  szProjectName : string;
begin
  inherited;
  if Sender = Self.RzToolButton7 then
  begin
  //拨款添加
    Node := Self.TreeView1.Selected;

    if Assigned(Node) then
    begin

      if Node.Level > 1 then
      begin
        dwADO.ADOAppend(Self.ADORecord);
      end
      else
      begin
        Application.MessageBox('该界面以限制，请新建一个工程项目，在进行操作！!!',m_title,MB_OK + MB_ICONQUESTION )
      end;

    end else
    begin
      Application.MessageBox('请选择要添加的合同!!',m_title,MB_OK + MB_ICONQUESTION )
    end;

  end else
  if Sender = Self.RzToolButton9 then
  begin
    //拨款删除
      if  Application.MessageBox( '您要删除所有选中的记录吗？', '删除记录？', MB_OKCANCEL + MB_ICONWARNING) = IDOK then
      begin
          with Self.tvRecord.DataController.DataSource.DataSet do
          begin
            if not IsEmpty then
            begin

              for I := 0 to Self.tvRecord.Controller.SelectedRowCount -1 do
              begin
                szName  := Self.tvRecordColumn3.DataBinding.FieldName;
                szIndex := Self.tvRecord.GetColumnByFieldName(szName).Index;
                Id := Self.tvRecord.Controller.SelectedRows[i].Values[szIndex];

                g_ProjectDir_1 := Concat(g_Resources,'\' + g_Dir_Collect_DetailTable); //明细目录
                g_ProjectDir_2 := Concat(g_Resources,'\' + g_income); //收款目录

                dir := ConcatEnclosure(g_ProjectDir_2, id);
                if DirectoryExists(dir) then  DeleteDirectory(dir);

                if Length(str) = 0 then
                  str := Id
                else
                  str := str + '","' + Id;
              end;

              with DM.Qry do
              begin
                Close;
                SQL.Clear;
                SQL.Text := 'delete * from ' +  g_Table_Income + ' where ' + 'numbers' +' in("' + str + '")';
                ExecSQL;
                SQL.Clear;
                SQL.Text := 'delete * from ' +  g_Table_DetailTable + ' where ' + 'parent' +' in("' + str + '")';
                ExecSQL;
              end;

              Self.TreeView1DblClick(Sender);
            end;

          end;

      end;

  end else
  if Sender = Self.RzToolButton10 then
  begin
  //表格设置
     Child := TfrmIsViewGrid.Create(Application);
     try
        Child.g_fromName := Self.Name + g_income;
        Child.ShowModal;
        SetcxGrid(Self.tvRecord,0,g_income);
     finally
        Child.Free;
     end;
  end;

end;

procedure TfrmManage.ShowSettleAccounts(AIndex : Byte ; ACode : string); //显示结算
begin
  //结算显示
  {
  with Self.ADOQuery1 do
  begin

    Close;
    SQL.Clear;
    SQL.Text := 'Select *from pactmoney where code="'+ ACode +'"';
    Open;
    if RecordCount <> 0 then
    begin
      if FieldByName('paymoney').AsCurrency <> 0 then
      begin
        Self.DBGridEh2.Visible := True;
        Self.Splitter1.Visible := True;
      end
      else
      begin
        Self.DBGridEh2.Visible := False;
        Self.Splitter1.Visible := False;
      end;

    end
    else
    begin
      Self.DBGridEh2.Visible := False;
      Self.Splitter1.Visible := False;
    end;

  end;
  }
end;

function GetTreeNameList(Node : TTreeNode):string;
var
  I: Integer;
  s: string;

begin
  Result := '';

  if Assigned(Node) then
  begin
    for I := 0 to Node.Count-1 do
    begin
      s := Node.Item[i].Text;
      if Result = ''  then
      begin
        Result := s
      end else
      begin
        Result := Result + ''',''' + s;
      end;
    end;
  end;
end;

procedure TfrmManage.ShowSumMoney();
var
  s : string;
  Node : TTreeNode;
  szSummary , szRemainder : Currency;
  DD1 , DD2 : Currency;
  szColFieldName : string;
  szlarge , szSmall : string;
  szDebtMoney : Currency;
  i : Integer;
  
begin
  szlarge := '大写:' ;
  szSmall := '小写:';

  Self.RzPanel10.Caption := '';
  Self.RzPanel9.Caption  := '';

  Self.RzPanel13.Caption := '';
  Self.RzPanel14.Caption := '';

  Self.RzPanel17.Caption := '';
  Self.RzPanel18.Caption := '';

  if Length(g_CodeList) <> 0 then
  begin
    //**************************
    //债务
    with DM.Qry do
    begin
      Close;
      SQL.Clear;
      SQL.Text := 'Select * from '+ g_Table_pactDebtmoney +
                  ' WHERE '+ 'Code' +'="' + g_TopLevelCode + '" AND ' + 'Projectcode' + '="' + g_PactCode +'"' ;
      Open;
      if RecordCount <> 0 then
      begin
        szDebtMoney := FieldByName( 'SumMoney' ).AsCurrency;
        i := FieldByName( 'MoneyType').AsInteger;
        case i of
          0:
          begin
            //支加进交易记录
          end;
          1:
          begin
            //收加进合同总价
          end;      
        end;
      end;

    end;
    //***********************************
    with DM.Qry do
    begin
      Close;
      SQL.Clear;
      szColFieldName := Self.tvDealGridColumn15.DataBinding.FieldName;
      SQL.Text := 'Select sum('+ szColFieldName +') as t from ' + g_Table_Collect_Receivables +
                  ' as e where ' + g_CodeList +
                  ' AND ' + Self.tvDealGridColumn19.DataBinding.FieldName + '=yes';  //合同总价
      Open;
      if 0 <> RecordCount then
      begin
        if i = 1  then
          g_Sum_Money := szDebtMoney + FieldByName('t').AsCurrency //合同总价
        else   
          g_Sum_Money := FieldByName('t').AsCurrency; //合同总价
          
        Self.RzPanel10.Caption := szSmall + CurrToStr(g_Sum_Money + dwCompanyMoney );
        Self.RzPanel9.Caption  := szlarge + MoneyConvert( g_Sum_Money + dwCompanyMoney );
      end;
      Close;
      s := 'SELECT SUM(SumMoney) as t FROM ' + g_Table_RuningAccount +
           ' where ' + g_ProjectList +' AND fromType=0 AND status=yes AND PaymentType="'+ g_TopLevelName +'"';  //流水组合
      SQL.Clear;
      SQL.Text := s;
      Open;
      if RecordCount <> 0 then g_GroupMoney := FieldByName('t').AsCurrency;

      Close;
      SQL.Clear;
      szColFieldName := Self.tvRecordColumn16.DataBinding.FieldName;
      SQL.Text := 'Select sum('+ szColFieldName +') as t from ' +
                  g_Table_Income +
                  ' as e where ' + g_CodeList +
                  ' AND ' + Self.tvRecordColumn2.DataBinding.FieldName + '=yes';  //交易记录
      Open;
      if 0 <> RecordCount then
      begin
        if i = 0 then
          g_Paid := szDebtMoney + FieldByName('t').AsCurrency
        else
          g_Paid := FieldByName('t').AsCurrency;
        Self.RzPanel13.Caption := szSmall + CurrToStr(g_Paid + g_GroupMoney);
        Self.RzPanel14.Caption := szlarge + MoneyConvert(g_Paid + g_GroupMoney);
      end;

      szRemainder := g_Sum_Money  - ( g_Paid + g_GroupMoney );
      Self.RzPanel17.Caption := szSmall +  FloatToStr(szRemainder);
      Self.RzPanel18.Caption := szlarge +  MoneyConvert(szRemainder);

    end;

  end;
end;


function GetTreeLevelCode(Node : TTreeNode ):string;
begin
  while Node <> nil do
  begin
    if Node.Level = 1 then
    begin
      Result := PNodeData(Node.Data).Code;
      Break;
    end;
    Node := Node.Parent;
  end;
end;

procedure TfrmManage.TreeView1Change(Sender: TObject; Node: TTreeNode);
var
  szNode : PNodeData;
begin
  inherited;
  if Node.Level > 0 then
  begin
    szNode :=  PNodeData(Node.Data);
    if Assigned(szNode) then
    begin
      g_TopLevelName:= GetTreeLevelName(Node);
      g_TopLevelCode:= GetTreeLevelCode(Node);
      g_ProjectName := Node.Text;// string;
      g_PactCode    := szNode.Code;

    end;
    
  end else
  begin
    Self.Master.Close;
    Self.ADORecord.Close;
  end;
end;

procedure TfrmManage.TreeView1DblClick(Sender: TObject);
var
  Node : TTreeNode;
  szCode : string;
  szNode : PNodeData;
  CodeList : string;
  szColName  : string;
  szTableName: string;
  t : Currency;
  szSQLText , s , szName : string;

begin
  inherited;
  Node := Self.TreeView1.Selected;
  if Assigned(Node) AND (Node.Level > 0) then
  begin
    szNode :=  PNodeData(Node.Data);
    if Assigned(szNode) then
    begin
      treeCldnode(Node,CodeList);
      if Node.Count = 0 then
      begin
        g_ProjectList := 'ProjectName=''' + Node.Text + '''';
        szSQLText := 'code='''+ g_PactCode + '''';  //没有子节点
      end else
      begin
        s := GetTreeNameList(Node);
        g_ProjectList :=  'ProjectName in (''' + s + ''')';
        szSQLText := 'code in('''+ CodeList  +''')'; //取得所有子节点
      end;
      g_CodeList := szSQLText;
      //************************************************************************
      dwMasterSQLText := 'Select * from ' + g_Table_Collect_Receivables + ' where ' + szSQLText;

      Self.Master.Close;
     // t := GetTickCount;
      with DM do
      begin
        ADOQuery1.Close;
        ADOQuery1.SQL.Clear;
        ADOQuery1.SQL.Text := dwMasterSQLText;
        ADOQuery1.Open;
        Self.Master.Data := DataSetProvider1.Data;
        Self.Master.Open;
      end;
      //************************************************************************

      dwRecordSQLText := 'Select * from '+ g_Table_Income  + ' where ' + szSQLText;// ' where code="' + g_PactCode + '"';
      with DM do
      begin
        ADOQuery1.Close;
        ADOQuery1.SQL.Clear;
        ADOQuery1.SQL.Text := dwRecordSQLText;
        ADOQuery1.Open;
        Self.ADORecord.Data := DataSetProvider1.Data;
        Self.ADORecord.Open;
      end;
      //g_ProjectList +
      s := 'SELECT ProjectName,SUM(SumMoney) as t,Drawee,PaymentType FROM '+
          g_Table_RuningAccount +' where ' +
          ' fromType=0 AND status=yes AND PaymentType="'+
          g_TopLevelName +'" GROUP BY ProjectName,Drawee,PaymentType';
      with DM do
      begin       //汇总
        ADOQuery1.Close;
        ADOQuery1.SQL.Clear;
        ADOQuery1.SQL.Text := s;

        Self.ADOGroup.Data := DataSetProvider1.Data;
        Self.ADOGroup.Open;
      end;
      //公司仓库汇总
      s := 'SELECT ReceiveCompany as c,SUM(SumMonery) as t FROM '+ g_Table_Company_StorageDetailed +' where ' +
            ' status=yes AND ReceiveCompany="'+ g_TopLevelName +'" AND AccountsStatus="营收挂账" GROUP BY ReceiveCompany';
      with DM.Qry do
      begin
        Close;
        SQL.Text := s;
        Open;
        if RecordCount <> 0 then
        begin
          t := FieldByName('t').AsCurrency;
          dxMemData1.Active := False;
          dxMemData1.Active := True;
          dwCompanyMoney := 0;
          while not Eof do
          begin
            dxMemData1.Append;
            dwCompanyMoney := dwCompanyMoney + t;
            dxMemData1.FieldByName('c').AsString   := FieldByName('c').AsString;
            dxMemData1.FieldByName('t').AsCurrency := t;
            dxMemData1.FieldByName('d').AsString   := MoneyConvert( t );
            Next;
          end;
        end;

      end;
      s := 'Select * from '+ g_Table_pactDebtmoney  + ' where ' + 'ProjectCode in('''+ CodeList  +''')';
      Self.ADODebt.Close;
      Self.ADODebt.SQL.Clear;
      Self.ADODebt.SQL.Text := s;
      Self.ADODebt.Open;

      ShowSumMoney();
      RefreshDetailTable; //明细表
    end;

  end;

end;

procedure TfrmManage.tvDealGridColumn12PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var
  szCount : Double;

begin
  inherited;
  //单价
  Self.tvDealGridColumn12.EditValue := DisplayValue;
  szCount := Double( Self.tvDealGridColumn13.EditValue );
  GetSumCount(szCount,Currency(DisplayValue));
end;

procedure TfrmManage.tvDealGridColumn13PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var
  szUnitPrice : Currency;
begin
  inherited;
  Self.tvDealGridColumn13.EditValue := DisplayValue;
  szUnitPrice := Currency( Self.tvDealGridColumn12.EditValue );
  GetSumCount(Double( DisplayValue ) ,szUnitPrice);
end;

procedure TfrmManage.tvDealGridColumn15PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var
  s : string;

begin
  inherited;
  Self.tvDealGridColumn15.EditValue := DisplayValue;
  if DisplayValue <> null then
  begin
    s := VarToStr(DisplayValue);
    Self.tvDealGridColumn17.EditValue := MoneyConvert(StrToCurrDef(s,0));
  end;
end;

procedure TfrmManage.tvDealGridColumn1GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  inherited;
  AText := IntToStr(ARecord.Index + 1);
end;

procedure TfrmManage.tvDealGridColumnSizeChanged(Sender: TcxGridTableView;
  AColumn: TcxGridColumn);
begin
  inherited;
  SetcxGrid(Self.tvDealGrid,1,g_CollectReceivables);
end;

procedure TfrmManage.tvDealGridEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  inherited;
  dwADO.ADOIsEditing(Self.Master,AAllow);

  if AItem.Index = Self.tvDealGridColumn19.Index then AAllow := True;
end;

function TfrmManage.GetSumCount(ADD1 : Double ; ADD2 : Currency):Boolean;
var
  szSumMoney : Currency;

begin
  Result := False;
  szSumMoney := ADD1 * ADD2;
  if szSumMoney <> 0 then
  begin
    Result := True;
    Self.tvDealGridColumn17.EditValue := MoneyConvert(szSumMoney);
    Self.tvDealGridColumn15.EditValue := szSumMoney;
  end;
end;

procedure TfrmManage.tvDealGridEditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
var
  s : string;

  szUnitPrice : Currency;
  szValue     : Double;
  szColFieldName   :  string;
  szSumMoney  : Currency;
  szName : string;

  szCount : Double;

begin
  inherited;

  if Key = 13 then
  begin
{
    if AItem.Editing then
    begin

      with Self.Master do
      begin
        if State <> dsInactive then
        begin
          szColFieldName := Self.tvDealGridColumn12.DataBinding.FieldName;
          szUnitPrice := FieldByName(szColFieldName).AsCurrency;
          szColFieldName := Self.tvDealGridColumn13.DataBinding.FieldName;
          szValue        := FieldByName(szColFieldName).Value;
          szSumMoney     := szUnitPrice * szValue;

          if (AItem.Name = Self.tvDealGridColumn12.Name) or
             (AItem.Name = Self.tvDealGridColumn13.Name) then
          begin
            szColFieldName := Self.tvDealGridColumn15.DataBinding.FieldName;
            FieldByName(szColFieldName).Value := szSumMoney;
            szColFieldName := Self.tvDealGridColumn17.DataBinding.FieldName;
            FieldByName(szColFieldName).Value := MoneyConvert(szSumMoney);
          end else
          begin

            if (AItem.Name = Self.tvDealGridColumn15.Name) and (szSumMoney = 0)  then
            begin
              szColFieldName := Self.tvDealGridColumn15.DataBinding.FieldName;
              FieldByName(Self.tvDealGridColumn17.DataBinding.FieldName).Value := MoneyConvert(FieldByName(szColFieldName).Value);

            end;

          end;

        end;

      end;

      if AItem.Name = Self.tvDealGridColumn15.Name then
      begin
        if Self.tvDealGrid.Controller.FocusedRow.IsLast then
        begin
          //在最后一行新增

          if IsNewRow() then
          begin
            with Self.Master do
            begin
              if (State = dsEdit) or (State = dsinsert) then
              begin
                Post;
              end;

            end;
            Self.RzToolButton1.Click;

          end;

        end;

      end;
      }
  //  end;
  end;

end;

procedure TfrmManage.tvDealGridKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = 46 then
  begin
    Self.RzToolButton3.Click;
  end;
end;

procedure TfrmManage.tvDealGridStylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
begin
  inherited;
  if ARecord.Values[Self.tvDealGridColumn19.Index] = False then
  begin
    AStyle := DM.cxStyle222;
  end;
end;

procedure TfrmManage.tvDealGridTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
  Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
  var AText: string);
var
  s : string;

begin
  inherited;
  try
    s := VarToStr(AValue);
    Self.tvDealGridColumn17.Summary.FooterFormat := MoneyConvert(StrToCurrDef(s,0));
  except
  end;

end;

procedure TfrmManage.tvDebtTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
  Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
  var AText: string);
var
  s : string;

begin
  inherited;
  if AValue <> null then
  begin
    s := VarToStr(AValue);
    Self.cxGridDBColumn6.Summary.FooterFormat := MoneyConvert( StrToCurrDef(s,0));
  end else
  begin
    Self.cxGridDBColumn6.Summary.FooterFormat := '';
  end;

end;

procedure TfrmManage.tvGrid1Column1GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  AText := IntToStr(ARecord.RecordIndex + 1);
end;

procedure TfrmManage.tvGrid3Column18CustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
  inherited;
  ACanvas.Font.Style := [fsBold];
  ACanvas.Font.Color := clBlue;
end;

procedure TfrmManage.tvGrid3Column1GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  inherited;
  AText := IntToStr(ARecord.Index + 1);
end;

procedure TfrmManage.tvView1Column1GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  inherited;
  AText := IntToStr(ARecord.Index + 1 );
end;

procedure TfrmManage.tvView1ColumnSizeChanged(Sender: TcxGridTableView;
  AColumn: TcxGridColumn);
begin
  inherited;
  SetcxGrid(Self.tvView,1,g_Dir_Collect_DetailTable);
end;

procedure TfrmManage.tvViewColumn1GetDisplayText(Sender: TcxCustomGridTableItem;
  ARecord: TcxCustomGridRecord; var AText: string);
begin
  inherited;
  AText := IntToStr( ARecord.Index + 1 );
end;

procedure TfrmManage.tvGroupColumn1GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  inherited;
  AText := IntToStr(ARecord.Index + 1);
end;

procedure TfrmManage.tvGroupTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
  Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
  var AText: string);
var
  s : string;

begin
  inherited;
  if AValue <> null then
  begin
    s := VarToStr(AValue);
    Self.tvGroupColumn5.Summary.FooterFormat := MoneyConvert( StrToCurrDef(s,0));
  end else
  begin
    Self.tvGroupColumn5.Summary.FooterFormat := '';
  end;
end;

procedure TfrmManage.tvRecordColumn16CustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
  inherited;
  ACanvas.Font.Style := [fsBold];
  ACanvas.Brush.Color:= clYellow;
  ACanvas.Font.Color := clBlue;
end;

procedure TfrmManage.tvRecordColumn16PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var
  s : string;

begin
  inherited;
  if DisplayValue <> null then
  begin
    s := VarToStr(DisplayValue);
    Self.tvRecordColumn16.EditValue := DisplayValue;
    Self.tvRecordColumn17.EditValue := MoneyConvert(StrToCurrDef(s,0));
  end;
end;

procedure TfrmManage.tvRecordColumn1GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  inherited;
  AText := IntToStr(ARecord.Index + 1);
end;

procedure TfrmManage.tvRecordColumnSizeChanged(Sender: TcxGridTableView;
  AColumn: TcxGridColumn);
begin
  inherited;
  SetcxGrid(Self.tvRecord,1,g_income);
end;

procedure TfrmManage.tvRecordEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  inherited;
  dwADO.ADOIsEditing(Self.ADORecord,AAllow);
  if AItem.Index = Self.tvRecordColumn2.Index then  AAllow := True;
end;

procedure TfrmManage.tvRecordEditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if (AItem.Index = Self.tvRecordColumn16.Index) AND (Key = 13) AND (Self.tvRecord.Controller.FocusedRow.IsLast) and (IsNewRow) then
  begin
    Self.RzToolButton7.Click;
  end;
end;

procedure TfrmManage.tvRecordStylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
begin
  inherited;
  if ARecord.Values[Self.tvRecordColumn2.Index] = False then
  begin
    AStyle := DM.cxStyle222;
  end;
end;

procedure TfrmManage.tvRecordTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
  Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
  var AText: string);
var
  s : string;
begin
  inherited;
  try
    if AValue <> null then
    begin
      s := VarToStr(AValue);
      Self.tvRecordColumn17.Summary.FooterFormat := MoneyConvert( StrToCurrDef(s,0));
    end;
  except
  end;
end;

end.
