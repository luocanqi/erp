unit ufrmPayMoney;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, ExtCtrls, Mask, RzEdit, RzButton, RzTabs, RzPanel ,
  TreeUtils,ufrmPayee, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, cxGroupBox, cxLabel,
  cxTextEdit, cxMaskEdit, cxDropDownEdit, dxCore, cxDateUtils, cxCurrencyEdit,
  cxCalendar, cxMemo;

type
  TfrmPayMoney = class(TForm)
    cxGroupBox1: TcxGroupBox;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    cxComboBox1: TcxComboBox;
    cxLabel3: TcxLabel;
    cxComboBox3: TcxComboBox;
    cxDateEdit1: TcxDateEdit;
    cxLabel4: TcxLabel;
    cxCurrencyEdit1: TcxCurrencyEdit;
    cxLabel5: TcxLabel;
    cxLabel6: TcxLabel;
    cxTextEdit1: TcxTextEdit;
    RzBitBtn1: TRzBitBtn;
    RzBitBtn2: TRzBitBtn;
    RzBitBtn3: TRzBitBtn;
    cxLabel7: TcxLabel;
    cxMemo1: TcxMemo;
    cxTextEdit2: TcxTextEdit;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure RzBitBtn1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure RzBitBtn3Click(Sender: TObject);
    procedure RzEdit4KeyPress(Sender: TObject; var Key: Char);
    procedure RzEdit11Change(Sender: TObject);
    procedure RzBitBtn4Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure RzMemo1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure RzMemo2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure RzBitBtn2Click(Sender: TObject);
    procedure cxCurrencyEdit1PropertiesChange(Sender: TObject);
  private
    { Private declarations }
    g_PatTree : TTreeUtils;
    g_frmPayee: TfrmPayee;
  public
    { Public declarations }
    g_ProjectDir : string;
    g_PostCode   : string;
    g_PostNumbers: string;
    g_tableName  : string;
    g_IsMofidy   : Boolean;
  protected
    procedure WMMove(var Message: TWMMove); message WM_MOVE;
  end;

var
  frmPayMoney : TfrmPayMoney;
//  g_tableName : string = 't_payoutlist';

implementation

uses
  TreeFillThrd,uDataModule,global,ShellAPI;

{$R *.dfm}

procedure TfrmPayMoney.WMMove(var Message: TWMMove);
begin
  if Assigned(g_frmPayee) then
  begin
    g_frmPayee.SetBounds(Left + Width + 10, Top, 300, Height);
  end;
  inherited;
end;

procedure TfrmPayMoney.cxCurrencyEdit1PropertiesChange(Sender: TObject);
begin
  Self.cxTextEdit1.Text := MoneyConvert(Self.cxCurrencyEdit1.Value);
end;

procedure TfrmPayMoney.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  g_frmPayee.Free;
end;

procedure TfrmPayMoney.FormCreate(Sender: TObject);
var
  s : string;
  i : Integer;
  pData : PPaytype;
begin
  with DM.Qry do
  begin
     Close;
     SQL.Clear;
     SQL.Text := 'Select * from pay_type';
     Open;
     Self.cxComboBox3.Clear;
     if not IsEmpty then
     begin
        while not Eof do
        begin
          New(pData);
          pData.ID      := FieldByName('编号').AsInteger;
          pData.Nmae    := FieldByName('pay_name').AsString;
          Self.cxComboBox3.Properties.Items.AddObject(pData.Nmae,TObject(pData));
          Next;
        end;
     end;
  end;
  i := Self.cxComboBox3.Properties.Items.IndexOf('现金');
  Self.cxComboBox3.ItemIndex := i;
end;

procedure TfrmPayMoney.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
    27:
    begin
      Close;
    end;
    13:
    begin
      Self.RzBitBtn2.Click;
    end;
  end;
end;

procedure TfrmPayMoney.FormShow(Sender: TObject);
var
  i : Integer;
  s : string;
begin
  g_frmPayee := TfrmPayee.Create(Self);
  try
    g_frmPayee.Show;
    g_frmPayee.Left   := Left + Width + 10;
    g_frmPayee.Top    := Top;
    g_frmPayee.Height := Height;
  finally
  end;

end;

procedure TfrmPayMoney.RzBitBtn1Click(Sender: TObject);
begin
  CreateOpenDir(Handle,Concat(g_ProjectDir,'\' + g_PostNumbers),True);
end;

procedure TfrmPayMoney.RzBitBtn2Click(Sender: TObject);
var
  szPayer : string;  //付款方
  szReceivingParty : string; //收款方
  szDate  : string;
  szRemarks  : string;
  szSumMoney : Currency;
  szPaytype  : Integer;
  szlargeMoney : string;

  pData : PPaytype;
  i : Integer;
  sqlstr : string;

begin
  szPayer := Self.cxComboBox1.Text;
  if szPayer = '' then
  begin
    Application.MessageBox('请添加付款人!',m_title,MB_OK + MB_ICONQUESTION ) ;
    Self.cxComboBox1.DroppedDown:=True;
  end else
  begin
    szReceivingParty := Self.cxTextEdit2.Text; //收款方
    if szReceivingParty = '' then
    begin
      Application.MessageBox('请添加收款方!',m_title,MB_OK + MB_ICONQUESTION ) ;
    end else
    begin
      szSumMoney:= Self.cxCurrencyEdit1.Value;
      if szSumMoney = 0 then
      begin
        Application.MessageBox('请添写交易金额!',m_title,MB_OK + MB_ICONQUESTION ) ;
      end else
      begin
        szDate    := Self.cxDateEdit1.Text;
        szRemarks := Self.cxMemo1.Text;
        szlargeMoney := self.cxTextEdit1.Text;
        i := Self.cxComboBox3.ItemIndex;

        pData := PPaytype( Self.cxComboBox3.Properties.Items.Objects[i] );
        if Assigned(pData) then
        begin
          szPaytype := pData^.ID;
          if g_IsMofidy then
          begin

            sqlstr := Format('Update '+ g_tableName +' set pay_type=%d,'+
                                                          'small_money=%f,'+
                                                          'large_money="%s",'+
                                                          'pay_time="%s",'+
                                                          'remarks="%s",'+
                                                          'payee="%s",'+
                                                          'ReceivingParty="%s"'+
                                                          ' where Numbers="%s"',
                                                          [
                                                           szPaytype,
                                                           szSumMoney,
                                                           szlargeMoney,
                                                           szDate,
                                                           szRemarks,
                                                           szPayer,
                                                           szReceivingParty,
                                                           g_PostNumbers
                                                           ]);

            OutputLog(sqlstr);

            with DM.Qry do
            begin
              Close;
              SQL.Clear;
              SQL.Text := sqlstr;
              if ExecSQL <> 0 then
              begin
                m_IsModify := True;
                Application.MessageBox('修改成功!',m_title,MB_OK + MB_ICONQUESTION )
              end
              else
                Application.MessageBox('修改失败!',m_title,MB_OK + MB_ICONQUESTION )

            end;

          end else
          begin

            sqlstr := Format( 'Insert into '+ g_tableName +
                                      ' (code,'       +
                                      'numbers,'      +
                                      'pay_type,'     +
                                      'small_money,'  +
                                      'large_money,'  +
                                      'pay_time,'     +
                                      'payee,'        +
                                      'remarks,'      +
                                      'ReceivingParty'+
                                      ') values("%s","%s",%d,%f,"%s","%s","%s","%s","%s")',[
                                                              g_PostCode,
                                                              g_PostNumbers,
                                                              szPaytype,
                                                              szSumMoney,
                                                              szlargeMoney,
                                                              szDate,
                                                              szPayer,
                                                              szRemarks,
                                                              szReceivingParty
                                                              ] );

            OutputLog(sqlstr);

            with DM.Qry do
            begin
              Close;
              SQL.Clear;
              SQL.Text := sqlstr;
              if ExecSQL <> 0 then
              begin
                g_PostNumbers := DM.getDataMaxDate(g_tableName);
                m_IsModify := True;
                Application.MessageBox('添加成功!',m_title,MB_OK + MB_ICONQUESTION );
              end
              else
                Application.MessageBox('添加失败!',m_title,MB_OK + MB_ICONQUESTION )
            end;

          end ;

        end else
        begin
          Application.MessageBox('选择交易类型出现未知错误!',m_title,MB_OK + MB_ICONQUESTION )
        end;

      end;

    end;

  end;

end;

procedure TfrmPayMoney.RzBitBtn3Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmPayMoney.RzBitBtn4Click(Sender: TObject);
var
  szName  : string;
  szRemarks   : string;
  szPhoto : string;
  szPic : string;
  szPayee : string;
  remarks  : string;
  sqlstr : string;
  pData : PPaytype;
  pay_type : Integer;
  Small_money : Currency;
  large_money   : string;
  i : Integer;
  szNumber :  Integer;
  
  szAppropriation : string;
  szInvoice : string;
  szHydropower : string;
  szfine    : string;
  sztax     : string;
  szArmourforwood : string;
  szLtIsa   : string;
  szreceiptsMoney : string;

begin
{
//支款修改
    szPayee := Self.ComboBox3.Text;
    if szPayee = '' then
    begin
      Application.MessageBox('请输入修改后的付款人名称!',m_title,MB_OK + MB_ICONQUESTION)
    end
    else
    begin

      if Length(Self.RzEdit11.Text) <> 0 then
         Small_money := StrToFloatDef( Self.RzEdit11.Text ,-0) //小写金额
      else
         Small_money := 0;

      if Small_money = 0  then
      begin
        //小写金额
        Application.MessageBox('请填写金额!',m_title,MB_OK + MB_ICONQUESTION )
      end
      else
      begin
        large_money := Self.RzEdit10.Text; //大写金额
        i := Self.ComboBox4.ItemIndex;
        if i<0 then
        begin
           Application.MessageBox('请填选择交易方式',m_title,MB_OK + MB_ICONQUESTION ) ;
           Self.ComboBox4.DroppedDown := True;
           Self.ComboBox4.ItemIndex := 0;
           
        end
        else
        begin
          m_IsModify := True;
          pData := PPaytype( Self.ComboBox2.Items.Objects[i] );
          if Assigned(pData) then
          begin
            pay_type := pData^.ID;

            szRemarks := Self.RzMemo2.Text; //备注
            szPic := '';
            Updateincome(pay_type,
                         Small_money,
                         large_money,
                         FormatDateTime('yyyy-MM-dd',ModifyDate.DateTime),
                         FormatDateTime('yyyy-MM-dd',Now),
                         szRemarks,
                         szPhoto,
                         szPayee,
                         szAppropriation,
                         szInvoice,
                         szHydropower,
                         szfine,
                         sztax,
                         szArmourforwood,
                         szLtIsa,
                         szreceiptsMoney,
                         g_Number);
          end;

        end;

      end;

    end;
}
end;

procedure TfrmPayMoney.RzEdit11Change(Sender: TObject);
var s : string;
begin
{
    if Sender = RzEdit4 then
    begin
      s := Self.RzEdit4.Text;
      if Length(s) = 0 then  s := '0';
      Self.RzEdit3.Text := MoneyConvert( StrToFloatDef( s,-0 ) );
    end;

    if Sender = RzEdit11 then
    begin
            s := Self.RzEdit11.Text;
      if Length(s) = 0 then  s := '0';
      Self.RzEdit10.Text := MoneyConvert( StrToFloatDef( s,-0 ) );
    end;
}
end;

procedure TfrmPayMoney.RzEdit4KeyPress(Sender: TObject; var Key: Char);
begin
  if (key<>#46) and ((key < #48) or (key > #57)) and (key <> #8) and (Key = #77) then//如果输入不是数字或小数点（#46代表小数点）
  begin
    key:=#0; //取消输入的内容（#0代表空值）
  end;

end;

procedure TfrmPayMoney.RzMemo1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 13 then
  begin
  //  Self.RzBitBtn3.Click;
  end;  
end;

procedure TfrmPayMoney.RzMemo2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 13 then
  begin
  //  Self.RzBitBtn4.Click;
  end;  
end;

end.
