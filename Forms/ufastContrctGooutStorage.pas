unit ufastContrctGooutStorage;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ufrmInputform, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, cxContainer, cxEdit, cxStyles, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator, Data.DB, cxDBData,
  cxTextEdit, cxCheckBox, cxDBLookupComboBox, cxSpinEdit, cxDropDownEdit,
  cxCurrencyEdit, Vcl.Menus, Datasnap.DBClient, Vcl.ExtCtrls, cxDBEdit,
  Vcl.StdCtrls, cxButtons, cxMemo, RzPanel, Vcl.DBCtrls, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, cxButtonEdit, cxLookupEdit, cxDBLookupEdit,
  cxCalendar, cxMaskEdit, cxLabel, cxGroupBox, cxDBNavigator, RzButton;

type
  TfastContrctGooutStorage = class(TfrmInputformBase)
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fastContrctGooutStorage: TfastContrctGooutStorage;

implementation

{$R *.dfm}

end.
