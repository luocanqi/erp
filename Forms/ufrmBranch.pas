unit ufrmBranch;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, StdCtrls, Mask, RzEdit, RzButton, jpeg, ExtCtrls,
  RzPanel, RzRadGrp, RzSplit, RzTabs, ComCtrls,ufrmBranchInput, DB, ADODB,
  DBGridEhGrouping, ToolCtrlsEh, DBGridEhToolCtrls, DynVarsEh, GridsEh,TreeUtils,
  DBAxisGridsEh, DBGridEh, Menus,PrnDbgeh,EhLibADO, RzLabel, FileCtrl, RzFilSys,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer,
  cxEdit, dxSkinsCore, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, cxMemo, cxTextEdit, cxMaskEdit, cxDropDownEdit,
  cxButtons,StrUtils, RzGrids, RzListVw, ImgList, cxStyles, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxClasses, cxGridCustomView, cxGrid, RzDBGrid, MemTableDataEh, MemTableEh,
  DBVertGridsEh, cxTL, cxTLdxBarBuiltInMenu, cxInplaceContainer, cxTLData,
  cxDBTL, RzStatus, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVisualStudio2013Blue,
  dxSkinVisualStudio2013Dark, dxSkinVisualStudio2013Light, dxSkinVS2010,
  dxSkinWhiteprint, dxBarBuiltInMenu, cxPC, cxLabel, dxCore, cxDateUtils,
  cxCalendar, cxGroupBox, cxNavigator, cxCurrencyEdit, cxGridBandedTableView,
  cxGridDBBandedTableView, cxSpinEdit, EhLibVCL,ufrmBaseController, cxSplitter,
  cxRadioGroup, dxRatingControl, dxDBSparkline, cxMRUEdit, cxDBLookupComboBox,
  dxToggleSwitch, frxClass, frxDBSet, cxCheckBox, cxCheckComboBox;

type
  TfrmBranch = class(TfrmBaseController)
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    PrintDBGridEh1: TPrintDBGridEh;
    ADOMechanics: TADOQuery;
    DataMechanics: TDataSource;
    DataLease: TDataSource;
    ADOLease: TADOQuery;
    ADOFinance: TADOQuery;
    DataFinance: TDataSource;
    ADOCash: TADOQuery;
    DataCash: TDataSource;
    DataConcrete: TDataSource;
    ADOConcrete: TADOQuery;
    Panel18: TPanel;
    TreeView1: TTreeView;
    ADORebar: TADOQuery;
    PopupMenu2: TPopupMenu;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    PopupMenu3: TPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    DataFrame: TDataSource;
    ADOFrame: TADOQuery;
    DataQualifications: TDataSource;
    ADOQualifications: TADOQuery;
    RzToolbar3: TRzToolbar;
    RzSpacer16: TRzSpacer;
    RzToolButton1: TRzToolButton;
    RzSpacer17: TRzSpacer;
    RzToolButton2: TRzToolButton;
    RzSpacer18: TRzSpacer;
    RzToolButton3: TRzToolButton;
    RzPanel2: TRzPanel;
    RzPanel4: TRzPanel;
    RzPanel5: TRzPanel;
    RzPanel6: TRzPanel;
    RzPanel7: TRzPanel;
    RzPanel16: TRzPanel;
    RzPanel21: TRzPanel;
    RzPanel22: TRzPanel;
    RzPanel23: TRzPanel;
    RzPanel24: TRzPanel;
    RzPanel25: TRzPanel;
    RzPanel26: TRzPanel;
    RzPanel27: TRzPanel;
    RzPanel28: TRzPanel;
    RzPanel29: TRzPanel;
    RzPanel30: TRzPanel;
    QryGrid4: TADOQuery;
    dsGrid4: TDataSource;
    Print: TPopupMenu;
    MenuItem4: TMenuItem;
    MenuItem5: TMenuItem;
    MenuItem6: TMenuItem;
    N5: TMenuItem;
    Execl1: TMenuItem;
    RzPageControl3: TRzPageControl;
    TabSheet13: TRzTabSheet;
    TabSheet14: TRzTabSheet;
    cxSplitter3: TcxSplitter;
    RzToolbar2: TRzToolbar;
    RzSpacer5: TRzSpacer;
    RzToolButton7: TRzToolButton;
    RzSpacer8: TRzSpacer;
    RzToolButton8: TRzToolButton;
    RzSpacer9: TRzSpacer;
    RzToolButton9: TRzToolButton;
    RzSpacer10: TRzSpacer;
    RzToolButton10: TRzToolButton;
    RzToolButton11: TRzToolButton;
    RzSpacer11: TRzSpacer;
    RzToolButton12: TRzToolButton;
    RzSpacer12: TRzSpacer;
    RzSpacer15: TRzSpacer;
    RzToolButton16: TRzToolButton;
    RzSpacer1: TRzSpacer;
    cxGrid1: TcxGrid;
    tvGrid3: TcxGridDBTableView;
    tvGrid3Column1: TcxGridDBColumn;
    tvGrid3Column2: TcxGridDBColumn;
    tvGrid3Column3: TcxGridDBColumn;
    tvGrid3Column4: TcxGridDBColumn;
    tvGrid3Column5: TcxGridDBColumn;
    tvGrid3Column6: TcxGridDBColumn;
    tvGrid3Column7: TcxGridDBColumn;
    tvGrid3Column8: TcxGridDBColumn;
    tvGrid3Column9: TcxGridDBColumn;
    tvGrid3Column10: TcxGridDBColumn;
    tvGrid3Column11: TcxGridDBColumn;
    tvGrid3Column12: TcxGridDBColumn;
    tvGrid3Column13: TcxGridDBColumn;
    tvGrid3Column14: TcxGridDBColumn;
    tvGrid3Column15: TcxGridDBColumn;
    tvGrid3Column16: TcxGridDBColumn;
    tvGrid3Column18: TcxGridDBColumn;
    tvGrid3Column17: TcxGridDBColumn;
    tvView: TcxGridDBTableView;
    tvViewColumn1: TcxGridDBColumn;
    tvViewColumn2: TcxGridDBColumn;
    tvViewColumn16: TcxGridDBColumn;
    tvViewColumn3: TcxGridDBColumn;
    tvViewColumn4: TcxGridDBColumn;
    tvViewColumn5: TcxGridDBColumn;
    tvViewColumn6: TcxGridDBColumn;
    tvViewColumn7: TcxGridDBColumn;
    tvViewColumn8: TcxGridDBColumn;
    tvViewColumn9: TcxGridDBColumn;
    tvViewColumn10: TcxGridDBColumn;
    tvViewColumn11: TcxGridDBColumn;
    tvViewColumn12: TcxGridDBColumn;
    tvViewColumn13: TcxGridDBColumn;
    tvViewColumn14: TcxGridDBColumn;
    tvViewColumn15: TcxGridDBColumn;
    Lv1: TcxGridLevel;
    Lv2: TcxGridLevel;
    Splitter1: TSplitter;
    RzPageControl4: TRzPageControl;
    t1: TRzTabSheet;
    t2: TRzTabSheet;
    t3: TRzTabSheet;
    t4: TRzTabSheet;
    t5: TRzTabSheet;
    Panel3: TPanel;
    RzBitBtn9: TRzBitBtn;
    RzBitBtn10: TRzBitBtn;
    RzBitBtn11: TRzBitBtn;
    RzBitBtn13: TRzBitBtn;
    RzBitBtn14: TRzBitBtn;
    RzBitBtn21: TRzBitBtn;
    RzBitBtn112: TRzBitBtn;
    RzBitBtn119: TRzBitBtn;
    RzBitBtn122: TRzBitBtn;
    cxGroupBox5: TcxGroupBox;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    cxTextEdit6: TcxTextEdit;
    cxDateEdit7: TcxDateEdit;
    cxDateEdit8: TcxDateEdit;
    cxButton7: TcxButton;
    DBGridEh1: TDBGridEh;
    RzPanel8: TRzPanel;
    RzLabel2: TRzLabel;
    RzEdit7: TRzEdit;
    RzBitBtn64: TRzBitBtn;
    RzBitBtn65: TRzBitBtn;
    RzBitBtn66: TRzBitBtn;
    RzFileListBox2: TRzFileListBox;
    Panel4: TPanel;
    RzBitBtn8: TRzBitBtn;
    RzBitBtn12: TRzBitBtn;
    RzBitBtn15: TRzBitBtn;
    RzBitBtn19: TRzBitBtn;
    RzBitBtn20: TRzBitBtn;
    RzBitBtn23: TRzBitBtn;
    RzBitBtn113: TRzBitBtn;
    RzBitBtn118: TRzBitBtn;
    RzBitBtn123: TRzBitBtn;
    cxGroupBox6: TcxGroupBox;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    cxTextEdit8: TcxTextEdit;
    cxDateEdit9: TcxDateEdit;
    cxDateEdit10: TcxDateEdit;
    cxButton8: TcxButton;
    DBGridEh4: TDBGridEh;
    RzPanel9: TRzPanel;
    RzLabel3: TRzLabel;
    RzEdit8: TRzEdit;
    RzBitBtn67: TRzBitBtn;
    RzBitBtn68: TRzBitBtn;
    RzBitBtn69: TRzBitBtn;
    RzFileListBox3: TRzFileListBox;
    Panel12: TPanel;
    RzBitBtn30: TRzBitBtn;
    RzBitBtn31: TRzBitBtn;
    RzBitBtn32: TRzBitBtn;
    RzBitBtn33: TRzBitBtn;
    RzBitBtn34: TRzBitBtn;
    RzBitBtn35: TRzBitBtn;
    RzBitBtn114: TRzBitBtn;
    RzBitBtn117: TRzBitBtn;
    RzBitBtn124: TRzBitBtn;
    cxGroupBox7: TcxGroupBox;
    Label28: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    Label46: TLabel;
    cxTextEdit9: TcxTextEdit;
    cxDateEdit11: TcxDateEdit;
    cxDateEdit12: TcxDateEdit;
    cxButton9: TcxButton;
    cxComboBox4: TcxComboBox;
    DBGridEh6: TDBGridEh;
    RzPanel11: TRzPanel;
    RzLabel4: TRzLabel;
    RzEdit9: TRzEdit;
    RzBitBtn70: TRzBitBtn;
    RzBitBtn71: TRzBitBtn;
    RzBitBtn72: TRzBitBtn;
    RzFileListBox4: TRzFileListBox;
    cxGroupBox8: TcxGroupBox;
    Label31: TLabel;
    Label32: TLabel;
    Label33: TLabel;
    cxTextEdit10: TcxTextEdit;
    cxDateEdit13: TcxDateEdit;
    cxDateEdit14: TcxDateEdit;
    cxButton10: TcxButton;
    cxGrid4: TcxGrid;
    tvEngineeringQuantity: TcxGridDBTableView;
    tvEngineeringQuantityColumn13: TcxGridDBColumn;
    tvEngineeringQuantityColumn1: TcxGridDBColumn;
    tvEngineeringQuantityColumn2: TcxGridDBColumn;
    tvEngineeringQuantityColumn3: TcxGridDBColumn;
    tvEngineeringQuantityColumn4: TcxGridDBColumn;
    tvEngineeringQuantityColumn5: TcxGridDBColumn;
    tvEngineeringQuantityColumn6: TcxGridDBColumn;
    tvEngineeringQuantityColumn7: TcxGridDBColumn;
    tvEngineeringQuantityColumn9: TcxGridDBColumn;
    tvEngineeringQuantityColumn10: TcxGridDBColumn;
    tvEngineeringQuantityColumn8: TcxGridDBColumn;
    tvEngineeringQuantityColumn11: TcxGridDBColumn;
    tvEngineeringQuantityColumn12: TcxGridDBColumn;
    tvEngineeringQuantityColumn14: TcxGridDBColumn;
    tvEngineeringQuantityColumn15: TcxGridDBColumn;
    tvDetailedGrid: TcxGridDBTableView;
    tvDetailedGridColumn1: TcxGridDBColumn;
    tvDetailedGridColumn2: TcxGridDBColumn;
    tvDetailedGridColumn3: TcxGridDBColumn;
    tvDetailedGridColumn4: TcxGridDBColumn;
    tvDetailedGridColumn5: TcxGridDBColumn;
    tvDetailedGridColumn6: TcxGridDBColumn;
    tvDetailedGridColumn7: TcxGridDBColumn;
    tvDetailedGridColumn8: TcxGridDBColumn;
    LvQuantity: TcxGridLevel;
    LvDetailed: TcxGridLevel;
    t6: TRzTabSheet;
    Panel14: TPanel;
    RzBitBtn42: TRzBitBtn;
    RzBitBtn43: TRzBitBtn;
    RzBitBtn44: TRzBitBtn;
    RzBitBtn45: TRzBitBtn;
    RzBitBtn46: TRzBitBtn;
    RzBitBtn47: TRzBitBtn;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh8: TDBGridEh;
    RzPanel13: TRzPanel;
    RzLabel6: TRzLabel;
    RzEdit13: TRzEdit;
    RzBitBtn76: TRzBitBtn;
    RzBitBtn77: TRzBitBtn;
    RzBitBtn78: TRzBitBtn;
    RzFileListBox6: TRzFileListBox;
    cxGroupBox3: TcxGroupBox;
    cxLabel4: TcxLabel;
    cxDateEdit2: TcxDateEdit;
    cxLabel5: TcxLabel;
    cxTextEdit4: TcxTextEdit;
    cxButton4: TcxButton;
    cxLabel7: TcxLabel;
    cxComboBox5: TcxComboBox;
    cxTabSheet2: TcxTabSheet;
    DBGridEh13: TDBGridEh;
    RzPanel19: TRzPanel;
    RzLabel11: TRzLabel;
    RzEdit18: TRzEdit;
    RzBitBtn97: TRzBitBtn;
    RzBitBtn98: TRzBitBtn;
    RzBitBtn99: TRzBitBtn;
    RzFileListBox11: TRzFileListBox;
    cxGroupBox1: TcxGroupBox;
    cxLabel1: TcxLabel;
    cxDateEdit1: TcxDateEdit;
    cxLabel2: TcxLabel;
    cxLabel3: TcxLabel;
    cxComboBox1: TcxComboBox;
    cxTextEdit3: TcxTextEdit;
    cxButton3: TcxButton;
    cxLabel10: TcxLabel;
    cxComboBox3: TcxComboBox;
    t7: TRzTabSheet;
    Panel16: TPanel;
    RzBitBtn54: TRzBitBtn;
    RzBitBtn55: TRzBitBtn;
    RzBitBtn56: TRzBitBtn;
    RzBitBtn57: TRzBitBtn;
    RzBitBtn58: TRzBitBtn;
    RzBitBtn59: TRzBitBtn;
    RzBitBtn126: TRzBitBtn;
    cxGroupBox9: TcxGroupBox;
    Label34: TLabel;
    Label35: TLabel;
    Label36: TLabel;
    cxTextEdit11: TcxTextEdit;
    cxDateEdit15: TcxDateEdit;
    cxDateEdit16: TcxDateEdit;
    cxButton11: TcxButton;
    DBGridEh10: TDBGridEh;
    RzPanel14: TRzPanel;
    RzLabel7: TRzLabel;
    RzEdit14: TRzEdit;
    RzBitBtn79: TRzBitBtn;
    RzBitBtn80: TRzBitBtn;
    RzBitBtn81: TRzBitBtn;
    RzFileListBox7: TRzFileListBox;
    t8: TRzTabSheet;
    Panel15: TPanel;
    RzBitBtn48: TRzBitBtn;
    RzBitBtn49: TRzBitBtn;
    RzBitBtn50: TRzBitBtn;
    RzBitBtn51: TRzBitBtn;
    RzBitBtn52: TRzBitBtn;
    RzBitBtn53: TRzBitBtn;
    RzBitBtn127: TRzBitBtn;
    cxGroupBox10: TcxGroupBox;
    Label37: TLabel;
    Label38: TLabel;
    Label39: TLabel;
    cxTextEdit12: TcxTextEdit;
    cxDateEdit17: TcxDateEdit;
    cxDateEdit18: TcxDateEdit;
    cxButton12: TcxButton;
    DBGridEh9: TDBGridEh;
    RzPanel15: TRzPanel;
    RzLabel8: TRzLabel;
    RzEdit15: TRzEdit;
    RzBitBtn82: TRzBitBtn;
    RzBitBtn83: TRzBitBtn;
    RzBitBtn84: TRzBitBtn;
    RzFileListBox8: TRzFileListBox;
    t9: TRzTabSheet;
    cxGroupBox11: TcxGroupBox;
    Label40: TLabel;
    Label41: TLabel;
    Label42: TLabel;
    Label47: TLabel;
    cxTextEdit13: TcxTextEdit;
    cxDateEdit19: TcxDateEdit;
    cxDateEdit20: TcxDateEdit;
    cxButton13: TcxButton;
    cxComboBox6: TcxComboBox;
    Panel1: TPanel;
    RzBitBtn88: TRzBitBtn;
    RzBitBtn89: TRzBitBtn;
    RzBitBtn90: TRzBitBtn;
    RzBitBtn91: TRzBitBtn;
    RzBitBtn92: TRzBitBtn;
    RzBitBtn93: TRzBitBtn;
    RzBitBtn128: TRzBitBtn;
    DBGridEh12: TDBGridEh;
    RzPanel18: TRzPanel;
    RzLabel10: TRzLabel;
    RzEdit17: TRzEdit;
    RzBitBtn94: TRzBitBtn;
    RzBitBtn95: TRzBitBtn;
    RzBitBtn96: TRzBitBtn;
    RzFileListBox10: TRzFileListBox;
    t10: TRzTabSheet;
    cxGroupBox12: TcxGroupBox;
    Label43: TLabel;
    Label44: TLabel;
    Label45: TLabel;
    cxTextEdit14: TcxTextEdit;
    cxDateEdit21: TcxDateEdit;
    cxDateEdit22: TcxDateEdit;
    cxButton14: TcxButton;
    DBGridEh14: TDBGridEh;
    RzPanel20: TRzPanel;
    RzLabel12: TRzLabel;
    RzEdit19: TRzEdit;
    RzBitBtn100: TRzBitBtn;
    RzBitBtn101: TRzBitBtn;
    RzBitBtn102: TRzBitBtn;
    RzFileListBox12: TRzFileListBox;
    RzToolbar1: TRzToolbar;
    RzSpacer2: TRzSpacer;
    RzToolButton4: TRzToolButton;
    RzSpacer3: TRzSpacer;
    RzToolButton5: TRzToolButton;
    RzSpacer4: TRzSpacer;
    RzToolButton6: TRzToolButton;
    RzSpacer6: TRzSpacer;
    RzToolButton17: TRzToolButton;
    RzSpacer7: TRzSpacer;
    RzToolButton18: TRzToolButton;
    RzSpacer14: TRzSpacer;
    RzToolButton15: TRzToolButton;
    RzSpacer20: TRzSpacer;
    RzToolButton19: TRzToolButton;
    RzSpacer21: TRzSpacer;
    Panel2: TPanel;
    RzBitBtn103: TRzBitBtn;
    RzBitBtn104: TRzBitBtn;
    RzBitBtn105: TRzBitBtn;
    RzBitBtn106: TRzBitBtn;
    RzBitBtn107: TRzBitBtn;
    RzBitBtn108: TRzBitBtn;
    RzBitBtn129: TRzBitBtn;
    dsGrid3: TDataSource;
    qryGrid3: TADOQuery;
    WorkGrid: TcxGrid;
    tvWorkView: TcxGridDBTableView;
    WorkCol1: TcxGridDBColumn;
    WorkCol2: TcxGridDBColumn;
    WorkCol3: TcxGridDBColumn;
    WorkCol4: TcxGridDBColumn;
    WorkCol5: TcxGridDBColumn;
    WorkCol6: TcxGridDBColumn;
    WorkCol7: TcxGridDBColumn;
    WorkCol8: TcxGridDBColumn;
    WorkCol9: TcxGridDBColumn;
    WorkCol10: TcxGridDBColumn;
    tvWorkViewColumn1: TcxGridDBColumn;
    WorkCol11: TcxGridDBColumn;
    WorkCol12: TcxGridDBColumn;
    WorkCol13: TcxGridDBColumn;
    WorkCol14: TcxGridDBColumn;
    WorkLv: TcxGridLevel;
    Lt1: TcxLabel;
    RzToolbar12: TRzToolbar;
    RzSpacer100: TRzSpacer;
    RzToolButton75: TRzToolButton;
    RzSpacer101: TRzSpacer;
    RzToolButton76: TRzToolButton;
    RzSpacer102: TRzSpacer;
    RzToolButton77: TRzToolButton;
    RzSpacer103: TRzSpacer;
    RzToolButton78: TRzToolButton;
    RzToolButton79: TRzToolButton;
    RzSpacer104: TRzSpacer;
    RzToolButton80: TRzToolButton;
    RzSpacer105: TRzSpacer;
    RzSpacer106: TRzSpacer;
    RzSpacer107: TRzSpacer;
    RzSpacer108: TRzSpacer;
    RzToolButton81: TRzToolButton;
    RzSpacer109: TRzSpacer;
    RzToolButton82: TRzToolButton;
    RzSpacer110: TRzSpacer;
    RzSpacer111: TRzSpacer;
    RzToolButton83: TRzToolButton;
    cxLabel50: TcxLabel;
    Date5: TcxDateEdit;
    cxLabel51: TcxLabel;
    Date6: TcxDateEdit;
    RzToolButton20: TRzToolButton;
    RzSpacer19: TRzSpacer;
    ADOWork: TADOQuery;
    DataWork: TDataSource;
    ADOMaterial: TADOQuery;
    DataMaterial: TDataSource;
    DataRebar: TDataSource;
    ADOQuantity: TADOQuery;
    DataQuantity: TDataSource;
    ADODetailed: TADOQuery;
    DataDetailed: TDataSource;
    RzSpacer22: TRzSpacer;
    RzToolButton21: TRzToolButton;
    frxGrid3: TfrxDBDataset;
    frxWork: TfrxDBDataset;
    frxQuantity: TfrxDBDataset;
    frxDetailed: TfrxDBDataset;
    RzSpacer23: TRzSpacer;
    cxLabel6: TcxLabel;
    Date1: TcxDateEdit;
    cxLabel8: TcxLabel;
    Date2: TcxDateEdit;
    RzToolButton22: TRzToolButton;
    RzSpacer24: TRzSpacer;
    RzSpacer25: TRzSpacer;
    cxLabel9: TcxLabel;
    Date3: TcxDateEdit;
    cxLabel11: TcxLabel;
    Date4: TcxDateEdit;
    RzToolButton23: TRzToolButton;
    RzSpacer26: TRzSpacer;
    RzSpacer27: TRzSpacer;
    cxSplitter1: TcxSplitter;
    tvGroupView: TcxGridDBTableView;
    WorkGroupLv: TcxGridLevel;
    WorkGroup: TcxGrid;
    tvGroupViewCol1: TcxGridDBColumn;
    tvGroupViewCol2: TcxGridDBColumn;
    tvGroupViewCol4: TcxGridDBColumn;
    tvGroupViewCol5: TcxGridDBColumn;
    ADOWorkGroup: TADOQuery;
    DataWorkGroup: TDataSource;
    cxgrdbclmnGroupViewColumn1: TcxGridDBColumn;
    GridProjectNameGrid: TcxGrid;
    tvProjectView: TcxGridDBTableView;
    lvProject: TcxGridLevel;
    ProjectCol1: TcxGridDBColumn;
    ProjectCol2: TcxGridDBColumn;
    ADOProjectName: TADOQuery;
    DataProjectName: TDataSource;
    RzPanel1: TRzPanel;
    RzPanel3: TRzPanel;
    Splitter3: TSplitter;
    Splitter4: TSplitter;
    cxGrid3DBTableView1: TcxGridDBTableView;
    cxGrid3Level1: TcxGridLevel;
    cxGrid3: TcxGrid;
    cxGrid3DBTableView1Column1: TcxGridDBColumn;
    cxGrid3DBTableView1Column2: TcxGridDBColumn;
    cxGrid3DBTableView1Column3: TcxGridDBColumn;
    cxGrid3DBTableView1Column4: TcxGridDBColumn;
    WorkCol15: TcxGridDBColumn;
    FlowingGrid: TcxGrid;
    tvFlowingView: TcxGridDBTableView;
    tvFlowingViewColumn1: TcxGridDBColumn;
    tvFlowingViewColumn2: TcxGridDBColumn;
    tvFlowingViewColumn3: TcxGridDBColumn;
    FlowingLv: TcxGridLevel;
    tvFlowingViewColumn4: TcxGridDBColumn;
    tvFlowingViewColumn5: TcxGridDBColumn;
    ADOFlowingGroup: TADOQuery;
    DataFlowingGroup: TDataSource;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure RzBitBtn9Click(Sender: TObject);
    procedure RzBitBtn13Click(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure RzBitBtn10Click(Sender: TObject);
    procedure RzBitBtn14Click(Sender: TObject);
    procedure DBGridEh1RowDetailPanelShow(Sender: TCustomDBGridEh;
      var CanShow: Boolean);
    procedure Image1DblClick(Sender: TObject);
    procedure TreeView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridEh4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridEh1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure RzBitBtn21Click(Sender: TObject);
    procedure RzBitBtn8Click(Sender: TObject);
    procedure TreeView1Click(Sender: TObject);
    procedure DBGridEh4RowDetailPanelShow(Sender: TCustomDBGridEh;
      var CanShow: Boolean);
    procedure DBGridEh6RowDetailPanelShow(Sender: TCustomDBGridEh;
      var CanShow: Boolean);
    procedure DBGridEh8RowDetailPanelShow(Sender: TCustomDBGridEh;
      var CanShow: Boolean);
    procedure DBGridEh9RowDetailPanelShow(Sender: TCustomDBGridEh;
      var CanShow: Boolean);
    procedure DBGridEh10RowDetailPanelShow(Sender: TCustomDBGridEh;
      var CanShow: Boolean);
    procedure TreeView1CustomDrawItem(Sender: TCustomTreeView; Node: TTreeNode;
      State: TCustomDrawState; var DefaultDraw: Boolean);
    procedure TreeView1DragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure TreeView1DragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure FormResize(Sender: TObject);
    procedure RzBitBtn63Click(Sender: TObject);
    procedure RzBitBtn61Click(Sender: TObject);
    procedure RzBitBtn62Click(Sender: TObject);
    procedure RzBitBtn66Click(Sender: TObject);
    procedure RzBitBtn64Click(Sender: TObject);
    procedure RzBitBtn65Click(Sender: TObject);
    procedure RzBitBtn69Click(Sender: TObject);
    procedure RzBitBtn67Click(Sender: TObject);
    procedure RzBitBtn68Click(Sender: TObject);
    procedure RzBitBtn15Click(Sender: TObject);
    procedure RzBitBtn30Click(Sender: TObject);
    procedure RzBitBtn31Click(Sender: TObject);
    procedure RzBitBtn72Click(Sender: TObject);
    procedure RzBitBtn70Click(Sender: TObject);
    procedure RzBitBtn71Click(Sender: TObject);
    procedure RzBitBtn32Click(Sender: TObject);
    procedure RzBitBtn42Click(Sender: TObject);
    procedure RzBitBtn44Click(Sender: TObject);
    procedure RzBitBtn77Click(Sender: TObject);
    procedure RzBitBtn76Click(Sender: TObject);
    procedure RzBitBtn78Click(Sender: TObject);
    procedure RzBitBtn81Click(Sender: TObject);
    procedure RzBitBtn79Click(Sender: TObject);
    procedure RzBitBtn80Click(Sender: TObject);
    procedure RzBitBtn56Click(Sender: TObject);
    procedure RzBitBtn48Click(Sender: TObject);
    procedure RzBitBtn49Click(Sender: TObject);
    procedure RzBitBtn50Click(Sender: TObject);
    procedure RzBitBtn84Click(Sender: TObject);
    procedure RzBitBtn82Click(Sender: TObject);
    procedure RzBitBtn83Click(Sender: TObject);
    procedure RzBitBtn87Click(Sender: TObject);
    procedure RzBitBtn85Click(Sender: TObject);
    procedure RzBitBtn86Click(Sender: TObject);
    procedure RzBitBtn54Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure RzBitBtn88Click(Sender: TObject);
    procedure DBGridEh11KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure RzBitBtn90Click(Sender: TObject);
    procedure RzBitBtn91Click(Sender: TObject);
    procedure DBGridEh12KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridEh12DblClick(Sender: TObject);
    procedure RzBitBtn96Click(Sender: TObject);
    procedure RzBitBtn94Click(Sender: TObject);
    procedure RzBitBtn95Click(Sender: TObject);
    procedure DBGridEh12RowDetailPanelShow(Sender: TCustomDBGridEh;
      var CanShow: Boolean);
    procedure DBGridEh4DblClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure DBGridEh13DblClick(Sender: TObject);
    procedure cxTextEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxDateEdit1PropertiesCloseUp(Sender: TObject);
    procedure cxComboBox1PropertiesCloseUp(Sender: TObject);
    procedure cxComboBox3PropertiesCloseUp(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure RzBitBtn103Click(Sender: TObject);
    procedure RzBitBtn102Click(Sender: TObject);
    procedure RzBitBtn100Click(Sender: TObject);
    procedure RzBitBtn101Click(Sender: TObject);
    procedure DBGridEh14RowDetailPanelShow(Sender: TCustomDBGridEh;
      var CanShow: Boolean);
    procedure DBGridEh14KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure RzBitBtn112Click(Sender: TObject);
    procedure RzBitBtn113Click(Sender: TObject);
    procedure RzBitBtn114Click(Sender: TObject);
    procedure RzBitBtn119Click(Sender: TObject);
    procedure RzBitBtn118Click(Sender: TObject);
    procedure RzBitBtn117Click(Sender: TObject);
    procedure tvGrid3Column1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure tvGrid3Column3CompareRowValuesForCellMerging(
      Sender: TcxGridColumn; ARow1: TcxGridDataRow;
      AProperties1: TcxCustomEditProperties; const AValue1: Variant;
      ARow2: TcxGridDataRow; AProperties2: TcxCustomEditProperties;
      const AValue2: Variant; var AAreEqual: Boolean);
    procedure RzToolButton7Click(Sender: TObject);
    procedure RzToolButton12Click(Sender: TObject);
    procedure RzToolButton16Click(Sender: TObject);
    procedure RzToolButton1Click(Sender: TObject);
    procedure RzToolButton8Click(Sender: TObject);
    procedure tvGrid3EditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure tvGrid3TcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
      Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
      var AText: string);
    procedure tvGrid3Column18CustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure tvGrid3Column11CustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure tvGrid3StylesGetContentStyle(Sender: TcxCustomGridTableView;
      ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem;
      var AStyle: TcxStyle);
    procedure tvGrid3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure RzToolButton4Click(Sender: TObject);
    procedure RzToolButton5Click(Sender: TObject);
    procedure RzToolButton19Click(Sender: TObject);
    procedure RzToolButton15Click(Sender: TObject);
    procedure WorkCol1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure tvWorkViewKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure RzPageControl3TabClick(Sender: TObject);
    procedure RzPageControl4TabClick(Sender: TObject);
    procedure tvGrid3Editing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure tvEngineeringQuantityColumn13GetDisplayText(
      Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
      var AText: string);
    procedure RzToolButton82Click(Sender: TObject);
    procedure RzToolButton20Click(Sender: TObject);
    procedure RzToolButton75Click(Sender: TObject);
    procedure RzToolButton80Click(Sender: TObject);
    procedure RzBitBtn122Click(Sender: TObject);
    procedure cxTextEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxButton7Click(Sender: TObject);
    procedure cxTextEdit8KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxButton8Click(Sender: TObject);
    procedure RzBitBtn123Click(Sender: TObject);
    procedure RzBitBtn124Click(Sender: TObject);
    procedure cxTextEdit9KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxComboBox4PropertiesCloseUp(Sender: TObject);
    procedure cxButton9Click(Sender: TObject);
    procedure cxTextEdit11KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxButton11Click(Sender: TObject);
    procedure cxTextEdit12KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxButton12Click(Sender: TObject);
    procedure cxTextEdit14KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxButton14Click(Sender: TObject);
    procedure cxTextEdit13KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxComboBox6PropertiesCloseUp(Sender: TObject);
    procedure cxButton13Click(Sender: TObject);
    procedure RzToolButton76Click(Sender: TObject);
    procedure RzToolButton83Click(Sender: TObject);
    procedure RzToolButton78Click(Sender: TObject);
    procedure tvEngineeringQuantityColumnSizeChanged(Sender: TcxGridTableView;
      AColumn: TcxGridColumn);
    procedure tvEngineeringQuantityEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure tvEngineeringQuantityKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure tvBalanceColumn1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure tvEngineeringQuantityTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems1GetText(
      Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
      var AText: string);
    procedure tvWorkViewDblClick(Sender: TObject);
    procedure RzToolButton21Click(Sender: TObject);
    procedure tvWorkViewColumnSizeChanged(Sender: TcxGridTableView;
      AColumn: TcxGridColumn);
    procedure MenuItem4Click(Sender: TObject);
    procedure MenuItem5Click(Sender: TObject);
    procedure MenuItem6Click(Sender: TObject);
    procedure Execl1Click(Sender: TObject);
    procedure RzToolButton22Click(Sender: TObject);
    procedure tvWorkViewTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
      Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
      var AText: string);
    procedure RzToolButton23Click(Sender: TObject);
    procedure tvViewColumn1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure tvGroupViewCol1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure ProjectCol1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure tvProjectViewDblClick(Sender: TObject);
    procedure tvFlowingViewColumn1GetDisplayText(
      Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
      var AText: string);
    procedure tvWorkViewStylesGetContentStyle(Sender: TcxCustomGridTableView;
      ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem;
      var AStyle: TcxStyle);
    procedure WorkCol15PropertiesChange(Sender: TObject);
    procedure tvWorkViewEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure tvWorkViewMouseEnter(Sender: TObject);
    procedure tvWorkViewMouseLeave(Sender: TObject);
    procedure tvWorkViewCellClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure tvGrid3Column18PropertiesChange(Sender: TObject);
  private
    { Private declarations }
    g_CodeList      : string;
    g_TopLevelName  : string;
    g_PactCode      : string;
    g_PostNumbers   : string;
//    g_WorkManageDir : string; //考勤
    g_Expenditure   : string; //材料
    g_Concrete      : string; //混凝土
//    g_Mechanics     : string; //机械
    g_WithdrawMoney : string; //支款
    g_EnterStorage  : string; //入库单
    g_RebarManage   : string;//钢筋
    g_Qualifications: string;//资质目录
    g_PatTree : TTreeUtils;
    g_Sum_Money : Currency;
    g_Paid      : Currency;//交易总额
    procedure WndProc(var Message: TMessage); override;  // 第一优先权
    procedure SetcxGrid(lpGridView : TcxGridDBTableView ; index : Byte ;lpSuffix:string);
    procedure RefreshDetailTable();
  public
    { Public declarations }
    //支款修改
    function IsPactCode(AParent,ACode : Integer; s : string):Boolean;stdcall;
    procedure ShowData(AIndex,APacttype: Integer; ACode : string);
    procedure ShowConcrete(AIndex : Integer ; ACode : string);//混凝土
    function getDBGridehCapital(DBGridEh : TDBGridEh;t1:Currency;AIndex : Integer = 5 ):Boolean;stdcall;
    procedure GetSumMoney(s : string);
    procedure GetSumBranch(lpId: string);
    procedure GetFlowingGroupMoney(lpSingName : string);
    //查询
    function GetSelectMaterial(s : string; Index:Byte):Boolean;
    function GetSelectConcrete(s : string; Index:Byte):Boolean;
    function GetSelectMechanics(s : string; Index:Byte):Boolean;
    function GetSelectFinance(s : string; Index:Byte):Boolean;
    function GetSelectCash(s : string; Index:Byte):Boolean;
    function GetSelectQualifications(s : string; Index:Byte):Boolean;
    function GetSelectRebar(s : string; Index:Byte):Boolean;

  end;

var
  frmBranch : TfrmBranch;
  g_Index : Integer;

implementation

uses
   ufrmCalcExpression,
   ufrmAccounts,
   ufrmIsViewGrid,
   ufrmDetailed,
   Math,
   ufrmMain,
   ufrmAddConcrete,
   uDataModule,
   global,
   TreeFillThrd,
   PrViewEh,
   Printers,
   ShellAPI,
   ufrmInspectPost,
   uInAccounts,
   ufrmN1,
   ufrmReadyMoney,
   ufunctions,
   UnitExpCalc,
   ufrmRebarPost,
   ufrmPostPact,
   ufrmCashierAccount,
   ufrmLease,
   ufrmEnterStorage,
   ufrmTurnover;

{$R *.dfm}

//******************************************************************************
function GetTableMaterial(Code : string):string;stdcall;
begin
  //材料
  Result  := 'Select (e.Price * e.num * e.truck) as t,' +
                                          'e.numbers,' +
                                          'e.code,'    +
                                          'e.caption,' +
                                          'e.content,' +
                                          'e.pay_time,'+
                                          'e.End_time,'+
                                          'e.truck,'   +
                                          'e.Units,'   +
                                          'e.remarks,' +
                                          'e.Price,'   +
                                          'e.num,'     +
                                          'e.编号,'    +
                                          'e.pacttype,'+
                                          'e.PurchaseCompany' +
                                          ' from '            + g_Table_Expenditure +
                                          ' as e where e.'    + Code +
                                          ' and e.pacttype=' + IntToStr(g_pacttype) + '' ;

end;

function  TfrmBranch.GetSelectMaterial(s : string; Index:Byte):Boolean;
var
  szColNameList : string;
  szColName  : string;
  szColName1 : string;
  szColName2 : string;
  szColName3 : string;
  Sqltext : string;
  List: TStringList;

begin
  //材料
  inherited;
  szColName1 := Self.DBGridEh1.Columns[1].FieldName;
  szColName2 := Self.DBGridEh1.Columns[2].FieldName;
  List := TStringList.Create;
  try
    s :=  StringReplace(s, '，', ',', [rfReplaceAll]);
    ExtractStrings([','],[],PWideChar( s ),List);
    case List.Count of
      1:
      begin
        Sqltext := ' AND (' + szColName1 + ' like "%' + List.Strings[0] + '%"' +
                   ' OR ' + szColName2 + ' like "%' + List.Strings[0] + '%")' ;
      end;
      2:
      begin
        Sqltext := ' AND '   + szColName1 + ' like "%' + List.Strings[0] +
                   '%" AND ' + szColName2 + ' like "%' + List.Strings[1] + '%"' ;
      end;
    end;

  finally
    List.Free;
  end;

  with Self.ADOMaterial do
  begin
    Close;

    case Index of
      0:
      begin
        SQL.Text :=GetTableMaterial(g_CodeList) + Sqltext;
      end;
      1:
      begin
        szColName:= Self.DBGridEh1.Columns[4].FieldName;
        SQL.Text :=GetTableMaterial(g_CodeList) + Sqltext +' and ' + szColName + ' between :t1 and :t2';
        Parameters.ParamByName('t1').Value := Self.cxDateEdit7.Date;  // StrToDate('2016-4-17 00:00:00');
        Parameters.ParamByName('t2').Value := Self.cxDateEdit8.Date;
      end;
    end;
    Open;
  end;
  s := 'Select sum(Price * num * truck) as t from ' +
                  g_Table_Expenditure + ' where ' +
                  g_CodeList + ' and pacttype=' +
                  IntToStr(g_pacttype) + Sqltext ;
  GetSumMoney(s);
end;
//******************************************************************************
//混凝土
function GetTableConcrete(Code : string):string;stdcall;
begin
  Result := 'Select (e.UnitPrice * e.Stere) as t,' +
                                          'e.Numbers,' +
                                          'e.code,'    +
                                          'e.Deliver,' +
                                          'e.Deliver_UnitPrice,' +
                                          'e.Concrete,'+
                                          'e.Concrete_UnitPrice,' +
                                          'e.Permeability,'+
                                          'e.Permeability_Unitprice,' +
                                          'e.chAttach,'  +
                                          'e.chAttach_UnitPrice,'+
                                          'e.chAdditive,'+
                                          'e.chAdditive_UnitPrice,'+
                                          'e.Technical,' +
                                          'e.Stere,'+
                                          'e.chPosition,'+
                                          'e.UnitPrice,'  +
                                          'e.WagonNumber,'+
                                          'e.Sign,' +
                                          'e.remarks,'+
                                          'e.pouringTime,'   +
                                          'e.leaveTime,'     +
                                          'e.chDate,'        +
                                          'e.chSupplyUnit,'  +
                                          'e.chProjectName,' +
                                          'e.PurchaseCompany,'+
                                          'e.编号'           +
                                          ' from '+ g_Table_Concrete +' as e where e.' + Code;
end;

function  TfrmBranch.GetSelectConcrete(s : string; Index:Byte):Boolean;
var
  szColNameList : string;
  szColName  : string;
  szColName1 : string;
  szColName2 : string;
  szColName3 : string;
  Sqltext : string;
  List: TStringList;

begin
//混凝土
  inherited;
  szColName1 := Self.DBGridEh4.Columns[1].FieldName;
  szColName2 := Self.DBGridEh4.Columns[2].FieldName;
  szColName3 := Self.DBGridEh4.Columns[7].FieldName;

  List := TStringList.Create;
  try
    s :=  StringReplace(s, '，', ',', [rfReplaceAll]);
    ExtractStrings([','],[],PWideChar( s ),List);
    case List.Count of
      1:
      begin
        Sqltext := ' AND (' + szColName1 + ' like "%' + List.Strings[0] + '%"' +
                   ' or '   + szColName2 + ' like "%' + List.Strings[0] + '%"' +
                   ' or '   + szColName3 + ' like "%' + List.Strings[0] + '%")';
      end;
      2:
      begin
        Sqltext := ' AND '   + szColName1 + ' like "%' + List.Strings[0] +
                   '%" AND ' + szColName2 + ' like "%' + List.Strings[1] + '%"' ;
      end;
      3:
      begin
        Sqltext := ' AND '   + szColName1 + ' like "%' + List.Strings[0] +
                   '%" AND ' + szColName2 + ' like "%' + List.Strings[1] +
                   '%" AND ' + szColName3 + ' like "%' + List.Strings[2] + '%"' ;
      end;
    end;

  finally
    List.Free;
  end;

  with Self.ADOConcrete do
  begin
    Close;

    case Index of
      0:
      begin
        SQL.Text :=GetTableConcrete(g_CodeList) + Sqltext;
        OutputLog(SQL.Text);
      end;
      1:
      begin
        szColName:= Self.DBGridEh4.Columns[3].FieldName;
        SQL.Text :=GetTableConcrete(g_CodeList) + Sqltext +' and ' + szColName + ' between :t1 and :t2';
        Parameters.ParamByName('t1').Value := Self.cxDateEdit9.Date;  // StrToDate('2016-4-17 00:00:00');
        Parameters.ParamByName('t2').Value := Self.cxDateEdit10.Date;
      end;
    end;
    Open;
  end;
  s := 'Select sum(UnitPrice * Stere) as t from ' +
                  g_Table_Concrete + ' where ' + g_CodeList + ' ' + Sqltext;
  GetSumMoney(s);
end;
//******************************************************************************
//机械
function  TfrmBranch.GetSelectMechanics(s : string; Index:Byte):Boolean;
var
  szColNameList : string;
  szColName  : string;
  szColName1 : string;
  szColName2 : string;
  szColName3 : string;
  Sqltext : string;
  List: TStringList;
  SQLStr : string;
begin
//机械
  inherited;

  ShowMessage(s);
  szColName1 := Self.DBGridEh6.Columns[1].FieldName;
  szColName2 := Self.DBGridEh6.Columns[2].FieldName;
  szColName3 := Self.DBGridEh6.Columns[11].FieldName; //机械类别

  SQLStr := 'Select * from ' + g_Table_Branch_Mechanics +' as e where e.' + g_CodeList ;

  List := TStringList.Create;
  try
    s :=  StringReplace(s, '，', ',', [rfReplaceAll]);
    ExtractStrings([','],[],PWideChar( s ),List);
    case List.Count of
      1:
      begin
        Sqltext := ' AND (' + szColName1 + ' like "%' + List.Strings[0] + '%"' +
                   ' or   ' + szColName3 + ' like "%' + List.Strings[0] + '%"' +
                   ' or   ' + szColName2 + ' like "%' + List.Strings[0] + '%")';
      end;
      2..3:
      begin
        Sqltext := ' AND ' + szColName1 + ' like "%' + List.Strings[0] +
                 '%" AND ' + szColName2 + ' like "%' + List.Strings[1] +
                 '%" AND ' + szColName3 + ' like "%' + List.Strings[2] + '%"' ;
      end;
    end;

  finally
    List.Free;
  end;

  with Self.ADOMechanics do
  begin
    Close;

    case Index of
      0:
      begin
        SQL.Text := SQLStr + Sqltext;
      end;
      1:
      begin
        szColName:= Self.DBGridEh6.Columns[3].FieldName;
        SQL.Text := SQLStr + Sqltext +' and ' + szColName + ' between :t1 and :t2';
        Parameters.ParamByName('t1').Value := Self.cxDateEdit11.Date;  // StrToDate('2016-4-17 00:00:00');
        Parameters.ParamByName('t2').Value := Self.cxDateEdit12.Date;
      end;
      2:
      begin

      end;
    end;
    Open;
  end;
  s := 'Select sum(SumMoney) as t from '+ g_Table_Branch_Mechanics +' where ' + g_CodeList + ' ' + Sqltext ;
  GetSumMoney(s);
end;
//******************************************************************************
//项目部财务
function  TfrmBranch.GetSelectFinance(s : string; Index:Byte):Boolean;
var
  szColNameList : string;
  szColName  : string;
  szColName1 : string;
  szColName2 : string;
  szColName3 : string;
  Sqltext : string;
  List: TStringList;

begin
  //项目部财务
  inherited;
  szColName1 := Self.DBGridEh10.Columns[1].FieldName;
  szColName2 := Self.DBGridEh10.Columns[2].FieldName;
  List := TStringList.Create;
  try
    s :=  StringReplace(s, '，', ',', [rfReplaceAll]);
    ExtractStrings([','],[],PWideChar( s ),List);
    case List.Count of
      1:
      begin
        Sqltext := ' AND (' + szColName1 + ' like "%' + List.Strings[0] + '%"' +
                   ' OR '   + szColName2 + ' like "%' + List.Strings[0] + '%")' ;
      end;
      2:
      begin
        Sqltext := ' AND '   + szColName1 + ' like "%' + List.Strings[0] +
                   '%" AND ' + szColName2 + ' like "%' + List.Strings[1] + '%"' ;
      end;
    end;

  finally
    List.Free;
  end;

  with Self.ADOFinance do
  begin
    Close;

    case Index of
      0:
      begin
        SQL.Text :=GetTableMaterial(g_CodeList) + Sqltext;
      end;
      1:
      begin
        szColName:= Self.DBGridEh10.Columns[4].FieldName;
        SQL.Text :=GetTableMaterial(g_CodeList) + Sqltext +' and ' + szColName + ' between :t1 and :t2';
        Parameters.ParamByName('t1').Value := Self.cxDateEdit15.Date;  // StrToDate('2016-4-17 00:00:00');
        Parameters.ParamByName('t2').Value := Self.cxDateEdit16.Date;
      end;
    end;
    Open;
  end;

  s := 'Select sum(Price * num * truck) as t from ' +
                  g_Table_Expenditure + ' where ' +
                  g_CodeList + ' and pacttype=' +
                  IntToStr(g_pacttype) + Sqltext ;
  GetSumMoney(s);
end;
//******************************************************************************
function  TfrmBranch.GetSelectCash(s : string; Index:Byte):Boolean;
var
  szColNameList : string;
  szColName  : string;
  szColName1 : string;
  szColName2 : string;
  szColName3 : string;
  Sqltext : string;
  List: TStringList;

begin
  //现金供应单位
  inherited;
  szColName1 := Self.DBGridEh9.Columns[1].FieldName;
  szColName2 := Self.DBGridEh9.Columns[2].FieldName;
  List := TStringList.Create;
  try
    s :=  StringReplace(s, '，', ',', [rfReplaceAll]);
    ExtractStrings([','],[],PWideChar( s ),List);
    case List.Count of
      1:
      begin
        Sqltext := ' AND (' + szColName1 + ' like "%' + List.Strings[0] + '%"' +
                   ' OR ' + szColName2 + ' like "%' + List.Strings[0] + '%")' ;
      end;
      2:
      begin
        Sqltext := ' AND '   + szColName1 + ' like "%' + List.Strings[0] +
                   '%" AND ' + szColName2 + ' like "%' + List.Strings[1] + '%"' ;
      end;
    end;

  finally
    List.Free;
  end;

  with Self.ADOCash do
  begin
    Close;

    case Index of
      0:
      begin
        SQL.Text :=GetTableMaterial(g_CodeList) + Sqltext;
      end;
      1:
      begin
        szColName:= Self.DBGridEh9.Columns[4].FieldName;
        SQL.Text :=GetTableMaterial(g_CodeList) + Sqltext +' and ' + szColName + ' between :t1 and :t2';
        Parameters.ParamByName('t1').Value := Self.cxDateEdit17.Date;  // StrToDate('2016-4-17 00:00:00');
        Parameters.ParamByName('t2').Value := Self.cxDateEdit18.Date;
      end;
    end;
    Open;
  end;

  s := 'Select sum(Price * num * truck) as t from ' +
                  g_Table_Expenditure + ' where ' +
                  g_CodeList + ' and pacttype=' +
                  IntToStr(g_pacttype) + Sqltext ;
  GetSumMoney(s);
end;
//******************************************************************************
function GetTableQualifications(Code : string):string;
begin
  Result := 'Select (e.Price * e.num) as t,' +
                                                'e.numbers,' +
                                                'e.code,'    +
                                                'e.caption,' +
                                                'e.content,' +
                                                'e.pay_time,'+
                                                'e.End_time,'+
                                                'e.Units,'   +
                                                'e.remarks,' +
                                                'e.Price,'   +
                                                'e.num,'     +
                                                'e.编号,'    +
                                                'e.ProjectDepartment' +
                                                ' from '+ g_Table_Qualifications +' as e where e.' + Code;

end;

function  TfrmBranch.GetSelectQualifications(s : string; Index:Byte):Boolean;
var
  szColNameList : string;
  szColName  : string;
  szColName1 : string;
  szColName2 : string;
  szColName3 : string;
  Sqltext : string;
  List: TStringList;

begin
  //资质管理
  inherited;
  szColName1 := Self.DBGridEh14.Columns[1].FieldName;
  szColName2 := Self.DBGridEh14.Columns[2].FieldName;

  List := TStringList.Create;
  try
    s :=  StringReplace(s, '，', ',', [rfReplaceAll]);
    ExtractStrings([','],[],PWideChar( s ),List);
    case List.Count of
      1:
      begin
        Sqltext := ' AND (' + szColName1 + ' like "%' + List.Strings[0] + '%"' +
                   ' OR ' + szColName2 + ' like "%' + List.Strings[0] + '%")' ;
      end;
      2:
      begin
        Sqltext := ' AND '   + szColName1 + ' like "%' + List.Strings[0] +
                   '%" AND ' + szColName2 + ' like "%' + List.Strings[1] + '%"' ;
      end;
    end;

  finally
    List.Free;
  end;

  with Self.ADOQualifications do
  begin
    Close;

    case Index of
      0:
      begin
        SQL.Text :=GetTableQualifications(g_CodeList) + Sqltext;
      end;
      1:
      begin
        szColName:= Self.DBGridEh14.Columns[4].FieldName;
        SQL.Text :=GetTableQualifications(g_CodeList) + Sqltext +' and ' + szColName + ' between :t1 and :t2';
        Parameters.ParamByName('t1').Value := Self.cxDateEdit21.Date;  // StrToDate('2016-4-17 00:00:00');
        Parameters.ParamByName('t2').Value := Self.cxDateEdit22.Date;
      end;
    end;
    Open;
  end;

  s := 'Select sum(Price * num) as t from ' +
                  g_Table_Qualifications + ' where ' +
                  g_CodeList + ' ' + Sqltext ;
  GetSumMoney(s);

end;
//******************************************************************************
function GetTableRebar(Code : string):string;
begin
  Result := 'Select * from '+ g_Table_Branch_RebarManage +' where ' + Code + ')' ;
end;

function  TfrmBranch.GetSelectRebar(s : string; Index:Byte):Boolean;
var
  szColNameList : string;
  szColName  : string;
  szColName1 : string;
  szColName2 : string;
  szColName3 : string;
  Sqltext : string;
  List: TStringList;

begin
  //钢筋管理
  inherited;

  szColName1 := Self.DBGridEh12.Columns[1].FieldName;
  szColName2 := Self.DBGridEh12.Columns[2].FieldName;
  szColName3 := Self.DBGridEh12.Columns[5].FieldName;

  List := TStringList.Create;
  try
    s :=  StringReplace(s, '，', ',', [rfReplaceAll]);
    ExtractStrings([','],[],PWideChar( s ),List);
    case List.Count of
      1:
      begin
        Sqltext := ' AND (' + szColName1 + ' like "%' + List.Strings[0] + '%"' +
                   ' OR '   + szColName3 + ' like "%' + List.Strings[0] + '%"' +
                   ' OR '   + szColName2 + ' like "%' + List.Strings[0] + '%")' ;
      end;
      2:
      begin
        Sqltext := ' AND '   + szColName1 + ' like "%' + List.Strings[0] +
                   '%" AND ' + szColName2 + ' like "%' + List.Strings[1] + '%"' ;
      end;
      3:
      begin
        Sqltext := ' AND '   + szColName1 + ' like "%' + List.Strings[0] +
                   '%" AND ' + szColName2 + ' like "%' + List.Strings[1] +
                   '%" AND ' + szColName3 + ' like "%' + List.Strings[2] + '"' ;
      end;
    end;

  finally
    List.Free;
  end;

  with Self.ADORebar do
  begin
    Close;

    case Index of
      0:
      begin
        SQL.Text :=GetTableRebar(g_CodeList) + Sqltext;
      end;
      1:
      begin
        szColName:= Self.DBGridEh12.Columns[3].FieldName;
        SQL.Text := GetTableRebar(g_CodeList) + Sqltext +' and ' + szColName + ' between :t1 and :t2';
        Parameters.ParamByName('t1').Value := Self.cxDateEdit21.Date;  // StrToDate('2016-4-17 00:00:00');
        Parameters.ParamByName('t2').Value := Self.cxDateEdit22.Date;
      end;
    end;
    Open;
  end;

  s := 'Select sum(chTotal) as t from '+ g_Table_Branch_RebarManage +' where ' + g_CodeList + ' ' + Sqltext;   // and e.pacttype=' + IntToStr(APacttype) + '
  GetSumMoney(s);
end;
//******************************************************************************

procedure TfrmBranch.SetcxGrid(lpGridView : TcxGridDBTableView ; index : Byte ;lpSuffix:string);
var
  i : Integer;
  szFieldName : string;
  szCaption   : string;
  szName : string;

begin
  for I := 0 to lpGridView.ColumnCount-1 do
  begin
    Application.ProcessMessages;
    SetGrid(lpGridView.Columns[i],index,Self.Name + lpSuffix);
  end;
end;

procedure TfrmBranch.WndProc(var Message: TMessage);
begin

  case Message.Msg of
    WM_LisView:
    begin
      Self.TreeView1Click(nil);
    end; // 这里加上Exit就不再继续传递了
  end;
  // 一定要加上这句，否则编译通不过。因为绝大部分消息没人处理了
  inherited WndProc(Message);
  // 会一路向上调用，直到TControl.WndProc调用Dispatch来寻找消息处理函数
end;

procedure TfrmBranch.WorkCol15PropertiesChange(Sender: TObject);
begin
  inherited;
  Self.ADOWork.UpdateBatch(arAll);
  Self.ADOWork.Requery();
end;

procedure TfrmBranch.WorkCol1GetDisplayText(Sender: TcxCustomGridTableItem;
  ARecord: TcxCustomGridRecord; var AText: string);
begin
  inherited;
  AText := IntToStr(ARecord.Index + 1 ) ;
end;

procedure getPactMoney(Anode: TTreeNode;var AMoney : Currency);
var
  i: integer;
  node: TTreeNode;
  szData : PNodeData;
begin
  if Anode.Count = 0 then
  begin
    szData := PNodeData(ANode.Data);
    AMoney := szData.sum_Money;
  end
  else
  begin
    for i := 0 to Anode.Count - 1 do
    begin
      Node := ANode.Item[i];
      if Assigned(node) then
      begin
        szData := PNodeData(Node.Data);
        AMoney := AMoney + szData.sum_Money;
      end;

      if (node.Count > 0)   then
      begin
        getPactMoney(Node , AMoney );
      end;
    end;

  end;

end;

function TfrmBranch.IsPactCode(AParent,ACode : Integer; s : string):Boolean;stdcall;
begin
  Result := False;
  if AParent <> ACode then
  begin
     Application.MessageBox(PChar( '请选择"' + s + '"对应合同在操作'),m_title,MB_OK + MB_ICONQUESTION );
  end else begin
     Result := True;
  end;
  
end;  

procedure TfrmBranch.cxButton11Click(Sender: TObject);
begin
  inherited;
  GetSelectFinance(Self.cxTextEdit11.Text,1);
end;

procedure TfrmBranch.cxButton12Click(Sender: TObject);
begin
  inherited;
  GetSelectCash(Self.cxTextEdit12.Text,1);
end;

procedure TfrmBranch.cxButton13Click(Sender: TObject);
var
  s : string;

begin
  inherited;
  if Self.cxTextEdit13.Text <> '' then
  begin
    s := Self.cxTextEdit13.Text;
  end;
  if Self.cxComboBox6.Text <> '' then
  begin
    if s <> '' then
    begin
      s := s + Self.cxComboBox6.Text;
    end else
    begin
      s := Self.cxComboBox6.Text;
    end;
  end;
  GetSelectRebar(s,1);
end;

procedure TfrmBranch.cxButton14Click(Sender: TObject);
begin
  inherited;
  GetSelectQualifications(Self.cxTextEdit14.Text,1);
end;

procedure TfrmBranch.cxButton3Click(Sender: TObject);
var
  s0,s1,s2,s3 : string;

begin
  s0 := Self.cxComboBox1.Text;
  s1 := Self.cxComboBox3.Text;
  s2 := Self.cxTextEdit3.Text;
  s3 := self.cxDateEdit1.Text;

  with Self.ADOFrame do
  begin
    Close;
    SQL.Clear;
    SQL.Text := 'Select * from ' + g_Table_Branch_EnterStorage + ' where UnitTypes="' + s0 +
                                                               '" and Types="' + s1 +
                                                               '" and Dates=#' + s3 + '# and SupplyUnit like "%'+ s2 +'%"' ;
    Open;
    //Sort := '';
  end;
end;

procedure TfrmBranch.cxButton7Click(Sender: TObject);
begin
  inherited;
  GetSelectMaterial(Self.cxTextEdit6.Text,1);
end;

procedure TfrmBranch.cxButton8Click(Sender: TObject);
begin
  inherited;
  //砼范围查询
  GetSelectConcrete(Self.cxTextEdit8.Text,1);
end;

procedure TfrmBranch.cxButton9Click(Sender: TObject);
var
  s : string;

begin
  inherited;
  if Self.cxTextEdit9.Text <> '' then  s := Self.cxTextEdit9.Text;

  if Self.cxComboBox4.Text <> '' then
  begin
    if s <> '' then
      s := s + ',' + Self.cxComboBox4.Text
    else
      s := Self.cxComboBox4.Text;
  end;

  GetSelectMechanics(S,1);
end;

procedure TfrmBranch.cxComboBox1PropertiesCloseUp(Sender: TObject);
var
  s0,s1 : string;

begin
  s0 := Self.cxComboBox1.Text;
  with Self.ADOFrame do
  begin
    Close;
    SQL.Clear;
    SQL.Text := 'Select * from ' + g_Table_Branch_EnterStorage + ' where UnitTypes="' + s0 + '"';
    Open;
    //Sort := '';
  end;
end;

procedure TfrmBranch.cxComboBox3PropertiesCloseUp(Sender: TObject);
var
  s : string;

begin

  s := Self.cxComboBox3.Text;
  with Self.ADOFrame do
  begin
    Close;
    SQL.Clear;
    SQL.Text := 'Select * from ' + g_Table_Branch_EnterStorage +' where Types="'+ s +'"' ;
    Open;
    //Sort := '';
  end;

end;

procedure TfrmBranch.cxComboBox4PropertiesCloseUp(Sender: TObject);
begin
  inherited;
  GetSelectMechanics(Self.cxComboBox4.Text,0);
end;

procedure TfrmBranch.cxComboBox6PropertiesCloseUp(Sender: TObject);
begin
  inherited;
  GetSelectRebar(Self.cxComboBox6.Text,0);
end;

procedure TfrmBranch.cxDateEdit1PropertiesCloseUp(Sender: TObject);
var
  s : string;

begin

  s := Self.cxDateEdit1.Text;

  with Self.ADOFrame do
  begin
    Close;
    SQL.Clear;
    SQL.Text := 'Select * from ' + g_Table_Branch_EnterStorage +' where Dates=#'+ s +'#' ;
    Open;
    //Sort := '';
  end;

end;

procedure TfrmBranch.tvFlowingViewColumn1GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  inherited;
  AText := IntToStr(ARecord.Index + 1);
end;

procedure TfrmBranch.cxTextEdit11KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = 13 then
  begin
    GetSelectFinance(Self.cxTextEdit11.Text,0);
  end;
end;

procedure TfrmBranch.cxTextEdit12KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = 13 then
  begin
    GetSelectCash(Self.cxTextEdit12.Text,0);
  end;
end;

procedure TfrmBranch.cxTextEdit13KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = 13 then
  begin
    GetSelectRebar(Self.cxTextEdit13.Text,0);
  end;
end;

procedure TfrmBranch.cxTextEdit14KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = 13 then
  begin
    GetSelectQualifications(Self.cxTextEdit14.Text,0);
  end;
end;

procedure TfrmBranch.cxTextEdit3KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  s : string;

begin
  if Key = 13 then
  begin
    //回车查询
    s := Self.cxTextEdit3.Text;

    with Self.ADOFrame do
    begin
      Close;
      SQL.Clear;
      SQL.Text := 'Select * from ' + g_Table_Branch_EnterStorage +' where SupplyUnit like "%'+ s +'%"' ;
      Open;
      //Sort := '';
    end;

  end;
end;

procedure TfrmBranch.cxTextEdit6KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = 13 then
  begin
    GetSelectMaterial(Self.cxTextEdit6.Text,0);
  end;
end;

procedure TfrmBranch.cxTextEdit8KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = 13 then
  begin
    //查询
    GetSelectConcrete(Self.cxTextEdit8.Text,0);
  end;
end;

procedure TfrmBranch.cxTextEdit9KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = 13 then
  begin
    GetSelectMechanics(Self.cxTextEdit9.Text,0);
  end;
end;

procedure TfrmBranch.DBGridEh10RowDetailPanelShow(Sender: TCustomDBGridEh;
  var CanShow: Boolean);
var
  str: string;
begin
  str := getEnclosurePath(g_Expenditure,Self.DBGridEh10);

  if not DirectoryExists(str) then CreateOpenDir(Handle,str,False);

  Self.RzFileListBox7.Directory := str;
  Self.RzFileListBox7.Update;
end;


procedure TfrmBranch.DBGridEh11KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 46 then
  begin
    Self.N3.Click;
  end;
end;

procedure TfrmBranch.DBGridEh12DblClick(Sender: TObject);
begin
  Self.RzBitBtn89.Click;
end;

procedure TfrmBranch.DBGridEh12KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 46 then
  begin
    Self.RzBitBtn90.Click;
  end else
  begin
  //  ShowMessage(IntToStr(Key));
    if Key = 13 then  //回车
    begin

    end;

  end;

end;

procedure TfrmBranch.DBGridEh12RowDetailPanelShow(Sender: TCustomDBGridEh;
  var CanShow: Boolean);
var
  str: string;
begin
  str := getEnclosurePath(g_RebarManage,Self.DBGridEh12);

  if not DirectoryExists(str) then CreateOpenDir(Handle,str,False);

  Self.RzFileListBox10.Directory := str;
  Self.RzFileListBox10.Update;
end;

procedure TfrmBranch.DBGridEh13DblClick(Sender: TObject);
begin
  Self.RzBitBtn43.Click;
end;

procedure TfrmBranch.DBGridEh14KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 46 then Self.RzBitBtn105.Click;
end;

procedure TfrmBranch.DBGridEh14RowDetailPanelShow(Sender: TCustomDBGridEh;
  var CanShow: Boolean);
var
  str: string;
begin
  str := getEnclosurePath(g_Qualifications,Self.DBGridEh14);

  if not DirectoryExists(str) then CreateOpenDir(Handle,str,False);

  Self.RzFileListBox12.Directory := str;
  Self.RzFileListBox12.Update;
end;

procedure TfrmBranch.DBGridEh1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 46 then Self.RzBitBtn11.Click;
end;

procedure TfrmBranch.DBGridEh1RowDetailPanelShow(Sender: TCustomDBGridEh;
  var CanShow: Boolean);
var
  str: string;
begin
  str := getEnclosurePath(g_Expenditure,Self.DBGridEh1);

  if not DirectoryExists(str) then CreateOpenDir(Handle,str,False);

  Self.RzFileListBox2.Directory := str;

  Self.RzFileListBox2.Update;

end;

procedure TfrmBranch.DBGridEh4DblClick(Sender: TObject);
begin
  Self.RzBitBtn12.Click;
end;

procedure TfrmBranch.DBGridEh4KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 46 then Self.RzBitBtn15.Click;
end;

procedure TfrmBranch.DBGridEh4RowDetailPanelShow(Sender: TCustomDBGridEh;
  var CanShow: Boolean);
var
  str: string;
begin
  str := getEnclosurePath(g_Concrete,Self.DBGridEh4);

  if not DirectoryExists(str) then CreateOpenDir(Handle,str,False);

  Self.RzFileListBox3.Directory := str;
  Self.RzFileListBox3.Update;
end;


procedure TfrmBranch.DBGridEh6RowDetailPanelShow(Sender: TCustomDBGridEh;
  var CanShow: Boolean);
var
  str: string;
begin
  str := getEnclosurePath(Concat( g_Resources ,'\' + g_dir_BranchMechanics),Self.DBGridEh6);

  if not DirectoryExists(str) then CreateOpenDir(Handle,str,False);

  Self.RzFileListBox4.Directory := str;
  Self.RzFileListBox4.Update;

end;

procedure TfrmBranch.DBGridEh8RowDetailPanelShow(Sender: TCustomDBGridEh;
  var CanShow: Boolean);
var
  str: string;
begin
  str := getEnclosurePath(g_Expenditure,Self.DBGridEh8);

  if not DirectoryExists(str) then CreateOpenDir(Handle,str,False);

  Self.RzFileListBox6.Directory := str;
  Self.RzFileListBox6.Update;
end;

procedure TfrmBranch.DBGridEh9RowDetailPanelShow(Sender: TCustomDBGridEh;
  var CanShow: Boolean);
var
  str: string;
begin
  str := getEnclosurePath(g_Expenditure,Self.DBGridEh9);

  if not DirectoryExists(str) then CreateOpenDir(Handle,str,False);

  Self.RzFileListBox8.Directory := str;
  Self.RzFileListBox8.Update;
end;

procedure TfrmBranch.Execl1Click(Sender: TObject);
begin
  inherited;
  //导出Excel
  case Self.RzPageControl3.ActivePageIndex of
    1:
    begin
      //交易记录
      CxGridToExcel(Self.cxGrid1,'支款交易明细');
    end;
    0:
    begin
      //帐目总价
      case Self.RzPageControl4.ActivePageIndex of
        0:
        begin
          //考勤
          CxGridToExcel(Self.cxGrid4,'考勤明细');
        end;
        1:
        begin

        end;
        2:
        begin

        end;
        3:
        begin

        end;
        4:
        begin

        end;
        5:
        begin

        end;
        6:
        begin

        end;
        7:
        begin

        end;
        8:
        begin

        end;
        9:
        begin

        end;
      end;

    end;

  end;

end;

procedure TfrmBranch.FormActivate(Sender: TObject);
var
  s : string;
  List : TStringList;
  i : Integer;

begin
  Self.cxComboBox6.Clear;
  for I := 0 to 15 do
  begin
    Self.cxComboBox6.Properties.Items.Add(g_RebarList[i].Model);
  end;

  List := TStringList.Create;
  try
    GetCompany(List,6);  //机械类别
    with Self.cxComboBox4 do
    begin
      Properties.Items:=List;
      ItemIndex := 0;
    end;
  finally
    List.Free;
  end;

  Self.cxDateEdit1.Date := Date;
  Self.cxDateEdit2.Date := Date;
  Self.cxDateEdit7.Date := Date;
  Self.cxDateEdit8.Date := Date;
  Self.cxDateEdit9.Date := Date;
  Self.cxDateEdit10.Date := Date;
  Self.cxDateEdit11.Date := Date;
  Self.cxDateEdit12.Date := Date;
  Self.cxDateEdit13.Date := Date;
  Self.cxDateEdit14.Date := Date;
  Self.cxDateEdit15.Date := Date;
  Self.cxDateEdit16.Date := Date;
  Self.cxDateEdit17.Date := Date;
  Self.cxDateEdit18.Date := Date;
  Self.cxDateEdit19.Date := Date;
  Self.cxDateEdit20.Date := Date;
  Self.cxDateEdit21.Date := Date;
  Self.cxDateEdit22.Date := Date;

  Self.Date1.Date := Date;
  Self.Date2.Date := Date;
  Self.Date3.Date := Date;
  Self.Date4.Date := Date;
  Self.Date5.Date := Date;
  Self.Date6.Date := Date;

  with (Self.tvGrid3Column9.Properties as TcxComboBoxProperties ) do
  begin
    Items.Clear;
    List := TStringList.Create;
    try
      GetPayType(List,0);
      for I := 0 to List.Count-1 do Items.Add(List.Strings[i]);
    finally
      List.Free;
    end;

  end;

end;

procedure TfrmBranch.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmBranch.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
//  inherited
  Self.DBGridEh1.SumList.Active := False;
  Self.DBGridEh4.SumList.Active := False;
  Self.DBGridEh6.SumList.Active := False;
  Self.DBGridEh8.SumList.Active := False;
  Self.DBGridEh9.SumList.Active := False;
  Self.DBGridEh10.SumList.Active := False;
  Self.DBGridEh12.SumList.Active := False;

end;

procedure TfrmBranch.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 27 then Close;
end;

procedure TfrmBranch.FormResize(Sender: TObject);
begin
  Self.RzBitBtn53.Left := Self.Panel15.Width  - 30 - Self.RzBitBtn53.Width;
  Self.RzBitBtn52.Left := Self.RzBitBtn53.Left- 15 - Self.RzBitBtn52.Width;
  Self.RzBitBtn51.Left := Self.RzBitBtn52.Left- 15 - Self.RzBitBtn51.Width;

  Self.RzBitBtn59.Left := Self.Panel16.Width  - 30 - Self.RzBitBtn59.Width;
  Self.RzBitBtn58.Left := Self.RzBitBtn59.Left- 15 - Self.RzBitBtn58.Width;
  Self.RzBitBtn57.Left := Self.RzBitBtn58.Left- 15 - Self.RzBitBtn57.Width;

  Self.RzBitBtn47.Left := Self.Panel14.Width  - 30 - Self.RzBitBtn47.Width;
  Self.RzBitBtn46.Left := Self.RzBitBtn47.Left- 15 - Self.RzBitBtn46.Width;
  Self.RzBitBtn45.Left := Self.RzBitBtn46.Left- 15 - Self.RzBitBtn45.Width;

  Self.RzBitBtn35.Left := Self.Panel12.Width  - 30 - Self.RzBitBtn35.Width;
  Self.RzBitBtn34.Left := Self.RzBitBtn35.Left- 15 - Self.RzBitBtn34.Width;
  Self.RzBitBtn33.Left := Self.RzBitBtn34.Left- 15 - Self.RzBitBtn33.Width;

  Self.RzBitBtn23.Left := Self.Panel4.Width   - 30 - Self.RzBitBtn23.Width;
  Self.RzBitBtn20.Left := Self.RzBitBtn23.Left- 15 - Self.RzBitBtn20.Width;
  Self.RzBitBtn19.Left := Self.RzBitBtn20.Left- 15 - Self.RzBitBtn19.Width;

  Self.RzBitBtn21.Left := Self.Panel3.Width   - 30 - Self.RzBitBtn21.Width;
  Self.RzBitBtn14.Left := Self.RzBitBtn21.Left- 15 - Self.RzBitBtn14.Width;
  Self.RzBitBtn13.Left := Self.RzBitBtn14.Left- 15 - Self.RzBitBtn13.Width;

  Self.RzBitBtn93.Left := Self.Panel1.Width  - 30 - Self.RzBitBtn93.Width;
  Self.RzBitBtn92.Left := Self.RzBitBtn93.Left- 15 - Self.RzBitBtn92.Width;
  Self.RzBitBtn91.Left := Self.RzBitBtn92.Left- 15 - Self.RzBitBtn91.Width;

  Self.RzBitBtn108.Left := Self.Panel2.Width  - 30 - Self.RzBitBtn108.Width;
  Self.RzBitBtn107.Left := Self.RzBitBtn108.Left- 15 - Self.RzBitBtn107.Width;
  Self.RzBitBtn106.Left := Self.RzBitBtn107.Left- 15 - Self.RzBitBtn106.Width;

end;

function GetUnitList(List : TcxComboBox ; chType : Integer):Boolean;stdcall;
var
  name , Remarks , id : string;
  ListItem : TListItem;
  i : Integer;
  s : string;

begin
  if m_user.dwType = 0  then
  begin
      DM.Qry.Close;
      DM.Qry.SQL.Clear;
      DM.Qry.SQL.Text := 'Select * from unit where chType=' + IntToStr(chType);
      DM.Qry.Open;
      i:=0;

      if not DM.Qry.IsEmpty then
      begin
        List.Clear;
        while not DM.Qry.Eof do
        begin
          name := DM.Qry.FieldByName('name').AsString;
          Inc(i);
          //有所有权限
          List.Properties.Items.Add(name);
          DM.Qry.Next;
        end;
      end;

  end;

end;


procedure TfrmBranch.FormShow(Sender: TObject);
var
  i : Integer;
begin

  Self.cxDateEdit1.Date := Date;
  GetUnitList(self.cxComboBox1,7);

  g_Expenditure   := Concat( g_Resources ,'\Expenditure');
  g_Concrete      := Concat( g_Resources ,'\ConcreteManage');
  g_WithdrawMoney := Concat( g_Resources ,'\WithdrawMoney');
  g_EnterStorage  := Concat( g_Resources ,'\EnterStorage');

  g_RebarManage   := Concat( g_Resources, '\RebarManage');
  g_Qualifications:= Concat( g_Resources, '\Qualifications');

  g_PatTree := TTreeUtils.Create(Self.TreeView1,
                                      DM.ADOconn,
                                      g_Table_PactTree,
                                      'parent',
                                      'PID',
                                      'Name',
                                      m_Code,
                                      m_money ,
                                      m_dwPhoto ,
                                      m_Remarks,
                                      'Price',
                                      'num',
                                      'Input_time',
                                      'End_time',
                                      'PatType',
                                      '支款管理'
                                      );


  g_PatTree.FillTree(g_Parent,g_pacttype);

  if (g_Parent <> 0) or (g_IsSelect) then
  begin
    with Self.RzPageControl4 do
    begin
        for i := 0 to PageCount - 1 do
        begin//父ID 不等于0 进行隐藏
          if (g_Parent <> 0) or (g_ModuleIndex = 100) then
          begin
            Pages[i].TabVisible := False;//隐藏
          end;
        end;

        case g_pacttype of
          0 : ActivePage := t1;
          1 : ActivePage := t2;
          2 : ActivePage := t3;
          3 : ActivePage := t4;
          4 : ActivePage := t5;
          5 : ActivePage := t6;
          6 : ActivePage := t7;
          7 : ActivePage := t8;
          8 : ActivePage := t9; //钢筋
          9 : ActivePage := t10; //资质
        end;
        Self.Lt1.Caption := t1.Caption;
    end;

  end;

end;


procedure TfrmBranch.Image1DblClick(Sender: TObject);
var
  s : string;

begin
  s := g_ImgPath;
  if FileExists(s) then ShellExecute( 0,'Open',PChar(s),nil,nil,sw_show);
end;

procedure TfrmBranch.RzBitBtn21Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmBranch.RzBitBtn30Click(Sender: TObject);
var
  s , Code : string;
  Node: TTreeNode;
  szNode : PNodeData;
begin
  if (m_user.dwType = 0) or (m_user.dwType = 1) then
  begin
    Node := Self.TreeView1.Selected;
    if Assigned(Node) then
    begin
       szNode := PNodeData(Node.Data);
       frmMechanics := TfrmMechanics.Create(Self);
       try
         frmMechanics.Caption := s + '添加';
         frmMechanics.g_PostCode  := szNode.Code;
         frmMechanics.g_ProjectDir:= Concat( g_Resources ,'\' + g_dir_BranchMechanics);

         frmMechanics.RzPageControl1.ActivePage := frmMechanics.TabSheet1;
         frmMechanics.ShowModal;
         Self.TreeView1Click(Sender);
         
       finally
         frmMechanics.Free;
       end;

    end;

  end;

  
end;

procedure TfrmBranch.RzBitBtn31Click(Sender: TObject);
var
  s : string;
  Node: TTreeNode;
  i : Integer;

begin
  if (m_user.dwType = 0) or (m_user.dwType = 1) then
  begin

    Node := Self.TreeView1.Selected;
    if Assigned(Node) then
    begin
        s := '机械';
        if not DBGridEh6.DataSource.DataSet.IsEmpty then
        begin
           frmMechanics := TfrmMechanics.Create(Self);
           try
             frmMechanics.Caption := s + '修改';
             frmMechanics.g_ProjectDir:= Concat( g_Resources ,'\' + g_dir_BranchMechanics);

             g_code:= Self.DBGridEh6.DataSource.DataSet.FieldByName(m_Numer).AsInteger;
             frmMechanics.g_PostCode   := Self.DBGridEh6.DataSource.DataSet.FieldByName('Code').AsString; //合同编号
             frmMechanics.g_PostNumbers := Self.DBGridEh6.DataSource.DataSet.FieldByName('numbers').AsString; //编号

             frmMechanics.RzEdit3.Text  := frmMechanics.g_PostNumbers;

             frmMechanics.RzEdit7.Text := Self.DBGridEh6.DataSource.DataSet.FieldByName('Price').AsString;; //单价
             frmMechanics.RzEdit8.Text := Self.DBGridEh6.DataSource.DataSet.FieldByName('daynum').AsString;; //数值
             frmMechanics.DateTimePicker10.Date := Self.DBGridEh6.DataSource.DataSet.FieldByName('WorkDate').AsDateTime; //工作日期

             frmMechanics.DateTimePicker6.DateTime := Self.DBGridEh6.DataSource.DataSet.FieldByName('start_date').AsDateTime ;//开始日期
             frmMechanics.DateTimePicker7.DateTime := Self.DBGridEh6.DataSource.DataSet.FieldByName('start_time').AsDateTime ;//开始时间

             frmMechanics.DateTimePicker8.Date := Self.DBGridEh6.DataSource.DataSet.FieldByName('end_date').AsDateTime ;//结束日期
             frmMechanics.DateTimePicker9.Time := Self.DBGridEh6.DataSource.DataSet.FieldByName('end_time').AsDateTime ;//结束时间

             frmMechanics.rzEdit17.Text := Self.DBGridEh6.DataSource.DataSet.FieldByName('addtime').AsString; //加班时间
             frmMechanics.Edit4.Text := Self.DBGridEh6.DataSource.DataSet.FieldByName('addprice').AsString; //加班单价

             frmMechanics.RzEdit11.Text := Self.DBGridEh6.DataSource.DataSet.FieldByName('Caption').AsString;
             frmMechanics.RzEdit12.Text := Self.DBGridEh6.DataSource.DataSet.FieldByName('Content').AsString;
             frmMechanics.RzEdit6.Text  := Self.DBGridEh6.DataSource.DataSet.FieldByName('BuildCompany').AsString;

             s := Self.DBGridEh6.DataSource.DataSet.FieldByName('mechanicstype').AsString;
             i := frmMechanics.ComboBox3.Items.IndexOf(s);
             frmMechanics.ComboBox3.ItemIndex := i;

             s := Self.DBGridEh6.DataSource.DataSet.FieldByName('Units').AsString;
             i := frmMechanics.ComboBox4.Items.IndexOf(s);
             frmMechanics.ComboBox4.ItemIndex := i;

             frmMechanics.RzPageControl1.ActivePage := frmMechanics.TabSheet2;
             frmMechanics.ShowModal;
             Self.TreeView1Click(Sender);
           finally
             frmMechanics.Free;
           end;
        end;
         

    end;
  end;
end;

procedure TfrmBranch.RzBitBtn32Click(Sender: TObject);
begin
  DBGridDelete(DBGridEh6,Self.ADOMechanics,g_Table_Branch_Mechanics,Concat( g_Resources ,'\' + g_dir_BranchMechanics));
  Self.TreeView1Click(Sender);
end;

procedure TfrmBranch.RzBitBtn42Click(Sender: TObject);
var
  s , Code : string;
  Node: TTreeNode;
  szNode : PNodeData;
  szEnterStorage  : TfrmEnterStorage;
  szfrmLease  : TfrmLease;

begin
  if (m_user.dwType = 0) or (m_user.dwType = 1) then
  begin
     case Self.cxPageControl1.ActivePageIndex of
       0:
       begin
         //租赁单子
          szfrmLease := TfrmLease.Create(Self);
          try
            szfrmLease.g_ProjectDir := g_Expenditure;
            szfrmLease.g_TableName  := g_Table_Expenditure;
            szfrmLease.g_Prefix     := '';
            if Sender = RzBitBtn42 then
            begin
                Node := Self.TreeView1.Selected;
                if Assigned(Node) then
                begin
                   szNode := PNodeData(Node.Data);
                   szfrmLease.g_PostCode:= szNode.Code;
                   szfrmLease.g_PostNumbers    := DM.getDataMaxDate(g_Table_Expenditure);
                   szfrmLease.cxDateEdit1.Date := Date;
                   szfrmLease.cxDateEdit2.Date := Date;
                   szfrmLease.ShowModal;
                   Self.TreeView1Click(Sender);
                end;

            end else
            if Sender = RzBitBtn43 then
            begin
               with Self.DBGridEh8.DataSource.DataSet do
               begin
                 if not IsEmpty then
                 begin
                   szfrmLease.g_IsModify := True;
                   szfrmLease.g_PostNumbers    := FieldByName('numbers').AsString;
                   szfrmLease.cxTextEdit1.Text := FieldByName('caption').AsString;
                   szfrmLease.cxTextEdit2.Text := FieldByName('content').AsString;
                   szfrmLease.cxComboBox1.Text := FieldByName('Units').AsString;
                   szfrmLease.cxDateEdit1.Date := FieldByName('pay_time').AsDateTime;
                   szfrmLease.cxDateEdit2.Date := FieldByName('End_time').AsDateTime;
                   szfrmLease.cxCurrencyEdit1.Value := FieldByName('Price').AsCurrency;
                   szfrmLease.cxCurrencyEdit2.Value := FieldByName('num').AsCurrency;
                   szfrmLease.cxMemo1.Text  := FieldByName('remarks').AsString;
                   szfrmLease.ShowModal;
                   Self.TreeView1Click(Sender);
                 end else
                 begin
                    Application.MessageBox('先选中一条信息在进行编辑!',m_title,MB_OK + MB_ICONEXCLAMATION ) ;
                 end;

               end;
            end;

          finally
            szfrmLease.Free;
          end;

       end;
       1:
       begin
         //入库单子
         szEnterStorage := TfrmEnterStorage.Create(Self);
         try
           szEnterStorage.g_Prefix      := '';
           szEnterStorage.g_ProjectDir  := g_EnterStorage;
           szEnterStorage.g_TableName   := g_Table_Branch_EnterStorage;

           if Sender = RzBitBtn42 then
           begin
             Node := Self.TreeView1.Selected;
             if Assigned(Node) then
             begin
               szNode := PNodeData(Node.Data);
               if Assigned(szNode) then
               begin

                 szEnterStorage.cxDateEdit1.Date := Date;
                 szEnterStorage.g_IsModify    := False;
                 szEnterStorage.g_PostCode    := szNode.Code;
                 szEnterStorage.g_PostNumbers := Concat( szEnterStorage.g_Prefix , DM.getDataMaxDate(g_Table_Branch_EnterStorage) );
                 szEnterStorage.ShowModal;
                 Self.TreeView1Click(Sender);
               end;

             end;

           end else
           if Sender = RzBitBtn43 then
           begin

             with Self.DBGridEh13.DataSource.DataSet do
             begin

               if not IsEmpty then
               begin
                 szEnterStorage.g_PostNumbers    := FieldByName('numbers').AsString;
                 szEnterStorage.cxTextEdit1.Text := FieldByName('Caption').AsString;
                 szEnterStorage.cxTextEdit2.Text := FieldByName('SupplyUnit').AsString;
                 szEnterStorage.cxTextEdit3.Text := FieldByName('Delivery').AsString;
                 szEnterStorage.cxTextEdit4.Text := FieldByName('Sign').AsString;
                 szEnterStorage.cxComboBox1.Text := FieldByName('UnitTypes').AsString;
                 szEnterStorage.cxComboBox2.Text := FieldByName('Types').AsString;
                 szEnterStorage.cxComboBox3.Text := FieldByName('Unit').AsString;
                 szEnterStorage.cxSpinEdit1.Value:= FieldByName('Num').AsInteger;
                 szEnterStorage.cxDateEdit1.Date := FieldByName('Dates').AsDateTime;
                 szEnterStorage.cxMemo1.Text     := FieldByName('Remarks').AsString;
                 szEnterStorage.g_IsModify    := True;
                 szEnterStorage.ShowModal;
                 Self.TreeView1Click(Sender);
               end else
               begin
                  Application.MessageBox('先选中一条信息在进行编辑!',m_title,MB_OK + MB_ICONEXCLAMATION ) ;
               end;

             end;

           end;

         finally
           szEnterStorage.Free;
         end;

       end;

     end;

  end;
  
end;

procedure TfrmBranch.RzBitBtn44Click(Sender: TObject);
begin
  case Self.cxPageControl1.ActivePageIndex of
    0:
    begin
      DBGridDelete(DBGridEh8,Self.ADOLease,g_Table_Expenditure,g_Expenditure);
    end;
    1:
    begin
      DBGridDelete(DBGridEh13,Self.ADOFrame,g_Table_Branch_EnterStorage,g_EnterStorage);
    end;
  end;
  Self.TreeView1Click(Sender);
end;

procedure TfrmBranch.RzBitBtn48Click(Sender: TObject);
var
  s , Code : string;
  Node: TTreeNode;
  szNode : PNodeData;
  szfrmReady : TfrmReadyMoney;
  
begin
  if (m_user.dwType = 0) or (m_user.dwType = 1) then
  begin

      Node := Self.TreeView1.Selected;
      if Assigned(Node) then
      begin
         szNode := PNodeData(Node.Data);
           s := '现金';
           szfrmReady := TfrmReadyMoney.Create(Self);
           try
             szfrmReady.Caption := s + '添加';
             szfrmReady.g_ProjectDir := g_Expenditure;

             szfrmReady.g_PostCode   := szNode.Code;
             szfrmReady.RzPageControl1.ActivePage := szfrmReady.TabSheet1;
             szfrmReady.ShowModal;
             Self.TreeView1Click(Sender);
             
           finally
             szfrmReady.Free;
           end;
      end;
  end;

end;

procedure TfrmBranch.RzBitBtn49Click(Sender: TObject);
var
  s , Code : string;
  Node: TTreeNode;
  szNode : PNodeData;
  szfrmReady : TfrmReadyMoney;

begin
  if (m_user.dwType = 0) or (m_user.dwType = 1) then
  begin
    Node := Self.TreeView1.Selected;
    if Assigned(Node) then
    begin
       szNode := PNodeData(Node.Data);
       s := '现金';
       if not getEditData(self.DBGridEh9)  then Exit;

       szfrmReady := TfrmReadyMoney.Create(Self);
       try
         szfrmReady.Caption := s + '修改';
         szfrmReady.g_ProjectDir := g_Expenditure;
         szfrmReady.RzPageControl1.ActivePage := szfrmReady.TabSheet2;
         szfrmReady.ShowModal;
       finally
         szfrmReady.Free;
       end;
    end;
  end;

end;

procedure TfrmBranch.RzBitBtn50Click(Sender: TObject);
begin
  DBGridDelete(DBGridEh9,Self.ADOCash,g_Table_Expenditure,g_Expenditure);
  Self.TreeView1Click(Sender);
end;

procedure TfrmBranch.RzBitBtn54Click(Sender: TObject);
var
  s , Code : string;
  Node: TTreeNode;
  szNode : PNodeData;
  Child : TfrmTurnover;
begin
  if (m_user.dwType = 0) or (m_user.dwType = 1) then
  begin
     Child := TfrmTurnover.Create(Self);
     try
        Child.g_ProjectDir := g_Expenditure;
        Child.g_TableName  := g_Table_Expenditure;

        if Sender = RzBitBtn54 then
        begin
          Child.g_IsModify := False;
          Node := Self.TreeView1.Selected;
          if Assigned(Node) then
          begin
             szNode := PNodeData(Node.Data);
             Child.g_PostCode := szNode.Code;
             Child.cxDateEdit1.Date := Date;
             Child.cxDateEdit2.Date := Date;
             Child.g_PostNumbers := DM.getDataMaxDate(g_Table_Expenditure);
             Child.ShowModal;
             Self.TreeView1Click(Sender);

          end;

        end else
        if Sender = RzBitBtn55 then
        begin
          with Self.DBGridEh10.DataSource.DataSet do
          begin
            if not IsEmpty then
            begin
              Child.g_IsModify := True;

              Child.g_PostNumbers := FieldByName('numbers').AsString;
              Child.cxTextEdit1.Text := FieldByName('caption').AsString;
              Child.cxTextEdit2.Text := FieldByName('content').AsString;
              Child.cxComboBox1.Text := FieldByName('Units').AsString;
              Child.cxDateEdit1.Date := FieldByName('pay_time').AsDateTime;
              Child.cxDateEdit2.Date := FieldByName('End_time').AsDateTime;
              Child.cxCurrencyEdit1.Value := FieldByName('Price').AsCurrency;
              Child.cxCurrencyEdit2.Value := FieldByName('num').AsCurrency;
              Child.cxMemo1.Text  := FieldByName('remarks').AsString;

              Child.ShowModal;
              Self.TreeView1Click(Sender);
            end else
            begin
              ShowMessage('未选中条件');
            end;
          end;
        end;
     finally
       Child.Free;
     end;

  end;

end;

procedure TfrmBranch.RzBitBtn56Click(Sender: TObject);
begin
  DBGridDelete(DBGridEh10,Self.ADOFinance,g_Table_Expenditure,g_Expenditure);
  Self.TreeView1Click(Sender);
end;

procedure TfrmBranch.RzBitBtn61Click(Sender: TObject);
begin
  {
  SearchFiles(Self.RzEdit1.Text,
              g_WorkManageDir,
              Self.DBGridEh5,
              self.RzFileListBox1
              );
  }
end;

procedure TfrmBranch.RzBitBtn62Click(Sender: TObject);
var
  str:string;
begin
//  str := getEnclosurePath(g_WorkManageDir,Self.DBGridEh5);
//  CreateOpenDir(Self.Handle,str,True);
end;

procedure TfrmBranch.RzBitBtn63Click(Sender: TObject);
var
  str : string;
begin
  {
  str := getEnclosurePath(g_WorkManageDir,Self.DBGridEh5);
  if str <> Self.RzFileListBox1.Directory then
  begin
    Self.RzFileListBox1.UpOneLevel;
  end;
  }
end;

procedure TfrmBranch.RzBitBtn64Click(Sender: TObject);
begin

  SearchFiles(Self.RzEdit7.Text,
              g_Expenditure,
              Self.DBGridEh1,
              self.RzFileListBox2
              );
end;

procedure TfrmBranch.RzBitBtn65Click(Sender: TObject);
begin
  CreateOpenDir(Self.Handle, getEnclosurePath(g_Expenditure,Self.DBGridEh1) ,True);
end;

procedure TfrmBranch.RzBitBtn66Click(Sender: TObject);
var
  str : string;
begin

  str := getEnclosurePath(g_Expenditure,Self.DBGridEh1);
  if str <> Self.RzFileListBox2.Directory then
  begin
    Self.RzFileListBox2.UpOneLevel;
  end;
  
end;

procedure TfrmBranch.RzBitBtn67Click(Sender: TObject);
begin
  SearchFiles(Self.RzEdit8.Text,
              g_Concrete,
              Self.DBGridEh4,
              self.RzFileListBox3
              );
end;

procedure TfrmBranch.RzBitBtn68Click(Sender: TObject);
begin
  CreateOpenDir(Self.Handle, getEnclosurePath(g_Concrete,Self.DBGridEh4) ,True);
end;

procedure TfrmBranch.RzBitBtn69Click(Sender: TObject);
var
  str : string;
begin
  str := getEnclosurePath(g_Concrete,Self.DBGridEh4);
  if str <> Self.RzFileListBox3.Directory then
  begin
    Self.RzFileListBox3.UpOneLevel;
  end;
end;

procedure TfrmBranch.RzBitBtn70Click(Sender: TObject);
begin
  SearchFiles(Self.RzEdit9.Text,
              Concat( g_Resources ,'\' + g_dir_BranchMechanics),
              Self.DBGridEh6,
              self.RzFileListBox4
              );
end;

procedure TfrmBranch.RzBitBtn71Click(Sender: TObject);
begin
  CreateOpenDir(Self.Handle, getEnclosurePath(Concat( g_Resources ,'\' + g_dir_BranchMechanics),Self.DBGridEh6) ,True);
end;

procedure TfrmBranch.RzBitBtn72Click(Sender: TObject);
var
  str : string;
begin

  str := getEnclosurePath(Concat( g_Resources ,'\' + g_dir_BranchMechanics),Self.DBGridEh6);
  if str <> Self.RzFileListBox4.Directory then
  begin
    Self.RzFileListBox4.UpOneLevel;
  end;

end;

procedure TfrmBranch.RzBitBtn76Click(Sender: TObject);
begin

  SearchFiles(Self.RzEdit13.Text,
              g_Expenditure,
              Self.DBGridEh8,
              self.RzFileListBox6
              );
end;

procedure TfrmBranch.RzBitBtn77Click(Sender: TObject);
begin
  CreateOpenDir(Self.Handle, getEnclosurePath(g_Expenditure,Self.DBGridEh8) ,True);
end;

procedure TfrmBranch.RzBitBtn78Click(Sender: TObject);
var
  str : string;
begin
  str := getEnclosurePath(g_Expenditure,Self.DBGridEh8);
  if str <> Self.RzFileListBox6.Directory then
  begin
    Self.RzFileListBox6.UpOneLevel;
  end;
end;

procedure TfrmBranch.RzBitBtn79Click(Sender: TObject);
begin
  SearchFiles(Self.RzEdit14.Text,
              g_Expenditure,
              Self.DBGridEh10,
              self.RzFileListBox7
              );
end;

procedure TfrmBranch.RzBitBtn80Click(Sender: TObject);
begin
  CreateOpenDir(Self.Handle, getEnclosurePath(g_Expenditure,Self.DBGridEh10) ,True);
end;

procedure TfrmBranch.RzBitBtn81Click(Sender: TObject);
var
  str : string;
begin
  str := getEnclosurePath(g_Expenditure,Self.DBGridEh10);
  if str <> Self.RzFileListBox7.Directory then
  begin
    Self.RzFileListBox7.UpOneLevel;
  end;
end;

procedure TfrmBranch.RzBitBtn82Click(Sender: TObject);
begin
  SearchFiles(Self.RzEdit15.Text,
              g_Expenditure,
              Self.DBGridEh9,
              self.RzFileListBox8
              );
end;

procedure TfrmBranch.RzBitBtn83Click(Sender: TObject);
begin
  CreateOpenDir(Self.Handle, getEnclosurePath(g_Expenditure,Self.DBGridEh9) ,True);
end;

procedure TfrmBranch.RzBitBtn84Click(Sender: TObject);
var
  str : string;
begin
  str := getEnclosurePath(g_Expenditure,Self.DBGridEh9);
  if str <> Self.RzFileListBox8.Directory then
  begin
    Self.RzFileListBox8.UpOneLevel;
  end;
end;

procedure TfrmBranch.RzBitBtn85Click(Sender: TObject);
begin
  {
  SearchFiles(Self.RzEdit16.Text,
              g_WithdrawMoney,
              Self.DBGridEh2,
              self.RzFileListBox9
              );
 }
end;

procedure TfrmBranch.RzBitBtn86Click(Sender: TObject);
begin
//  CreateOpenDir(Self.Handle, getEnclosurePath(g_WithdrawMoney,Self.DBGridEh2) ,True);
end;

procedure TfrmBranch.RzBitBtn87Click(Sender: TObject);
//var
//  str : string;
begin
  {
  str := getEnclosurePath(g_WithdrawMoney,Self.DBGridEh2);
  if str <> Self.RzFileListBox9.Directory then
  begin
    Self.RzFileListBox9.UpOneLevel;
  end;
  }
end;

procedure TfrmBranch.RzBitBtn88Click(Sender: TObject);
var
  Child : TfrmRebarPost;
  s , Code : string;
  Node: TTreeNode;
  szNode : PNodeData;
  f : Double;

begin
  if (m_user.dwType = 0) or (m_user.dwType = 1) then
  begin
    Node := Self.TreeView1.Selected;
    if Assigned(Node) then
    begin
      szNode := PNodeData(Node.Data);

      Child := TfrmRebarPost.Create(Application);
      try
        Child.g_ProjectDir:= g_RebarManage;
        Child.g_TableName := g_Table_Branch_RebarManage;
        Child.g_PostCode  := szNode.Code;
        
        if Sender = RzBitBtn88 then
        begin
           Child.g_PostNumbers := DM.getDataMaxDate(g_Table_Branch_RebarManage);
           Child.g_IsModify := False;
           Child.ShowModal;
           Self.TreeView1Click(Sender);
        end else
        begin
           Child.g_IsModify := True;
           with Self.DBGridEh12.DataSource.DataSet do
           begin
             if not IsEmpty then
             begin
               Child.g_PostNumbers         := FieldByName('Numbers').AsString;
               Child.cxTextEdit1.Text      := FieldByName('chCaption').AsString;
               Child.cxTextEdit2.Text      := FieldByName('chForGoods').AsString;
               Child.cxTextEdit3.Text      := FieldByName('chInGoodsPeople').AsString;
               Child.cxCurrencyEdit7.Value := FieldByName('chPieceCount').AsInteger;
               Child.RzComboBox1.Text      := FieldByName('chDiameter').AsString;
               f := FieldByName('chTheoryWeight').AsFloat;
               Child.cxCurrencyEdit1.Text  := Formatfloat('0.####',f);
               Child.cxCurrencyEdit2.Value := FieldByName('chMeter').AsInteger;
               Child.cxCurrencyEdit3.Value := FieldByName('chRootCount').AsInteger;
               Child.cxCurrencyEdit4.Value := FieldByName('chUnitPrice').AsCurrency;
               Child.cxCurrencyEdit5.Value := FieldByName('chTotal').AsCurrency;
               Child.DateTimePicker1.Date  := FieldByName('chDate').AsDateTime;
               f := FieldByName('chWeight').AsFloat;
               Child.cxCurrencyEdit6.Value := RoundTo(f,-3);// Formatfloat('0.00',f);
               Child.cxMemo1.Text := FieldByName('chRemarks').AsString;
               Child.ShowModal;
               Self.TreeView1Click(Sender);
             end;

           end;


        end;

      finally
        Child.Free;
      end;

    end;

  end;

end;

procedure TfrmBranch.RzBitBtn8Click(Sender: TObject);
var
  Code : string;
  Node: TTreeNode;
  szNode : PNodeData;
  Child  : TfrmAddConcrete;
begin
  if (m_user.dwType = 0) or (m_user.dwType = 1) then
  begin
    Node := Self.TreeView1.Selected;
    if Assigned(Node) then
    begin
      szNode := PNodeData(Node.Data);
      Child := TfrmAddConcrete.Create(Self);
      try
        Child.g_Handte := Handle;
        Child.g_PostCode := szNode.Code;
        Child.g_TableName:= g_Table_Concrete;
        if Sender = RzBitBtn8 then
        begin
          Child.cxTimeEdit1.Time := Time;
          Child.cxTimeEdit2.Time := Time;
          Child.cxDateEdit1.Date := Date;
          Child.g_PostNumbers := DM.getDataMaxDate(g_Table_Concrete);
          Child.g_IsModify := False;
          
          Child.ShowModal;
          Self.TreeView1Click(Sender);
        end
        else
        if Sender = RzBitBtn12 then
        begin

          if not Self.DBGridEh4.DataSource.DataSet.IsEmpty then
          begin
            Child.g_IsModify := True;
            Child.g_PostNumbers         := Self.DBGridEh4.DataSource.DataSet.FieldByName('Numbers').Asstring;
            Child.cxTextEdit1.Text      := Self.DBGridEh4.DataSource.DataSet.FieldByName('chSupplyUnit').AsString;
            Child.cxTextEdit2.Text      := Self.DBGridEh4.DataSource.DataSet.FieldByName('chProjectName').AsString;
            Child.cxComboBox1.Text      := Self.DBGridEh4.DataSource.DataSet.FieldByName('Deliver').AsString;//输送方式
            Child.cxCurrencyEdit1.Value := Self.DBGridEh4.DataSource.DataSet.FieldByName('Deliver_UnitPrice').AsCurrency; //输送单价
            Child.cxComboBox2.Text      := Self.DBGridEh4.DataSource.DataSet.FieldByName('Concrete').AsString;//砼强度
            Child.cxCurrencyEdit2.Value := Self.DBGridEh4.DataSource.DataSet.FieldByName('Concrete_UnitPrice').AsCurrency;
            Child.cxComboBox3.Text      := Self.DBGridEh4.DataSource.DataSet.FieldByName('Permeability').AsString; //抗渗等级
            Child.cxCurrencyEdit3.Value := Self.DBGridEh4.DataSource.DataSet.FieldByName('Permeability_Unitprice').AsCurrency;
            Child.cxTextEdit3.Text      := Self.DBGridEh4.DataSource.DataSet.FieldByName('Technical').AsString;
            Child.cxCurrencyEdit6.Value := Self.DBGridEh4.DataSource.DataSet.FieldByName('Stere').AsFloat;
            Child.cxTextEdit4.Text      := Self.DBGridEh4.DataSource.DataSet.FieldByName('chPosition').AsString;
            Child.cxCurrencyEdit7.Value := Self.DBGridEh4.DataSource.DataSet.FieldByName('UnitPrice').AsCurrency;
            Child.cxTextEdit5.Text      := Self.DBGridEh4.DataSource.DataSet.FieldByName('WagonNumber').AsString;
            Child.cxTextEdit6.Text      := Self.DBGridEh4.DataSource.DataSet.FieldByName('Sign').AsString;
            Child.cxMemo1.Text          := Self.DBGridEh4.DataSource.DataSet.FieldByName('Remarks').AsString;
            Child.cxDateEdit1.Date      := Self.DBGridEh4.DataSource.DataSet.FieldByName('chDate').AsDateTime;
            Child.cxTimeEdit1.Time      := Self.DBGridEh4.DataSource.DataSet.FieldByName('PouringTime').AsDateTime;
            Child.cxTimeEdit2.Time      := Self.DBGridEh4.DataSource.DataSet.FieldByName('leaveTime').AsDateTime;
            Child.cxComboBox4.Text      := Self.DBGridEh4.DataSource.DataSet.FieldByName('chAttach').AsString; //附加
            Child.cxCurrencyEdit4.Value := Self.DBGridEh4.DataSource.DataSet.FieldByName('chAttach_UnitPrice').AsCurrency;
            Child.cxComboBox5.Text      := Self.DBGridEh4.DataSource.DataSet.FieldByName('chAdditive').AsString;//外加剂
            Child.cxCurrencyEdit5.Value := Self.DBGridEh4.DataSource.DataSet.FieldByName('chAdditive_UnitPrice').AsCurrency;
            Child.ShowModal;
            Self.TreeView1Click(Sender);

          end else
          begin

          end;  

        end;

      finally
        Child.Free;
      end;

    end
    else
      Application.MessageBox('请选择要添加的合同!!',m_title,MB_OK + MB_ICONQUESTION )

  end;

end;

procedure TfrmBranch.RzBitBtn90Click(Sender: TObject);
begin
  DBGridDelete(DBGridEh12,Self.ADORebar,g_Table_Branch_RebarManage,g_RebarManage);
  Self.TreeView1Click(Sender);
end;

procedure TfrmBranch.RzBitBtn91Click(Sender: TObject);
begin
  DBGridExport(DBGridEh12);
end;

procedure TfrmBranch.RzBitBtn94Click(Sender: TObject);
begin
  SearchFiles(Self.RzEdit17.Text,
              g_RebarManage,
              Self.DBGridEh12,
              self.RzFileListBox10
              );
end;

procedure TfrmBranch.RzBitBtn95Click(Sender: TObject);
begin
  CreateOpenDir(Self.Handle, getEnclosurePath(g_RebarManage,Self.DBGridEh12) ,True);
end;

procedure TfrmBranch.RzBitBtn96Click(Sender: TObject);
var
  str : string;
begin
  str := getEnclosurePath(g_RebarManage,Self.DBGridEh12);
  if str <> Self.RzFileListBox10.Directory then
  begin
    Self.RzFileListBox10.UpOneLevel;
  end;
end;

procedure TfrmBranch.RzBitBtn9Click(Sender: TObject);
var
  s , Code : string;
  Node: TTreeNode;
  szNode : PNodeData;
  
begin
  if (m_user.dwType = 0) or (m_user.dwType = 1) then
  begin
      Node := Self.TreeView1.Selected;
      if Assigned(Node) then
      begin
         szNode := PNodeData(Node.Data);
         if Sender = Self.RzBitBtn9 then
         begin
           s := '材料';
           frmBranchlnput := TfrmBranchlnput.Create(Self);
           try
             frmBranchlnput.Caption      := s + '添加';
             frmBranchlnput.g_Handte     := Handle;
             frmBranchlnput.g_PostCode   := szNode.Code;
             frmBranchlnput.g_ProjectDir := g_Expenditure;
             
             frmBranchlnput.RzPageControl1.ActivePage := frmBranchlnput.TabSheet1;
             frmBranchlnput.ShowModal;
             Self.TreeView1Click(Sender);

           finally
             frmBranchlnput.Free;
           end;
           
         end;
      end
      else
      begin
        Application.MessageBox('请选择要添加的合同!!',m_title,MB_OK + MB_ICONQUESTION )
      end;

  end;

end;

procedure TfrmBranch.RzPageControl3TabClick(Sender: TObject);
begin
  inherited;
  Self.TreeView1Click(Sender);
end;

procedure TfrmBranch.RzPageControl4TabClick(Sender: TObject);
var
  Node : TTreeNode;
  szCode : string;
  szCodeList : string;
  szPactType : Byte;
begin
  inherited;
  g_pacttype := Self.RzPageControl4.ActivePageIndex;
  Self.TreeView1Click(Sender);
  case self.RzPageControl4.TabIndex of
    0:
    begin
    //  g_pacttype := 1; //人工
    //  Self.TreeView1Click(Sender);
    end;
    1:
    begin
   //   g_pacttype := 2;  //材料
    //  Self.TreeView1Click(Sender);
    end;
    2:
    begin
      //  g_pacttype := 0; //混凝土
    //  szCode := Self.RzEdit7.Text;
      {
      if Length(szCode) <> 0 then
      begin
        Node := Self.TreeView1.Selected;
        if Assigned(Node) then
        begin
          treeCldnode(Node,szCodeList);
          ShowConcrete(Node.Level,szCode);
          UpdateMoney(szCode);
        end;

      end;
      g_pacttype := 0;
      Exit;
      }
    end;
    3:
    begin
    //  g_pacttype := 3; //机械　
    //  Self.TreeView1Click(Sender);
    end;
    4:
    begin
    //  g_pacttype := 4; //工程量
    //  Self.TreeView1Click(Sender);
    end;
    5:
    begin
    //  g_pacttype := 5; //租赁
    //  Self.TreeView1Click(Sender);
    end;
    6:
    begin
    //  g_pacttype := 6; //其他
    //  Self.TreeView1Click(Sender);
    end;
    7:
    begin
    //  g_pacttype := 8; //现金
    //  Self.TreeView1Click(Sender);
    end;
    8:
    begin
    //  g_pacttype := 9;//钢筋
    end;
  end;

//  if szPactType = g_pacttype then
//  begin
  //  Self.TreeView1Click(Sender);
//  end;
end;

procedure TfrmBranch.RzToolButton12Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmBranch.RzToolButton15Click(Sender: TObject);
begin
  inherited;
  CreateOpenDir(Self.Handle, Concat( g_Resources ,'\AttendanceSheet' ) , True);
end;

procedure TfrmBranch.RefreshDetailTable();
begin
  with Self.QryGrid4 do
  begin
    Close;
    SQL.Clear;
    SQL.Text := 'Select * from ' + g_Table_DetailTable;
    Open;
    if RecordCount <> 0 then SetcxGrid(Self.tvView,0,g_dir_DetailTable);

  end;

end;

procedure TfrmBranch.RzToolButton16Click(Sender: TObject);
var
  Child   : TfrmPayDetailed;
  szName  : string;
  szMoney : Currency;

begin
  inherited;
  with Self.tvGrid3.DataController.DataSource.DataSet do
  begin
      if not IsEmpty then
      begin
          Child := TfrmPayDetailed.Create(Self);
          try
            Self.tvGrid3.Controller.FocusedRow.Collapse(True);
            szName := Self.tvGrid3Column11.DataBinding.FieldName;

            szMoney:= FieldByName(szName).AsCurrency;
            Child.g_Money    := szMoney;
            Child.g_formName := Self.Name;

            Child.g_Detailed := g_dir_DetailTable;

            szName := Self.tvGrid3Column2.DataBinding.FieldName;
            Child.g_PactCode   := g_PactCode;
            Child.g_TableName  := g_Table_DetailTable;

            Child.g_parentCode := FieldByName(szName).AsString;
            Child.ShowModal;
            SetcxGrid(Self.tvView,0,g_dir_DetailTable);
            Self.RefreshDetailTable;
            Self.tvGrid3.Controller.FocusedRow.Expand(True);
          finally
            Child.Free;
          end;

      end;
  end;


end;

procedure TfrmBranch.RzToolButton19Click(Sender: TObject);
begin
  inherited;
  CreateOpenDir(Self.Handle, Concat( g_Resources ,'\PactAttendanceSheet' ) , True);
end;

procedure TfrmBranch.RzToolButton1Click(Sender: TObject);
var
  Node : TTreeNode;
  Child : TfrmPostPact;
  szCodeList  : string;
  szTableList : array[0..3] of TTableList;
  szNode : PNodeData;

begin

{
  if m_user.dwType = 0 then
  begin
    Node := Self.TreeView1.Selected;
    if Assigned(Node) then
    begin

      if Node.Level > 0 then
      begin
        szNode := PNodeData(Node.Data);

        Child := TfrmPostPact.Create(Self);
        try
          Child.g_Prefix     := 'ZK-';
          Child.g_Suffix     := '-GC';
          Child.g_PatTree    := g_PatTree;
          Child.g_PostCode   := szNode.Code;
          Child.g_TableName  := g_Table_PactTree;

          Child.g_ProjectDir := ''; //附件目录

          if Sender = Self.RzToolButton1 then
          begin //新增
            Child.dtp1.Date  := Now;
            Child.g_IsModify := False;
            Child.ShowModal;
          end else
          if Sender = Self.RzToolButton2 then
          begin //编辑
            if Node.Level > 1 then
            begin
              Child.dtp1.Date    := szNode.Input_time;
              Child.RzEdit1.Text := szNode.Caption;
              Child.RzMemo1.Text := szNode.remarks;
              Child.g_IsModify   := True;
              Child.ShowModal;
            end else
            begin
              Application.MessageBox('顶级节点禁止编辑!',m_title,MB_OK + MB_ICONINFORMATION ) ;
            end;

          end else
          if Sender = Self.RzToolButton3 then
          begin //删除
             if Node.Level <= 1 then
             begin
               Application.MessageBox('顶级帐目禁止删除!',m_title,MB_OK + MB_ICONEXCLAMATION ) ;
             end else
             begin
                if MessageBox(handle, PChar('你确认要删除"' + szNode.Caption + '"'),
                         '提示', MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2) = IDYES then
                begin

                  szTableList[0].ATableName := g_Table_BranchMoney;
                  szTableList[0].ADirectory := g_dir_BranchMoney;

                  szTableList[1].ATableName := g_Table_Pactmoney; //删除结算表
                  szTableList[1].ADirectory := '';

                  case g_pacttype of
                    0:
                    begin //混凝土
                      szTableList[2].ATableName := g_Table_Concrete;
                      szTableList[2].ADirectory := 'ConcreteManage';
                    end;
                    1:
                    begin  //人工
                      szTableList[2].ATableName := g_Table_Branch_Work;// 'sk_work';
                      szTableList[2].ADirectory := g_Branch_dir_Work;// 'WorkManage';
                    end;
                    2:
                    begin //材料
                      szTableList[2].ATableName := g_Table_Expenditure;
                      szTableList[2].ADirectory := g_Table_Expenditure;
                    end;
                    3:
                    begin  //机械
                      szTableList[2].ATableName := g_Table_Branch_Mechanics;// 'sk_Mechanics';
                      szTableList[2].ADirectory := g_dir_BranchMechanics;// 'Mechanics';
                    end;
                    4:
                    begin//工程量
                      szTableList[2].ATableName := g_Table_Branch_EngineeringQuantity;
                      szTableList[2].ADirectory := g_dir_EngineeringQuantity;

                      szTableList[3].ATableName := g_Table_Branch_EngineeringDetailed;
                      szTableList[3].ADirectory := '';

                    end;
                    5:
                    begin //租赁
                      szTableList[2].ATableName := g_Table_Expenditure;
                      szTableList[2].ADirectory := g_Table_Expenditure;
                    end;
                    6:
                    begin  //周转款
                      szTableList[2].ATableName := g_Table_Expenditure;
                      szTableList[2].ADirectory := g_Table_Expenditure;
                    end;
                    8:
                    begin  //现金
                      szTableList[2].ATableName := g_Table_Expenditure;
                      szTableList[2].ADirectory := g_Table_Expenditure;
                    end;
                    9:
                    begin
                      szTableList[2].ATableName := g_Table_Branch_RebarManage;
                      szTableList[2].ADirectory := g_RebarManage;
                    end;
                  end;

                  treeCldnode(Node,szCodeList);
                  DeleteTableFile(szTableList,szCodeList);

                  DM.InDeleteData(g_TableName,szCodeList);

                  g_PatTree.DeleteTree(Node);

                end;

             end;

          end;
        finally
          Child.Free;
        end;

      end else begin
        Application.MessageBox('顶级节点禁止添加/编辑!',m_title,MB_OK + MB_ICONEXCLAMATION ) ;
      end;

    end else begin
      Application.MessageBox('请选择合同在点击添加/编辑/删除!',m_title,MB_OK + MB_ICONERROR )
    end;

  end;
}
end;

procedure TfrmBranch.RzToolButton20Click(Sender: TObject);
begin
  inherited;
  CreateOpenDir(Self.Handle, Concat( g_Resources ,'\PactSummaryD' ) , True);
end;

procedure TfrmBranch.RzToolButton21Click(Sender: TObject);
var
  Child : TfrmIsViewGrid;
begin
  inherited;
  {
  Child := TfrmIsViewGrid.Create(Application);
  try
    Child.g_fromName := Self.Name + g_Branch_dir_Work;
    Child.ShowModal;
    SetcxGrid(Self.tvWorkView,0,g_Branch_dir_Work);
  finally
    Child.Free;
  end;
  }
end;

procedure TfrmBranch.RzToolButton22Click(Sender: TObject);
var
  t0 , t1 : TDateTime;
  s , szColName : string;
begin
  inherited;
  {
  t0 := Self.Date1.Date;
  t1 := Self.Date2.Date;
  with Self.ADOWork do
  begin
    Close;
    SQL.Clear;
    szColName := Self.WorkCol3.DataBinding.FieldName;
    SQL.Text := 'Select * from ' + g_Table_Branch_Work + ' where ' + g_CodeList + ' and ' + szColName + ' between :t1 and :t2';;
    Parameters.ParamByName('t1').Value := t0;
    Parameters.ParamByName('t2').Value := t1;
    Open;
  end;

  }

end;

procedure TfrmBranch.RzToolButton23Click(Sender: TObject);
var
  t0 , t1 : TDateTime;
  szColName : string;
begin
  inherited;
//支款交易明细查询
  t0 := Self.Date3.Date;
  t1 := Self.Date4.Date;
  with Self.qryGrid3 do
  begin
    Close;
    SQL.Clear;
    szColName := Self.tvGrid3Column3.DataBinding.FieldName;
    SQL.Text := 'Select * from ' + g_Table_BranchMoney + ' where ' + g_CodeList + ' and ' + szColName + ' between :t1 and :t2';
    Parameters.ParamByName('t1').Value := t0;
    Parameters.ParamByName('t2').Value := t1;
    Open;
  end;
  Self.RefreshDetailTable;
  GetSumBranch(g_CodeList);
end;

procedure TfrmBranch.RzToolButton4Click(Sender: TObject);
var
  Node: TTreeNode;
  szNode : PNodeData;
  Child : TfrmInspectPost;
  szColName : string;
  
begin
  //考勤添加
  inherited;
  {
  Node := Self.TreeView1.Selected;
  if Assigned(Node) then
  begin
     szNode := PNodeData(Node.Data);
     Child := TfrmInspectPost.Create(Self);
     try
       Child.g_PostCode   := szNode.Code;
       Child.g_ProjectDir := g_Branch_dir_Work;
       Child.g_TableName  := g_Table_Branch_Work;
       Child.ds.DataSet   := Self.ADOWork;

       if Sender = Self.RzToolButton4 then
       begin
         if Node.Level > 1 then
         begin
           with Self.ADOWork do
           begin
             if State <> dsInactive then
             begin
               if (State <> dsInsert) or (State <> dsEdit) then
               begin
                 Append;
                 FieldByName('Code').Value := g_PactCode;
                 szColName := Self.WorkCol2.DataBinding.FieldName;
                 Child.g_ColNumbers := szColName;

                 FieldByName(szColName).Value := DM.getDataMaxDate(g_Table_Branch_Work);
                 
                 szColName := Child.cxDBDateEdit2.DataBinding.DataField;
                 FieldByName(szColName).Value := Now;
                 szColName := Child.cxDBDateEdit3.DataBinding.DataField;
                 FieldByName(szColName).Value := Now;
                 szColName := Child.cxDBDateEdit1.DataBinding.DataField;
                 FieldByName(szColName).Value := Date;
                 szColName := Child.cxDBDateEdit4.DataBinding.DataField;
                 FieldByName(szColName).Value := Date;
                 szColName := Child.cxDBDateEdit5.DataBinding.DataField;
                 FieldByName(szColName).Value := Date;
               end;
               Child.ShowModal;

               if (State = dsInsert) or (State = dsEdit) then
               begin
                 Refresh;
               end;
             end;

           end;

         end else
         begin
           Application.MessageBox('该界面以限制，请新建一个工程项目，在进行操作！!!',m_title,MB_OK + MB_ICONQUESTION )
         end;

       end else
       if Sender = Self.RzToolButton5 then
       begin
         Child.ShowModal;
         Self.TreeView1Click(Sender);
       end else
       if Sender = Self.RzToolButton6 then
       begin
         cxGridDeleteData(Self.tvWorkView,g_Branch_dir_Work,g_Table_Branch_Work);
         Self.TreeView1Click(Sender);
       end;
     finally
       Child.Free;
     end;

  end;
  }
end;

procedure TfrmBranch.RzToolButton5Click(Sender: TObject);
var
  Node: TTreeNode;
  i : Integer;

begin
  inherited;
  {
  if (m_user.dwType = 0) or (m_user.dwType = 1) then
  begin

    Node := Self.TreeView1.Selected;
    if Assigned(Node) then
    begin
       g_tabIndex := 1;
       if not DBGridEh5.DataSource.DataSet.IsEmpty then
       begin
           frmInspectPost := TfrmInspectPost.Create(Self);
           try
             frmInspectPost.Caption := '修改';
             frmInspectPost.RzPageControl1.ActivePage := frmInspectPost.TabSheet2;


             g_code:= Self.DBGridEh5.DataSource.DataSet.FieldByName(m_Numer).AsInteger;
             frmInspectPost.g_ProjectDir := g_WorkManageDir; //附件目录
             frmInspectPost.g_PostCode   := Self.DBGridEh5.DataSource.DataSet.FieldByName('Code').AsString; //编号
             frmInspectPost.RzEdit3.Text := Self.DBGridEh5.DataSource.DataSet.FieldByName('numbers').AsString; //编号
             frmInspectPost.g_PostNumbers:= Self.DBGridEh5.DataSource.DataSet.FieldByName('numbers').AsString; //编号

             frmInspectPost.RzEdit7.Text := Self.DBGridEh5.DataSource.DataSet.FieldByName('Price').AsString;; //单价
             frmInspectPost.RzEdit8.Text := Self.DBGridEh5.DataSource.DataSet.FieldByName('daynum').AsString;; //数值
             frmInspectPost.DateTimePicker10.Date := Self.DBGridEh5.DataSource.DataSet.FieldByName('WorkDate').AsDateTime; //工作日期
             frmInspectPost.DateTimePicker6.Date  := Self.DBGridEh5.DataSource.DataSet.FieldByName('start_date').AsDateTime ;//开始日期
             frmInspectPost.DateTimePicker7.Time  := Self.DBGridEh5.DataSource.DataSet.FieldByName('start_time').AsDateTime ;//开始时间
             frmInspectPost.DateTimePicker8.Date  := Self.DBGridEh5.DataSource.DataSet.FieldByName('end_date').AsDateTime ;//结束日期
             frmInspectPost.DateTimePicker9.Time  := Self.DBGridEh5.DataSource.DataSet.FieldByName('end_time').AsDateTime ;//结束时间
             frmInspectPost.rzEdit17.Text := Self.DBGridEh5.DataSource.DataSet.FieldByName('addtime').AsString; //加班时间
             frmInspectPost.Edit4.Text    := Self.DBGridEh5.DataSource.DataSet.FieldByName('addprice').AsString; //加班单价
             frmInspectPost.RzEdit11.Text := Self.DBGridEh5.DataSource.DataSet.FieldByName('Caption').AsString;
             frmInspectPost.RzEdit12.Text := Self.DBGridEh5.DataSource.DataSet.FieldByName('Content').AsString;

             frmInspectPost.ShowModal;
             Self.TreeView1Click(Sender);

           finally
             frmInspectPost.Free;
           end;
       end;
    end;
  end;
  }
end;

procedure TfrmBranch.RzToolButton75Click(Sender: TObject);
var
  szName : string;
  szIndex: Integer;
  szNumbers : string;
  str:string;
  Id : string;
  dir: string;
  i : Integer;
begin
  inherited;

  with Self.ADOQuantity do
  begin
    if State = dsInactive then
    begin
      Application.MessageBox( '当前查询状态无效，单击项目管理在进行操作？', '提示:', MB_OKCANCEL + MB_ICONWARNING)
    end else
    begin
        if Sender = Self.RzToolButton75 then
        begin
          //添加
          if State = dsInsert then
          begin

          end else
          begin
            Append;
            FieldByName('Code').Value := g_PactCode;

          //  szName := Self.tvEngineeringQuantityColumn3.DataBinding.FieldName;
          //  FieldByName(szName).Value := g_ProjectName;

            szName := Self.tvEngineeringQuantityColumn1.DataBinding.FieldName;
            szNumbers := 'BGC-' + DM.getDataMaxDate(g_Table_Branch_EngineeringQuantity); //获取Id
            FieldByName(szName).Value := szNumbers;
            szName := Self.tvEngineeringQuantityColumn2.DataBinding.FieldName;
            FieldByName(szName).Value := Date;
            Post;
            Refresh;
            Self.cxGrid4.SetFocus;
            SetGridFocus(Self.tvEngineeringQuantity);
            Self.tvEngineeringQuantity.DataController.Edit;

          end;
        end else
        if Sender = Self.RzToolButton77 then
        begin
          //删除

          if  Application.MessageBox( '您要删除所有选中的记录吗？', '删除记录？', MB_OKCANCEL + MB_ICONWARNING) = IDOK then
          begin
              with Self.tvEngineeringQuantity.DataController.DataSource.DataSet do
              begin
                if not IsEmpty then
                begin
                  for I := 0 to Self.tvEngineeringQuantity.Controller.SelectedRowCount -1 do
                  begin
                    szName := Self.tvEngineeringQuantityColumn1.DataBinding.FieldName;
                    szIndex := Self.tvEngineeringQuantity.GetColumnByFieldName(szName).Index;
                    Id := Self.tvEngineeringQuantity.Controller.SelectedRows[i].Values[szIndex];

                    dir := ConcatEnclosure(Concat( g_Resources,'\' + g_dir_EngineeringQuantity) , id);
                    if DirectoryExists(dir) then  DeleteDirectory(dir);

                    if Length(str) = 0 then
                      str := Id
                    else
                      str := str + '","' + Id;
                  end;

                  with DM.Qry do
                  begin
                    Close;
                    SQL.Clear;
                    SQL.Text := 'delete * from ' +  g_Table_Branch_EngineeringQuantity + ' where ' + 'numbers' +' in("' + str + '")';
                    ExecSQL;
                    SQL.Clear;
                    SQL.Text := 'delete * from ' +  g_Table_Branch_EngineeringDetailed + ' where ' + 'parent' +' in("' + str + '")';
                    ExecSQL;
                  end;
                  Self.TreeView1Click(Sender);
                //    g_Table_Branch_EngineeringQuantity : string = 'sk_Branch_EngineeringQuantity';

                end;

              end;

          end;

        end;
    end;
  end;

end;

procedure TfrmBranch.RzToolButton76Click(Sender: TObject);
begin
  inherited;
  SaveGrid(Self.ADOQuantity);
end;

procedure TfrmBranch.RzToolButton78Click(Sender: TObject);
var
  Child : TfrmIsViewGrid;
  i : Integer;
begin
  inherited;
  Child := TfrmIsViewGrid.Create(Application);
  try
    Child.g_fromName := Self.Name + g_dir_EngineeringQuantity;
    Child.ShowModal;
    SetcxGrid(Self.tvEngineeringQuantity,0,g_dir_EngineeringQuantity);
  finally
    Child.Free;
  end;
end;

procedure TfrmBranch.RzToolButton7Click(Sender: TObject);
var
  s , str : string;
  Node: TTreeNode;
  Code: string;
  szNode : PNodeData;
  szName : string;
  i , szIndex : Integer;
  Id , dir: string;
  Child : TfrmIsViewGrid;
  szProjectName : string;
begin
  inherited;

  if Sender = Self.RzToolButton7 then
  begin
  //支款添加
    Node := Self.TreeView1.Selected;
    if Assigned(Node) then
    begin
      if Node.Level > 1 then
      begin
        szProjectName := Node.Text;

        with Self.qryGrid3 do
        begin
          if State = dsInactive then
          begin
            Application.MessageBox( '当前状态无效，选择左侧后在尝试新增!', '提示:',MB_OKCANCEL + MB_ICONWARNING)
          end else
          begin
            if (State = dsInsert) or (State = dsedit) then
            begin
              Application.MessageBox( '有条目正处于编辑状态，需保存后才能进行新增!', '提示:',MB_OKCANCEL + MB_ICONWARNING)
            end else
            begin
              Append;
              FieldByName('Code').Value := g_PactCode;
              szName := Self.tvGrid3Column2.DataBinding.FieldName;
              FieldByName(szName).Value := DM.getDataMaxDate(g_Table_BranchMoney);
              szName := Self.tvGrid3Column3.DataBinding.FieldName;
              FieldByName(szName).Value := Date;
              szName := Self.tvGrid3Column4.DataBinding.FieldName;
              FieldByName(szName).Value := szProjectName;
              szName := Self.tvGrid3Column8.DataBinding.FieldName;
              FieldByName(szName).Value := g_TopLevelName;
              szName := Self.tvGrid3Column18.DataBinding.FieldName;
              FieldByName(szName).Value := True ; //g_SettleStatus[1];
              Post;
              Refresh;
              Self.cxGrid1.SetFocus;
              SetGridFocus(Self.tvgrid3);
              Self.tvGrid3.DataController.Edit;
            end;

          end;

        end;

      end else
      begin
        Application.MessageBox('该界面以限制，请新建一个工程项目，在进行操作！!!',m_title,MB_OK + MB_ICONQUESTION )
      end;

    end else
    begin
      Application.MessageBox('请选择要添加的合同!!',m_title,MB_OK + MB_ICONQUESTION )
    end;

  end else
  if Sender = Self.RzToolButton9 then
  begin
  //支款删除

      if  Application.MessageBox( '您要删除所有选中的记录吗？', '删除记录？', MB_OKCANCEL + MB_ICONWARNING) = IDOK then
      begin
          with Self.tvGrid3.DataController.DataSource.DataSet do
          begin
            if not IsEmpty then
            begin

              for I := 0 to Self.tvGrid3.Controller.SelectedRowCount -1 do
              begin
                szName  := Self.tvGrid3Column2.DataBinding.FieldName;
                szIndex := Self.tvGrid3.GetColumnByFieldName(szName).Index;
                Id := Self.tvGrid3.Controller.SelectedRows[i].Values[szIndex];

              //  g_ProjectDir_1 := Concat(g_Resources,'\' + g_DetailTable); //明细目录
              //  g_ProjectDir_2 := Concat(g_Resources,'\' + g_income); //收款目录

                dir := ConcatEnclosure( Concat(g_Resources ,  '\' + g_dir_BranchMoney) , id);
                if DirectoryExists(dir) then  DeleteDirectory(dir);

                if Length(str) = 0 then
                  str := Id
                else
                  str := str + '","' + Id;
              end;

              with DM.Qry do
              begin
                Close;
                SQL.Clear;
                SQL.Text := 'delete * from ' +  g_Table_BranchMoney + ' where ' + 'numbers' +' in("' + str + '")';
                ExecSQL;
                SQL.Clear;
                SQL.Text := 'delete * from ' +  g_Table_DetailTable + ' where ' + 'parent' +' in("' + str + '")';
                ExecSQL;
              end;

              Self.TreeView1Click(Sender);
            end;

          end;

      end;

  end else
  if Sender = Self.RzToolButton10 then
  begin
     //表格设置
     Child := TfrmIsViewGrid.Create(Application);
     try
        Child.g_fromName := Self.Name +  g_dir_BranchMoney;
        Child.ShowModal;
        SetcxGrid(Self.tvGrid3,0,g_dir_BranchMoney);
     finally
        Child.Free;
     end;
  end;

end;

procedure TfrmBranch.RzToolButton80Click(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TfrmBranch.RzToolButton82Click(Sender: TObject);
begin
  inherited;
  CreateOpenDir(Self.Handle, Concat( g_Resources ,'\SummaryD' ) , True);
end;

procedure TfrmBranch.RzToolButton83Click(Sender: TObject);
var
  Child : TfrmCalcExpression;
  szTableName : string;
  szNumbers : string;
  szProjectName : string;
  szUnitPrice   : Currency;
  szRecordCount : Integer;

begin
  inherited;
  Child := TfrmCalcExpression.Create(Application);
  try
    Child.g_TableName := g_Table_Branch_EngineeringQuantity;
    with Self.tvEngineeringQuantity.DataController.DataSource.DataSet do
    begin

      if IsEmpty then
      begin
        ShowMessage('未选择工程量,无法查看或新增明细!');
      end else
      begin
        szTableName   := Self.tvEngineeringQuantityColumn1.DataBinding.FieldName;
        szNumbers     := FieldByName(szTableName).AsString ;
        with DM.Qry do
        begin
          Close;
          SQL.Clear;
          SQL.Text := 'Select *from ' + g_Table_Branch_EngineeringQuantity + ' Where ' + szTableName + '="' + szNumbers + '"';
          Open;
          szRecordCount := RecordCount;
        end;

        if 0 = szRecordCount then
        begin
          ShowMessage(Self.tvEngineeringQuantityColumn1.Caption + ':' + szNumbers + ' 未保存，需要保存后才能新增编辑工程量明细');
        end else
        begin
          szTableName   := Self.tvEngineeringQuantityColumn3.DataBinding.FieldName;
          szProjectName := FieldByName(szTableName).AsString;
          Child.g_PostCode := g_PactCode;
          Child.g_Prefix   := 'BMX-';
          Child.g_TableName    := g_Table_Branch_EngineeringDetailed;
          Child.g_ParentNumber := szNumbers;
          Child.StatusBar1.Panels[0].Text := Self.tvEngineeringQuantityColumn1.Caption +  ':' + szNumbers;
          Child.StatusBar1.Panels[1].Text := Self.tvEngineeringQuantityColumn3.Caption +  ':' + szProjectName;
          szTableName := Self.tvEngineeringQuantityColumn9.DataBinding.FieldName;
          szUnitPrice := FieldByName(szTableName).AsCurrency;
          if szUnitPrice = 0 then
          begin
            Self.tvEngineeringQuantityColumn7.FocusWithSelection;
            Application.MessageBox( '请输入工程量单价，否则无法进行计算总价', '提示', MB_OKCANCEL + MB_ICONWARNING);
          end else
          begin
            Child.ShowModal;

            Edit;
            szTableName := Self.tvEngineeringQuantityColumn11.DataBinding.FieldName;
            FieldByName(szTableName).AsCurrency := StrToFloatDef(Child.g_CalcCount,0) * szUnitPrice;
            szTableName := Self.tvEngineeringQuantityColumn8.DataBinding.FieldName;
            FieldByName(szTableName).Value  := Child.g_CalcCount;
            szTableName := Self.tvEngineeringQuantityColumn10.DataBinding.FieldName;
            FieldByName(szTableName).AsString := Child.g_Company;
          //  Self.qry2.UpdateBatch(arAll);
          //  GetEngineeringDetailedData;
          end;

        end;

      end;

    end;
  finally
    Child.Free;
  end;
end;

procedure TfrmBranch.RzToolButton8Click(Sender: TObject);
begin
  inherited;
//保存
  SaveGrid(Self.qryGrid3);
end;

function TfrmBranch.getDBGridehCapital(DBGridEh:TDBGridEh;t1:Currency; AIndex : Integer = 5 ):Boolean;stdcall;
begin
  {
  DBGridEh.Columns[AIndex].Footers.Add;
  DBGridEh.Columns[AIndex].Footers[0].ValueType:=fvtStaticText; //显示文本
  DBGridEh.Columns[AIndex].Footers[0].Value:='大写(' + MoneyConvert(t1) + ')';
  }
end;  

function ADOQueryData(Qry : TADOQuery; s : string): Integer;stdcall ;
begin
  with Qry do
  begin
    Close;
    SQL.Clear;
    SQL.Text := s;
    Open;
    Sort := 'pay_time ASC';
    Result := RecordCount;
  end;
end;
{
procedure TfrmBranch.UpdateMoney(ACode : string);
begin
  with DM.Qry do
  begin
    Close;
    SQL.Clear;
    SQL.Text := 'Select *from t_paymoney where code="' +  ACode + '"';
    Open;
    if RecordCount <> 0 then
       SQL.Text := Format('Update t_paymoney set sum_money=''%m'' where code="%s"',[ g_Sum_Money, ACode ])
    else
       SQL.Text := 'Insert into t_paymoney (code,sum_money) values("'+  ACode +'",'+ FloatToStr(g_Sum_Money) +')';

    ExecSQL;
  end;
end;  

procedure TfrmBranch.ShowSettleAccounts(AIndex : Byte ; ACode : string); //显示结算
begin
  with DM.Qry do
  begin
    Close;
    SQL.Clear;
    SQL.Text := 'Select * from '+ g_Table_Pactmoney +' where code="' +  ACode + '"';
    Open;

    if RecordCount <> 0 then
       SQL.Text := Format('Update '+ g_Table_Pactmoney +' set sum_money=''%m'' where code="%s"',[ g_Sum_Money, ACode ])
    else
       SQL.Text := 'Insert into '+ g_Table_Pactmoney +' (code,sum_money) values("'+ ACode +'",'+ FloatToStr( g_Sum_Money ) +')';

    ExecSQL;

  end;
end;  
}
procedure TfrmBranch.ShowConcrete(AIndex : Integer ; ACode : string);//混凝土
var
  s, s1 : string;
begin
  s1 := 'code in('''+ ACode +'''';
 //  + e.Deliver_UnitPrice + e.Concrete_UnitPrice + e.Permeability_Unitprice + e.chAttach_UnitPrice + e.chAdditive_UnitPrice
 //  + e.Deliver_UnitPrice + e.Concrete_UnitPrice + e.Permeability_Unitprice + e.chAttach_UnitPrice + e.chAdditive_UnitPrice
  s := 'Select (e.UnitPrice * e.Stere) as t,' +
                                          'e.Numbers,' +
                                          'e.code,'    +
                                          'e.Deliver,' +
                                          'e.Deliver_UnitPrice,' +
                                          'e.Concrete,'+
                                          'e.Concrete_UnitPrice,' +
                                          'e.Permeability,'+
                                          'e.Permeability_Unitprice,' +
                                          'e.chAttach,'  +
                                          'e.chAttach_UnitPrice,'+
                                          'e.chAdditive,'+
                                          'e.chAdditive_UnitPrice,'+
                                          'e.Technical,' +
                                          'e.Stere,'+
                                          'e.chPosition,'+
                                          'e.UnitPrice,'  +
                                          'e.WagonNumber,'+
                                          'e.Sign,' +
                                          'e.remarks,'+
                                          'e.pouringTime,'   +
                                          'e.leaveTime,'     +
                                          'e.chDate,'        +
                                          'e.chSupplyUnit,'  +
                                          'e.chProjectName,' +
                                          'e.编号'           +
                                          ' from '+ g_Table_Concrete +' as e where e.' + s1 + ')';
//  OutputLog(s);
  
  with Self.ADOConcrete do
  begin
    Close;
    SQL.Clear;
    SQL.Text := s;
    Open;
    Sort := 'chDate ASC';
  end;

  s := 'Select sum(e.UnitPrice * e.Stere ) as s from '+ g_Table_Concrete +' as e where e.' + s1 + ')';
//  OutputLog(s);
  with DM.Qry do
  begin
    Close;
    SQL.Clear;
    SQL.Text := s;
    open;
    if 0 <> RecordCount then
    begin
      g_Sum_Money := FieldByName('s').AsCurrency;
    end;

    getDBGridehCapital(Self.DBGridEh4,g_Sum_Money,16);
  end;
//  PayList(ACode,s1,g_Sum_Money);

end;

procedure TfrmBranch.ShowData(AIndex,APacttype: Integer; ACode : string);
var
  s , s1 , s2 : string;
  t2 : Currency;


begin

  s1 := 'code in('''+ ACode +'''';
  s  := 'Select (e.Price * e.num * e.truck) as t,' +
                                          'e.numbers,' +
                                          'e.code,'    +
                                          'e.caption,' +
                                          'e.content,' +
                                          'e.pay_time,'+
                                          'e.End_time,'+
                                          'e.truck,'   +
                                          'e.Units,'   +
                                          'e.remarks,' +
                                          'e.Price,'   +
                                          'e.num,'     +
                                          'e.编号,'    +
                                          'e.pacttype' +
                                          ' from '+ g_Table_Expenditure +' as e where e.' + s1 + ') and e.pacttype=' + IntToStr(APacttype) + '' ;


  case APacttype of
    1:
    begin
      //项目部考勤
      s1 := 'code in('''+ ACode +'''';
    //  s := 'Select * from ' + g_Table_Branch_Work +' as e where e.' + s1 + ')' ;
      //************************************************************************
    end;
    2:;//ADOQueryData(Self.Qry,s); //材料
    3:
    begin
      s1 := 'code in('''+ ACode +'''';
      s := 'Select * from ' + g_Table_Branch_Mechanics +' as e where e.' + s1 + ')' ;
      with Self.ADOMechanics do  //机械
      begin
        Close;
        SQL.Clear;
        SQL.Text := s;
        Open;
        Sort := 'WorkDate ASC';
      end;
      //************************************************************************
    end;
    4:
    begin
      {
      s := 'Select (e.Price * e.num) as t,' +
                                          'e.numbers,' +
                                          'e.code,'    +
                                          'e.caption,' +
                                          'e.content,' +
                                          'e.Pay_time,'+
                                          'e.End_time,'+
                                          'e.Units,'   +
                                          'e.remarks,' +
                                          'e.Price,'   +
                                          'e.num,'     +
                                          'e.编号,'    +
                                          'e.pacttype' +
                                          ' from '+ g_Table_EngineeringQuantity +' as e where e.' + s1 + ') and e.pacttype=' + IntToStr(APacttype) + '' ;

      }
      {
      with Self.ADOQuery3 do  //机械
      begin
        Close;
        SQL.Clear;
        SQL.Text := s;
        Open;
        Sort := 'Pay_time ASC';
      end;
      }
      //************************************************************************
    end;
    5:
    begin
      //查询入库单
      ADOQueryData(Self.ADOLease,s); //租赁

      with Self.ADOFrame do
      begin
        Close;
        SQL.Clear;
        SQL.Text := 'Select * from ' + g_Table_Branch_EnterStorage + ' as e where e.' + s1 + ')';
      //  OutputLog(SQL.Text);
        Open;
      //  Sort := '编号 ASC';
      end;

    end;
    6:ADOQueryData(Self.ADOFinance,s); //其他
    8:ADOQueryData(Self.ADOCash,s);
    9:
    begin
      //钢筋
      s := 'Select * from '+ g_Table_Branch_RebarManage +' as e where e.' + s1 + ')' ;
      with Self.ADORebar do
      begin
        Close;
        SQL.Clear;
        SQL.Text := s;
        Open;
        Sort := '编号 ASC';
      end;

    end;
    10:
    begin
      //资质
      s := 'Select (e.Price * e.num) as t,' +
                                          'e.numbers,' +
                                          'e.code,'    +
                                          'e.caption,' +
                                          'e.content,' +
                                          'e.pay_time,'+
                                          'e.End_time,'+
                                          'e.Units,'   +
                                          'e.remarks,' +
                                          'e.Price,'   +
                                          'e.num,'     +
                                          'e.编号'    +
                                          ' from '+ g_Table_Qualifications +' as e where e.' + s1 + ')';


      OutputLog(s);

      with Self.ADOQualifications do  //资质
      begin
        Close;
        SQL.Clear;
        SQL.Text := s;
        Open;
        Sort := '编号 ASC';
      end;

    end;
  end;

  if m_user.dwType = 0 then
  begin
    s := 'Select sum(e.Price * e.num * e.truck) as s from '+ g_Table_Expenditure +' as e where e.' + s1 + ') and e.pacttype=' + IntToStr(APacttype) ;   // and e.pacttype=' + IntToStr(APacttype) + '
    OutputLog(s);
    //**************************************************************************
    case APacttype of
      1:
      begin //考勤
      //  s := 'Select sum(e.t) as s from '+ g_Table_Branch_Work +' as e where e.' + s1 + ')' ;   // and e.pacttype=' + IntToStr(APacttype) + '
      //  OutputLog(s);
      end;
      3:
      begin
        s := 'Select sum(e.t) as s from '+ g_Table_Branch_Mechanics +' as e where e.' + s1 + ')' ;   // and e.pacttype=' + IntToStr(APacttype) + '
       OutputLog(s);
      end;
      4:
      begin
        //工程量
      //  s := 'Select sum(e.Price * e.num) as s from '+ g_Table_EngineeringQuantity +' as e where e.' + s1 + ')' ;   // and e.pacttype=' + IntToStr(APacttype) + '
      //  OutputLog(s);
      end;
      9:
      begin
      //  s := 'Select sum(e.chTotal) as s from '+ g_Table_RebarManage +' as e where e.' + s1 + ')' ;   // and e.pacttype=' + IntToStr(APacttype) + '
      //  OutputLog(s);
      end;
      10:
      begin
      //  s := 'Select sum(e.Price * e.num) as s from '+ g_Table_Qualifications +' as e where e.' + s1 + ')' ;   // and e.pacttype=' + IntToStr(APacttype) + '
      //  OutputLog(s);
      end;
    end;
    //**************************************************************************
    with DM.Qry do
    begin
      Close;
      SQL.Clear;
      SQL.Text := s;
      open;
      if 0 <> RecordCount then
      begin
        g_Sum_Money := FieldByName('s').AsCurrency;
      end;

    end;

  end;

  Self.RzPanel29.Caption := FloatToStr(g_Sum_Money);
  Self.RzPanel28.Caption := MoneyConvert(g_Sum_Money);

//  PayList(ACode,s1,g_SumMeney);
      {
    //输出大写金额
    case APacttype of
      1:getDBGridehCapital(Self.DBGridEh5,g_SumMeney,11);  //人工
      2:getDBGridehCapital(Self.DBGridEh1,g_SumMeney,10);  //材料
      3:getDBGridehCapital(Self.DBGridEh6,g_SumMeney,9);  //机械
      4:getDBGridehCapital(Self.DBGridEh7,g_SumMeney,7);  //工程量
      5:getDBGridehCapital(Self.DBGridEh8,g_SumMeney,10);  //租赁
      6:getDBGridehCapital(Self.DBGridEh10,g_SumMeney,10); //附加
      8:getDBGridehCapital(Self.DBGridEh9,g_SumMeney,10);  //现金
    end;
    }
  
end;

procedure TfrmBranch.GetSumBranch(lpId: string);
var
  s : string;
  szColName : string;
  szBalance : Currency;

begin
  with DM.Qry do
  begin
    Close;
    szColName := Self.tvGrid3Column11.DataBinding.FieldName;
    s := 'Select sum(' + szColName +') as t from '+ g_Table_BranchMoney +' where ' + lpId  ;
    SQL.Clear;
    SQL.Text := s;
    Open;
    if 0 <> RecordCount then
    begin
      g_Paid := FieldByName('t').AsCurrency;
      szBalance := g_Sum_Money - g_Paid;
      if szBalance <> 0 then
      begin
        Self.RzPanel6.Caption := CurrToStr(szBalance);
        Self.RzPanel7.Caption := MoneyConvert(szBalance);
      end;
    end;

  end;

  {
  with Self.ADOBalance do
  begin
    Close;
    SQL.Clear;
    SQL.Text := 'Select * from '+ g_Table_Pactmoney  +' where code="' + g_PactCode + '" and status=1';
    Open;
  end;
  }
end;

procedure TfrmBranch.GetSumMoney(s : string);
begin
  with DM.Qry do
  begin
    Close;
    SQL.Clear;
    SQL.Text := s;
    Open;
    if 0 <> RecordCount then
    begin
      g_Sum_Money := FieldByName('t').AsCurrency;
      Self.RzPanel29.Caption := CurrToStr(g_Sum_Money);
      Self.RzPanel28.Caption := MoneyConvert( g_Sum_Money);
    end;

  end;

end;

procedure SelectGroupData(Lv : Integer; SQLText: string);
var
  szColList : string;
  szSumListName : string;
  
begin

end;  

procedure TfrmBranch.TreeView1Click(Sender: TObject);
var
  szColName : string;
  szNode : PNodeData;
  szCodeList : string;
  s : string;
  ImgPath : string;
  szSum_Money : Currency;
  t1 , t2 : Currency;
  parent : string;
  szLevel: Integer;
  szIndex: Byte;
  Node : TTreeNode;
  szColList : string;
  szProjectName : string;
  szSumListName : string;
begin
    g_Sum_Money := 0;
    Self.RzPanel6.Caption  := '';
    Self.RzPanel7.Caption  := '';
    Self.RzPanel28.Caption := '';
    Self.RzPanel29.Caption := '';
    Self.RzPanel6.Caption  := '';
    Self.RzPanel7.Caption  := '';
    Node := Self.TreeView1.Selected;
    if Assigned(Node) then
    begin
      if Node.Level > 0 then
      begin

        szNode := PNodeData(Node.Data);
        g_PactCode    := szNode^.Code;
        g_TopLevelName:= GetTreeLevelName(Node);
        szProjectName := Node.Text;
        treeCldnode(Node,szCodeList);
        //交易记录

        //**********************************************************************
        g_CodeList := 'Code in('''+ szCodeList + ''')';
        with Self.qryGrid3 do
        begin
          Close;
          SQL.Clear;
          SQL.Text := 'Select * from ' + g_Table_BranchMoney + ' where ' + g_CodeList;
          Open;
        end;
        Self.RefreshDetailTable;
        SetcxGrid(Self.tvGrid3,0,g_dir_BranchMoney);
       //**********************************************************************
        case g_pacttype of
          0:
          begin
            {
            //考勤
            //******************************************************************
            s := 'Select * from ' + g_Table_Branch_Work + ' where ' + g_CodeList ;
            if DM.ADOQueryData(Self.ADOWork,s) <> 0 then

          //  s := 'Select sum(SumMoney) as t from '+ g_Table_Branch_Work +' where ' + szCode + ')' ;   // and e.pacttype=' + IntToStr(APacttype) + '
          //  GetSumMoney(s);
            SetcxGrid(Self.tvWorkView,0,g_Branch_dir_Work);
            
            szColList := Self.ProjectCol2.DataBinding.FieldName;
            if Node.Level = 1 then
            begin
              s := 'SELECT '+ szColList +' FROM '+ g_Table_Project_Worksheet +' where SignName="'+ Node.Text +'" GROUP BY ' + szColList;

              with Self.ADOProjectName do
              begin
                Close;
                SQL.Clear;
                SQL.Text := s;
                Open;
              end;

            end else
            begin
              Self.ADOProjectName.Close;
            end;
            }

//            SELECT ProjectName,SUM(SumMoney) as t,SUM(WorkDays) as w , SUM(OverSumday) as O FROM sk_Project_Worksheet where SignName="朱峰" GROUP BY ProjectName

//select
//   o.custid,COUNT(*) as '订单数'
//   from [Sales.Orders] o inner join [Sales.Customers] c
//   on o.custid=c.custid
//   where c.country='按时打算'
//group by o.custid;


            //******************************************************************
            szColList := Self.tvGroupViewCol2.DataBinding.FieldName;
            szSumListName := 'SUM(SumMoney) as t,SUM(WorkDays) as w , SUM(OverSumday) as O';
            if Node.Level = 1 then
            begin                        
              s := 'SELECT '+ szColList +','+ szSumListName +' FROM '+ g_Table_Project_Worksheet +' where SignName="'+ Node.Text +'" GROUP BY ' + szColList;
            end else
            begin
            //  szColName := Self.WorkCol4.DataBinding.FieldName;
            //  s := 'SELECT '+ szColList +','+ szSumListName +' FROM ' + g_Table_Branch_Work + ' WHERE '+ szColName +'="'+ Node.Text +'" GROUP BY '+ szColList;
            end;
            OutputLog(s);
            with Self.ADOWorkGroup do
            begin
              Close;
              SQL.Clear;
              SQL.Text := s;
              Open;
            end;      
          
            //******************************************************************
          end;
          1:
          begin
            //材料
            //******************************************************************
            s := GetTableMaterial(g_CodeList);
            if DM.ADOQueryData(Self.ADOMaterial,s ) <> 0 then
            {
            s := 'Select sum(Price * num * truck) as t from ' +
                  g_Table_Expenditure + ' where ' + szCode + ') and pacttype=' +  IntToStr(g_pacttype) ;
            GetSumMoney(s);
            }
            //******************************************************************
          end;
          2:
          begin
            //混凝土
            //******************************************************************
            s:=GetTableConcrete(g_CodeList);
            with Self.ADOConcrete do
            begin
              Close;
              SQL.Clear;
              SQL.Text := s;
              Open;
            //  Sort := 'chDate ASC';
            end;
            s := 'Select sum(UnitPrice * Stere) as t from ' +
                  g_Table_Concrete + ' where ' + g_CodeList;
            GetSumMoney(s);
            //******************************************************************
          end;
          3:
          begin
            //机械土石方
            //******************************************************************
            s := 'Select * from ' + g_Table_Branch_Mechanics +' as e where e.' + g_CodeList;
            with Self.ADOMechanics do  //机械
            begin
              Close;
              SQL.Clear;
              SQL.Text := s;
              Open;
            //  Sort := 'WorkDate ASC';
            end;
            s := 'Select sum(SumMoney) as t from '+ g_Table_Branch_Mechanics +' where ' + g_CodeList ;
            GetSumMoney(s);
            //******************************************************************
          end;
          4:
          begin
            //项目分包单位
            s := 'Select * from ' + g_Table_Branch_EngineeringQuantity +' where ' + g_CodeList ;
            if DM.ADOQueryData(Self.ADOQuantity,s) <> 0 then //查询后有结果

            s := 'Select sum(Total) as t from '+ g_Table_Branch_EngineeringQuantity +' where ' + g_CodeList ;   // and e.pacttype=' + IntToStr(APacttype) + '
            GetSumMoney(s);

            s := 'Select * from ' + g_Table_Branch_EngineeringDetailed ;
            if DM.ADOQueryData(Self.ADODetailed,s) <> 0 then

            SetcxGrid(Self.tvEngineeringQuantity,0,g_dir_EngineeringQuantity);
          end;
          5:
          begin
            //租赁单位
            //查询入库单
            //******************************************************************
            s := GetTableMaterial(g_CodeList);
            ADOQueryData(Self.ADOLease,s); //租赁

            with Self.ADOFrame do
            begin
              Close;
              SQL.Clear;
              SQL.Text := 'Select * from ' + g_Table_Branch_EnterStorage + ' as e where e.' + g_CodeList;
              Open;
            end;

            //******************************************************************
          end;
          6:
          begin
            //项目部财务
            s := GetTableMaterial(g_CodeList);
            ADOQueryData(Self.ADOFinance,s); //其他
            {
            s := 'Select sum(Price * num * truck) as t from ' +
                  g_Table_Expenditure + ' where ' + szCode + ') and pacttype=' +  IntToStr(g_pacttype) ;
            GetSumMoney(s);
            }
          end;
          7:
          begin
            //现金供应单位
            s := GetTableMaterial(g_CodeList);
            ADOQueryData(Self.ADOCash,s); //其他
          end;
          8:
          begin
            //钢材供应单位
            s := 'Select * from '+ g_Table_Branch_RebarManage +' where ' + g_CodeList ;
            with Self.ADORebar do
            begin
              Close;
              SQL.Clear;
              SQL.Text := s;
              Open;
            //  Sort := '编号 ASC';
            end;
            s := 'Select sum(chTotal) as t from '+ g_Table_Branch_RebarManage +' where ' + g_CodeList ;   // and e.pacttype=' + IntToStr(APacttype) + '
            GetSumMoney(s);
          end;
          9:
          begin
            //资质单位
            s := GetTableQualifications(g_CodeList);
            with Self.ADOQualifications do  //资质
            begin
              Close;
              SQL.Clear;
              SQL.Text := s;
              Open;
            end;
            s := 'Select sum(Price * num) as t from '+ g_Table_Qualifications +' where ' + g_CodeList ;   // and e.pacttype=' + IntToStr(APacttype) + '
            GetSumMoney(s);
          end;
        end;

        if (g_pacttype = 1) or
           (g_pacttype = 5) or
           (g_pacttype = 6) or
           (g_pacttype = 7) then
        begin
          s := 'Select sum(Price * num * truck) as t from ' +
                  g_Table_Expenditure + ' where ' + g_CodeList + ' and pacttype=' +  IntToStr(g_pacttype) ;
          GetSumMoney(s);
        end;
        GetSumBranch(g_CodeList);
        GetFlowingGroupMoney(szProjectName);
      //  ShowSettleAccounts(0,g_PactCode);

      end;

    end;

end;

procedure TfrmBranch.TreeView1CustomDrawItem(Sender: TCustomTreeView;
  Node: TTreeNode; State: TCustomDrawState; var DefaultDraw: Boolean);
begin
  if (Node.Level = 1) then  {and (该叶子节点满足某条件)}
  begin
    if (cdsSelected in State) then
     begin
        with Sender.Canvas do
        begin
          Brush.Color := clGradientActiveCaption;
          Font.Color :=clRed;
        end;
     end
     else
     begin
       Sender.Canvas.Font.Color := clBlue;
     end;

  end;

  DefaultDraw := True
end;

procedure TfrmBranch.TreeView1DragDrop(Sender, Source: TObject; X, Y: Integer);
var TargetNode, SourceNode: TTreeNode;
begin
  TargetNode := TreeView1.DropTarget;
  SourceNode := TreeView1.Selected;
  if MessageBox(handle, '您确认要移动合同吗？', '提示', MB_ICONQUESTION
    + MB_YESNO) = IDYES then
    if g_PatTree.ModifyNodePID(PNodeData(TargetNode.Data)^.Index, SourceNode) then
       g_PatTree.FillTree(g_Parent,g_pacttype);
end;

procedure TfrmBranch.TreeView1DragOver(Sender, Source: TObject; X, Y: Integer;
  State: TDragState; var Accept: Boolean);
begin
  if Source = TreeView1 then Accept := True
  else Accept := False;
end;

procedure TfrmBranch.TreeView1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 46 then
  begin
    Self.RzToolButton3.Click;
  end;
end;

procedure TfrmBranch.tvBalanceColumn1GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  inherited;
  AText := IntToStr(ARecord.Index+1);
end;

procedure TfrmBranch.tvEngineeringQuantityColumn13GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  inherited;
  AText := IntToStr(ARecord.Index + 1 );
end;

procedure TfrmBranch.tvEngineeringQuantityColumnSizeChanged(
  Sender: TcxGridTableView; AColumn: TcxGridColumn);
begin
  inherited;
  SetcxGrid(Self.tvEngineeringQuantity,1,g_dir_EngineeringQuantity);
end;

procedure TfrmBranch.tvEngineeringQuantityEditKeyDown(
  Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem;
  AEdit: TcxCustomEdit; var Key: Word; Shift: TShiftState);
var
  szValue  : Double;
  szColName: string;
  szUnitPrice : Currency;

begin
  inherited;
  if (Key = 13) or (Key = 9)  then
  begin

    if (AItem.Name = Self.tvEngineeringQuantityColumn8.Name) then
    begin
        try

          szValue := AEdit.EditingValue;
          if szValue = 0 then
          begin
            Self.RzToolButton83.Click;
          end else
          begin
            Self.tvEngineeringQuantity.DataController.UpdateData;
            with Self.ADOQuantity do
            begin
              if State <> dsInactive then
              begin
                szColName  := Self.tvEngineeringQuantityColumn9.DataBinding.FieldName;
                szUnitPrice:= FieldByName(szColName).Value;
                szColName := Self.tvEngineeringQuantityColumn11.DataBinding.FieldName;
                Edit;
                FieldByName(szColName).AsCurrency := szValue * szUnitPrice;
              end;
            end;

          end;

        except
        end;
    end;

    if AItem.Name = Self.tvEngineeringQuantityColumn11.Name then
    begin

      if Self.tvEngineeringQuantity.Controller.FocusedRow.IsLast then
      begin
        if IsNewRow then
        begin
          //在最后一行新增
          Self.RzToolButton75.Click;
        end;

      end;


    end;

  end;

end;

procedure TfrmBranch.tvEngineeringQuantityKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = 46 then
  begin
  //  Self.RzBitBtn38.Click;
  end;
end;

procedure TfrmBranch.tvEngineeringQuantityTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems1GetText(
  Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
  var AText: string);
var
  t : Currency;
  s : string;

begin
  inherited;
//  Self.RzPanel58.Caption := '';
//  Self.RzPanel59.Caption := '';
  Self.tvEngineeringQuantityColumn12.Summary.FooterFormat := '';
  try
    s := VarToStr(AValue);
    t := StrToCurrDef(s,0);
    if t <> 0 then
    begin
    //  Self.RzPanel58.Caption := s;
    //  Self.RzPanel59.Caption := MoneyConvert(t);
      Self.tvEngineeringQuantityColumn12.Summary.FooterFormat := MoneyConvert(t);
    end;

  except
  end;
end;

procedure TfrmBranch.tvGrid3Column11CustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
  inherited;
  ACanvas.Font.Style := [fsBold];
  ACanvas.Brush.Color:= clYellow;
  ACanvas.Font.Color := clBlue;
end;

procedure TfrmBranch.tvGrid3Column18CustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
  inherited;
  ACanvas.Font.Style := [fsBold];
  ACanvas.Font.Color := clBlue;
end;

procedure TfrmBranch.tvGrid3Column18PropertiesChange(Sender: TObject);
begin
  inherited;
  Self.qryGrid3.UpdateBatch();
  Self.qryGrid3.Requery();
end;

procedure TfrmBranch.tvGrid3Column1GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin

  AText := IntToStr(ARecord.Index + 1);
end;

procedure TfrmBranch.tvGrid3Column3CompareRowValuesForCellMerging(
  Sender: TcxGridColumn; ARow1: TcxGridDataRow;
  AProperties1: TcxCustomEditProperties; const AValue1: Variant;
  ARow2: TcxGridDataRow; AProperties2: TcxCustomEditProperties;
  const AValue2: Variant; var AAreEqual: Boolean);
begin
  ARow1.CanFocus;
end;

procedure TfrmBranch.tvGrid3Editing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
var
  szColName : string;
begin
  inherited;
  szColName := Self.tvGrid3Column18.DataBinding.FieldName;
  if AItem.Index <> Self.tvGrid3Column18.Index then
  begin
      with Self.qryGrid3  do
      begin
        if State <> dsInactive then
        begin

          if FieldByName(szColName).Value = False then
          begin
            AAllow := False;
          end else
          begin
            AAllow := True;
          end;

        end;

      end;

  end;

end;

{
var
  szColName : string;
begin
  inherited;

  with Self.qryGrid3 do
  begin
    if State <> dsInactive then
    begin
      szColName := Self.tvGrid3Column18.DataBinding.FieldName;
      if FieldByName(szColName).Value then  //g_SettleStatus[0]
      begin
        AAllow := False;
      end else
      begin
        AAllow := True;
      end;
    end;
  end;

end;
}
procedure TfrmBranch.tvGrid3EditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
var
  szSumMoney : Currency;
  szColName  : string;
begin
  inherited;
  if Key = 13 then
  begin
    if AItem.Editing then
    begin
        if AItem.Name = Self.tvGrid3Column11.Name then
        begin
          Self.tvGrid3.DataController.UpdateData;
          szSumMoney := AEdit.EditValue;
        //  Self.cxGrid1.BeginUpdate();
          with Self.tvGrid3Column12.DataBinding do
          begin
            szColName := FieldName;
            with DataController.DataSet do
            begin
              Edit;
              FieldByName(szColName).Value := MoneyConvert(szSumMoney);
              Post;
              Refresh;
            end;
          end;
        //  Self.cxGrid1.EndUpdate;

          if Self.tvGrid3.Controller.FocusedRow.IsLast then
          begin
            //在最后一行新增
            Self.tvGrid3.DataController.UpdateData;
            if IsNewRow then
            begin
              Self.RzToolButton7.Click;
            end;

          end;

        end;

    end;

  end;
end;

procedure TfrmBranch.tvGrid3KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = 46 then
  begin
    Self.RzToolButton9.Click;
  end;
end;

procedure TfrmBranch.tvGrid3StylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
var
  szIndex : Integer;
  Value : Variant;
begin
  inherited;
  Value := ARecord.Values[Self.tvGrid3Column18.Index];
  if Value = False then
  begin
    AStyle := DM.cxStyle222;
  end;
end;
{
var
  szColIndex : Integer;

begin
  inherited;

  szColIndex := Self.tvGrid3Column18.Index;
  if ARecord.Values[szColIndex] then
  begin
    AStyle := DM.cxStyle222;
  end;


  if ARecord.DisplayTexts[Self.tvGrid3Column18.Index] = g_SettleStatus[0] then
  begin
    AStyle := DM.cxStyle222;
  end;
end;
}
procedure TfrmBranch.tvGrid3TcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
  Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
  var AText: string);
var
  szValue : Currency;
  s , Large : string;

begin
  inherited;
  try
    Self.RzPanel24.Caption := '';
    Self.RzPanel25.Caption := '';
    s := VarToStr(AValue);
    szValue := StrToCurrDef(s,0);
    if szValue <> 0 then
    begin
      Large := MoneyConvert( szValue );
      Self.tvGrid3Column12.Summary.FooterFormat := Large;
      Self.RzPanel24.Caption := AText;
      Self.RzPanel25.Caption := Large;
    end;
  except
  end;
end;

procedure TfrmBranch.tvGroupViewCol1GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  inherited;
  AText := IntToStr(ARecord.Index + 1) ;
end;

procedure TfrmBranch.GetFlowingGroupMoney(lpSingName : string);
var
  s : string;
  szSumListName : string ;
  szColList : string;
  szColName : string;
  szQry : TADOQuery;

begin
  szColList := 'Receivables,ProjectName,CategoryName,Payee';
  szSumListName := 'SUM(SumMoney) as t';
  szColName := 'ProjectName';
  s := 'SELECT '+ szColList + ','+ szSumListName +' FROM '+ g_Table_RuningAccount +' where '+ szColName
                   +'="'+ lpSingName +'" GROUP BY ' + szColList;
  with  ADOFlowingGroup do
  begin
    Close;
    SQL.Clear;
    SQL.Text := s;
    Open;
  end;

end;

procedure TfrmBranch.tvProjectViewDblClick(Sender: TObject);
var
  szName : string;
  szColName : string;
  szColList : string;
  szSumListName : string;
  s : string;
  
begin
  inherited;
  szColName := Self.ProjectCol2.DataBinding.FieldName;
  with Self.ADOProjectName do
  begin
    if State <> dsInactive then
     begin
       if not IsEmpty then
        begin
          szName := FieldByName(szColName).AsString;
          szColList := Self.tvGroupViewCol2.DataBinding.FieldName;

          case Self.RzPageControl3.ActivePageIndex of
            0:
            begin
              //帐目总价
              case Self.RzPageControl4.ActivePageIndex of
                0:
                begin
                  //考勤汇总
                  szSumListName := 'SUM(SumMoney) as t,SUM(WorkDays) as w , SUM(OverSumday) as O';

                  s := 'SELECT '+ szColList +','+ szSumListName +' FROM '+ g_Table_Project_Worksheet +' where '+ szColName
                       +'="'+ szName +'" GROUP BY ' + szColList;
                end;
                1:
                begin
                  s := 'SELECT ProjectName,SUM(Total*UnitPrice) as t,SUM(Total) as n FROM sk_Project_BuildStorage where ProjectName="三里工程" GROUP BY ProjectName';
                end;
                2:
                begin

                end;

              end;

              if Length(s) <> 0 then
              begin
                with Self.ADOWorkGroup do
                begin
                  Close;
                  SQL.Clear;
                  SQL.Text := s;
                  Open;
                end;
              end;

            end;
            1:
            begin
              GetFlowingGroupMoney(szName);
            end;
          end;




        end; 
         
     end; 
      
  end;
  
end;

procedure TfrmBranch.tvViewColumn1GetDisplayText(Sender: TcxCustomGridTableItem;
  ARecord: TcxCustomGridRecord; var AText: string);
begin
  inherited;
  AText := IntToStr(ARecord.Index +1);
end;

procedure TfrmBranch.tvWorkViewCellClick(Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
var
  RowIndex : Integer;
  ColIndex : Integer;
  Value : Variant;
begin
  inherited;
  RowIndex := Self.tvWorkView.DataController.FocusedRowIndex;
  Value :=  Self.tvWorkView.ViewData.DataController.Values[RowIndex,Self.WorkCol15.Index];
  if Value = False then
  begin

  end;
end;

procedure TfrmBranch.tvWorkViewColumnSizeChanged(Sender: TcxGridTableView;
  AColumn: TcxGridColumn);
begin
  inherited;
//  SetcxGrid(Self.tvWorkView,1,g_Branch_dir_Work);
end;

procedure TfrmBranch.tvWorkViewDblClick(Sender: TObject);
begin
  inherited;
//  CreateEnclosure(Self.tvWorkView.DataController.DataSource.DataSet,Handle, Concat( g_Resources ,'\' + g_Branch_dir_Work));

end;

procedure TfrmBranch.tvWorkViewEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
var
  szColName : string;
begin
  inherited;
  szColName := Self.WorkCol15.DataBinding.FieldName;
  if AItem.Index <> Self.WorkCol15.Index then
  begin
      with Self.ADOWork do
      begin
        if State <> dsInactive then
        begin

          if FieldByName(szColName).Value = False then
          begin
            AAllow := False;
          end else
          begin
            AAllow := True;
          end;

        end;

      end;

  end;

end;

procedure TfrmBranch.tvWorkViewKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = 46 then
  begin
    Self.RzToolButton6.Click;
  end;
end;

procedure TfrmBranch.tvWorkViewMouseEnter(Sender: TObject);
begin
  inherited;
  //移动

end;

procedure TfrmBranch.tvWorkViewMouseLeave(Sender: TObject);
begin
  inherited;
  //移动完成
end;

procedure TfrmBranch.tvWorkViewStylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
var
  szIndex : Integer;
  Value : Variant;
begin
  inherited;
  Value := ARecord.Values[Self.WorkCol15.Index];
  if Value = False then
  begin
    AStyle := DM.cxStyle222;
  end;
end;

procedure TfrmBranch.tvWorkViewTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
  Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
  var AText: string);
var
  s : string;
begin
  inherited;
  Self.RzPanel29.Caption := '';
  Self.RzPanel28.Caption := '';
  try
    s := VarToStr(AValue);
    if Length(s) <> 0 then
    begin
      g_Sum_Money := StrToCurrDef(s,0);
      if g_Sum_Money <> 0 then
      begin
        Self.RzPanel29.Caption := CurrToStr(g_Sum_Money);
        Self.RzPanel28.Caption := MoneyConvert( g_Sum_Money);
      end;

    end;

  except
  end;
end;

procedure TfrmBranch.RzBitBtn100Click(Sender: TObject);
begin
  SearchFiles(Self.RzEdit19.Text,
              g_Qualifications,
              Self.DBGridEh14,
              self.RzFileListBox12
              );
end;

procedure TfrmBranch.RzBitBtn101Click(Sender: TObject);
begin
  CreateOpenDir(Self.Handle, getEnclosurePath(g_Qualifications,Self.DBGridEh14) ,True);
end;

procedure TfrmBranch.RzBitBtn102Click(Sender: TObject);
var
  str : string;
begin
  str := getEnclosurePath(g_Qualifications,Self.DBGridEh14);
  if str <> Self.RzFileListBox12.Directory then
  begin
    Self.RzFileListBox12.UpOneLevel;
  end;
end;

procedure TfrmBranch.RzBitBtn103Click(Sender: TObject);
begin

end;
{
var
  s , Code : string;
  Node: TTreeNode;
  szNode : PNodeData;
  Child  : TfrmQualifications;
  szfrmReady : TfrmReadyMoney;

begin
  if (m_user.dwType = 0) or (m_user.dwType = 1) then
  begin
      Child := TfrmQualifications.Create(Self);
      try
        Child.g_ProjectDir := g_Qualifications;
        Child.g_TableName  := g_Table_Qualifications;

        if Sender = RzBitBtn103 then
        begin
          Node := Self.TreeView1.Selected;
          if Assigned(Node) then
          begin
            szNode := PNodeData(Node.Data);
            Child.g_PostCode := szNode.Code;
            Child.cxDateEdit1.Date := Date;
            Child.cxDateEdit2.Date := Date;
            Child.g_PostNumbers := DM.getDataMaxDate(g_Table_Qualifications);
            Child.ShowModal;
            Self.TreeView1Click(Sender);
          end else
          begin
            Application.MessageBox('请选择要添加的合同!!',m_title,MB_OK + MB_ICONQUESTION )
          end;

        end else
        if Sender = RzBitBtn104 then
        begin
           with Self.DBGridEh14.DataSource.DataSet do
           begin
              if not IsEmpty then
              begin
                Child.g_IsModify := True;

                Child.g_PostNumbers    := FieldByName('numbers').AsString;
                Child.cxTextEdit1.Text := FieldByName('caption').AsString;
                Child.cxTextEdit2.Text := FieldByName('content').AsString;
                Child.cxComboBox1.Text := FieldByName('Units').AsString;
                Child.cxDateEdit1.Date := FieldByName('pay_time').AsDateTime;
                Child.cxDateEdit2.Date := FieldByName('End_time').AsDateTime;
                Child.cxCurrencyEdit1.Value := FieldByName('Price').AsCurrency;
                Child.cxCurrencyEdit2.Value := FieldByName('num').AsCurrency;
                Child.cxMemo1.Text  := FieldByName('remarks').AsString;
                Child.ShowModal;
                Self.TreeView1Click(Sender);
              end else
              begin
                Application.MessageBox('请选择要修改的条件!!',m_title,MB_OK + MB_ICONQUESTION )
              end;
           end;


        end else
        if Sender = RzBitBtn105 then
        begin
          DBGridDelete(DBGridEh14,Self.ADOQualifications,g_Table_Qualifications,g_Qualifications);
          Self.TreeView1Click(Sender);
        end;

      finally
        Child.Free;
      end;

  end;

end;
}

procedure TfrmBranch.RzBitBtn10Click(Sender: TObject);
var
  s : string;
  Node: TTreeNode;
  i : Integer;

begin
  if (m_user.dwType = 0) or (m_user.dwType = 1) then
  begin

    Node := Self.TreeView1.Selected;
    if Assigned(Node) then
    begin
       g_tabIndex := 1;
       if Sender = Self.RzBitBtn10 then
       begin
         s := '材料';
         if not getEditData(self.DBGridEh1) then Exit;

          frmBranchlnput := TfrmBranchlnput.Create(Application);
          try
            frmBranchlnput.g_ProjectDir := g_Expenditure;
            frmBranchlnput.Caption := s + '修改';
            frmBranchlnput.g_Handte     := Handle;
            frmBranchlnput.RzPageControl1.ActivePage := frmBranchlnput.TabSheet2;
            frmBranchlnput.ShowModal;
            Self.TreeView1Click(Sender);
          finally
            frmBranchlnput.Free;
          end;
          
       end;

    end
    else
    begin
      Application.MessageBox('请选择要修改的合同!!',m_title,MB_OK + MB_ICONQUESTION )
    end;
  end;

end;

procedure msg();
begin
  Application.MessageBox('请单击左边的序号选中一条记录或拖选一段记录，' +
            #13#10 +
            '也可以用Ctrl及Shift来进行多选，然后再删除记录。', '提示',
            MB_OK + MB_ICONINFORMATION);
end;

function DeleteData(ATableName: string; AINdex : Byte; DBGridEh : TDBGridEh):Boolean;stdcall;
var
  szNumberList : string;
  Number : Integer;
  i : Integer;
  s : string;
  
begin
   Result := False;
   if not DBGridEh.DataSource.DataSet.IsEmpty then
   begin

       if DBGridEh.SelectedRows.Count > 0 then
       begin
         if MessageBox(frmBranch.Handle, '您要删除所有选中的记录吗？', '删除记录？', MB_OKCANCEL + MB_ICONWARNING) = IDOK then
         begin
              with DBGridEh.DataSource.DataSet do
              begin
                  for i:=0 to (DBGridEh.SelectedRows.Count - 1) do
                  begin
                    GotoBookmark(DBGridEh.SelectedRows.Items[i]);
                  //  GotoBookmark(pointer(DBGridEh.SelectedRows.Items[i])); //关键是这一句
                    Number := DBGridEh.DataSource.DataSet.FieldByName('编号').AsInteger;
                    if Length(szNumberList) = 0 then
                       szNumberList := IntToStr(Number)
                    else
                      szNumberList := szNumberList + ',' + IntToStr(Number);
                  end;

              end;

              with DM.Qry do
              begin

                if AINdex = 0 then
                   s := 'delete * from ' + ATableName + ' where '+ m_Numer +' in(' +  szNumberList+ ')'
                else
                   s := 'delete * from ' + 'concrete' + ' where '+ m_Numer +' in(' +  szNumberList+ ')';

                Close;
                SQL.Clear;
                SQL.Text := s;
                if 0 = ExecSQL then
                begin
                  Application.MessageBox('删除失败',m_title,MB_OK + MB_ICONQUESTION ) ;
                end else
                begin
                  Result := True;
                end;  
              end;

         end
         else
         begin
           msg;
         end;

       end;

   end;

end;

procedure TfrmBranch.RzBitBtn112Click(Sender: TObject);
begin
  CreateOpenDir(Self.Handle, Concat( g_Resources ,'\SummaryA' ) , True);
end;

procedure TfrmBranch.RzBitBtn113Click(Sender: TObject);
begin
  CreateOpenDir(Self.Handle, Concat( g_Resources ,'\SummaryB' ) , True);
end;

procedure TfrmBranch.RzBitBtn114Click(Sender: TObject);
begin
  CreateOpenDir(Self.Handle, Concat( g_Resources ,'\SummaryC' ) , True);
end;

procedure TfrmBranch.RzBitBtn117Click(Sender: TObject);
begin
  CreateOpenDir(Self.Handle, Concat( g_Resources ,'\PactSummaryC' ) , True);
end;

procedure TfrmBranch.RzBitBtn118Click(Sender: TObject);
begin
CreateOpenDir(Self.Handle, Concat( g_Resources ,'\PactSummaryB' ) , True);
end;

procedure TfrmBranch.RzBitBtn119Click(Sender: TObject);
begin
  CreateOpenDir(Self.Handle, Concat( g_Resources ,'\PactSummaryA' ) , True);
end;

procedure TfrmBranch.RzBitBtn122Click(Sender: TObject);
begin
  inherited;
  with Self.cxGroupBox5 do
  begin
    if Visible then
    begin
      Visible := False;
    end else
    begin
      Visible := True;
    end;
  end;
end;

procedure TfrmBranch.RzBitBtn123Click(Sender: TObject);
begin
  inherited;
  with Self.cxGroupBox6 do
  begin
    if Visible then
    begin
      Visible := False;
    end else
    begin
      Visible := True;
    end;
  end;
end;

procedure TfrmBranch.RzBitBtn124Click(Sender: TObject);
begin
  inherited;
  with Self.cxGroupBox7 do
  begin
    if Visible then
    begin
      Visible := False;
    end else
    begin
      Visible := True;
    end;
  end;
end;

procedure TfrmBranch.RzBitBtn13Click(Sender: TObject);
begin
  {
   try
     if sender = self.RzBitBtn13 then
     begin
       // '材料明细表';
        DBGridExport(DBGridEh1);
     end else if Sender = Self.RzBitBtn27 then
     begin
       // '人工明细表';
       DBGridExport(DBGridEh5);
     end else if Sender = Self.RzBitBtn33 then
     begin
       //'机械明细表';
       DBGridExport(DBGridEh6);
     end else if Sender = Self.RzBitBtn39 then
     begin
       //'工程量明细表';
       DBGridExport(DBGridEh7);
     end else if Sender = Self.RzBitBtn45 then
     begin
       case Self.cxPageControl1.ActivePageIndex of
         0:
         begin
           // '租赁明细表';
           DBGridExport(DBGridEh8);
         end;
         1:
         begin
           // '架体明细表';
           DBGridExport(DBGridEh13);
         end;
       end;
     end else if Sender = Self.RzBitBtn51 then
     begin
       //'其他明细表';
       DBGridExport(DBGridEh9);
     end else if Sender = Self.RzBitBtn19 then
     begin
       //'混凝土明细表';
       DBGridExport(DBGridEh4);
     end else if Sender = Self.RzBitBtn51 then
     begin
       // '现金明细表';
       DBGridExport(DBGridEh9);
     end else if Sender = Self.RzBitBtn106 then
     begin
       DBGridExport(DBGridEh14);
     end else
     if Sender = Self.RzBitBtn109 then
     begin
       DBGridExport(DBGridEh11);
     end;

   finally
   end;
   }
end;

procedure TfrmBranch.RzBitBtn14Click(Sender: TObject);
begin
{
  if Sender = Self.RzBitBtn14 then
  begin
    DBPrint(DBGridEh1);
  end else if Sender = Self.RzBitBtn28 then
  begin
    DBPrint(DBGridEh5);
  end else if Sender = Self.RzBitBtn34 then
  begin
    DBPrint(DBGridEh6);
  end else if Sender = Self.RzBitBtn40 then
  begin
    DBPrint(DBGridEh7);
  end else if Sender = Self.RzBitBtn46 then
  begin
    case Self.cxPageControl1.ActivePageIndex of
      0:
      begin
        DBPrint(DBGridEh8);
      end;
      1:
      begin
        DBPrint(DBGridEh13);
      end;
    end;

  end else if Sender = Self.RzBitBtn52 then
  begin
    DBPrint(DBGridEh9);
  end else if Sender = Self.RzBitBtn20 then
  begin
    DBPrint(DBGridEh4);
  end else if Sender = Self.RzBitBtn92 then
  begin
    DBPrint(DBGridEh12);
  end else if Sender = Self.RzBitBtn107 then
  begin
    DBPrint(DBGridEh14);
  end;
  }
end;

procedure TfrmBranch.RzBitBtn15Click(Sender: TObject);
begin
  DBGridDelete(DBGridEh4,Self.ADOConcrete,g_Table_Concrete,g_Concrete);
  Self.TreeView1Click(Sender);
end;
//*********************************************************************
//计算合同总额
procedure GetMoney( PID : Integer ;var AMoney : Currency);
var
  Query: TADOQuery;
  Caption : string;
  Code : string;
  parent : Integer;
  sum_Money:Currency;
  szPID : integer;
  
begin
  Query := TADOQuery.Create(nil);
  try
      Query.Connection := DM.ADOconn;

      Query.SQL.Text := 'Select * from ' + g_TableName + ' where ' + 'PID' + ' =' + IntToStr( PID );
      if Query.Active then
         Query.Close;

      Query.Open;

      while not Query.Eof do
      begin
        parent  := Query.FieldByName('parent').AsInteger;
        sum_Money:= Query.FieldByName('sum_Money').AsCurrency;
        szPID   := Query.FieldByName('PID').AsInteger;
        AMoney   := sum_Money + AMoney;
        GetMoney(parent,AMoney);
        Query.Next;
      end;
  finally
    Query.Free;
  end;

end;

procedure ReckonPactValue( PID : Integer);
var
  Query: TADOQuery;
  Caption : string;
  Code : string;
  parent : Integer;
  sum_Money:Currency;
  szPID : integer;
  szMoney : Currency;
  Money : Currency;

begin
  Query := TADOQuery.Create(nil);
  try
      Query.Connection := DM.ADOconn;

      Query.SQL.Text := 'Select * from ' + g_TableName + ' where ' + 'PID' + ' =' + IntToStr( PID );
      if Query.Active then
         Query.Close;

      Query.Open;

      while not Query.Eof do
      begin
      //  Caption := Query.FieldByName('name').AsString;
        Code    := Query.FieldByName('code').AsString;
        parent  := Query.FieldByName('parent').AsInteger;
        sum_Money:= Query.FieldByName('sum_Money').AsCurrency;
        szPID   := Query.FieldByName('PID').AsInteger;
        if szPID = 0 then
        begin
          szMoney := 0;
          GetMoney(parent,szMoney);
          Money := szMoney + sum_Money;

          with DM.Qry do
          begin
            Close;
            SQL.Clear;
            SQL.Text := 'Select *from t_paymoney where code="' + Code + '"';
            Open;
            if RecordCount <> 0 then
               SQL.Text := Format('Update t_paymoney set sum_money=''%m'' where code="%s"',[ Money,code ])
            else
               SQL.Text := 'Insert into t_paymoney (code,sum_money) values("'+ Code +'",'+ FloatToStr(Money) +')';

            ExecSQL;
          end;

        end;

        Query.Next;

      end;

  finally
    Query.Free;
  end;

end;

//*********************************************************************
procedure TfrmBranch.MenuItem4Click(Sender: TObject);
var
  szIsRport : Boolean;
  szFrxDataset : TfrxDBDataset;

begin
  inherited;
  //报表设计
  frxReport.Clear;
  GetfrxDBDataset(Self.frxGrid3,Self.tvGrid3);
  GetfrxDBDataset(Self.frxWork,Self.tvWorkView);
  GetfrxDBDataset(Self.frxQuantity,Self.tvEngineeringQuantity);
  GetfrxDBDataset(Self.frxDetailed,Self.tvDetailedGrid);

  case Self.RzPageControl3.ActivePageIndex of
    1:
    begin
      //交易记录
      if Sender = Self.Execl1 then
         CxGridToExcel(Self.cxGrid1,'交易记录')
      else
      begin
        szIsRport := IsRport(Self.qryGrid3);
        if szIsRport then
        begin
          frxReport.LoadFromFile(g_ReportPath + '\' + Self.frxGrid3.UserName + '.fr3');
        end;
      end;
    end;
    0:
    begin
      //帐目总价
      case Self.RzPageControl4.ActivePageIndex of
        0:
        begin
          //考勤
          if Sender = Self.Execl1 then
            CxGridToExcel(Self.cxGrid4,'交易记录')
          else
          begin
            szIsRport := IsRport(Self.qryGrid3);
            if szIsRport then
            begin
              frxReport.LoadFromFile(g_ReportPath + '\' + Self.frxWork.UserName + '.fr3');
            end;
          end;
        end;
        1:
        begin
          if Sender = Self.Execl1 then
          else
        end;
        2:
        begin
          if Sender = Self.Execl1 then
          else
        end;
        3:
        begin
          if Sender = Self.Execl1 then
          else
        end;
        4:
        begin
          //项目分包
          if Sender = Self.Execl1 then
            CxGridToExcel(Self.cxGrid4,'支款项目分包')
          else
          begin
            szIsRport := IsRport(Self.ADOQuantity);
            if szIsRport then
            begin
              frxReport.LoadFromFile(g_ReportPath + '\' + Self.frxQuantity.UserName + '.fr3');
            end;
          end;
        end;
        5:
        begin
          if Sender = Self.Execl1 then
          else
        end;
        6:
        begin
          if Sender = Self.Execl1 then
          else
        end;
        7:
        begin
          if Sender = Self.Execl1 then
          else
        end;
        8:
        begin
          if Sender = Self.Execl1 then
          else
        end;
        9:
        begin
          if Sender = Self.Execl1 then
          else
        end;
      end;
    end;

  end;
  if szIsRport then
  begin
      if Sender = Self.MenuItem4 then
      begin
        frxReport.DesignReport();
      end else
      if Sender = Self.MenuItem5 then
      begin
        frxReport.ShowReport();
      end else
      if Sender = Self.MenuItem6 then
      begin
        frxReport.Print;
      end;
  end;

end;

procedure TfrmBranch.MenuItem5Click(Sender: TObject);
begin
  inherited;
  //打印预览
  case Self.RzPageControl3.ActivePageIndex of
    1:
    begin
      //交易记录

    end;
    0:
    begin
      //帐目总价
      case Self.RzPageControl4.ActivePageIndex of
        0:
        begin
          //考勤
        end;
        1:
        begin

        end;
        2:
        begin

        end;
        3:
        begin

        end;
        4:
        begin

        end;
        5:
        begin

        end;
        6:
        begin

        end;
        7:
        begin

        end;
        8:
        begin

        end;
        9:
        begin

        end;
      end;
    end;
  end;

end;

procedure TfrmBranch.MenuItem6Click(Sender: TObject);
begin
  inherited;
  //直接打印
  case Self.RzPageControl3.ActivePageIndex of
      1:
      begin
        //交易记录

      end;
      0:
      begin
        //帐目总价
        case Self.RzPageControl4.ActivePageIndex of
          0:
          begin
            //考勤
          end;
          1:
          begin

          end;
          2:
          begin

          end;
          3:
          begin

          end;
          4:
          begin

          end;
          5:
          begin

          end;
          6:
          begin

          end;
          7:
          begin

          end;
          8:
          begin

          end;
          9:
          begin

          end;
        end;
      end;
  end;

end;

procedure TfrmBranch.ProjectCol1GetDisplayText(Sender: TcxCustomGridTableItem;
  ARecord: TcxCustomGridRecord; var AText: string);
begin
  inherited;
  AText := IntToStr(ARecord.Index +1 );
end;

end.
