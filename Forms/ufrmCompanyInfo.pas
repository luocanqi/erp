unit ufrmCompanyInfo;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxTextEdit,
  cxDBLookupComboBox, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid, RzButton, RzPanel,
  Vcl.ExtCtrls,UnitADO, Datasnap.DBClient, Vcl.Menus,ufrmBaseController;

type
  TfrmCompanyInfo = class(TfrmBaseController)
    RzToolbar2: TRzToolbar;
    RzSpacer12: TRzSpacer;
    RzToolButton10: TRzToolButton;
    RzSpacer13: TRzSpacer;
    RzToolButton11: TRzToolButton;
    RzSpacer14: TRzSpacer;
    RzToolButton12: TRzToolButton;
    RzSpacer15: TRzSpacer;
    RzToolButton13: TRzToolButton;
    cxGrid1: TcxGrid;
    tvCompany: TcxGridDBTableView;
    tvCompanyColumn1: TcxGridDBColumn;
    tvCompanyColumn2: TcxGridDBColumn;
    cxGridLevel1: TcxGridLevel;
    Company: TClientDataSet;
    CompanySource: TDataSource;
    RzSpacer1: TRzSpacer;
    RzToolButton1: TRzToolButton;
    pm: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    procedure RzToolButton10Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure RzToolButton13Click(Sender: TObject);
    procedure RzToolButton11Click(Sender: TObject);
    procedure RzToolButton1Click(Sender: TObject);
    procedure RzToolButton12Click(Sender: TObject);
    procedure tvCompanyColumn2PropertiesCloseUp(Sender: TObject);
    procedure tvCompanyEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure tvCompanyDblClick(Sender: TObject);
    procedure CompanyAfterInsert(DataSet: TDataSet);
    procedure tvCompanyColumn1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
  private
    { Private declarations }
    dwADO : TADO;
    dwCompanySqlText : string;
  public
    { Public declarations }
    dwTableName : string;
    dwCompanyName : string;
  end;

var
  frmCompanyInfo: TfrmCompanyInfo;

implementation

uses
   global,uDataModule,ufunctions;

{$R *.dfm}

procedure TfrmCompanyInfo.CompanyAfterInsert(DataSet: TDataSet);
var
  parent : Integer;
begin
  with DataSet do
  begin
    parent := DM.getLastId( dwTableName );
    FieldByName('parent').Value := Parent;
    FieldByName('Code').Value   := GetRowCode( dwTableName ,'Code');
  end;
end;

procedure TfrmCompanyInfo.FormActivate(Sender: TObject);
begin
  dwCompanySqlText := 'Select * from ' + dwTableName + ' Where PID=0';
  dwADO.OpenSQL(Self.Company,dwCompanySqlText); //读取公司
end;

procedure TfrmCompanyInfo.RzToolButton10Click(Sender: TObject);
begin
  dwADO.ADOAppend(Self.Company);
  Self.tvCompanyColumn2.FocusWithSelection;
end;

procedure TfrmCompanyInfo.RzToolButton11Click(Sender: TObject);
begin
  dwADO.ADOIsEdit(Self.Company);
end;

procedure TfrmCompanyInfo.RzToolButton12Click(Sender: TObject);
var
  szCodeList : string;
  szColName  : string;
  szTableList: array[0..4] of TTableList;
  szCode : string;
  szParent: Integer;
begin
  inherited;
  //删除
  if MessageBox(handle, '是否删除公司节点？', '提示', MB_ICONQUESTION
     + MB_YESNO) = IDYES then
  begin
    with Company do
    begin
      if not IsEmpty then
      begin
        szCode   := FieldByName('Code').AsString;
        szParent := FieldByName('parent').AsInteger;
      end;

    end;
    DM.getTreeCode(g_Table_Company_StorageTree, szParent,szCodeList); //获取要删除的id表

    szTableList[0].ATableName := g_Table_Company_StorageBillList;
    szTableList[0].ADirectory := '';

    szTableList[1].ATableName := g_Table_Company_StorageDetailed;
    szTableList[1].ADirectory := '';

    szTableList[2].ATableName := g_Table_Company_Storage_ContractList;
    szTableList[2].ADirectory := '';

    szTableList[3].ATableName := g_Table_Company_Storage_ContractDetailed;
    szTableList[3].ADirectory := '';

    szTableList[4].ATableName := g_Table_Company_Storage_GroupUnitPrice;
    szTableList[4].ADirectory := '';

    DeleteTableFile(szTableList,szCodeList);
    DM.InDeleteData(g_Table_Company_StorageTree,szCodeList);
    Self.FormActivate(Sender);
  end;
end;

procedure TfrmCompanyInfo.RzToolButton13Click(Sender: TObject);
begin
  dwADO.ADOSaveData(Self.Company,dwCompanySqlText);
end;

procedure TfrmCompanyInfo.RzToolButton1Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmCompanyInfo.tvCompanyColumn1GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  AText := IntToStr(ARecord.Index +1);
end;

procedure TfrmCompanyInfo.tvCompanyColumn2PropertiesCloseUp(Sender: TObject);
var
  szColContentA : Variant;

  szRowIndex:Integer;
  szColName : string;
  DD1 : string;
  i : Integer;
  szIsExist:Boolean;

begin
  inherited;
  with (Sender AS TcxLookupComboBox).Properties.Grid do
  begin
    szRowIndex := FocusedRowIndex;

    if szRowIndex >= 0 then
    begin
      szColContentA :=  DataController.Values[szRowIndex,0] ; //名称

      szIsExist  := False;
      with tvCompany do
      begin

        for I := 0 to ViewData.RowCount-1 do
        begin
          DD1 := VarToStr( ViewData.Rows[i].Values[tvCompanyColumn2.Index] ); //姓名
          if ( DD1 = VarToStr(szColContentA)) and (i <> Controller.FocusedRowIndex) then
          begin
            szIsExist := True;
            tvCompanyColumn2.EditValue := null;
            tvCompanyColumn2.Editing;
            Application.MessageBox(PWideChar( VarToStr(szColContentA) + ' 已经存在' ), '？', MB_OKCANCEL + MB_ICONWARNING);
            Break;
          end;
        end;

      end;

    end;
  end;

end;

procedure TfrmCompanyInfo.tvCompanyDblClick(Sender: TObject);
begin
  dwCompanyName := tvCompanyColumn2.EditValue;
  Close;
end;

procedure TfrmCompanyInfo.tvCompanyEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  inherited;
  dwADO.ADOIsEditing(Company,AAllow);
end;

end.


