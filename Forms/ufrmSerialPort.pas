unit ufrmSerialPort;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, SPComm, Vcl.StdCtrls, Vcl.ExtCtrls;

type
  TfrmSerialPort = class(TForm)
    grpComSetting: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    cmbCommName: TComboBox;
    cmbBaudrate: TComboBox;
    cmbDatabits: TComboBox;
    cmbStopbits: TComboBox;
    cmbParity: TComboBox;
    grpError: TGroupBox;
    MmoError: TMemo;
    grpReceived: TGroupBox;
    mmoReceived: TMemo;
    grpSend: TGroupBox;
    mmoSend: TMemo;
    pnlCommand: TPanel;
    btnOpen: TButton;
    btnClose: TButton;
    btnSend: TButton;
    procedure FormCreate(Sender: TObject);
    procedure btnOpenClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
    FComm: TComm;
    function GetByteSize(AIndex: Integer): TByteSize;
    function GetParity(AIndex: Integer): TParity;
    function GetStopBits(AIndex: Integer): TStopBits;
    procedure ReceiveData(Sender: TObject; Buffer: Pointer; BufferLength: Word);
    procedure ReceiveError(Sender: TObject; EventMask: DWORD);
    procedure ToggleEnabled;
    procedure CreateCommPorts;
  public
    { Public declarations }
  end;

var
  frmSerialPort: TfrmSerialPort;

implementation

{$R *.dfm}

//串口区域

function TfrmSerialPort.GetByteSize(AIndex: Integer): TByteSize;
begin
  Result := _8;
  case AIndex of
    0: Result := _5;
    1: Result := _6;
    2: Result := _7;
    3: Result := _8;
  end;
end;

function TfrmSerialPort.GetParity(AIndex: Integer): TParity;
begin
  Result := None;
  case AIndex of
    0: Result := None;
    1: Result := Odd;
    2: Result := Even;
  end;
end;

function TfrmSerialPort.GetStopBits(AIndex: Integer): TStopBits;
begin
  Result := _1;
  case AIndex of
    0: Result := _1;
    1: Result := _1_5;
    2: Result := _2;
  end;
end;

procedure TfrmSerialPort.ReceiveData(Sender: TObject; Buffer: Pointer; BufferLength: Word);
var
  S: AnsiString;
begin
  SetLength(S, BufferLength);
  Move(Buffer^, PAnsiChar(S)^, BufferLength);
  ShowMessage(s);
//  mmoReceived.Lines.Add(S);
end;

// EventMask取值: 参见 http://bbs.csdn.net/topics/60311989
procedure TfrmSerialPort.ReceiveError(Sender: TObject; EventMask: DWORD);
var
  S: string;
begin
  s := Format('OnReceiveError: EventMask=%d', [EventMask]);
  case EventMask of
    CE_BREAK:     // 硬件检测到有个终端条件. (现在不支持了)
      S := '硬件检测到有个终止条件.';
    CE_DNS:       // 仅用于win95: 没有选择相应的驱动.
      S := '仅用于win95: 没有选择相应的驱动.';
    CE_FRAME:     // 检测到有个侦差错.
      S := '检测到有个侦差错.';
    CE_IOE:       // 设备通信中出现一个I/O错误.
      S := '设备通信中出现一个I/O错误.';
    CE_MODE:      // 要求的模式不支持, 或hFile 句柄的参数是非法的。
      S := '要求的模式不支持, 或hFile 句柄的参数是非法的。';
    CE_OOP:       // 仅用于win95: 相应的驱动超出了文件的范围。
      S := '仅用于win95: 相应的驱动超出了文件的范围。';
    CE_OVERRUN:   // 缓冲区字符溢出，有数据丢失。
      S := '缓冲区字符溢出，有数据丢失。';
    CE_PTO:       // 仅用于win95: 有相应设备使用时间事件超时.
      S := '仅用于win95: 有相应设备使用时间事件超时.';
    CE_RXOVER:    // 输入缓冲区字符溢出，或在收到文件接收结束表示符后，又接到字符。
      S := '输入缓冲区字符溢出，或在收到文件接收结束表示符后，又接到字符。';
    CE_RXPARITY:  // 奇偶检验错误。
      S := '奇偶检验错误。';
    CE_TXFULL:    // 在输出缓冲区已满的情况下，尝试输出字符。
      S := '在输出缓冲区已满的情况下，尝试输出字符。';
  end;
  ShowMessage(s);
end;

procedure TfrmSerialPort.btnOpenClick(Sender: TObject);
begin
  with FComm do
  begin
    try
      CommName := cmbCommName.Text;
      Parity := GetParity(cmbParity.ItemIndex);
      StopBits := GetStopBits(cmbStopbits.ItemIndex);
      ByteSize := GetByteSize(cmbDatabits.ItemIndex);
      BaudRate := StrToIntDef(cmbBaudrate.Text, 9600);
      ParityCheck := True;

      OnReceiveError := ReceiveError;
      OnReceiveData  := ReceiveData;

      StopComm;
      StartComm;

      ToggleEnabled;
    except
      on E: Exception do
      begin
        mmoError.Lines.Add(E.Message)
      end;
    end;
  end;
end;


procedure TfrmSerialPort.CreateCommPorts;
var
  I: Integer;
begin

  with cmbCommName do
  begin
    Items.Clear;
    for I := 1 to 254 do
      Items.Add(Format('COM%d:', [I]));

    ItemIndex := 0;
  end;

end;


procedure TfrmSerialPort.ToggleEnabled;
var
  ACanSend: Boolean;
begin
  ACanSend := FComm.Connected;

  grpComSetting.Enabled := not ACanSend;
  btnOpen.Enabled       := not ACanSend;
  btnClose.Enabled      := ACanSend;
  btnSend.Enabled       := ACanSend;

end;

procedure TfrmSerialPort.FormCreate(Sender: TObject);
begin
  //串口区域
  FComm := TComm.Create(nil);
  CreateCommPorts;
  ToggleEnabled;
end;

procedure TfrmSerialPort.FormDestroy(Sender: TObject);
begin
  FreeAndNil(FComm);
end;

end.
