unit ufrmCustomerSummary;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, dxmdaset, cxGridCardView, cxGridDBCardView, cxGridCustomLayoutView,
  cxTL, cxTextEdit, cxTLdxBarBuiltInMenu, cxVGrid, cxDBVGrid,
  cxInplaceContainer, cxTLData, cxContainer, cxLabel, cxCurrencyEdit, cxDBEdit,
  cxGroupBox, Vcl.StdCtrls, Vcl.Samples.Spin, cxMaskEdit, cxSpinEdit, cxDBLabel,
  Vcl.ExtCtrls, RzPanel, RzButton, cxMemo, Vcl.Menus, Datasnap.DBClient , UnitADO,
  cxButtonEdit,System.StrUtils,ufrmBaseController;

type
  TfrmCustomerSummary = class(TfrmBaseController)
    RzPanel1: TRzPanel;
    Bevel2: TBevel;
    RzPanel2: TRzPanel;
    tvView: TcxGridDBTableView;
    Lv: TcxGridLevel;
    Grid: TcxGrid;
    RzToolbar1: TRzToolbar;
    RzSpacer1: TRzSpacer;
    RzToolButton1: TRzToolButton;
    RzSpacer2: TRzSpacer;
    RzToolButton2: TRzToolButton;
    RzSpacer3: TRzSpacer;
    RzToolButton3: TRzToolButton;
    RzPanel21: TRzPanel;
    RzPanel22: TRzPanel;
    RzPanel24: TRzPanel;
    RzPanel25: TRzPanel;
    RzPanel29: TRzPanel;
    RzPanel32: TRzPanel;
    RzPanel33: TRzPanel;
    tvViewColumn1: TcxGridDBColumn;
    tvViewColumn2: TcxGridDBColumn;
    tvViewColumn3: TcxGridDBColumn;
    tvViewColumn4: TcxGridDBColumn;
    RzPanel17: TRzPanel;
    RzPanel18: TRzPanel;
    RzPanel19: TRzPanel;
    RzPanel20: TRzPanel;
    RzPanel72: TRzPanel;
    RzPanel73: TRzPanel;
    RzPanel74: TRzPanel;
    RzPanel75: TRzPanel;
    RzPanel76: TRzPanel;
    RzPanel77: TRzPanel;
    RzPanel78: TRzPanel;
    RzPanel79: TRzPanel;
    RzPanel80: TRzPanel;
    RzPanel81: TRzPanel;
    RzPanel82: TRzPanel;
    RzPanel83: TRzPanel;
    RzPanel84: TRzPanel;
    RzPanel85: TRzPanel;
    RzPanel86: TRzPanel;
    RzPanel87: TRzPanel;
    RzPanel88: TRzPanel;
    RzPanel89: TRzPanel;
    RzPanel90: TRzPanel;
    RzPanel91: TRzPanel;
    RzPanel92: TRzPanel;
    RzPanel93: TRzPanel;
    RzPanel94: TRzPanel;
    RzPanel95: TRzPanel;
    RzPanel96: TRzPanel;
    RzPanel97: TRzPanel;
    RzPanel98: TRzPanel;
    RzPanel99: TRzPanel;
    RzPanel100: TRzPanel;
    RzPanel101: TRzPanel;
    RzPanel102: TRzPanel;
    RzPanel103: TRzPanel;
    RzPanel60: TRzPanel;
    RzPanel61: TRzPanel;
    RzPanel62: TRzPanel;
    RzPanel104: TRzPanel;
    RzPanel105: TRzPanel;
    RzPanel106: TRzPanel;
    RzPanel107: TRzPanel;
    RzPanel108: TRzPanel;
    RzPanel109: TRzPanel;
    RzPanel110: TRzPanel;
    RzPanel111: TRzPanel;
    RzPanel112: TRzPanel;
    RzPanel113: TRzPanel;
    RzPanel114: TRzPanel;
    RzPanel115: TRzPanel;
    RzPanel65: TRzPanel;
    RzPanel66: TRzPanel;
    RzPanel67: TRzPanel;
    RzPanel68: TRzPanel;
    RzPanel69: TRzPanel;
    RzPanel70: TRzPanel;
    RzPanel71: TRzPanel;
    RzPanel116: TRzPanel;
    RzSpacer4: TRzSpacer;
    RzToolButton4: TRzToolButton;
    RzPanel63: TRzPanel;
    RzPanel151: TRzPanel;
    RzPanel59: TRzPanel;
    RzPanel117: TRzPanel;
    RzPanel120: TRzPanel;
    RzPanel58: TRzPanel;
    RzPanel130: TRzPanel;
    RzPanel131: TRzPanel;
    RzPanel132: TRzPanel;
    RzPanel138: TRzPanel;
    RzPanel139: TRzPanel;
    RzPanel140: TRzPanel;
    RzPanel141: TRzPanel;
    RzPanel142: TRzPanel;
    RzPanel143: TRzPanel;
    RzPanel144: TRzPanel;
    RzPanel145: TRzPanel;
    RzSpacer5: TRzSpacer;
    RzToolButton5: TRzToolButton;
    RzPanel64: TRzPanel;
    RzPanel3: TRzPanel;
    RzPanel5: TRzPanel;
    RzPanel6: TRzPanel;
    RzPanel7: TRzPanel;
    RzPanel8: TRzPanel;
    RzPanel9: TRzPanel;
    RzPanel10: TRzPanel;
    RzPanel11: TRzPanel;
    RzPanel12: TRzPanel;
    RzPanel13: TRzPanel;
    RzPanel14: TRzPanel;
    RzPanel15: TRzPanel;
    RzPanel16: TRzPanel;
    RzPanel23: TRzPanel;
    RzPanel27: TRzPanel;
    RzPanel57: TRzPanel;
    RzPanel119: TRzPanel;
    RzPanel125: TRzPanel;
    RzPanel126: TRzPanel;
    RzPanel26: TRzPanel;
    RzPanel46: TRzPanel;
    RzPanel47: TRzPanel;
    RzPanel48: TRzPanel;
    RzPanel49: TRzPanel;
    RzPanel28: TRzPanel;
    RzPanel31: TRzPanel;
    RzPanel34: TRzPanel;
    RzPanel35: TRzPanel;
    RzPanel36: TRzPanel;
    RzPanel37: TRzPanel;
    RzPanel38: TRzPanel;
    RzPanel39: TRzPanel;
    RzPanel40: TRzPanel;
    RzPanel41: TRzPanel;
    RzPanel42: TRzPanel;
    RzPanel43: TRzPanel;
    RzPanel44: TRzPanel;
    RzPanel30: TRzPanel;
    RzPanel45: TRzPanel;
    RzPanel50: TRzPanel;
    RzPanel51: TRzPanel;
    RzPanel52: TRzPanel;
    RzPanel53: TRzPanel;
    RzSpacer6: TRzSpacer;
    RzToolButton6: TRzToolButton;
    RzPanel54: TRzPanel;
    RzPanel55: TRzPanel;
    RzPanel56: TRzPanel;
    RzPanel127: TRzPanel;
    RzPanel128: TRzPanel;
    RzPanel129: TRzPanel;
    RzPanel146: TRzPanel;
    RzPanel147: TRzPanel;
    RzPanel148: TRzPanel;
    RzPanel149: TRzPanel;
    dsSounceInfo: TDataSource;
    dsClientInfo: TClientDataSet;
    cxDBSpinEdit1: TcxDBSpinEdit;
    cxDBSpinEdit4: TcxDBSpinEdit;
    dsDataMaster: TDataSource;
    dsMaster: TClientDataSet;
    cxCurrencyEdit2: TcxCurrencyEdit;
    RzPanel133: TRzPanel;
    RzPanel134: TRzPanel;
    RzPanel135: TRzPanel;
    RzPanel136: TRzPanel;
    RzPanel137: TRzPanel;
    RzPanel150: TRzPanel;
    RzPanel152: TRzPanel;
    cxDBSpinEdit2: TcxDBSpinEdit;
    RzPanel153: TRzPanel;
    RzPanel118: TRzPanel;
    RzPanel121: TRzPanel;
    RzPanel122: TRzPanel;
    RzPanel123: TRzPanel;
    RzPanel124: TRzPanel;
    cxDBSpinEdit7: TcxDBSpinEdit;
    RzPanel154: TRzPanel;
    RzPanel155: TRzPanel;
    RzPanel156: TRzPanel;
    RzPanel157: TRzPanel;
    cxDBMemo1: TcxDBMemo;
    cxCurrencyEdit5: TcxCurrencyEdit;
    cxCurrencyEdit6: TcxCurrencyEdit;
    cxDBCurrencyEdit1: TcxDBCurrencyEdit;
    cxDBCurrencyEdit2: TcxDBCurrencyEdit;
    tvViewColumn5: TcxGridDBColumn;
    RzPanel4: TRzPanel;
    cxDBCurrencyEdit3: TcxDBCurrencyEdit;
    cxDBCurrencyEdit4: TcxDBCurrencyEdit;
    cxDBCurrencyEdit5: TcxDBCurrencyEdit;
    tmr: TTimer;
    pm: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    tvViewColumn6: TcxGridDBColumn;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    N7: TMenuItem;
    N8: TMenuItem;
    procedure tvViewColumn1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure FormActivate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure RzToolButton6Click(Sender: TObject);
    procedure RzToolButton4Click(Sender: TObject);
    procedure cxDBSpinEdit4PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure cxDBSpinEdit4PropertiesEditValueChanged(Sender: TObject);
    procedure cxDBSpinEdit1PropertiesEditValueChanged(Sender: TObject);
    procedure cxDBCurrencyEdit1PropertiesEditValueChanged(Sender: TObject);
    procedure cxDBSpinEdit2PropertiesEditValueChanged(Sender: TObject);
    procedure cxDBSpinEdit7PropertiesEditValueChanged(Sender: TObject);
    procedure cxDBCurrencyEdit2PropertiesEditValueChanged(Sender: TObject);
    procedure RzToolButton1Click(Sender: TObject);
    procedure tvViewGetCellHeight(Sender: TcxCustomGridTableView;
      ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem;
      ACellViewInfo: TcxGridTableDataCellViewInfo; var AHeight: Integer);
    procedure tvViewTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
      Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
      var AText: string);
    procedure tvViewDataControllerSummaryAfterSummary(ASender: TcxDataSummary);
    procedure dsMasterAfterInsert(DataSet: TDataSet);
    procedure tvViewEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure RzToolButton3Click(Sender: TObject);
    procedure tvViewKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure tvViewColumn3PropertiesEditValueChanged(Sender: TObject);
    procedure tvViewColumn3PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure RzToolButton2Click(Sender: TObject);
    procedure tvViewEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure tmrTimer(Sender: TObject);
    procedure cxDBCurrencyEdit5PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure RzToolButton5Click(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure tvViewStylesGetContentStyle(Sender: TcxCustomGridTableView;
      ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem;
      var AStyle: TcxStyle);
    procedure N4Click(Sender: TObject);
  private
    { Private declarations }
    dwADO : TADO;
    dwSmall : Integer;
    dwSumCheckSQLText : string;
    dwInfoSQLText : string;
    procedure GetSumCost();
    procedure DesignerCalcBonus();
    procedure MarketingCalcBonus();
    procedure cxGridViewSetFocus(FieldName: string; GridView: TcxCustomGridTableView);
    procedure UpdataValue(lpFielName : string ; lpValue : Currency);
  public
    { Public declarations }
    dwListSummary : Currency;
    dwRebateSummary : Currency;
    dwManpowerSummary:Currency;
    dwMaterialSummary:Currency;
    dwTaxGold:Currency;
    dwSumMoney:Currency;
    dwAfterDismantSummary:Currency;
    dwLaborCost:Currency;
    dwCostSummary:Currency;
    dwAcreage : Double;
    dwDesignCost: Currency;
    dwNodeIndex : Integer;
    dwManpowerCost : Currency;
  end;


var
  frmCustomerSummary: TfrmCustomerSummary;

implementation

{$R *.dfm}

uses
   uDataModule,
   global,
   System.Math,
   cxGridDBDataDefinitions,
   uGUID,
   UnitExpCalc;

procedure TfrmCustomerSummary.UpdataValue(lpFielName : string ; lpValue : Currency);
begin
  with dsClientInfo do
  begin
    if State <> dsInactive then
    begin
      Edit;
      FieldByName(lpFielName).Value := lpValue;
      {
      if (State <> dsEdit) or (State <> dsInsert) then
      begin
        POST;
      end;
      }
    end;
  end;
end;

procedure TfrmCustomerSummary.cxGridViewSetFocus(FieldName: string; GridView: TcxCustomGridTableView);
var
  cxGrid: TcxGrid;
  AItem: TcxCustomGridTableItem;
  AData : TcxGridDataController;
begin
  cxGrid := TcxGrid(GridView.Control);
  if cxGrid.ActiveLevel <> GridView.Level then
    cxGrid.ActiveLevel := TcxGridLevel(GridView.Level);
  if (cxGrid.Focused=False) and (cxGrid.CanFocus) then
    cxGrid.SetFocus;

  with TcxGridDBDataController(GridView.DataController) do
  begin
    AItem := GetItemByFieldName(FieldName);
    if AItem <> nil then
    begin
      if AItem.Visible = False then
        AItem.Visible := True;
      AItem.Focused := True;
      GridView.Controller.EditingController.ShowEdit(AItem);
    end;

  end;

end;

procedure TfrmCustomerSummary.DesignerCalcBonus();
var DD1 , DD2 , DD3 : Double;
  DD4 : Currency;
  s : string;
  lvFieldName : string;
begin
  //设计师提成计算
  DD1 := cxDBCurrencyEdit1.Value;
  DD2 := cxCurrencyEdit6.Value;
  DD3 := StrToFloatDef( VarTostr( cxDBSpinEdit2.Value ) , 0);

  DD4 := (DD1 + DD2) / 100 * DD3;
  lvFieldName := cxDBCurrencyEdit4.DataBinding.DataField;
  UpdataValue(lvFieldName,DD4);
  GetSumCost;

  if (DD2 <> 0) and (DD3 <> 0) then
  begin
  end;
end;


procedure TfrmCustomerSummary.dsMasterAfterInsert(DataSet: TDataSet);
var
  lvCode : string;
begin
  with DataSet do
  begin
    FieldByName('status').Value:= True;
    FieldByName('tages').Value:= dwNodeIndex ;
    FieldByName('NoId').Value := GetULID(Now) ;
    FieldByName(tvViewColumn5.DataBinding.FieldName).Value := Date;
  end;
end;

procedure TfrmCustomerSummary.MarketingCalcBonus();
var DD1 , DD2 , DD3 : Double;
  DD4 : Currency;
  s : string;
  lvFieldName : string;
begin
  //营销提成计算
  DD1 := cxDBCurrencyEdit2.Value;
  DD2 := cxCurrencyEdit5.Value;
  DD3 := StrToFloatDef( VarTostr( cxDBSpinEdit7.Value ) , 0);
  DD4 := (DD1 + DD2) / 100 * DD3;
  lvFieldName := cxDBCurrencyEdit5.DataBinding.DataField;
  UpdataValue(lvFieldName,DD4);
  GetSumCost;
  if (DD2 <> 0) and (DD3<>0) then
  begin
  end;
end;

procedure TfrmCustomerSummary.N1Click(Sender: TObject);
var
  lvRowRecoud : Integer;
  lvStatus : Boolean;
  I: Integer;
  lvIndex : Integer;

begin
  inherited;
  lvIndex := tvViewColumn6.Index;
  with tvView.Controller do
  begin
   lvRowRecoud := SelectedRowCount ;
    if lvRowRecoud <> 0 then
    begin
      for I := 0 to lvRowRecoud-1 do
      begin
        if Sender = N1 then
          SelectedRows[i].Values[lvIndex] := False  ;

        if Sender = N2 then
          SelectedRows[i].Values[lvIndex] := True;
      end;

    end;
  end;

end;

procedure TfrmCustomerSummary.N4Click(Sender: TObject);
var
  lvDesignCheckStatus: Boolean;
  lvMarketingCheckStatus: Boolean;
begin
  inherited;
//冻结解冻设计师
  with dsClientInfo do
  begin
    if State <> dsInactive then
    begin
      if Sender = N4 then
      begin
        //冻结设计师
        lvDesignCheckStatus := False;
      end;
      if Sender = N5 then
      begin
        //解冻设计师
        lvDesignCheckStatus := True;
      end;
      if Sender = N7 then
      begin
        //冻结营销员
        lvMarketingCheckStatus := False;
      end;
      if Sender = N8 then
      begin
        //解冻营销员
        lvMarketingCheckStatus := True;
      end;
      FieldByName('MarketingCheckStatus').Value := lvMarketingCheckStatus;
      FieldByName('DesignCheckStatus').Value := lvDesignCheckStatus;
    end;
  end;
  if lvDesignCheckStatus then
  begin
    Self.cxDBSpinEdit1.Enabled := True;
    Self.cxDBSpinEdit2.Enabled := True;
  end else
  begin
    Self.cxDBSpinEdit1.Enabled := False;
    Self.cxDBSpinEdit2.Enabled := False;

  end;

  if lvMarketingCheckStatus then
  begin
    Self.cxDBSpinEdit7.Enabled := True;
  end else
  begin
    Self.cxDBSpinEdit7.Enabled := False;
  end;
end;

//成本汇总
procedure TfrmCustomerSummary.GetSumCost();
var
  lvDesignDivideInto  : Currency; //分成
  lvDesignBonus : Currency; //提成
  lvMarketingBonus : Currency;//营销提成
  s : string;
  lvTmpSummary : Currency;
  lvCost : Currency;
  lvDirectCost : Currency;
  lvProfit : Currency;
  lvSummary: Currency;

begin
  lvDirectCost :=0;
  lvDesignDivideInto := 0;
  lvDesignBonus := 0;

  lvDirectCost := cxCurrencyEdit5.Value; //直接费
  lvDesignDivideInto := cxDBCurrencyEdit3.Value;
  lvDesignBonus := cxDBCurrencyEdit4.Value;
  lvMarketingBonus := cxDBCurrencyEdit5.Value;

  lvCost := lvDesignDivideInto +
            lvDirectCost +
            lvDesignBonus +
            lvMarketingBonus +
            dwListSummary;

  RzPanel148.Caption := Format('%2.2m',[ lvCost ]);
  RzPanel149.Caption := MoneyConvert( lvCost );
  //*******************************************************
  //折后利润
  lvTmpSummary := RoundTo( dwAfterDismantSummary - lvCost   ,-2);
  RzPanel80.Caption := Format('%2.2m',[lvTmpSummary]);
  RzPanel83.Caption := MoneyConvert(lvTmpSummary);
  //利润率
  if lvTmpSummary > 0 then
    lvSummary := (lvTmpSummary / dwAfterDismantSummary) * 100
  else
    lvSummary := 0;

  RzPanel73.Caption := Format('%*.*f%%',[3,dwSmall,lvSummary]);
  if lvCost > 0 then
    lvSummary := ( lvCost / dwAfterDismantSummary) * 100
  else
    lvSummary := 0;
  RzPanel20.Caption := Format('%*.*f%%',[3,dwSmall,lvSummary ]);
  //折前利润
  lvTmpSummary := RoundTo( dwSumMoney - lvCost , -2 );
  RzPanel98.Caption := Format('%2.2m',[lvTmpSummary]);
  RzPanel101.Caption:= MoneyConvert(lvTmpSummary);

  if lvTmpSummary > 0 then
    lvSummary := (lvTmpSummary / dwSumMoney) * 100
  else
    lvSummary := 0;

  RzPanel91.Caption := Format('%*.*f%%',[3,dwSmall, lvSummary ]);
  if lvCost > 0 then
    lvSummary := (lvCost / dwSumMoney) * 100
  else
    lvSummary := 0;
  RzPanel89.Caption := Format('%*.*f%%',[3,dwSmall,lvSummary]);
  //********************************************************
end;

procedure TfrmCustomerSummary.cxDBCurrencyEdit1PropertiesEditValueChanged(
  Sender: TObject);
begin
  DesignerCalcBonus;
end;

procedure TfrmCustomerSummary.cxDBCurrencyEdit2PropertiesEditValueChanged(
  Sender: TObject);
begin
  MarketingCalcBonus;
end;

procedure TfrmCustomerSummary.cxDBCurrencyEdit5PropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
  cxDBCurrencyEdit5.EditValue := DisplayValue;
end;

procedure TfrmCustomerSummary.cxDBSpinEdit1PropertiesEditValueChanged(
  Sender: TObject);
var
  i : Currency;
  d : Double;
  s : string;
  lvValue : Variant;
  lvFieldName : string;
begin
  lvValue := TcxDBSpinEdit(Sender).Value ;
  if lvValue <> null then
  begin
    i := cxCurrencyEdit2.Value;
    s := VarToStr( lvValue );
    d := StrToFloatDef(s,0);
  //  cxDBCurrencyEdit3.EditValue := i / 100 * d ;
    lvFieldName := cxDBCurrencyEdit3.DataBinding.DataField;
    UpdataValue(lvFieldName, i / 100 * d );
  end;

end;

procedure TfrmCustomerSummary.cxDBSpinEdit2PropertiesEditValueChanged(
  Sender: TObject);
begin
  DesignerCalcBonus;
end;

procedure TfrmCustomerSummary.cxDBSpinEdit4PropertiesEditValueChanged(
  Sender: TObject);
var
  lvValue : Variant;

  s: string;
  i: Double;
  lvTmpSummary: Currency;
begin
  lvTmpSummary := 0;
  lvValue := TcxCustomTextEdit(Sender).EditValue;
  if lvValue <> null then
  begin
    s := VarToStr( lvValue );
    i:= StrToFloatDef(s,0);
    if i > 0 then lvTmpSummary := (dwAfterDismantSummary / 100 * i);
  end;
  RzPanel110.Caption := Format('%2.2m',[ RoundTo(  lvTmpSummary  ,-2) ]);
  RzPanel113.Caption := MoneyConvert(lvTmpSummary);
end;

procedure TfrmCustomerSummary.cxDBSpinEdit4PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
begin
  Self.cxDBSpinEdit4.EditValue := DisplayValue;
end;

procedure TfrmCustomerSummary.cxDBSpinEdit7PropertiesEditValueChanged(
  Sender: TObject);
begin
  MarketingCalcBonus;
end;

procedure TfrmCustomerSummary.FormActivate(Sender: TObject);
var
  lvTmpSummary , lvDirectCost : Currency;
  lvSQLText : string;
begin
  dwListSummary := 0;
  dwSmall := 2;
  //折前造价
  RzPanel54.Caption := Format('%2.2m',[dwAfterDismantSummary]);
  RzPanel55.Caption := MoneyConvert(dwAfterDismantSummary);
  //折后造价
  RzPanel56.Caption := Format('%2.2m',[dwSumMoney]);
  RzPanel127.Caption := MoneyConvert(dwSumMoney);
  //税金
  RzPanel49.Caption := Format('%2.2m',[dwTaxGold]);
  RzPanel47.Caption := MoneyConvert(dwTaxGold);
  //设计费
  cxCurrencyEdit2.Value := dwDesignCost;
  //平方核算
  RzPanel70.Caption := Format('%f',[dwAcreage]);
  RzPanel69.Caption := Format('%2.2m',[ dwAfterDismantSummary / dwAcreage ]);
  //人工直接费
  lvTmpSummary := dwLaborCost  + dwManpowerCost;  //dwManpowerSummary +
  RzPanel128.Caption := Format('%2.2m',[ lvTmpSummary  ]);
  //人工成本率
  RzPanel126.Caption := Format('%*.*f%%',[3,dwSmall,( lvTmpSummary ) / dwAfterDismantSummary * 100]);

  //材料直接费
  RzPanel53.Caption  := Format('%2.2m',[ dwMaterialSummary ]);

  //材料成本率
  RzPanel125.Caption := Format('%*.*f%%',[3,dwSmall,dwMaterialSummary / dwAfterDismantSummary * 100]);

  //直接费
  lvDirectCost := lvTmpSummary + dwMaterialSummary; // dwManpowerSummary +  dwLaborCost +
  RzPanel146.Caption := Format('%2.2m',[ lvDirectCost ]);
  RzPanel147.Caption := MoneyConvert( lvDirectCost );

  //直接费成本率
  RzPanel23.Caption  := Format('%*.*f%%',[3,dwSmall,lvDirectCost / dwAfterDismantSummary * 100]);

  cxCurrencyEdit6.Value := lvDirectCost;
  cxCurrencyEdit5.Value := lvDirectCost;

  {
  //*******************************************************
  //折后利润
  lvTmpSummary := RoundTo( dwAfterDismantSummary - lvDirectCost   ,-2);
  RzPanel80.Caption := Format('%2.2m',[lvTmpSummary]);
  RzPanel83.Caption := MoneyConvert(lvTmpSummary);

  //利润率
  RzPanel73.Caption := Format('%*.*f%%',[3,lvSmall,lvTmpSummary / dwAfterDismantSummary * 100 ]);
  RzPanel20.Caption := Format('%*.*f%%',[3,lvSmall,lvDirectCost/ dwAfterDismantSummary * 100 ]);

  //折前利润
  lvTmpSummary := RoundTo( dwSumMoney - lvDirectCost , -2 );
  RzPanel98.Caption := Format('%2.2m',[lvTmpSummary]);
  RzPanel101.Caption:= MoneyConvert(lvTmpSummary);

  RzPanel91.Caption := Format('%*.*f%%',[3,lvSmall,lvTmpSummary / dwSumMoney * 100 ]);
  RzPanel89.Caption := Format('%*.*f%%',[3,lvSmall,lvDirectCost/ dwSumMoney * 100]);
  //********************************************************
  }
  tmr.Enabled := True;
end;

procedure TfrmCustomerSummary.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 27 then
    Close;
end;

procedure TfrmCustomerSummary.RzToolButton1Click(Sender: TObject);
begin
  dwADO.ADOAppend(Self.dsMaster);

  cxGridViewSetFocus(tvViewColumn1.DataBinding.FieldName,tvView);
end;

procedure TfrmCustomerSummary.RzToolButton2Click(Sender: TObject);
begin
  dwADO.ADOIsEdit(Self.dsMaster);
end;

procedure TfrmCustomerSummary.RzToolButton3Click(Sender: TObject);
begin
  tvView.Controller.DeleteSelection;
end;

procedure TfrmCustomerSummary.RzToolButton4Click(Sender: TObject);
var
  ErrorCount : Integer;
  lvFieldName: string;

begin
//  cxDBCurrencyEdit3.PostEditValue;
//  cxDBCurrencyEdit5.PostEditValue;
//  cxDBCurrencyEdit2.PostEditValue;

  DM.ADOconn.BeginTrans; //开始事务
  try
    lvFieldName := tvViewColumn1.DataBinding.FieldName;
    if dwADO.DeleteRowEmpty(dsMaster,lvFieldName) then

    DM.ADOQuery1.Close;
    DM.ADOQuery1.SQL.Text := dwInfoSQLText;

    with dsClientInfo do
    begin
      if State in [DsEdit, DSInsert] then Post;

      if ChangeCount > 0 then
      begin
        DM.DataSetProvider1.ApplyUpdates(Delta,0,ErrorCount);
        MergeChangeLog;
      end;
    end;

    if dwADO.UpdateTableData(dwSumCheckSQLText,'NoId',1,dsMaster) then
    begin
      dsMaster.MergeChangeLog;
      ShowMessage('保存成功。');
    end else
    begin
      ShowMessage('保存失败！');
    end;

    DM.ADOconn.Committrans; //提交事务
  except
    on E:Exception do
    begin
      DM.ADOconn.RollbackTrans;           // 事务回滚
    end;
  end;
end;

procedure TfrmCustomerSummary.RzToolButton5Click(Sender: TObject);
begin
  inherited;
  CxGridToExcel(Grid,'明细表');
end;

procedure TfrmCustomerSummary.RzToolButton6Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmCustomerSummary.tmrTimer(Sender: TObject);
var
  lvDesignCheckStatus: Boolean;
  lvMarketingCheckStatus: Boolean;
begin
  tmr.Enabled := False;
  dwInfoSQLText := 'Select * from ' + g_Table_Decorate_OfferInfo + ' Where tages=' + IntToStr(dwNodeIndex);
  dwADO.OpenSQL(dsClientInfo,dwInfoSQLText);
  with dsClientInfo do
  begin
    if State <> dsInactive then
    begin
      lvMarketingCheckStatus := FieldByName('MarketingCheckStatus').Value;
      lvDesignCheckStatus := FieldByName('DesignCheckStatus').Value;
    end;
  end;

  if not lvMarketingCheckStatus then
  begin
    Self.cxDBSpinEdit7.Enabled := False;
  end;

  if not lvDesignCheckStatus then
  begin
    Self.cxDBSpinEdit1.Enabled := False;
    self.cxDBSpinEdit2.Enabled := False;
  end;
  

  dwSumCheckSQLText := 'Select * from ' + g_Table_Decorate_OfferSumCheck + ' Where tages=' + IntToStr(dwNodeIndex);
  dwADO.OpenSQL(dsMaster,dwSumCheckSQLText);
  GetSumCost;
end;

procedure TfrmCustomerSummary.tvViewColumn1GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  AText := IntToStr(ARecord.Index +1);
end;

procedure TfrmCustomerSummary.tvViewColumn3PropertiesEditValueChanged(
  Sender: TObject);
var
  lvValue : string;
  i : Integer;
  f : Double;
begin
  lvValue := FunExpCalc(VarToStr(tvViewColumn3.EditValue),2);
  if TryStrToInt(lvValue,i) or TryStrToFloat(lvValue, f) then
  begin
    tvViewColumn4.EditValue := lvValue;
    tvView.DataController.UpdateData;
  end;
end;


procedure TfrmCustomerSummary.tvViewColumn3PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
begin
  Self.tvViewColumn3.EditValue := DisplayValue;
end;

procedure TfrmCustomerSummary.tvViewDataControllerSummaryAfterSummary(
  ASender: TcxDataSummary);
var
  lvDataController : TcxGridDataController;
  lvView : TcxGridDBTableView;
  AView: TcxGridDBTableView;
  i,j,k:integer;
  bqfsg,sqfsg:real;

begin
//  GetSumCost;
  {
  lvDataController := TcxGridDataController(ASender.DataController);
  lvView := TcxGridDBTableView(lvDataController.GridView);
  for I := 0 to lvView.ViewData.RecordCount-1 do
  begin

  end;
  }
  {
  ADataController := TcxGridDBDataController(ASender.DataController);
  AView := TcxGridDBTableView(ADataController.GridView);
  for I := 0 to ASender.FooterSummaryItems.Count - 1 do
  if (ASender.FooterSummaryItems[I].Kind=sknone) and
    (pos('zjfd',TcxGridDBTableSummaryItem(ASender.FooterSummaryItems[I]).FieldName)>0) then
  begin bqfsg:=0; sqfsg:=0;

    k:=strtoint(copy(TcxGridDBTableSummaryItem(ASender.FooterSummaryItems[I]).FieldName,5,1));

    for j:=0 to ASender.FooterSummaryItems.Count - 1 do
    begin
      if (TcxGridDBTableSummaryItem(ASender.FooterSummaryItems[j]).FieldName='bqfs'+inttostr(k)) and (ASender.FooterSummaryValues[j]<>null) then
      begin
        bqfsg:=ASender.FooterSummaryValues[j];
      end;

      if (TcxGridDBTableSummaryItem(ASender.FooterSummaryItems[j]).FieldName='sntq'+inttostr(k)) and (ASender.FooterSummaryValues[j]<>null) then
         sqfsg:=ASender.FooterSummaryValues[j];
    end;
    if (bqfsg<>0) and (sqfsg<>0) then
    ASender.FooterSummaryValues[I] :=formatfloat('0.00',(bqfsg-sqfsg)/sqfsg*100)+'%';

  end;
  }
end;

procedure TfrmCustomerSummary.tvViewEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  dwADO.ADOIsEditing(dsMaster,AAllow);
end;

procedure TfrmCustomerSummary.tvViewEditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = 13) and (AItem.Index = tvViewColumn4.Index) then
  begin         
    SetFocus;
    RzToolButton1.Click;
  end;
end;

procedure TfrmCustomerSummary.tvViewGetCellHeight(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; ACellViewInfo: TcxGridTableDataCellViewInfo;
  var AHeight: Integer);
begin
  if AHeight < 23 then
  begin
    AHeight := 23;
  end;
end;

procedure TfrmCustomerSummary.tvViewKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
    13:
    begin

    end;
    46:
    begin
      RzToolButton3.Click;
    end;  
  end;
end;

procedure TfrmCustomerSummary.tvViewStylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
var
  lvRowValue : Variant;

begin
  inherited;
  lvRowValue := ARecord.Values[tvViewColumn6.Index];
  if lvRowValue<>null then
  begin
    if lvRowValue = False then
      AStyle := DM.cxStyle222;
  end;
end;

procedure TfrmCustomerSummary.tvViewTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
  Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
  var AText: string);
var
  s : string;
begin
  if AValue <> null then
  begin
    s := VarToStr(AValue);
    dwListSummary := StrToCurrDef(s,0);
  end else
  begin
  //  GetSumCost;
  end;
  GetSumCost;
end;

end.
