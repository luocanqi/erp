unit ufrmSection;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Data.Win.ADODB, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData,
  cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, cxTextEdit, RzButton, RzPanel,
  Vcl.ExtCtrls,ufrmBaseController;

type
  TfrmSection = class(TfrmBaseController)
    ADOQuery1: TADOQuery;
    DataSource1: TDataSource;
    tvSection: TcxGridDBTableView;
    Lv: TcxGridLevel;
    Grid: TcxGrid;
    tvSectionColumn1: TcxGridDBColumn;
    tvSectionColumn2: TcxGridDBColumn;
    RzToolbar1: TRzToolbar;
    RzSpacer1: TRzSpacer;
    RzToolButton1: TRzToolButton;
    RzSpacer2: TRzSpacer;
    RzToolButton2: TRzToolButton;
    RzSpacer3: TRzSpacer;
    RzToolButton3: TRzToolButton;
    RzSpacer4: TRzSpacer;
    RzToolButton4: TRzToolButton;
    RzPanel7: TRzPanel;
    procedure RzToolButton4Click(Sender: TObject);
    procedure RzToolButton1Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure tvSectionColumn1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure tvSectionEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure tvSectionEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSection: TfrmSection;

implementation

uses
   uDataModule,global;

{$R *.dfm}

procedure TfrmSection.FormActivate(Sender: TObject);
begin
  with Self.ADOQuery1 do
  begin
    Close;
    SQL.Clear;
    SQL.Text := 'Select *from ' + g_Table_Maintain_section;
    Open;

  end;
end;

procedure TfrmSection.RzToolButton1Click(Sender: TObject);
begin
  if Sender = Self.RzToolButton1 then
  begin
    with Self.ADOQuery1 do
    begin
      if State <>  dsInactive then
      begin
        Append;
        Self.tvSectionColumn2.FocusWithSelection;
        keybd_event(VK_RETURN,0,0,0);
      end;
    end;

  end else
  if Sender = Self.RzToolButton2 then
  begin
    IsDeleteEmptyData(Self.ADOQuery1,
                      g_Table_Maintain_section ,
                      Self.tvSectionColumn2.DataBinding.FieldName);

  end else
  if Sender = Self.RzToolButton3 then
  begin
    IsDelete(Self.tvSection);
  end;
end;

procedure TfrmSection.RzToolButton4Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmSection.tvSectionColumn1GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  AText := IntToStr(ARecord.Index + 1);
end;

procedure TfrmSection.tvSectionEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  if AItem.Index = Self.tvSectionColumn1.Index then
  begin
    AAllow := False;
  end;
end;

procedure TfrmSection.tvSectionEditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 13 then
  begin
    if AItem.Index = Self.tvSectionColumn2.Index then
    begin
      if Self.tvSection.Controller.FocusedRow.IsLast then
      begin
           with Self.ADOQuery1 do
          begin
            if State <>  dsInactive then
            begin
              Append;
              Self.tvSectionColumn2.FocusWithSelection;
              keybd_event(VK_RETURN,0,0,0);
            end;

          end;
      end;

    end;

  end;

end;

end.
