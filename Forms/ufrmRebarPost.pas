unit ufrmRebarPost;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, dxSkinsCore, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, cxMaskEdit, cxDropDownEdit, cxTextEdit, StdCtrls,
  RzButton, cxMemo, RzCmboBx, cxCurrencyEdit, Mask, RzEdit, Menus, cxButtons,
  ComCtrls, dxSkinBlueprint, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle,
  dxSkinHighContrast, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinSevenClassic, dxSkinSharpPlus,
  dxSkinTheAsphaltWorld, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint;

type
  TfrmRebarPost = class(TForm)
    GroupBox1: TGroupBox;
    cxTextEdit1: TcxTextEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label8: TLabel;
    RzBitBtn7: TRzBitBtn;
    RzBitBtn17: TRzBitBtn;
    RzBitBtn1: TRzBitBtn;
    cxMemo1: TcxMemo;
    Label6: TLabel;
    Label10: TLabel;
    Label12: TLabel;
    RzComboBox1: TRzComboBox;
    cxCurrencyEdit2: TcxCurrencyEdit;
    cxCurrencyEdit3: TcxCurrencyEdit;
    cxCurrencyEdit4: TcxCurrencyEdit;
    cxCurrencyEdit1: TcxCurrencyEdit;
    cxButton1: TcxButton;
    Label7: TLabel;
    cxCurrencyEdit5: TcxCurrencyEdit;
    Label9: TLabel;
    Label11: TLabel;
    Label13: TLabel;
    cxCurrencyEdit6: TcxCurrencyEdit;
    cxCurrencyEdit7: TcxCurrencyEdit;
    Label14: TLabel;
    Label15: TLabel;
    DateTimePicker1: TDateTimePicker;
    cxTextEdit2: TcxTextEdit;
    Label16: TLabel;
    cxTextEdit3: TcxTextEdit;
    Label17: TLabel;
    Label18: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure RzComboBox1Select(Sender: TObject);
    procedure RzBitBtn1Click(Sender: TObject);
    procedure RzBitBtn17Click(Sender: TObject);
    procedure RzBitBtn7Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure cxCurrencyEdit4PropertiesChange(Sender: TObject);
    procedure cxCurrencyEdit1Exit(Sender: TObject);
    procedure cxCurrencyEdit5PropertiesChange(Sender: TObject);
    procedure cxCurrencyEdit6Exit(Sender: TObject);
    procedure cxCurrencyEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    g_TableName : string;
    g_ProjectDir: string;
    g_IsModify  : Boolean;
    g_PostNumbers : string;
    g_PostCode  : string;
    
  end;

var
  frmRebarPost: TfrmRebarPost;

implementation

uses
   uDataModule,global,Math;

{$R *.dfm}

procedure TfrmRebarPost.cxButton1Click(Sender: TObject);
begin
  if Self.cxCurrencyEdit4.Value = 0 then
  begin
    //单价为 0不能参与计算
    Application.MessageBox('单价为0，计算失败!',m_title,MB_OK + MB_ICONQUESTION )
  end else
  begin
    if Self.cxCurrencyEdit3.Value = 0 then
    begin
       //根数为0
       Application.MessageBox('根数为0，计算失败!',m_title,MB_OK + MB_ICONQUESTION )
    end else
    begin
        if Self.cxCurrencyEdit1.Value = 0 then
        begin
          Application.MessageBox('理论重量/称重为0，计算失败',m_title,MB_OK + MB_ICONQUESTION )
        end else
        begin
          if Self.cxCurrencyEdit7.Value = 0 then
          begin
            //件数
            Application.MessageBox('件数不能为0，计算失败',m_title,MB_OK + MB_ICONQUESTION )
          end
          else
          begin
            Self.cxCurrencyEdit1Exit(Sender);
          end;    

        end;

    end;

  end;

end;

procedure TfrmRebarPost.cxCurrencyEdit1Exit(Sender: TObject);
var
  szWeight    : Double; //重量
  szMeter     : Single;   //米数
  szRootCount : Currency; //根数
  szUnitPrice : Currency; //单价
  szTotal     : Currency; //总价
  szTheoryWeight : Currency; //理论重量
  szPieceCount: Double; //件数
  
begin
  szTheoryWeight:= Self.cxCurrencyEdit1.Value;
  szMeter       := Self.cxCurrencyEdit2.Value;
  szRootCount   := Self.cxCurrencyEdit3.Value;
  szUnitPrice   := Self.cxCurrencyEdit4.Value;
  szPieceCount  := Self.cxCurrencyEdit7.Value;

  if szUnitPrice <> 0 then
  begin
    if szRootCount <> 0 then
    begin
        if szTheoryWeight <> 0 then
        begin

          szWeight := (szTheoryWeight * szMeter * (szRootCount * szPieceCount)) / 1000;
          szWeight := RoundTo(szWeight,-3);
          Self.cxCurrencyEdit6.Value := szWeight;
          szTotal := szWeight * szUnitPrice;
          Self.cxCurrencyEdit5.Value := szTotal;

        end;

    end;

  end;

end;

procedure TfrmRebarPost.cxCurrencyEdit4PropertiesChange(Sender: TObject);
begin
  Self.Label12.Caption := MoneyConvert(Self.cxCurrencyEdit4.Value);
end;

procedure TfrmRebarPost.cxCurrencyEdit5PropertiesChange(Sender: TObject);
begin
  Self.Label11.Caption := MoneyConvert(Self.cxCurrencyEdit5.Value);
end;

procedure TfrmRebarPost.cxCurrencyEdit6Exit(Sender: TObject);
var
  szWeight    : Double; //重量
  szTotal     : Currency; //总价
begin
  szWeight := Self.cxCurrencyEdit6.Value;
  szTotal := szWeight * Self.cxCurrencyEdit4.Value;
  Self.cxCurrencyEdit5.Value := szTotal;
end;

procedure TfrmRebarPost.cxCurrencyEdit6KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 13 then Self.cxCurrencyEdit6Exit(Sender);
  
end;

procedure TfrmRebarPost.FormCreate(Sender: TObject);
var
  i : Integer;
  s : string;

begin
  Self.RzComboBox1.Clear;
  for I := 0 to 15 do
  begin
    Self.RzComboBox1.Items.Add(g_RebarList[i].Model);
  end;
//  GetUnitList(Self.ComboBox3,6);
  Self.Label12.Caption := '';
  Self.Label11.Caption := '';
  Self.DateTimePicker1.DateTime := Now;
end;

procedure TfrmRebarPost.RzBitBtn17Click(Sender: TObject);
var
  szCaption   : string;
  szDiameter  : string; //直径
  szTheoryWeight    : Currency; //理论重量
  szMeter     : Single; //米数
  szRootCount : Currency;//根数
  szUnitPrice : Currency; //单价
  szTotal     : Currency; //总价
  szCompany   : string; //单位
  szRemarks   : string;//备注// Remarks
  s : string;
  szWeight    : Double; //重量
  szPieceCount: Double; //件儿数
  szForGoods  : string; //供货单位
  szInGoodsPeople   :  string; //收货人
begin
  szCaption := Self.cxTextEdit1.Text;
  if Length(szCaption) = 0 then
  begin
    Application.MessageBox('购货单位不能为空!',m_title,MB_OK + MB_ICONQUESTION )
  end else
  begin
    szForGoods := Self.cxTextEdit2.Text;
    if Length(szForGoods) = 0 then
    begin
      Application.MessageBox('供货单位不能为空!',m_title,MB_OK + MB_ICONQUESTION )
    end else
    begin
      szInGoodsPeople := Self.cxTextEdit3.Text;
      if Length(szInGoodsPeople) = 0 then
      begin
        Application.MessageBox('收货人不能为空!',m_title,MB_OK + MB_ICONQUESTION )
      end else
      begin
      
        szDiameter:= Self.RzComboBox1.Text;
       // if Length( szDiameter ) = 0 then
        //begin
        //  Self.RzComboBox1.DroppedDown := True;
        //end else
        //begin

            {
            if szTheoryWeight = 0 then
            begin
              Application.MessageBox('理论重量不能为零，否则不能进行计算!',m_title,MB_OK + MB_ICONQUESTION )
            end else
            begin
               szWeight    := Self.cxCurrencyEdit6.Value; //重量
               if szWeight = 0 then
               begin
                 Application.MessageBox('重量不能为零，请先进行计算!',m_title,MB_OK + MB_ICONQUESTION )
               end else
               begin

                 szRootCount := Self.cxCurrencyEdit3.Value;
                 if szRootCount = 0 then
                 begin
                   Application.MessageBox('根数不能为零，否则不能进行计算!',m_title,MB_OK + MB_ICONQUESTION )
                 end else
                 begin
                   szUnitPrice := Self.cxCurrencyEdit4.Value;
                   if szUnitPrice = 0 then
                   begin
                     Application.MessageBox('单价不能为零，否则不能进行计算!',m_title,MB_OK + MB_ICONQUESTION )
                   end else
                   begin
                      szPieceCount  := Self.cxCurrencyEdit7.Value;
                      if szPieceCount = 0 then
                      begin
                        Application.MessageBox('件数不能为零，否则不能进行计算!',m_title,MB_OK + MB_ICONQUESTION )
                      end else
                      begin
                       }
                       szTheoryWeight  := Self.cxCurrencyEdit1.Value;
                       szWeight        := Self.cxCurrencyEdit6.Value; //重量
                       szRootCount := Self.cxCurrencyEdit3.Value;
                       szUnitPrice := Self.cxCurrencyEdit4.Value;
                       szPieceCount  := Self.cxCurrencyEdit7.Value;
                          szMeter   := Self.cxCurrencyEdit2.Value; //米数
                          szTotal   := Self.cxCurrencyEdit5.Value;
                          szRemarks := Self.cxMemo1.Text;
                          szCompany := '吨';

                          if g_IsModify then
                          begin
                            //编辑
                            s := Format('Update '+ g_TableName +' set chCaption="%s",'      +
                                                                     'chForGoods="%s",'     +
                                                                     'chInGoodsPeople="%s",'+
                                                                     'chDate="%s",'         +
                                                                     'chDiameter="%s",'     +
                                                                     'chWeight=%s,'         +
                                                                     'chTheoryWeight=%s,'   +
                                                                     'chMeter=%s,'          +
                                                                     'chRootCount=%s,'      +
                                                                     'chUnitPrice=%s,'      + //单价
                                                                     'chTotal=%s,'          +  //总价
                                                                     'chCompany="%s",'      + //单位
                                                                     'chPieceCount=%s,'      + //件儿
                                                                     'chRemarks="%s"'       + //备注
                                                                     ' where Numbers="%s"',
                                                                      [
                                                                      szCaption,
                                                                      szForGoods,
                                                                      szInGoodsPeople,
                                                                      FormatDateTime('yyyy-MM-dd',Self.DateTimePicker1.Date),
                                                                      szDiameter,
                                                                      FormatFloat('0.00',szWeight),
                                                                      Formatfloat('0.####',szTheoryWeight),
                                                                      Formatfloat('0.####',szMeter),
                                                                      FormatFloat('0.####',szRootCount),
                                                                      FormatFloat('0.00',szUnitPrice),
                                                                      FormatFloat('0.00',szTotal),
                                                                      szCompany,
                                                                      FormatFloat('0.00',szPieceCount),
                                                                      szRemarks,
                                                                      g_PostNumbers
                                                                       ]);
                            with DM.Qry do
                            begin
                              Close;
                              SQL.Clear;
                              SQL.Text := s;
                              if ExecSQL  <> 0 then
                              begin
                                Application.MessageBox('编辑成功!',m_title,MB_OK + MB_ICONQUESTION )
                              end else
                              begin
                                Application.MessageBox('编辑失败!',m_title,MB_OK + MB_ICONQUESTION )
                              end;  
                            end;

                          end else
                          begin
                           //新增
                            s := Format( 'Insert into '+ g_TableName +' ( code,'+
                                                                         'Numbers,'+
                                                                         'chCaption,'       +
                                                                         'chForGoods,'      +
                                                                         'chInGoodsPeople,' +
                                                                         'chDiameter,'      +
                                                                         'chWeight,'        +
                                                                         'chTheoryWeight,'  +
                                                                         'chMeter,'         +
                                                                         'chRootCount,'     +
                                                                         'chUnitPrice,'     + //单价
                                                                         'chTotal,'         + //总价
                                                                         'chPieceCount,'    + //件儿
                                                                         'chCompany,'       + //单位
                                                                         'chDate,'          + //时间
                                                                         'chRemarks' + //备注
                                                                         ') values("%s","%s","%s","%s","%s","%s",%s,%s,%s,%s,%s,%s,%s,"%s","%s","%s")',[
                                                                         g_PostCode,
                                                                         g_PostNumbers,
                                                                         szCaption,
                                                                         szForGoods,
                                                                         szInGoodsPeople,
                                                                         szDiameter,
                                                                         FormatFloat('0.00',szWeight),
                                                                         Formatfloat('0.####',szTheoryWeight),
                                                                         Formatfloat('0.####',szMeter),
                                                                         FormatFloat('0.####',szRootCount),
                                                                         FormatFloat('0.00',szUnitPrice),
                                                                         FormatFloat('0.00',szTotal),
                                                                         FormatFloat('0.00',szPieceCount),
                                                                         szCompany,
                                                                         FormatDateTime('yyyy-MM-dd',Self.DateTimePicker1.Date),
                                                                         szRemarks
                                                                              ]);


                            OutputLog(s);
                  
                            with DM.Qry do
                            begin
                              Close;
                              SQL.Clear;
                              SQL.Text := s;
                              if ExecSQL  <> 0 then
                              begin
                                g_PostNumbers := DM.getDataMaxDate(g_TableName);
                                Application.MessageBox('新增成功!',m_title,MB_OK + MB_ICONQUESTION )
                              end else
                              begin
                                Application.MessageBox('新增成功!',m_title,MB_OK + MB_ICONQUESTION )
                              end;  
                            end;

                          end;
            {
                      end;

                   end;

                 end;

               end;

            end;
            }

      //  end;

      end;

    end;   

  end;    

end;

procedure TfrmRebarPost.RzBitBtn1Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmRebarPost.RzBitBtn7Click(Sender: TObject);
var
  s : string;
begin
  //打开目录
  s := Concat(g_ProjectDir,'\' + g_PostNumbers);
  CreateOpenDir(Handle,s,True);
end;

procedure TfrmRebarPost.RzComboBox1Select(Sender: TObject);
var
  i : Integer;

begin
  i := Self.RzComboBox1.ItemIndex;
  Self.cxCurrencyEdit1.Text := Formatfloat('0.####',g_RebarList[i].Weight) ;
end;

end.
