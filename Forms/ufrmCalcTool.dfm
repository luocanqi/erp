object frmCalcTool: TfrmCalcTool
  Left = 0
  Top = 0
  BorderStyle = bsToolWindow
  Caption = #24037#20855
  ClientHeight = 326
  ClientWidth = 343
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object RzPanel3: TRzPanel
    Left = 0
    Top = 0
    Width = 343
    Height = 326
    Align = alClient
    BorderOuter = fsNone
    BorderShadow = clBlack
    TabOrder = 1
    ExplicitWidth = 340
    object Bevel1: TBevel
      Left = 10
      Top = 40
      Width = 319
      Height = 2
    end
    object cxGroupBox4: TcxGroupBox
      Left = 10
      Top = 48
      Caption = #38050#31563#35745#31639#22120
      ParentBackground = False
      TabOrder = 2
      Height = 81
      Width = 319
      object Label23: TLabel
        Left = 163
        Top = 25
        Width = 52
        Height = 13
        Caption = #20214#12288#12288#25968':'
      end
      object Label24: TLabel
        Left = 24
        Top = 51
        Width = 47
        Height = 13
        Caption = ' '#21333#26681'/'#31859':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label26: TLabel
        Left = 171
        Top = 51
        Width = 44
        Height = 13
        Caption = #21333#20214'/'#26681':'
      end
      object Label3: TLabel
        Left = 19
        Top = 25
        Width = 52
        Height = 13
        Caption = #38050#31563#35268#26684':'
      end
      object cxSpinEdit1: TcxSpinEdit
        Left = 221
        Top = 22
        Properties.EditFormat = '0.##;-0.##'
        Properties.ValueType = vtFloat
        Properties.OnChange = cxSpinEdit1PropertiesChange
        TabOrder = 1
        Width = 80
      end
      object cxSpinEdit2: TcxSpinEdit
        Left = 77
        Top = 49
        Properties.DisplayFormat = '0.##;-0.##'
        Properties.EditFormat = '0.##;-0.##'
        Properties.ValueType = vtFloat
        Properties.OnChange = cxSpinEdit1PropertiesChange
        TabOrder = 2
        Value = 9.000000000000000000
        Width = 76
      end
      object cxSpinEdit5: TcxSpinEdit
        Left = 221
        Top = 49
        Properties.DisplayFormat = '0.##;-0.##'
        Properties.EditFormat = '0.##;-0.##'
        Properties.ValueType = vtFloat
        Properties.OnChange = cxSpinEdit1PropertiesChange
        TabOrder = 3
        Width = 80
      end
      object cxComboBox1: TcxComboBox
        Left = 77
        Top = 22
        Properties.OnCloseUp = cxComboBox1PropertiesCloseUp
        Style.BorderStyle = ebs3D
        TabOrder = 0
        Width = 72
      end
    end
    object cxGroupBox5: TcxGroupBox
      Left = 10
      Top = 135
      Caption = #25968#37327#35745#31639#22120
      TabOrder = 3
      Height = 122
      Width = 319
      object Label4: TLabel
        Left = 18
        Top = 30
        Width = 52
        Height = 13
        Caption = #35745#31639#20844#24335':'
      end
      object Label2: TLabel
        Left = 30
        Top = 82
        Width = 40
        Height = 13
        Caption = #21464#37327#25968':'
      end
      object Label6: TLabel
        Left = 170
        Top = 82
        Width = 52
        Height = 13
        Caption = #35745#31639#25968#20540':'
      end
      object VariableCount: TcxSpinEdit
        Left = 76
        Top = 79
        Properties.DisplayFormat = '0.#######;-0.#######'
        Properties.EditFormat = '0.#######;-0.#######'
        Properties.HideSelection = False
        Properties.ValueType = vtFloat
        TabOrder = 0
        Width = 88
      end
      object NumericalValue: TcxSpinEdit
        Left = 228
        Top = 79
        Properties.DisplayFormat = '0.#######;-0.#######'
        Properties.EditFormat = '0.#######;-0.#######'
        Properties.ValueType = vtFloat
        TabOrder = 1
        Width = 77
      end
    end
    object cxLabel13: TcxLabel
      Left = 12
      Top = 13
      Caption = #25968#37327':'
    end
    object cxLabel14: TcxLabel
      Left = 159
      Top = 13
      Caption = #23567#25968#20445#30041#20301#25968':'
    end
    object cxComboBox2: TcxComboBox
      Left = 245
      Top = 13
      Properties.Items.Strings = (
        '1'#20301
        '2'#20301
        '3'#20301
        '4'#20301
        '5'#20301
        '6'#20301
        '7'#20301)
      Properties.OnCloseUp = cxComboBox2PropertiesCloseUp
      Style.BorderStyle = ebs3D
      TabOrder = 1
      Text = '3'#20301
      Width = 66
    end
    object cxButton4: TcxButton
      Left = 61
      Top = 278
      Width = 67
      Height = 25
      Caption = #30830#23450
      LookAndFeel.NativeStyle = False
      LookAndFeel.SkinName = 'Office2013White'
      OptionsImage.ImageIndex = 17
      OptionsImage.Images = DM.cxImageList1
      TabOrder = 6
      OnClick = cxButton4Click
    end
    object cxButton5: TcxButton
      Left = 206
      Top = 278
      Width = 67
      Height = 25
      Caption = #21462#28040
      LookAndFeel.NativeStyle = False
      LookAndFeel.SkinName = 'Office2013White'
      OptionsImage.ImageIndex = 51
      OptionsImage.Images = DM.cxImageList1
      TabOrder = 7
      OnClick = cxButton5Click
    end
    object CountTotal: TcxSpinEdit
      Left = 50
      Top = 13
      Properties.DisplayFormat = '0.#######;-0.#######'
      Properties.EditFormat = '0.#######;-0.#######'
      Properties.ValueType = vtFloat
      TabOrder = 0
      Width = 103
    end
  end
  object cxMemo1: TcxMemo
    Left = 86
    Top = 161
    Lines.Strings = (
      'cxMemo1')
    Properties.OnChange = cxMemo1PropertiesChange
    TabOrder = 0
    Height = 47
    Width = 229
  end
end
