object frmContrctList: TfrmContrctList
  Left = 0
  Top = 0
  Caption = #21512#21516#21015#34920
  ClientHeight = 379
  ClientWidth = 798
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Grid: TcxGrid
    Left = 0
    Top = 29
    Width = 798
    Height = 350
    Align = alClient
    TabOrder = 0
    LookAndFeel.NativeStyle = True
    LookAndFeel.SkinName = ''
    object tView: TcxGridDBTableView
      OnDblClick = tViewDblClick
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = MasterSource
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      FilterRow.Visible = True
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.InvertSelect = False
      OptionsSelection.MultiSelect = True
      OptionsSelection.CellMultiSelect = True
      OptionsView.DataRowHeight = 23
      OptionsView.GroupByBox = False
      OptionsView.HeaderHeight = 23
      OptionsView.Indicator = True
      OptionsView.IndicatorWidth = 14
      object tView1: TcxGridDBColumn
        Caption = #24207#21495
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taCenter
        OnGetDisplayText = tView1GetDisplayText
        Width = 40
      end
      object tView7: TcxGridDBColumn
        Caption = #29366#24577
        DataBinding.FieldName = 'status'
        Width = 40
      end
      object tView8: TcxGridDBColumn
        Caption = #32534#21495
        DataBinding.FieldName = 'NoId'
        Visible = False
        Width = 139
      end
      object tView2: TcxGridDBColumn
        Caption = #26085#26399
        DataBinding.FieldName = 'InputDate'
        Width = 82
      end
      object tView3: TcxGridDBColumn
        Caption = #24448#26469#21333#20301
        DataBinding.FieldName = 'SignName'
        Width = 137
      end
      object tView4: TcxGridDBColumn
        Caption = #21512#21516#21517#31216
        DataBinding.FieldName = 'ContractName'
        Width = 132
      end
      object tView5: TcxGridDBColumn
        Caption = #20179#24211#21517#31216
        DataBinding.FieldName = 'StorageName'
        Width = 124
      end
      object tView6: TcxGridDBColumn
        Caption = #22791#27880
        DataBinding.FieldName = 'remarks'
        Width = 180
      end
      object tView9: TcxGridDBColumn
        Caption = #27169#22359#21517
        DataBinding.FieldName = 'ModuleIndex'
        Visible = False
        Width = 60
      end
    end
    object Lv: TcxGridLevel
      GridView = tView
    end
  end
  object RzToolbar4: TRzToolbar
    Left = 0
    Top = 0
    Width = 798
    Height = 29
    AutoStyle = False
    Images = DM.cxImageList1
    BorderInner = fsNone
    BorderOuter = fsNone
    BorderSides = [sdBottom]
    BorderWidth = 0
    GradientColorStyle = gcsCustom
    TabOrder = 1
    VisualStyle = vsGradient
    ToolbarControls = (
      RzSpacer26
      cxLabel1
      cxTextEdit1
      RzSpacer1
      RzToolButton1
      RzSpacer2
      RzToolButton2)
    object RzSpacer26: TRzSpacer
      Left = 4
      Top = 2
    end
    object RzSpacer1: TRzSpacer
      Left = 225
      Top = 2
    end
    object RzToolButton1: TRzToolButton
      Left = 233
      Top = 2
      ImageIndex = 59
      OnClick = RzToolButton1Click
    end
    object RzSpacer2: TRzSpacer
      Left = 258
      Top = 2
    end
    object RzToolButton2: TRzToolButton
      Left = 266
      Top = 2
      Width = 65
      ImageIndex = 8
      ShowCaption = True
      UseToolbarButtonSize = False
      UseToolbarShowCaption = False
      Caption = #36864#20986
      OnClick = RzToolButton2Click
    end
    object cxLabel1: TcxLabel
      Left = 12
      Top = 6
      Caption = #20851#38190#23383#65306
      Transparent = True
    end
    object cxTextEdit1: TcxTextEdit
      Left = 64
      Top = 4
      TabOrder = 1
      OnKeyDown = cxTextEdit1KeyDown
      Width = 161
    end
  end
  object tmr: TTimer
    Interval = 100
    OnTimer = tmrTimer
    Left = 192
    Top = 208
  end
  object Master: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 80
    Top = 208
  end
  object MasterSource: TDataSource
    DataSet = Master
    Left = 80
    Top = 264
  end
end
