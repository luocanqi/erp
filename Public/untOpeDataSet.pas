unit untOpeDataSet;

interface

uses
  SysUtils,db, adodb;

type

  TopeDataSet = class

  public

    {修改已发生，但还没有得到应用的记录，在关闭窗口前应给予提示}

    function adoUpdatesPending(adoDataSet: TCustomAdoDataSet): boolean;

    {增加}

    procedure Append(adq: TadoQuery);

    {保存}

    procedure Save(adq: TadoQuery);

    {删除}

    procedure delete(adq: TadoQuery);

    {刷新数据集}

    procedure flash(adq: TadoQuery);

    {事务提交}

    procedure adqCommit(adq: TadoQuery);

    {查询数据集}

    procedure adqQuery(adq: TadoQuery; s: string);

    {带参查询数据集}

    procedure adqQueryPara(adq: TadoQuery; s: string; s1: variant);

    procedure adqQueryPara2(adq: TadoQuery; s: string; s1: variant; s2: variant);

    procedure adqQueryPara3(adq: TadoQuery; s: string; s1: variant; s2: variant; s3: variant);

    procedure adqQueryPara4(adq: TadoQuery; s: string; s1: variant; s2: variant; s3: variant; s4:       variant);

    {更新数据集}

    procedure adqExec(adq: TadoQuery; s: string);

    {带参更新数据集}

    procedure adqExecPara(adq: TadoQuery; s: string; s1: variant);

    procedure adqExecPara2(adq: TadoQuery; s: string; s1: variant; s2: variant);

    procedure adqExecPara3(adq: TadoQuery; s: string; s1: variant; s2: variant; s3: variant);

    procedure adqExecPara5(adq: TadoQuery; s: string; s1: variant; s2: variant; s3: variant; s4:       variant; s5: variant);

    procedure adqExecPara6(adq: TadoQuery; s: string; s1: variant; s2: variant; s3: variant; s4:       variant; s5: variant; s6: variant);

  end;



implementation

{ TopeDataSet }

function TopeDataSet.adoUpdatesPending(
  adoDataSet: TCustomAdoDataSet): boolean;
var
  clone: TadoDataSet;
begin

  clone := TadoDataSet.create(nil);

  try

    clone.clone(adoDataSet);               //复制

    clone.filterGroup := fgPendingRecords; //已得到修改但还没有得到应用的所有记录

    clone.filtered := true;

    result := not(clone.bof and clone.eof);

    clone.close;

  finally

    clone.free;

  end;

end;



procedure TopeDataSet.adqCommit(adq: TadoQuery);

begin

  try
    adq.Connection.CommitTrans;
  except
    adq.Connection.RollbackTrans;
  end;

end;



procedure TopeDataSet.adqExec(adq: TadoQuery; s: string);

begin

  adq.Close;

  adq.SQL.Clear;

  adq.SQL.Text := s;

  adq.ExecSQL;

end;



procedure TopeDataSet.adqExecPara(adq: TadoQuery; s: string;

  s1: variant);

begin

  adq.Close;

  adq.SQL.Clear;

  adq.SQL.Text := s;

  adq.Parameters[0].Value := s1;

  adq.ExecSQL;

end;



procedure TopeDataSet.adqExecPara2(adq: TadoQuery; s: string; s1,

  s2: variant);

begin

  adq.Close;

  adq.SQL.Clear;

  adq.SQL.Text := s;

  adq.Parameters[0].Value := s1;

  adq.Parameters[1].Value := s2;

  adq.ExecSQL;

end;



procedure TopeDataSet.adqExecPara3(adq: TadoQuery; s: string; s1,

  s2, s3: variant);

begin

  adq.Close;

  adq.SQL.Clear;

  adq.SQL.Text := s;

  adq.Parameters[0].Value := s1;

  adq.Parameters[1].Value := s2;

  adq.Parameters[2].Value := s3;

  adq.ExecSQL;

end;



procedure TopeDataSet.adqExecPara5(adq: TadoQuery; s: string; s1,

  s2, s3, s4, s5: variant);

begin

  adq.Close;

  adq.SQL.Clear;

  adq.SQL.Text := s;

  adq.Parameters[0].Value := s1;

  adq.Parameters[1].Value := s2;

  adq.Parameters[2].Value := s3;

  adq.Parameters[3].Value := s4;

  adq.Parameters[4].Value := s5;

  adq.ExecSQL;

end;



procedure TopeDataSet.adqExecPara6(adq: TadoQuery; s: string; s1,

  s2, s3, s4, s5, s6: variant);

begin
  adq.Close;
  adq.SQL.Clear;
  adq.SQL.Text := s;
  adq.Parameters[0].Value := s1;
  adq.Parameters[1].Value := s2;
  adq.Parameters[2].Value := s3;
  adq.Parameters[3].Value := s4;
  adq.Parameters[4].Value := s5;
  adq.Parameters[5].Value := s6;
  adq.ExecSQL;
end;

procedure TopeDataSet.adqQuery(adq: TadoQuery; s: string);
begin
  adq.Close;
  adq.SQL.Clear;
  adq.SQL.Text := s;
  adq.Open;
end;

procedure TopeDataSet.adqQueryPara(adq: TadoQuery; s: string;

  s1: variant);

begin
  adq.Close;
  adq.SQL.Clear;
  adq.SQL.Text := s;
  adq.Parameters[0].Value := s1;
  adq.open;
end;



procedure TopeDataSet.adqQueryPara2(adq: TadoQuery;

  s: string; s1, s2: variant);

begin

  adq.Close;

  adq.SQL.Clear;

  adq.SQL.Text := s;

  adq.Parameters[0].Value := s1;

  adq.Parameters[1].Value := s2;

  adq.open;

end;

procedure TopeDataSet.adqQueryPara3(adq: TadoQuery;
  s: string; s1, s2, s3: variant);
begin
  adq.Close;
  adq.SQL.Clear;
  adq.SQL.Text := s;
  adq.Parameters[0].Value := s1;
  adq.Parameters[1].Value := s2;
  adq.Parameters[2].Value := s3;
  adq.Open;

end;

procedure TopeDataSet.adqQueryPara4(adq: TadoQuery;
  s: string; s1, s2, s3, s4: variant);
begin
  adq.Close;
  adq.SQL.Clear;
  adq.SQL.Text := s;
  adq.Parameters[0].Value := s1;
  adq.Parameters[1].Value := s2;
  adq.Parameters[2].Value := s3;
  adq.Parameters[3].Value := s4;
  adq.Open;
end;

procedure TopeDataSet.Append(adq: TadoQuery);
begin
  adq.Append;
end;

procedure TopeDataSet.delete(adq: TadoQuery);
var
  i: integer;

begin

  if adq.IsEmpty then

    abort;

  i := adq.RecNo;

  if queryInfo(SifDel, Scorp) then
  begin
    try
      adq.RecNo := i;
      adq.Delete;

      adq.UpdateBatch();  //批更新

    except
      adq.CancelBatch();
      info(SdelError, Scorp, 16);

    end;

  end;

end;



procedure TopeDataSet.flash(adq: TadoQuery);

begin
  adq.Close;
  adq.Open;
end;



procedure TopeDataSet.Save(adq: TadoQuery);
begin

  try

    adq.UpdateBatch();

    info(Ssave_success, Scorp);

  except

    adq.CancelBatch();

    info(Ssave_fail, Scorp, 16);

  end;

end;



end.

procedure adqExec(adq: TadoQuery; s: string);

begin

  adq.Close;

  adq.SQL.Clear;

  adq.SQL.Text := s;

  adq.ExecSQL;

end;



procedure adqExecPara(adq: TadoQuery; s: string;

  s1: variant);

begin

  adq.Close;

  adq.SQL.Clear;

  adq.SQL.Text := s;

  adq.Parameters[0].Value := s1;

  adq.ExecSQL;

end;



procedure adqQuery(adq: TadoQuery; s: string);

begin

  adq.Close;

  adq.SQL.Clear;

  adq.SQL.Text := s;

  adq.Open;

end;



procedure adqQueryPara(adq: TadoQuery; s: string; s1: variant);

begin

  adq.Close;

  adq.SQL.Clear;

  adq.SQL.Text := s;

  adq.Parameters[0].Value := s1;

  adq.open;

end;



procedure Append(adq: TadoQuery);

begin

  adq.Append;

end;



procedure delete(adq: TadoQuery);

var

  i: integer;

begin

  if adq.IsEmpty then

    abort;

  i := adq.RecNo;

  if queryInfo(SifDel, Scorp) then

  begin

    try

      adq.RecNo := i;

      adq.Delete;

      adq.UpdateBatch();

    except

      adq.CancelBatch();

      showmessage('Fail');

    end;

  end;

end;



procedure flash(adq: TadoQuery);

begin

  adq.Close;

  adq.Open;

end;



procedure Save(adq: TadoQuery);

begin

  try

    adq.UpdateBatch();

    showmessage('Success');

  except

    adq.CancelBatch();

    showmessage('Fail');

  end;

end;



end.
