unit FileTree;

interface

uses
  Windows, SysUtils,Classes;

  function DeleteTree(DeleteTreePath: string; IncludeCurrDir: boolean = true; DelFaileFileLst: TStrings = nil; DelSuccessFileLst: TStrings = nil): boolean; /// 删除一个目录及其下所有文件

implementation

{

附文件属性常量
    faReadOnly $00000001 Read-only files 只读文件
    faHidden $00000002 Hidden files 隐藏文件
    faSysFile $00000004 System files 系统文件
    faVolumeID $00000008 Volume ID files 卷标文件
    faDirectory $00000010 Directory files 目录
    faArchive $00000020 Archive files 归档文件
    faAnyFile $0000003F Any file 任意文件
}

function DeleteTree(DeleteTreePath: string; IncludeCurrDir: boolean = true; DelFaileFileLst: TStrings = nil; DelSuccessFileLst: TStrings = nil): boolean; /// 删除一个目录及其下所有文件

    function DeleteFileIntr(_STPath: string): boolean;
    var
        f: TSearchRec;
        // R: boolean;
    begin

        result := false;
        if Length(_stPath) = 0 then exit;
        if _StPath[length(_StPath)] <> '\' then
            _StPath := _stPath + '\';

        Result := true;

        if FindFirst(_StPath + '*.*',faAnyFile, f) = 0 then // faDirectory + faReadOnly + faHidden + faSysFile + 
        begin
            repeat
                if f.Attr and faDirectory > 0 then
                    if (f.Name = '.') or (f.name = '..') then
                    else
                    begin
                        if DeleteFileIntr(_StPath + f.name + '\') then
                            if not RemoveDir(_StPath + f.name) then
                                Result := false;
                    end;
            until FindNext(f) <> 0;
            FindClose(f);
        end;

        
        if FindFirst(_StPath + '*.*', faAnyFile, f) = 0 then   //faArchive + faReadOnly + faHidden + faSysFile +
        begin
            repeat
                if f.Attr and faDirectory = 0 then
                    if (f.Name = '.') or (f.name = '..') then
                    else
                    begin
                        if not DeleteFile(_StPath + f.name) then
                        begin
                            Result := false;
                            if DelFaileFileLst <> nil then DelFaileFileLst.Add(_StPath + f.name + '                           ...' + SysErrorMessage(GetlastError));
                        end else
                        begin
                            if DelSuccessFileLst <> nil then DelSuccessFileLst.Add(_StPath + f.name);
                        end;
                    end;
            until FindNext(f) <> 0;
            FindClose(f);
        end;
    end;
begin
//    if DelFaileFileLst <> nil then DelFaileFileLst.Clear;
//    if DelSuccessFileLst <> nil then DelSuccessFileLst.Clear;
    Result := DeleteFileIntr(DeleteTreePath);
    if Length(DeleteTreePath) = 0 then exit;
    
    if IncludeCurrDir then
    begin
        if DeleteTreePath[length(DeleteTreePath)] = '\' then
            DeleteTreePath := Copy(DeleteTreePath, 1, Length(DeleteTreePath) - 1);
    //    fileSetAttr(DeleteTreePath,faDirectory);
        if not RemoveDir(DeleteTreePath) then
            Result := false;
    end;
end;

end.
