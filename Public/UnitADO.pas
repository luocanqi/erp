unit UnitADO;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Data.DB,Data.Win.ADODB,Datasnap.DBClient,Vcl.Forms, Vcl.Dialogs,Vcl.Graphics,
  Vcl.Controls,  Vcl.ComCtrls,Vcl.ExtCtrls,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid,cxGridDBBandedTableView;

type
   TADO = class
     private
       {私有变量}
     public
       {公开变量或函数}
       function DeleteRowEmpty(lpDataSet : TClientDataSet; lpFieldName : string):Boolean;
       function ADOAppend(DataSet : TClientDataSet):Boolean;
       function ADOInsert(DataSet : TClientDataSet):Boolean;
       function UpdateTableData(ATableName, AKeyField: AnsiString; AIndex : Byte; var AData: TClientDataSet): Boolean;
       function ADOSaveData(DataSet : TClientDataSet ; lpSqlText : string ):Boolean;
       function ADODeleteSelection(lpGrid : TcxGridDBTableView ; lpDir  , lpTableName , lpFieldName : string ) : Boolean;
       function ADOExitIsSaveData(lphanlde : Cardinal ;DataSet : TClientDataSet ; lpSqlText : string ):Boolean;
       function ADOSearchDateRange(StartDate , EndDate , Sqltext : string;Client : TClientDataSet):Boolean;
       function IsExist(DataSet : TClientDataSet; lpName , lpFieldName : string):Boolean;
       function DeleteData( DataSet : TClientDataSet ; lpSqlText : string ):Boolean;
       function ADOIsSaveData(lphanlde : Cardinal ;  DataSet : TClientDataSet):Boolean;
       procedure ADODeleteSelectionData(lpGrid : TcxGridDBTableView; DataSet : TClientDataSet ; lpSqlText : string ); overload;
       procedure ADODeleteSelectionData(lpGrid : TcxGridDBBandedTableView; DataSet : TClientDataSet ; lpSqlText : string ); overload;
       procedure ADOIsEditing( DataSet : TClientDataSet ; var  AAllow : Boolean);
       procedure ADOIsEdit(DataSet : TClientDataSet);
       procedure OpenSQL(DataSet : TClientDataSet; s : string);
       procedure ExecSQL(s : string);

  end;

implementation

//http://www.cnblogs.com/del/archive/2010/01/22/1654475.html

uses
  uDataModule,global,DateUtils;

procedure TADO.OpenSQL(DataSet : TClientDataSet; s : string);
begin
  OutputLog(s);
  with DM.ADOQuery1 do
  begin
    DisableControls;
      Close;
      SQL.Clear;
      SQL.Text := s ;
      Open;
      DataSet.Close;
      DataSet.Data := DM.DataSetProvider1.Data;
      DataSet.Open;
    EnableControls;
  end;
end;

procedure TADO.ExecSQL(s : string);
begin
  with DM.Qry do
  begin
    DisableControls;
    Close;
    SQL.Clear;
    SQL.Text := s ;
    ExecSQL;
    EnableControls;
  end;
end;

function TADO.DeleteRowEmpty(lpDataSet : TClientDataSet ; lpFieldName : string):Boolean;
begin
  Result := False;

  with lpDataSet do
  begin
    if State in [dsEdit,dsInsert] then Post;
    First;
    while Not Eof do
    begin
      if null = FieldByName(lpFieldName).Value then
      begin
        Delete;
        continue;
      end;
      Next;
    end;
  end;

end;


function TADO.IsExist(DataSet : TClientDataSet; lpName , lpFieldName : string):Boolean;
var
  I: Integer;
  s: string;

begin
  Result := False;
  with DataSet do
  begin
    while not Eof do
    begin
      s := FieldByName(lpFieldName).AsString;
      if s = lpName then
      begin
        Result := True;
        Break;
      end;
      Next;
    end;
  end;

end;

procedure TADO.ADOIsEdit(DataSet : TClientDataSet);
begin
  with DataSet do
  begin
    if State <> dsInactive then
    begin
      if (State <> dsEdit) and (State <> dsInsert)  then
      begin
        Edit;
      end;
    end;
  end;
end;

procedure TADO.ADOIsEditing( DataSet : TClientDataSet ; var  AAllow : Boolean);
begin
  with DataSet do
  begin
    if (State <> dsInactive) and (State <> dsEdit) and (State <> dsInsert) then
    begin
      AAllow := False;
    end else
    begin
      AAllow := True;
    end;
  end;

end;

function TADO.ADOAppend(DataSet : TClientDataSet):Boolean;
begin
  with DataSet do
  begin
    if State <> dsInactive then
    begin
      Append;
    end else
    begin
      ShowMessage('数据未知!');
    end;
  end;
end;

function TADO.ADOInsert(DataSet : TClientDataSet):Boolean;
begin
  with DataSet do
  begin
    if State <> dsInactive then
    begin
      First;
      Insert;
    end else
    begin
      ShowMessage('数据未知!');
    end;
  end;
end;


function TADO.UpdateTableData(ATableName, AKeyField: AnsiString; AIndex : Byte; var AData: TClientDataSet): Boolean;
var
  LErrCount : Integer;
  LSql , Err : string;
  LDataSet : TDataSet;
  i : Integer;
begin
  Result := False;
  case AIndex of
    0: LSql := 'select * from ' + ATableName + ' where 1 = 2';
    1: LSql := ATableName;
  end;

  with DM do
  begin
    ADOQuery1.Close;
    ADOQuery1.SQL.Clear;
    ADOQuery1.SQL.Text := LSql;
    if AKeyField <> '' then
    begin
      LDataSet := ADOQuery1;
      AKeyField := LowerCase(AKeyField);

      for I := 0 to LDataSet.FieldCount - 1 do
      begin
        if (LowerCase(LDataSet.Fields[I].FieldName) = AKeyField) or
            (Pos(';' + LowerCase(LDataSet.Fields[I].FieldName) + ';', AKeyField) > 0) then
          LDataSet.Fields[I].ProviderFlags := LDataSet.Fields[I].ProviderFlags + [pfInKey]
        else
          LDataSet.Fields[I].ProviderFlags := [];
      end;
    end;

    try
      if AData.State in [dsEdit,dsInsert] then AData.Post;
      if AData.ChangeCount > 0 then
      begin
      //  DataSetProvider1.UpdateMode := upWhereKeyOnly;
        DataSetProvider1.ApplyUpdates(AData.Delta, -1, LErrCount);
      //  AData.MergeChangeLog;
      end;
    except
      on E:Exception do
      begin
      //  DM.ADOconn.RollbackTrans;           // 事务回滚
        err:=E.Message;
        ShowMessage(err);
      end;
    end;
  end;
  Result := LErrCount = 0;
end;

function TADO.ADOSaveData(DataSet : TClientDataSet ; lpSqlText : string ):Boolean;
var
  ErrorCount : Integer;
begin
  Result := False;
  DM.ADOQuery1.Close;
  DM.ADOQuery1.SQL.Text := lpSqlText;

  with DataSet do
  begin
    if State in [DsEdit, DSInsert] then Post;

    if ChangeCount > 0 then
    begin

      {
      with DM.DataSetProvider1.DataSet.FieldByName( 'Id' ) do
      begin
        ProviderFlags := ProviderFlags + [ pfInKey ];
      end;
      }

      DM.DataSetProvider1.ApplyUpdates(DataSet.Delta,0,ErrorCount);
      DataSet.MergeChangeLog;
      case ErrorCount of
        0:
         begin
           Application.MessageBox( '数据保存成功！', '提示:', MB_OK + MB_ICONINFORMATION)
         end;
        1:
         begin
           Application.MessageBox( '数据保存失败！', '提示:', MB_OK + MB_ICONWARNING)
         end;
      end;
    end else
    begin
      ShowMessage('暂时没有修改或新增的数据，无需保存!');
    end;
  end;

  DM.ADOQuery1.Open;
  DataSet.Close;
  DataSet.Data := DM.DataSetProvider1.Data;
  DataSet.Open;

end;

function GetId(ATable : string):Integer;
var
  ASql : string;
  ATb  : string;
begin
  Result := 0;
  ATb := UpperCase(ATable);
end;

function TADO.ADOExitIsSaveData(lphanlde : Cardinal ;  DataSet : TClientDataSet ; lpSqlText : string ):Boolean;
var
  ErrorCount : Integer;
begin
  Result := False;
  DM.ADOQuery1.Close;
  DM.ADOQuery1.SQL.Text := lpSqlText;

  with DataSet do
  begin

    if (State = dsEdit) or (State = dsInsert) then Post;

    if ChangeCount > 0 then
    begin
      if MessageBox(lphanlde, PChar('有数据未保存，现在是否要保存后并退出?'),'提示',
         MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2) = IDYES then
      begin
        DM.DataSetProvider1.ApplyUpdates(DataSet.Delta,0,ErrorCount);
        DataSet.MergeChangeLog;
        if ErrorCount = 0 then Result := True;
      end else
      begin
        Result := True;
      end;

    end else
    begin
      Result := True;
    end;

  end;

end;

function TADO.ADOIsSaveData(lphanlde : Cardinal ;  DataSet : TClientDataSet):Boolean;
var
  ErrorCount : Integer;
begin
  Result := False;
  with DataSet do
  begin
    if (State = dsEdit) or (State = dsInsert) then Post;

    if ChangeCount > 0 then
    begin
      if MessageBox(lphanlde, PChar('有数据未保存，现在是否要退出?'),'提示',
         MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2) = IDNO then
      begin
        Result := True
      end;
    end;

  end;

end;

//////////////////////////////////////////
//
//   删除带附件的数据
//
//////////////////////////////////////////

function TADO.ADODeleteSelection(lpGrid : TcxGridDBTableView ; lpDir  , lpTableName , lpFieldName : string ):Boolean;
var
  Id : string;
  szRecordCount : Integer;
  str , dir :string;
  I: Integer;
  szCount : Integer;
  szRowsCount : string;
  szIndex : Integer;
  s : Variant ;

begin
  szCount := lpGrid.Controller.SelectedRowCount;
  szRowsCount := IntToStr(szCount) ;
  if szCount > 0 then
  begin
      if Application.MessageBox(PWideChar( '您共选择: ( '+ szRowsCount +' ) 条数据, 确定要删除所有选中的记录吗？' ), '删除记录？', MB_OKCANCEL + MB_ICONWARNING) = IDOK then
      begin

        with lpGrid  do
        begin
          if not DataController.DataSource.DataSet.IsEmpty then
          begin
            str := '';
            for I := szCount -1 downto 0 do
            begin

              szIndex := GetColumnByFieldName(lpFieldName).Index;
              s := Controller.SelectedRows[i].Values[szIndex];
              Id := VarToStr(s);

              if Length(id) <> 0 then
              begin

                if Length(lpDir) <> 0 then
                begin
                  dir := ConcatEnclosure(lpDir, id);
                  if DirectoryExists(dir) then
                  begin
                  //  ShowMessage('删除:' + dir);
                    DeleteDirectory(dir);
                  end;

                end;

                if Length(str) = 0 then
                  str := Id
                else
                  str := str + '","' + Id;
              end;

            end;

            Controller.DeleteSelection;

            if (str <> ',') and (Length(str) <> 0) and ( Length( lpFieldName ) <> 0) then
            begin

              with DM.Qry do
              begin

                Close;
                SQL.Clear;
                SQL.Text := 'delete * from ' +  lpTableName +' where '+ lpFieldName +' in("' + str + '")';

                ExecSQL;

              end;

            end;

          end;

        end;

      end;

  end;

end;

function TADO.DeleteData( DataSet : TClientDataSet ; lpSqlText : string ):Boolean;
var
  ErrorCount : Integer;
begin
  DM.ADOQuery1.Close;
  DM.ADOQuery1.SQL.Text := lpSqlText;
  with DataSet do
  begin
    if ChangeCount > 0 then
    begin
      DM.DataSetProvider1.ApplyUpdates(DataSet.Delta,0,ErrorCount);
      case ErrorCount of
        0:
         begin
           DataSet.MergeChangeLog;
           Application.MessageBox( '数据删除成功！', '提示:', MB_OK + MB_ICONINFORMATION)
         end;
        1:
         begin
           Application.MessageBox( '删除失败！', '提示:', MB_OK +  MB_ICONWARNING)
         end;
      end;

    end;

  end;
  DM.ADOQuery1.Open;
  DataSet.Close;
  DataSet.Data := DM.DataSetProvider1.Data;
  DataSet.Open;

end;  

//////////////////////////////////////////
//
//   删除带附件的数据
//
//////////////////////////////////////////

procedure TADO.ADODeleteSelectionData(lpGrid : TcxGridDBTableView;
                                      DataSet: TClientDataSet ;
                                      lpSqlText : string );
var
  szRecordCount : Integer;
  szCount , ErrorCount : Integer;
  szRowsCount : string;
begin
  szCount := lpGrid.Controller.SelectedRowCount;
  szRowsCount := IntToStr(szCount) ;
  if szCount > 0 then
  begin
    if Application.MessageBox(PWideChar( '您共选择: ( '+ szRowsCount +' ) 条数据, 确定要删除所有选中的记录吗？' ), '删除记录？', MB_OKCANCEL + MB_ICONWARNING) = IDOK then
    begin
      lpGrid.DataController.UpdateData;   //提交到内存
      lpGrid.Controller.DeleteSelection;  //删除选中的文本
      lpGrid.DataController.UpdateData;   //提交到内存
      if (DataSet.State <> dsEdit) then

      DeleteData(DataSet,lpSqlText);
    end;
  end;
end;

//////////////////////////////////////////
//
//   删除数据
//
//////////////////////////////////////////

procedure TADO.ADODeleteSelectionData(lpGrid : TcxGridDBBandedTableView;
                                      DataSet : TClientDataSet ;
                                      lpSqlText : string );
var
  szRecordCount : Integer;
  szCount , ErrorCount : Integer;
  szRowsCount : string;
begin
  szCount := lpGrid.Controller.SelectedRowCount;
  szRowsCount := IntToStr(szCount) ;
  if szCount > 0 then
  begin
    if Application.MessageBox(PWideChar( '您共选择: ( '+ szRowsCount +' ) 条数据, 确定要删除所有选中的记录吗？' ), '删除记录？', MB_OKCANCEL + MB_ICONWARNING) = IDOK then
    begin
      lpGrid.DataController.UpdateData;   //提交到内存
      lpGrid.Controller.DeleteSelection;  //删除选中的文本
      lpGrid.DataController.UpdateData;   //提交到内存

      DeleteData(DataSet,lpSqlText);
    end;

  end;

end;

function TADO.ADOSearchDateRange(StartDate , EndDate , Sqltext : string;Client : TClientDataSet):Boolean;
var
  i : Integer;

begin
  if (StartDate = '') and (EndDate = '') then
  begin
    Application.MessageBox( '起始日期或结束日期不是有效的查询日期，查询失败!!', '提示:', MB_OKCANCEL + MB_ICONWARNING)
  end else
  begin
    i := CompareDateTime(StrToDate(EndDate),StrToDate(StartDate));
    if i <> -1 then
    begin

      with DM.ADOQuery1 do
      begin
        Close;
        SQL.Clear;
        SQL.Text := sqltext;

        Parameters.ParamByName('t1').Value := StartDate;  // StrToDate('2016-4-17 00:00:00');
        Parameters.ParamByName('t2').Value := EndDate;
        Open;

        Client.Close;
        Client.Data := DM.DataSetProvider1.Data;
        Client.Open;

      end;

    end else
    begin
      Application.MessageBox( '起始日期不能大于结束日期，范围查询失败!!', '提示:', MB_OKCANCEL + MB_ICONWARNING)

    end;

  end;

end;


procedure UpdateData( const pkField, theSql, theDelta: string );
    procedure SetDateTimeFieldProviderFlags( theField: TField );
    begin
        if theField.DataType in [ ftDate, ftTime, ftDateTime ] then
            theField.ProviderFlags := theField.ProviderFlags - [ pfInWhere ];
    end;
var
  FCds : TClientDataSet;

begin
  {
  with FCds do
  begin
      FProvider.DataSet.Close; //    关闭adoQuery
      Close;
      CommandText := theSql;
      Open;
      XMLData := theDelta; //        客户端传过来的更新,是midas的delta封包
      //        ShowMessageEx(IntToStr(FProvider.DataSet.FieldCount));

              //日期型字段是不能定位记录的主要原因,将其provierFlags设置
      //        ForEachFieldInDataset( FCds, @SetDateTimeFieldProviderFlags );
      DoDatasetOpen; //打开adoquery以获得Fields信息
    //设置其主键字段为keyField

      ApplyUpdates( 0 );
  end;
  }
end;

end.
