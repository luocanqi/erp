unit uProjectFrame;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB,
  cxDBData, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid, Vcl.ComCtrls,
  RzButton, RzPanel, Vcl.ExtCtrls, Data.Win.ADODB,
  UtilsTree,  //树管理
  FillThrdTree, cxLabel, cxTextEdit;

type
  TProjectFrame = class(TFrame)
    RzPanel2: TRzPanel;
    RzToolbar5: TRzToolbar;
    RzSpacer27: TRzSpacer;
    RzBut1: TRzToolButton;
    RzSpacer28: TRzSpacer;
    RzBut2: TRzToolButton;
    RzSpacer29: TRzSpacer;
    RzBut3: TRzToolButton;
    tv: TTreeView;
    ProjectGrid: TcxGrid;
    tvProjectView: TcxGridDBTableView;
    tvProjectViewColumn1: TcxGridDBColumn;
    tvProjectViewColumn2: TcxGridDBColumn;
    ProjectLv: TcxGridLevel;
    RzPanel3: TRzPanel;
    ADOQuery1: TADOQuery;
    DataSource1: TDataSource;
    RzSpacer1: TRzSpacer;
    RzToolButton1: TRzToolButton;
    procedure tvProjectViewDblClick(Sender: TObject);
    procedure tvProjectViewColumn1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure tvProjectViewEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure tvChange(Sender: TObject; Node: TTreeNode);
    procedure FrameClick(Sender: TObject);
    procedure RzBut1Click(Sender: TObject);
    procedure RzToolButton1Click(Sender: TObject);
  private
    { Private declarations }
    g_PatTree  : TNewUtilsTree;
    procedure OnShow(Sender: TObject);
  public
    { Public declarations }
    IsSystem : Boolean;
    dwParentHandle : THandle;
    constructor create(AOwner: TComponent);override;
  end;

implementation

uses
  global,uDataModule,ufrmProjectFinance,ufrmPostPact;

{$R *.dfm}

constructor TProjectFrame.create(AOwner: TComponent);
begin
  inherited;
end;

procedure TProjectFrame.FrameClick(Sender: TObject);
begin
  OnShow(Sender);
end;

function GetTreeNodeValue(Node : TTreeNode ;AType : Byte):string;stdcall;
var
  szNode : PNodeData;

begin
  Result := '';

  if Assigned(Node) then
  begin

    szNode := PNodeData(Node.Data);
    if Assigned(szNode) then
    begin
      case AType of
        0:
        begin
          Result := szNode.SignName;
        end;
        1:
        begin
          Result := szNode.Code;
        end;
      end;

    end;
  //  Application.MessageBox('先选择一个名称在操作此项.',m_title,MB_OK + MB_ICONQUESTION) ;
  end;

end;

procedure TProjectFrame.OnShow(Sender: TObject);
  function GetTableData(AQry : TADOQuery ; IsDelete : Integer ):Boolean;
  begin
    with AQry do
    begin
      Close;
      {
      SQL.Text := 'Select * from '     + g_Table_Projects_Tree  +
                  ' AS PT right join ' + g_Table_Project_Status +
                  ' AS PS on PT.'+  Self.tvProjectViewColumn2.DataBinding.FieldName + ' = PS.ProjectName WHERE PT.PID=0 and IsDelete = '+ IntToStr( IsDelete ) +';' ;
      }
      SQL.Text := 'Select * from ' + g_Table_Projects_Tree  +
                  ' WHERE PID=0 and IsDelete = '+ IntToStr( IsDelete ) +';' ;

      Open;
    end;
  end;
var
  szRootName : string;
begin

  Self.tv.FullExpand;
  GetTableData(Self.ADOQuery1,0);

  szRootName := '项目部工程管理';
  g_PatTree := TNewUtilsTree.Create(Self.tv,
                                    DM.ADOconn,
                                    g_Table_Projects_Tree,
                                    szRootName
                                    );
  Self.tvProjectViewDblClick(Sender);

end;

procedure TProjectFrame.RzBut1Click(Sender: TObject);
var
   Node   : TTreeNode;
   szNode : PNodeData;
   Child  : TfrmPostPact;
   szCodeList  : string;

   pMsg : TProject;
begin
  Node := Self.tv.Selected;
  if Assigned(Node) then
  begin
    if Node.Level > 0 then
    begin
       szNode :=  PNodeData(Node.Data);
       treeCldnode(Node,szCodeList);
       Child := TfrmPostPact.Create(Self);
       try
         Child.g_IsOnly := True;
         Child.g_PatTree    := g_PatTree;
         Child.g_PostCode   := szNode.Code;
         Child.g_TableName  := g_Table_Projects_Tree;
         Child.g_Prefix     := 'DTGC';
         Child.g_ProjectDir := ''; //附件目录
         Child.g_TreeNode   := Node;
         if Sender = Self.RzBut1 then
         begin
           //新增
           Child.cxDateEdit1.Date := Date;
           Child.g_IsModify := False;
           Child.ShowModal;
         end else
         if Sender = Self.RzBut2 then
         begin
           //编辑

           Child.cxDateEdit1.Date := szNode.InputDate;
           Child.cxLookupComboBox1.Text := szNode.SignName;
           Child.cxMemo1.Text := szNode.remarks;
           Child.g_IsModify   := True;
           Child.ShowModal;

         end else
         if Sender = Self.RzBut3 then
         begin

           if Node.Level <= 1 then
           begin
             Application.MessageBox('顶级帐目禁止删除!',m_title,MB_OK + MB_ICONQUESTION ) ;
           end
           else
           begin
             //可以删除

              szNode := PNodeData(Node.Data);
              if MessageBox(handle, PChar('确认要删除"' + szNode.SignName + '"'),
                       '提示', MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2) = IDYES then
              begin
                  pMsg.dwName:= Node.Text;
                  pMsg.dwCode:= GetTreeNodeValue( Node , 1);
                  pMsg.dwCodeList := szCodeList;

                  SendMessage(dwParentHandle, WM_FrameDele, 0, LPARAM(@pMsg));  // 发消息
                  g_PatTree.DeleteTree(Node);

              end;

           end;

         end;

       finally
         Child.Free;
       end;

    end else
    begin
      Application.MessageBox('顶级节点禁止新增/编辑!',m_title,MB_OK + MB_ICONQUESTION ) ;
    end;

  end else
  begin
    Application.MessageBox('请选择合同在点击新增/编辑/删除!',m_title,MB_OK + MB_ICONQUESTION )
  end;

end;

procedure TProjectFrame.RzToolButton1Click(Sender: TObject);
begin
  Self.tvProjectViewDblClick(Sender);
end;

procedure TProjectFrame.tvChange(Sender: TObject; Node: TTreeNode);
var
  s : string;
  szPostCode : string;
  szProjectName : string;
  pMsg : TProject;

begin
  inherited;
  if Assigned(node) then
  begin
    pMsg.dwName:= Node.Text;
    pMsg.dwCode:= GetTreeNodeValue( Node , 1);
    if Length( pMsg.dwCode ) <> 0 then
    begin
      SendMessage(dwParentHandle, WM_FrameView, 0, LPARAM(@pMsg));  // 第一种发消息方法
    end;
  end;

end;

procedure TProjectFrame.tvProjectViewColumn1GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  AText := IntToStr(ARecord.Index +1 );
end;

procedure TProjectFrame.tvProjectViewDblClick(Sender: TObject);
var
  szRootName: string;
  szColName : string;
  szParent  : Integer;
  szProjectName : string;
  szProjectCode : string;
  szIsSystem : Boolean;

begin
  inherited;
  szRootName := '项目部工程管理';
  with Self.ADOQuery1 do
  begin
    if State <> dsInactive then
    begin
      szColName := Self.tvProjectViewColumn2.DataBinding.FieldName;
      szProjectName := FieldByName(szColName).AsString;
      if not IsSystem then
      begin
        if Application.MessageBox( PWideChar( '是否要切换到“'+ szProjectName + '“ 工程？' ), '提示:',
          MB_OKCANCEL + MB_ICONWARNING) = ID_OK then
        begin
          if Length(szProjectName) <> 0 then
          begin
            //A
            szParent  := FieldByName('parent').AsInteger;
            g_PatTree.GetTreeTable(szParent);
            SendMessage(dwParentHandle, WM_FrameClose, 0, 0);
          end;

        end;

      end else
      begin
        //B
        if Length(szProjectName) <> 0 then
        begin
          szParent  := FieldByName('parent').AsInteger;
          g_PatTree.GetTreeTable(szParent);
          SendMessage(dwParentHandle, WM_FrameClose, 0, 0);
          IsSystem := False;
        end;

      end;



    end;

  end;

end;

procedure TProjectFrame.tvProjectViewEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  AAllow := False;
end;

end.
