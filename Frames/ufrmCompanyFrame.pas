unit ufrmCompanyFrame;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB,
  cxDBData, cxTextEdit, cxCurrencyEdit, cxContainer, cxMaskEdit, cxDropDownEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxLabel, RzButton, RzPanel,
  cxGridLevel, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxClasses, cxGridCustomView, cxGrid, Vcl.ExtCtrls, Data.Win.ADODB;

type
  TfrmCompanyFrame = class(TFrame)
    RzPanel2: TRzPanel;
    Splitter3: TSplitter;
    PeopleGrid: TcxGrid;
    tvpeopleView: TcxGridDBTableView;
    tvpeopleViewCol1: TcxGridDBColumn;
    tvpeopleViewCol2: TcxGridDBColumn;
    tvpeopleViewCol4: TcxGridDBColumn;
    tvpeopleViewColumn1: TcxGridDBColumn;
    tvpeopleViewColumn2: TcxGridDBColumn;
    tvpeopleViewColumn3: TcxGridDBColumn;
    tvpeopleViewColumn4: TcxGridDBColumn;
    tvpeopleViewColumn5: TcxGridDBColumn;
    tvpeopleViewColumn6: TcxGridDBColumn;
    tvpeopleViewColumn7: TcxGridDBColumn;
    peopleLv: TcxGridLevel;
    RzToolbar3: TRzToolbar;
    RzSpacer12: TRzSpacer;
    RzSpacer13: TRzSpacer;
    RzToolButton13: TRzToolButton;
    cxLabel2: TcxLabel;
    cxLookupComboBox1: TcxLookupComboBox;
    Grid: TcxGrid;
    tvCompany: TcxGridDBTableView;
    tvCompanyColumn1: TcxGridDBColumn;
    tvCompanyColumn2: TcxGridDBColumn;
    lv: TcxGridLevel;
    RzToolbar1: TRzToolbar;
    RzSpacer23: TRzSpacer;
    RzSpacer24: TRzSpacer;
    RzSpacer25: TRzSpacer;
    cxLabel13: TcxLabel;
    cxLabel14: TcxLabel;
    duties: TcxLookupComboBox;
    Section: TcxLookupComboBox;
    RzToolbar5: TRzToolbar;
    RzSpacer26: TRzSpacer;
    RzSpacer27: TRzSpacer;
    RzToolButton5: TRzToolButton;
    RzSpacer28: TRzSpacer;
    RzToolButton6: TRzToolButton;
    cxLabel15: TcxLabel;
    cxTextEdit1: TcxTextEdit;
    ADOQuery1: TADOQuery;
    DataSource1: TDataSource;
    qry: TADOQuery;
    ds: TDataSource;
    ADOCompany: TADOQuery;
    DataCompany: TDataSource;
    procedure FrameClick(Sender: TObject);
    procedure tvCompanyDblClick(Sender: TObject);
    procedure SectionPropertiesCloseUp(Sender: TObject);
    procedure tvpeopleViewDblClick(Sender: TObject);
    procedure dutiesPropertiesCloseUp(Sender: TObject);
    procedure RzToolButton5Click(Sender: TObject);
    procedure cxTextEdit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure RzToolButton6Click(Sender: TObject);
    procedure tvCompanyColumn1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure RzToolButton13Click(Sender: TObject);
    procedure cxLookupComboBox1PropertiesCloseUp(Sender: TObject);
  private
    { Private declarations }
    dwIsSelectCompany : Boolean;
    dwCompayInfoIndex : Integer;
    function GetCompayInfoList(lpCompayName : string):Boolean;
  public
    { Public declarations }
    dwParentHandle : THandle;
  end;

implementation

uses
   global,uDataModule;

{$R *.dfm}

procedure TfrmCompanyFrame.cxLookupComboBox1PropertiesCloseUp(Sender: TObject);
begin
  Self.RzToolButton13.Click;
end;

procedure TfrmCompanyFrame.cxTextEdit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 13 then
  begin
    Self.RzToolButton5.Click;
  end;
end;

procedure TfrmCompanyFrame.dutiesPropertiesCloseUp(Sender: TObject);
var
  Section , s : string;
  szColName : string;
  duties : string;

begin
  Section := Self.Section.Text ;
  if Section <> '' then
  begin
    duties := Self.duties.Text;
    szColName := Self.tvpeopleViewColumn6.DataBinding.FieldName;
    S := 'Select * from '+ g_Table_Maintain_CompanyInfoList + ' WHERE TreeId=' +
         IntToStr( dwCompayInfoIndex ) + ' AND ' + szColName + '="' + Section + '" and ' + Self.tvpeopleViewCol4.DataBinding.FieldName + '="' + duties + '"';
    with Qry do
    begin
      Close;
      SQL.Clear;
      SQL.Text := s;
      Open;
    end;
  end else
  begin
    ShowMessage('请选择一个部门');
  end;

end;

procedure TfrmCompanyFrame.FrameClick(Sender: TObject);
begin
  with Self.ADOCompany do
  begin
    Close;
    SQL.Text := 'Select * from ' + g_Table_Maintain_CompanyInfoTree + ' WHere PID=0';
    Open;
  end;
end;

function TfrmCompanyFrame.GetCompayInfoList(lpCompayName : string):Boolean;
var
  s : string;
  szFildName : string;

begin
  with DM.Qry do
  begin
    Close;
    SQL.Text := 'Select * from ' + g_Table_Maintain_CompanyInfoTree + ' Where SignName="' + lpCompayName + '"'; //parent
    Open;
    if RecordCount <> 0 then
    begin
      dwCompayInfoIndex := FieldByName('parent').AsInteger;
    end;
  end;
  S := 'Select * from '+
                g_Table_Maintain_CompanyInfoList +
                ' WHERE TreeId=' + IntToStr( dwCompayInfoIndex );

  if DM.ADOOOPEN(Self.qry,s) <> 0 then
  begin
    Self.Section.Enabled := True;
    Self.duties.Enabled  := True;
    Self.cxTextEdit1.Enabled := True;
    Self.RzToolButton5.Enabled := True;
    Self.RzToolButton6.Enabled := True;
  end else
  begin
    Self.Section.Enabled := False;
    Self.duties.Enabled  := False;
    Self.cxTextEdit1.Enabled := False;
    Self.RzToolButton5.Enabled := False;
    Self.RzToolButton6.Enabled := False;
  end;

  szFildName := Self.tvpeopleViewCol4.DataBinding.FieldName;
  S := 'Select '+ szFildName +' from '+
                g_Table_Maintain_CompanyInfoList +
                ' WHERE TreeId=' + IntToStr( dwCompayInfoIndex ) + ' GROUP BY ' + szFildName;
 if DM.ADOOOPEN(Self.ADOQuery1,s) <> 0 then
 {
  with Self.ADOQuery1 do  //获取职务
  begin
    Close;
    SQL.Text := s ;
    Open;
  end;
  }
end;

function GridLocateRecord(View : TcxGridDBTableView; FieldName, LocateValue : String) : Boolean;
begin
  {表格数据查找定位}
  Result := False;
  if (View.GetColumnByFieldName(FieldName) <> nil) then
  begin
    Result := View.DataController.Search.Locate(View.GetColumnByFieldName(FieldName).Index, LocateValue);
  end;
end;

procedure TfrmCompanyFrame.RzToolButton13Click(Sender: TObject);
begin
  GetCompayInfoList(Self.cxLookupComboBox1.Text);
end;

procedure TfrmCompanyFrame.RzToolButton5Click(Sender: TObject);
var
  s : string;
begin
  inherited;
  s := cxTextEdit1.Text;
  GridLocateRecord(Self.tvpeopleView,Self.tvpeopleViewCol2.DataBinding.FieldName,s);
  Self.tvpeopleViewDblClick(Sender);
end;

procedure TfrmCompanyFrame.RzToolButton6Click(Sender: TObject);
begin
  Self.ADOQuery1.Requery();
  Self.qry.Requery();
  Self.ADOCompany.Requery();
//  GetMaintainInfo;
  ShowMessage('员工信息刷新完成!');
end;

procedure TfrmCompanyFrame.SectionPropertiesCloseUp(Sender: TObject);
var
  s : string;
  szColName : string;

begin
  s := Self.Section.Text ;
  szColName := Self.tvpeopleViewColumn6.DataBinding.FieldName;
  S := 'Select * from '+ g_Table_Maintain_CompanyInfoList + ' WHERE TreeId=' +
       IntToStr( dwCompayInfoIndex ) + ' AND ' + szColName + '="' + s + '"';
  with Qry do
  begin
    Close;
    SQL.Clear;
    SQL.Text := s;
    Open;
  end;
end;

procedure TfrmCompanyFrame.tvCompanyColumn1GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  AText := IntToStr(ARecord.Index + 1 );
end;

procedure TfrmCompanyFrame.tvCompanyDblClick(Sender: TObject);
var
  szRowIndex : Integer;
  s : string;

begin
  if (Self.qry.Active) and (dwIsSelectCompany) then
  begin
     if MessageBox(handle, PChar('是否切换公司?'),'提示',
               MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2) = IDNO then
    begin
      Exit;
    end;
  end;

  szRowIndex := Self.tvCompany.Controller.FocusedRowIndex;
  if szRowIndex >= 0 then
  begin
    s := VarToStr(Self.tvCompany.DataController.Values[szRowIndex,Self.tvCompanyColumn2.Index]);
    GetCompayInfoList(s);
    dwIsSelectCompany := True;

  end;

end;

procedure TfrmCompanyFrame.tvpeopleViewDblClick(Sender: TObject);
var
  szRowIndex : Integer;
  szColName : string;
  szPrice : Currency;
//  pStaff  = ^TStaff;
  pMsg : TStaff;

begin
  inherited;
  szRowIndex := Self.tvpeopleView.Controller.FocusedRowIndex;
//  http://ymg97526.blog.163.com/blog/static/1736581602011332353527/
  if szRowIndex >= 0 then
  begin
    {
    pMsg.dwName:= Node.Text;
    pMsg.dwCode:= GetTreeNodeValue( Node , 1);
    if Length( pMsg.dwCode ) <> 0 then
    begin
      SendMessage(dwParentHandle, WM_FrameView, 0, LPARAM(@pMsg));  // 第一种发消息方法
    end;
    }

    {
    if (Self.ADODays.Active) and (dwIsSelectStaff) then
    begin
      if MessageBox(handle, PChar('是否切换员工?'),'提示',
           MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2) = IDNO then
      begin
        Exit;
      end;
    end;
    }
    if (Self.qry.Active) then
    begin
      if MessageBox(handle, PChar('是否切换员工?'),'提示',
           MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2) = IDNO then
      begin
        Exit;
      end;
    end;

    with Self.qry do
    begin
      if State <> dsInactive then
      begin
        pMsg.dwParent   := FieldByName('ID').AsString;
        pMsg.dwFullname := FieldByName(Self.tvpeopleViewCol2.DataBinding.FieldName).AsString;
        pMsg.dwSex:= FieldByName(Self.tvpeopleViewColumn5.DataBinding.FieldName).AsString;//性别
        pMsg.dwUnitPrice:= FieldByName(Self.tvpeopleViewColumn1.DataBinding.FieldName).AsCurrency; //单价
        pMsg.dwOverPrice:= FieldByName(Self.tvpeopleViewColumn2.DataBinding.FieldName).AsCurrency; //加班单价
        pMsg.dwwagestype:= FieldByName(Self.tvpeopleViewColumn3.DataBinding.FieldName).AsString;   //工资类型
        pMsg.dwLeastWork:= FieldByName(Self.tvpeopleViewColumn4.DataBinding.FieldName).AsInteger;  //最少工作时间
        pMsg.dwsection  := FieldByName(Self.tvpeopleViewColumn6.DataBinding.FieldName).AsString;   //部门
        pMsg.dwCompanyName := Self.tvCompanyColumn2.EditValue;

        szColName  := Self.tvpeopleViewColumn7.DataBinding.FieldName;

        pMsg.dwNumbers  := FieldByName(Self.tvpeopleViewColumn7.DataBinding.FieldName).AsString; //编号
        SendMessage(dwParentHandle, WM_FrameView, 0, LPARAM(@pMsg));  // 第一种发消息方法

      end;

    end;

  end;

end;


end.
