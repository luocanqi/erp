unit ufrmNewBaseStorage;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxTextEdit, cxCheckBox,
  cxCalendar, cxDropDownEdit, cxDBLookupComboBox, cxMemo, cxCurrencyEdit,
  cxSpinEdit, cxContainer, Vcl.Menus, Vcl.ExtCtrls, cxLookupEdit,
  cxDBLookupEdit, cxDBEdit, Vcl.StdCtrls, cxButtons, cxLabel, cxMaskEdit,
  cxGroupBox, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid, RzButton, RzPanel,UnitADO,ufrmBaseController,
  Datasnap.DBClient;

type
  TfrmNewBaseStorage = class(TfrmBaseController)
    RzPanel1: TRzPanel;
    RzToolbar13: TRzToolbar;
    RzSpacer30: TRzSpacer;
    RzToolButton85: TRzToolButton;
    RzSpacer113: TRzSpacer;
    RzToolButton87: TRzToolButton;
    RzToolButton88: TRzToolButton;
    RzToolButton90: TRzToolButton;
    RzSpacer120: TRzSpacer;
    RzSpacer1: TRzSpacer;
    RzSpacer10: TRzSpacer;
    btnGridSet: TRzToolButton;
    Grid: TcxGrid;
    tvView: TcxGridDBTableView;
    tvViewColumn24: TcxGridDBColumn;
    tvViewColumn28: TcxGridDBColumn;
    tvViewColumn1: TcxGridDBColumn;
    tvViewColumn2: TcxGridDBColumn;
    tvViewColumn3: TcxGridDBColumn;
    tvViewColumn15: TcxGridDBColumn;
    tvViewColumn17: TcxGridDBColumn;
    tvViewColumn16: TcxGridDBColumn;
    tvViewColumn4: TcxGridDBColumn;
    tvViewColumn5: TcxGridDBColumn;
    tvViewColumn21: TcxGridDBColumn;
    tvViewColumn11: TcxGridDBColumn;
    tvViewColumn7: TcxGridDBColumn;
    tvViewColumn6: TcxGridDBColumn;
    tvViewColumn8: TcxGridDBColumn;
    tvViewColumn10: TcxGridDBColumn;
    tvViewColumn12: TcxGridDBColumn;
    tvViewColumn14: TcxGridDBColumn;
    tvViewColumn25: TcxGridDBColumn;
    tvViewColumn26: TcxGridDBColumn;
    tvViewColumn27: TcxGridDBColumn;
    tvViewColumn13: TcxGridDBColumn;
    Lv: TcxGridLevel;
    RzPanel2: TRzPanel;
    Splitter1: TSplitter;
    RzSpacer2: TRzSpacer;
    RzToolButton1: TRzToolButton;
    RzToolButton2: TRzToolButton;
    RzSpacer3: TRzSpacer;
    RzToolButton3: TRzToolButton;
    Master: TClientDataSet;
    DataMaster: TDataSource;
    ADOMaking: TClientDataSet;
    DataMaking: TDataSource;
    Print: TPopupMenu;
    Excel1: TMenuItem;
    N2: TMenuItem;
    RightMenu: TPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    N1: TMenuItem;
    N3: TMenuItem;
    DataSignName: TDataSource;
    ADOSignName: TClientDataSet;
    RzPanel8: TRzPanel;
    RzPanel13: TRzPanel;
    cxButton6: TcxButton;
    cxButton7: TcxButton;
    cxGroupBox1: TcxGroupBox;
    Label5: TLabel;
    Label1: TLabel;
    cxDBLookupComboBox1: TcxDBLookupComboBox;
    cxDBLookupComboBox2: TcxDBLookupComboBox;
    RzPanel4: TRzPanel;
    Bevel1: TBevel;
    cxDBTextEdit3: TcxDBTextEdit;
    cxLabel6: TcxLabel;
    cxLabel7: TcxLabel;
    cxDBComboBox1: TcxDBComboBox;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    RzPanel3: TRzPanel;
    Bevel2: TBevel;
    cxGroupBox4: TcxGroupBox;
    Label23: TLabel;
    Label24: TLabel;
    Label26: TLabel;
    Label3: TLabel;
    cxSpinEdit1: TcxSpinEdit;
    cxSpinEdit2: TcxSpinEdit;
    cxSpinEdit5: TcxSpinEdit;
    cxComboBox1: TcxComboBox;
    cxGroupBox2: TcxGroupBox;
    Label4: TLabel;
    Label2: TLabel;
    Label6: TLabel;
    cxDBMemo2: TcxDBMemo;
    cxDBSpinEdit2: TcxDBSpinEdit;
    cxDBSpinEdit3: TcxDBSpinEdit;
    cxLabel1: TcxLabel;
    cxDBSpinEdit1: TcxDBSpinEdit;
    cxLabel2: TcxLabel;
    cxComboBox2: TcxComboBox;
    cxButton3: TcxButton;
    cxButton4: TcxButton;
    DataSpec: TDataSource;
    ADOSpec: TClientDataSet;
    tvViewColumn9: TcxGridDBColumn;
    RzSpacer4: TRzSpacer;
    RzPanel5: TRzPanel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure RzToolButton88Click(Sender: TObject);
    procedure btnGridSetClick(Sender: TObject);
    procedure RzToolButton1Click(Sender: TObject);
    procedure RzToolButton90Click(Sender: TObject);
    procedure RzToolButton2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure tvViewColumn24GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure MasterAfterInsert(DataSet: TDataSet);
    procedure RzToolButton85Click(Sender: TObject);
    procedure RzToolButton3Click(Sender: TObject);
    procedure tvViewEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure cxDBSpinEdit1PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure cxComboBox2PropertiesCloseUp(Sender: TObject);
    procedure cxSpinEdit1PropertiesChange(Sender: TObject);
    procedure cxSpinEdit5PropertiesChange(Sender: TObject);
    procedure cxDBMemo2PropertiesChange(Sender: TObject);
    procedure cxDBSpinEdit3PropertiesChange(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure cxDBComboBox1PropertiesInitPopup(Sender: TObject);
    procedure cxButton6Click(Sender: TObject);
    procedure cxButton7Click(Sender: TObject);
    procedure tvViewColumn4PropertiesCloseUp(Sender: TObject);
    procedure tvViewColumn4PropertiesInitPopup(Sender: TObject);
    procedure tvViewColumn5PropertiesInitPopup(Sender: TObject);
    procedure MasterAfterPost(DataSet: TDataSet);
    procedure MasterAfterDelete(DataSet: TDataSet);
    procedure tvViewDblClick(Sender: TObject);
    procedure tvViewColumn10PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure tvViewColumn8PropertiesCloseUp(Sender: TObject);
    procedure tvViewColumn8PropertiesInitPopup(Sender: TObject);
    procedure tvViewColumn8PropertiesPopup(Sender: TObject);
    procedure tvViewColumn15PropertiesInitPopup(Sender: TObject);
    procedure cxComboBox1PropertiesCloseUp(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure Excel1Click(Sender: TObject);
    procedure tvViewColumn9GetPropertiesForEdit(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AProperties: TcxCustomEditProperties);
    procedure tvViewTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
      Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
      var AText: string);
    procedure cxDBSpinEdit2PropertiesChange(Sender: TObject);
  private
    { Private declarations }
    Radix : Integer;
  
    procedure SetcxGrid(lpGridView : TcxGridDBTableView ; index : Byte ;lpSuffix:string);
    procedure PopupKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    function GetRadix(lpRadix : Integer):BOOL;
    procedure GetSumMoney(Count , UnitPrice : Variant);
  public
    { Public declarations }
    dwTypes: Integer;
    dwADO  : TADO;
    dwPrefix  : string;
    dwSuffix  : string;
    dwSQLtext : string;
    dwIsSelect: Boolean;
    g_ProjectName : string;
    g_PostCode : string;
    function GetCodeList():string;
    
  end;
//http://blog.csdn.net/liaojiafan/article/details/4879178
//进销存数据库设计
var
  frmNewBaseStorage: TfrmNewBaseStorage;

implementation

uses
   uProjectFrame,global,uDataModule,ufunctions,System.Math,ufrmIsViewGrid,ufrmStorageManage;

type
  TCusDropDownEdit = class(TcxCustomDropDownEdit);

{$R *.dfm}

procedure TfrmNewBaseStorage.PopupKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = 27 then //如果按下“ESC”键
    TcxCustomEditPopupWindow(sender).ClosePopup;//关闭弹出窗
end;

function TfrmNewBaseStorage.GetCodeList():string;
var
  s : string;
  i , szIndex : Integer;
  szCodeList : string;
   
begin
  szIndex := Self.tvViewColumn1.Index;
  szCodeList := '';
  s := '';
  with Self.tvView do
  begin
    for I := 0 to ViewData.RecordCount-1 do
    begin
      s := VarToStr( Self.tvView.ViewData.Rows[i].Values[szIndex] );

      if Length(s) = 0 then
        szCodeList := s
      else
        szCodeList := szCodeList + '","' + s ;
    end;
  end;
  Result := szCodeList;
end;  

procedure TfrmNewBaseStorage.SetcxGrid(lpGridView : TcxGridDBTableView ; index : Byte ;
lpSuffix:string);
var
  i : Integer;
begin
  for I := 0 to lpGridView.ColumnCount - 1 do
  begin
    Application.ProcessMessages;
    SetGrid(lpGridView.Columns[i],index, Self.Name + lpSuffix);
  end;
end;

procedure TfrmNewBaseStorage.GetSumMoney(Count , UnitPrice : Variant);
var
  szRowIndex : Integer;
  szSumMoney : Currency;
  szColName : string;
begin
  inherited;

  if (UnitPrice <> null) and (Count <> null) then
  begin

    szSumMoney := StrToCurrDef(VarToStr(UnitPrice),0) * StrToFloatDef( VarToStr(Count) ,0 );

    with Self.Master do
    begin
      if State <> dsInactive then
      begin
        if (State = dsInsert) or (State = dsEdit) then
        begin
          szColName := Self.tvViewColumn12.DataBinding.FieldName; //金额
          FieldByName(szColName).Value := szSumMoney;
          szColName := Self.tvViewColumn10.DataBinding.FieldName; //单价
          FieldByName(szColName).Value := UnitPrice;
          szColName := Self.tvViewColumn8.DataBinding.FieldName;  //数量
          FieldByName(szColName).Value := Count;
        end;

      end;

    end;

  end;

end;

procedure TfrmNewBaseStorage.tvViewColumn10PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var
  szRowIndex : Integer;
  Count : Variant;
  szSumMoney : Currency;
  szColName : string;

begin
  inherited;
  szRowIndex := Self.tvView.Controller.FocusedRowIndex;
  Count := Self.tvView.DataController.Values[szRowIndex,Self.tvViewColumn8.Index];
  GetSumMoney(Count,DisplayValue);

end;

procedure TfrmNewBaseStorage.tvViewColumn15PropertiesInitPopup(Sender: TObject);
begin
  inherited;
  TCusDropDownEdit(sender).PopupWindow.KeyPreview := true;//令弹出窗按键事件有效
  TCusDropDownEdit(sender).PopupWindow.OnKeyDown  := PopupKeyDown;//关联按键事件给弹出窗
end;

procedure TfrmNewBaseStorage.tvViewColumn24GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  AText := IntToStr(ARecord.Index + 1);
end;

procedure TfrmNewBaseStorage.tvViewColumn4PropertiesCloseUp(Sender: TObject);
var
  szColContentA : Variant;
  szColContentB : Variant;
  szColContentC : Variant;
  szColContentD : Variant;
  szRowIndex:Integer;
  szColName : string;
  szCount : Variant;

begin
  inherited;
  with (Sender AS TcxLookupComboBox).Properties.Grid do
  begin
    szRowIndex := FocusedRowIndex;

    if szRowIndex >= 0 then
    begin
      szColContentA :=  DataController.Values[szRowIndex,0] ; //名称
    //  szColContentB :=  DataController.Values[szRowIndex,1] ; //型号
      szColContentC :=  DataController.Values[szRowIndex,2] ; //规格
      szColContentD :=  DataController.Values[szRowIndex,3] ; //单位

      if szColContentA <> null then
      begin

        with Self.Master do
        begin
          if (State <> dsInactive) then
          begin
            if (State <> dsInsert) or (State <> dsEdit) then  Edit;
            if szColContentA <> null then  Self.tvViewColumn4.EditValue := szColContentA; //名称
            if szColContentC <> null then  Self.tvViewColumn5.EditValue := szColContentC; //规格
          //  if szColContentD <> null then  Self.tvViewColumn9.EditValue := szColContentD; //单位
          end;

        end;

      end;

    end;

  end;

end;

procedure TfrmNewBaseStorage.tvViewColumn4PropertiesInitPopup(Sender: TObject);
var
  s  : Variant;
  szRowIndex : Integer;
  szColName : string;
  
begin
  inherited;
  TCusDropDownEdit(sender).PopupWindow.KeyPreview := true;//令弹出窗按键事件有效
  TCusDropDownEdit(sender).PopupWindow.OnKeyDown  := PopupKeyDown;//关联按键事件给弹出窗
  {
  szRowIndex := Self.tvView.Controller.FocusedRowIndex;
  s := Self.tvView.DataController.Values[szRowIndex,Self.tvViewColumn15.Index];
  if s <> null then
  begin

    szColName := Self.tvViewColumn15.DataBinding.FieldName;
    with DM do
    begin
      ADOQuery1.Close;
      ADOQuery1.SQL.Text := 'Select * from '+ g_Table_Maintain_Science +' WHERE '+ szColName +'="' +
                             VarToStr(s) + '" and '+ Self.tvViewColumn3.DataBinding.FieldName +'="' + g_ProjectName +'"';
      ADOQuery1.Open;
      Self.ADOMaking.Data := DataSetProvider1.Data;
      Self.ADOMaking.Open;
    end;
  end else
  begin
    ShowMessage('请填写("' + Self.tvViewColumn15.Caption + '")内容后在选择材料');
  end;
  }
end;

procedure TfrmNewBaseStorage.tvViewColumn5PropertiesInitPopup(Sender: TObject);
begin
  inherited;
  TCusDropDownEdit(sender).PopupWindow.KeyPreview := true;//令弹出窗按键事件有效
  TCusDropDownEdit(sender).PopupWindow.OnKeyDown  := PopupKeyDown;//关联按键事件给弹出窗
end;

procedure TfrmNewBaseStorage.tvViewColumn8PropertiesCloseUp(Sender: TObject);
var
  szRowIndex : Integer;
  Count,UnitPrice : Variant;

begin
  inherited;
  //Self.tvViewColumn8.Index //数量
  szRowIndex := Self.tvView.Controller.FocusedRowIndex;
  UnitPrice := Self.tvView.DataController.Values[szRowIndex,Self.tvViewColumn10.Index];
  Count := Self.cxDBSpinEdit1.EditingValue;

  GetSumMoney(Count,UnitPrice);
end;

procedure TfrmNewBaseStorage.tvViewColumn8PropertiesInitPopup(Sender: TObject);
begin
  inherited;
  TCusDropDownEdit(sender).PopupWindow.KeyPreview := true;//令弹出窗按键事件有效
  TCusDropDownEdit(sender).PopupWindow.OnKeyDown  := PopupKeyDown;//关联按键事件给弹出窗
end;

procedure TfrmNewBaseStorage.tvViewColumn8PropertiesPopup(Sender: TObject);
begin
  inherited;
  GetRadix(Self.cxComboBox2.ItemIndex);
end;

procedure TfrmNewBaseStorage.tvViewColumn9GetPropertiesForEdit(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AProperties: TcxCustomEditProperties);
var
  List : TStringList;
  i : Integer;
begin
  inherited;
  with (AProperties AS TcxComboBoxProperties) do
  begin
    Items.Clear;
    List := TStringList.Create;
    try
      GetCompany(List,0);
      for I := 0 to List.Count-1 do Items.Add(List.Strings[i]);
    finally
      List.Free;
    end;

  end;
end;

procedure TfrmNewBaseStorage.tvViewDblClick(Sender: TObject);
var
  szDir : string;
begin
  inherited;
  szDir := Concat(g_Resources, '\'+ g_DirBuildStorage);
  CreateEnclosure(Self.tvView.DataController.DataSource.DataSet,Handle,szDir);
end;

procedure TfrmNewBaseStorage.tvViewEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  inherited;
  dwADO.ADOIsEditing(Self.Master,AAllow);
  
  if (AItem.Index = Self.tvViewColumn24.Index) or 
     (AItem.Index = Self.tvViewColumn1.Index) then  AAllow := False;  
end;

procedure TfrmNewBaseStorage.tvViewTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
  Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
  var AText: string);
var
  t : Currency;
  szCapital : string;

begin
  inherited;

  try
    szCapital := '';
    if AValue <> null then
    begin
      t := StrToCurr( VarToStr(AValue) );
      szCapital := MoneyConvert(t);
    end else
    begin
      szCapital := '';
    end;
    Self.tvViewColumn13.Summary.FooterFormat := szCapital;
  except
  end;
end;

procedure TfrmNewBaseStorage.btnGridSetClick(Sender: TObject);
var
  Child : TfrmIsViewGrid;
  i : Integer;
begin
  inherited;

  Child := TfrmIsViewGrid.Create(Application);
  try
    Child.g_fromName := Self.Name + g_DirBuildStorage;
    Child.ShowModal;
    SetcxGrid(Self.tvView,0, g_DirBuildStorage);
  finally
    Child.Free;
  end;

end;

function TfrmNewBaseStorage.GetRadix(lpRadix : Integer):BOOL;
begin
  case lpRadix of
    0: Radix := -1;
    1: Radix := -2;
    2: Radix := -3;
    3: Radix := -4;
    4: Radix := -5;
    5: Radix := -6;
    6: Radix := -7;
    7: Radix := -8;
  end;
end;

procedure TfrmNewBaseStorage.cxButton1Click(Sender: TObject);
begin
  inherited;
  Self.tvViewColumn5.Editing := False;
end;

procedure TfrmNewBaseStorage.cxButton2Click(Sender: TObject);
begin
  inherited;
  Self.tvViewColumn5.Editing := False;
end;

procedure TfrmNewBaseStorage.cxButton3Click(Sender: TObject);
var
  szValue : Variant;
  s : string;

begin
  inherited;
  with Self.tvViewColumn8 do
  begin
    szValue := Self.cxDBSpinEdit1.EditValue;
    if szValue <> null then
    begin
      s := Self.cxDBSpinEdit1.EditValue;
      EditValue := RoundTo( StrToFloatDef(s , 0 )  , Radix );
    end;
    Editing   := false;
  end;

end;

procedure TfrmNewBaseStorage.cxButton4Click(Sender: TObject);
begin
  inherited;
  Self.tvViewColumn8.Editing := False;
end;

procedure TfrmNewBaseStorage.cxButton6Click(Sender: TObject);
begin
  inherited;
  Self.tvViewColumn15.Editing := False;
end;

procedure TfrmNewBaseStorage.cxButton7Click(Sender: TObject);
begin
  inherited;
  Self.tvViewColumn15.Editing := False;
end;

procedure TfrmNewBaseStorage.cxComboBox1PropertiesCloseUp(Sender: TObject);
var
  i : Integer;
begin
  inherited;

  i := Self.cxComboBox1.ItemIndex;
  if i >= 0 then
  begin
    Self.cxDBSpinEdit2.EditValue := RebarList[i].Weight;
    Self.cxDBSpinEdit2.PostEditValue;
    Self.cxDBTextEdit3.EditValue := Self.cxComboBox1.Text;
    Self.cxDBTextEdit3.PostEditValue;
  end;
end;

procedure TfrmNewBaseStorage.cxComboBox2PropertiesCloseUp(Sender: TObject);
begin
  inherited;
  GetRadix(Self.cxComboBox2.ItemIndex);
end;

procedure TfrmNewBaseStorage.cxDBComboBox1PropertiesInitPopup(Sender: TObject);
var
  List : TStringList;
begin
  inherited;

  List := TStringList.Create;
  try
    List.Clear;
    GetCompany(List,7); //租赁
    with Self.cxDBComboBox1 do
    begin
      Properties.Items := List;
      ItemIndex := 0;
    end;
  finally
    List.Free;
  end;

end;

procedure TfrmNewBaseStorage.cxDBMemo2PropertiesChange(Sender: TObject);
var
  s : Variant;
  szValue : string;
  i : Integer;
  f : Double;
begin
  inherited;

  s := Self.cxDBMemo2.EditingValue;

  if s <> null then
  begin
    szValue := FunExpCalc(s,2);

    if TryStrToInt(szValue,i) or TryStrToFloat(szValue, f) then
    begin

      with Self.Master do
      begin
        if State <> dsInactive then
        begin
          if (State = dsInsert) or (State = dsEdit) then
          begin

            FieldByName(Self.cxDBSpinEdit3.DataBinding.DataField).Value := szValue;
            FieldByName(Self.cxDBSpinEdit1.DataBinding.DataField).Value := RoundTo( StrToFloatDef( szValue , 0 ) , Radix );

          end;
        end;
      end;

    end;
  end;

end;


procedure TfrmNewBaseStorage.cxDBSpinEdit1PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var
  s : string;

begin
  inherited;

  if DisplayValue <> null then
  begin
    s := VarToStr(DisplayValue);
    Self.cxDBSpinEdit1.EditValue := RoundTo( StrToFloatDef(s,0) , Radix );
    Self.cxDBSpinEdit1.PostEditValue;
  end;
end;

procedure TfrmNewBaseStorage.cxDBSpinEdit2PropertiesChange(Sender: TObject);
var
  szValue : Variant;
  s : string;

begin
  inherited;
  szValue := Self.cxDBSpinEdit3.EditingValue;

  if szValue <> null then
  begin
    s := VarToStr(szValue);

    Self.cxDBSpinEdit1.EditValue := RoundTo( StrToFloatDef(s,0) * Self.cxDBSpinEdit2.Value, Radix ) ;
    Self.cxDBSpinEdit1.PostEditValue;
  end;
end;

procedure TfrmNewBaseStorage.cxDBSpinEdit3PropertiesChange(Sender: TObject);
var
  szValue : Variant;
  s : string;

begin
  inherited;
  szValue := Self.cxDBSpinEdit3.EditingValue;
  if szValue <> null then
  begin
    s := VarToStr(szValue);
    Self.cxDBSpinEdit1.EditValue := RoundTo( StrToFloatDef(s,0) , Radix ) ;
    Self.cxDBSpinEdit1.PostEditValue;
  end;

end;

procedure TfrmNewBaseStorage.cxSpinEdit1PropertiesChange(Sender: TObject);
var
  szWeight    : Double;   //重量
  szMeter     : Double;   //米数
  szRootCount : Double;   //根数
  szTheoryWeight : Double; //理论重量
  szPieceCount: Double;   //件数

begin
  inherited;
  szTheoryWeight:= Self.cxDBSpinEdit2.EditingValue;
  szMeter       := Self.cxSpinEdit2.Value;
  szRootCount   := Self.cxSpinEdit5.Value;
  szPieceCount  := Self.cxSpinEdit1.Value;

  if (szRootCount <> 0) then
  begin
    szWeight := (szMeter * (szRootCount * szPieceCount) );

    szWeight := RoundTo( szWeight * szTheoryWeight ,  Radix );
  //  szWeight := RoundTo(szWeight,-3);
    Self.cxDBSpinEdit3.EditValue := szWeight; //计算数值编辑框
    Self.cxDBSpinEdit3.PostEditValue;

    Self.cxDBSpinEdit1.EditValue := szWeight;// szWeight;
    Self.cxDBSpinEdit1.PostEditValue;

  end;

end;

procedure TfrmNewBaseStorage.cxSpinEdit5PropertiesChange(Sender: TObject);
var
  szWeight    : Double;   //重量
  szMeter     : Double;   //米数
  szRootCount : Double;   //根数
  szTheoryWeight : Double; //理论重量
  szPieceCount: Double;   //件数

begin
  inherited;
  szTheoryWeight:= Self.cxDBSpinEdit2.EditingValue;
  szMeter       := Self.cxSpinEdit2.Value;
  szRootCount   := Self.cxSpinEdit5.Value;
  szPieceCount  := Self.cxSpinEdit1.Value;

  if (szRootCount <> 0) then
  begin
    szWeight := (szMeter * (szRootCount * szPieceCount) );

    szWeight := RoundTo( szWeight * szTheoryWeight ,  Radix );
  //  szWeight := RoundTo(szWeight,-3);
    Self.cxDBSpinEdit3.EditValue := szWeight; //计算数值编辑框
    Self.cxDBSpinEdit3.PostEditValue;

    Self.cxDBSpinEdit1.EditValue := szWeight;// szWeight;
    Self.cxDBSpinEdit1.PostEditValue;

  end;

end;


procedure TfrmNewBaseStorage.Excel1Click(Sender: TObject);
begin
  inherited;
  //Excel
  CxGridToExcel(Self.Grid,'明细表');
end;

procedure TfrmNewBaseStorage.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmNewBaseStorage.FormCreate(Sender: TObject);
var
  ChildFrame : TProjectFrame;
  i : Integer;

begin
  inherited;
  ChildFrame := TProjectFrame.Create(Self);
  ChildFrame.dwParentHandle := Handle;
  ChildFrame.IsSystem := True;
  ChildFrame.Parent := Self.RzPanel2;
  ChildFrame.FrameClick(Self);
  dwSQLtext :=  'Select * from ' + g_Table_Project_BuildStorage + ' Where 1=2';
  SetcxGrid(Self.tvview,0,g_DirBuildStorage);
  Self.cxComboBox1.Clear;
  for I := 0 to High(RebarList) do Self.cxComboBox1.Properties.Items.Add( RebarList[i].Model );

  with DM do
  begin
    ADOQuery1.Close;
    ADOQuery1.SQL.Text := 'Select * from '+ g_Table_MakingsList ;
    ADOQuery1.Open;
    Self.ADOMaking.Data := DataSetProvider1.Data;
    Self.ADOMaking.Open;
  end;
end;

procedure TfrmNewBaseStorage.MasterAfterDelete(DataSet: TDataSet);
begin
  inherited;
  Self.RzToolButton2.Enabled := False;
end;

procedure TfrmNewBaseStorage.MasterAfterInsert(DataSet: TDataSet);
var
  szCode : string;
  szColName : string;

begin
  dwPrefix := 'CK';
  dwSuffix := '00002';
  with DataSet do
  begin
    FieldByName('Code').Value    := g_PostCode;
    Self.tvViewColumn3.EditValue := g_ProjectName;
    Self.tvViewColumn2.EditValue := Date;
    szColName := Self.tvViewColumn1.DataBinding.FieldName;
    szCode := dwPrefix + GetRowCode(g_Table_Project_BuildStorage,szColName,dwSuffix,70000);
    Self.tvViewColumn1.EditValue := szCode;
    Self.tvViewColumn28.EditValue:= True;
    Self.RzToolButton2.Enabled := False;
  end;
end;

procedure TfrmNewBaseStorage.MasterAfterPost(DataSet: TDataSet);
begin
  inherited;
  with DataSet do
  begin
    Self.RzToolButton2.Enabled := True;
  end;  
end;

procedure TfrmNewBaseStorage.N2Click(Sender: TObject);
begin
  inherited;
  DM.BasePrinterLink1.Component := Self.Grid;
  DM.BasePrinter.Preview(True,nil);
end;

procedure TfrmNewBaseStorage.RzToolButton1Click(Sender: TObject);
begin
  dwIsSelect := False;
  dwADO.ADOAppend(Self.Master);
  Self.Grid.SetFocus;
  Self.tvViewColumn15.FocusWithSelection;
  keybd_event(VK_RETURN,0,0,0);
  
end;

procedure TfrmNewBaseStorage.RzToolButton2Click(Sender: TObject);
begin
  dwIsSelect := False;
  dwADO.ADOIsEdit(Self.Master);
end;

procedure TfrmNewBaseStorage.RzToolButton3Click(Sender: TObject);
begin
  inherited;
  dwIsSelect := True;
  TfrmStorageManage.Create(Application);
end;

procedure TfrmNewBaseStorage.RzToolButton85Click(Sender: TObject);
var 
  szSqlText , szCodeList : string;
begin
  if dwIsSelect then  //是否为查询
  begin
    
  end;
  {
  szCodeList := GetCodeList;  //取出列表代码
  szSqlText := 'Select * from ' + g_Table_Project_BuildStorage +
   ' Where ' + Self.tvViewColumn1.DataBinding.FieldName +
   ' in ("'+ szCodeList +'")';
  }
  szSqlText := dwSQLtext;
  dwADO.ADODeleteSelection(Self.tvView,
                           g_DirBuildStorage,
                           g_Table_Project_BuildStorage,
                           Self.tvViewColumn1.DataBinding.FieldName);
end;

procedure TfrmNewBaseStorage.RzToolButton88Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmNewBaseStorage.RzToolButton90Click(Sender: TObject);
var
  ErrorCount : Integer;
  I: Integer;
  s , szSqlText : string;
  szCodeList  : string;

begin
  if not dwIsSelect then  //是否为查询
  begin
    szCodeList := GetCodeList;  //取出列表代码
    szSqlText := 'Select * from ' + g_Table_Project_BuildStorage +
       ' Where ' + Self.tvViewColumn1.DataBinding.FieldName +
       ' in ("'+ szCodeList +'")';
       
  end else
  begin
    szSqlText := dwSQLtext;
  end;
  dwADO.ADOSaveData(Self.Master,szSqlText);
  {
  Try
    DM.ADOQuery1.Close;
    DM.ADOQuery1.SQL.Text := dwSqlText;

    with Self.Master do
    begin

      if ChangeCount > 0 then
      begin
        if State in [DsEdit, DSInsert] then Post;

        DM.DataSetProvider1.DataSet.Open;
        with DM.DataSetProvider1.DataSet.FieldByName('Id') do ProviderFlags := ProviderFlags + [pfinkey];

        DM.DataSetProvider1.ApplyUpdates(Delta,0,ErrorCount);
        case ErrorCount of
          0:
          begin
            MergeChangeLog;
            Application.MessageBox( '数据保存成功！', '提示:', MB_OK + MB_ICONINFORMATION)
          end;
          1:
          begin
            Application.MessageBox( '数据保存失败！', '提示:', MB_OK + MB_ICONWARNING)
          end;
        end;

      end else
      begin
        ShowMessage('暂时没有修改或新增的数据，无需保存!');
      end;

    end;

  Except
    on e:Exception do
    begin
      ShowMessage(e.Message);
    end;
  End;
  }
end;

end.
