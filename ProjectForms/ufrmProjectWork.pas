unit ufrmProjectWork;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, RzButton, RzPanel, Vcl.ComCtrls,
  Vcl.ExtCtrls, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, dxSkinsCore, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxCore,
  cxDateUtils, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar, cxLabel,
  Vcl.Menus, cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxNavigator, Data.DB, cxDBData, cxCheckBox, cxDBLookupComboBox,
  cxSpinEdit, cxCurrencyEdit, cxMemo, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid,
  Vcl.StdCtrls, cxButtons, cxGroupBox, Data.Win.ADODB,ufrmBaseController ,
  System.DateUtils, Math, frxClass, frxDBSet;

type
  TfrmProjectWork = class(TfrmBaseController)
    Splitter1: TSplitter;
    RzPanel1: TRzPanel;
    RzToolbar9: TRzToolbar;
    RzSpacer64: TRzSpacer;
    RzToolButton49: TRzToolButton;
    RzSpacer65: TRzSpacer;
    RzToolButton50: TRzToolButton;
    RzSpacer66: TRzSpacer;
    RzToolButton51: TRzToolButton;
    RzSpacer67: TRzSpacer;
    RzToolButton52: TRzToolButton;
    RzToolButton53: TRzToolButton;
    RzSpacer68: TRzSpacer;
    RzToolButton54: TRzToolButton;
    RzSpacer69: TRzSpacer;
    RzSpacer70: TRzSpacer;
    RzSpacer71: TRzSpacer;
    RzSpacer72: TRzSpacer;
    RzToolButton55: TRzToolButton;
    RzSpacer73: TRzSpacer;
    RzToolButton56: TRzToolButton;
    RzSpacer74: TRzSpacer;
    RzToolButton57: TRzToolButton;
    RzSpacer75: TRzSpacer;
    RzSpacer76: TRzSpacer;
    cxLabel44: TcxLabel;
    cxDateEdit1: TcxDateEdit;
    cxLabel45: TcxLabel;
    cxDateEdit2: TcxDateEdit;
    WorkGrid: TcxGrid;
    tvWorkGrid: TcxGridDBTableView;
    tvWorkGridColumn16: TcxGridDBColumn;
    tvWorkGridColumn19: TcxGridDBColumn;
    tvWorkGridColumn1: TcxGridDBColumn;
    tvWorkGridColumn2: TcxGridDBColumn;
    tvWorkGridColumn3: TcxGridDBColumn;
    tvWorkGridColumn17: TcxGridDBColumn;
    tvWorkGridColumn4: TcxGridDBColumn;
    tvWorkGridColumn5: TcxGridDBColumn;
    tvWorkGridColumn18: TcxGridDBColumn;
    tvWorkGridColumn6: TcxGridDBColumn;
    tvWorkGridColumn13: TcxGridDBColumn;
    tvWorkGridColumn14: TcxGridDBColumn;
    tvWorkGridColumn15: TcxGridDBColumn;
    tvWorkGridColumn7: TcxGridDBColumn;
    tvWorkGridColumn8: TcxGridDBColumn;
    tvWorkGridColumn9: TcxGridDBColumn;
    tvWorkGridColumn10: TcxGridDBColumn;
    tvWorkGridColumn11: TcxGridDBColumn;
    tvWorkGridColumn12: TcxGridDBColumn;
    lv2: TcxGridLevel;
    RzPanel2: TRzPanel;
    ADOWork: TADOQuery;
    DataWork: TDataSource;
    qry: TADOQuery;
    ds: TDataSource;
    RzPanel25: TRzPanel;
    Label6: TLabel;
    Label15: TLabel;
    Label19: TLabel;
    Label39: TLabel;
    Label40: TLabel;
    Label41: TLabel;
    Label42: TLabel;
    Label43: TLabel;
    Label44: TLabel;
    cxDateEdit11: TcxDateEdit;
    cxDateEdit12: TcxDateEdit;
    cxButton11: TcxButton;
    cxCurrencyEdit1: TcxCurrencyEdit;
    tvWorkGridColumn20: TcxGridDBColumn;
    frxWorkReport: TfrxDBDataset;
    Print: TPopupMenu;
    Excel1: TMenuItem;
    N2: TMenuItem;
    tvWorkGridColumn21: TcxGridDBColumn;
    tvWorkGridColumn22: TcxGridDBColumn;
    RzToolButton1: TRzToolButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure RzToolButton54Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure RzToolButton49Click(Sender: TObject);
    procedure ADOWorkAfterInsert(DataSet: TDataSet);
    procedure tvWorkGridColumn16GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure RzToolButton50Click(Sender: TObject);
    procedure tvWorkGridColumn5PropertiesCloseUp(Sender: TObject);
    procedure tvWorkGridEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure tvWorkGridEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure RzToolButton51Click(Sender: TObject);
    procedure RzToolButton52Click(Sender: TObject);
    procedure tvWorkGridColumnSizeChanged(Sender: TcxGridTableView;
      AColumn: TcxGridColumn);
    procedure RzToolButton56Click(Sender: TObject);
    procedure RzToolButton57Click(Sender: TObject);
    procedure RzToolButton55Click(Sender: TObject);
    procedure cxButton11Click(Sender: TObject);
    procedure tvWorkGridColumn19PropertiesChange(Sender: TObject);
    procedure tvWorkGridStylesGetContentStyle(Sender: TcxCustomGridTableView;
      ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem;
      var AStyle: TcxStyle);
    procedure tvWorkGridKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure tvWorkGridDblClick(Sender: TObject);
    procedure tvWorkGridColumn17GetPropertiesForEdit(
      Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
      var AProperties: TcxCustomEditProperties);
    procedure ADOWorkAfterOpen(DataSet: TDataSet);
    procedure tvWorkGridTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
      Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
      var AText: string);
    procedure tvWorkGridColumn7PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure tvWorkGridColumn8PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure tvWorkGridColumn9PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure tvWorkGridColumn10PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure Excel1Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure RzToolButton1Click(Sender: TObject);
  private
    { Private declarations }
    g_ProjectName : string;
    g_PostCode    : string;
    IsSave : Boolean;
    Prefix : string;
    Suffix : string;
    procedure  SetcxGrid(lpGridView : TcxGridDBTableView ; index : Byte ;lpSuffix:string);
    function SumMoney(DaysCount , UnitPrice , Overtimer , OverPrice : Variant):Variant;
  public
    { Public declarations }
    procedure WndProc(var Message: TMessage); override;  // 第一优先权

  end;

var
  frmProjectWork: TfrmProjectWork;


implementation

uses
  uProjectFrame,uDataModule,global,ufunctions,ufrmIsViewGrid;

{$R *.dfm}

procedure  TfrmProjectWork.SetcxGrid(lpGridView : TcxGridDBTableView ; index : Byte ;
lpSuffix:string);
var
  i : Integer;
begin
  for I := 0 to lpGridView.ColumnCount - 1 do
  begin
    Application.ProcessMessages;
    SetGrid(lpGridView.Columns[i],index, Self.Name + lpSuffix);
  end;
end;

procedure TfrmProjectWork.WndProc(var Message: TMessage);
var
  pMsg : PProject;
  s : string;
  szCodeList : string;
  szTableList : array[0..1] of TTableList;
begin
  case Message.Msg of
    WM_FrameClose:
      begin
        Self.ADOWork.Close;
        Self.qry.Close;
      end;
    WM_FrameView :
    begin
      pMsg := PProject(Message.LParam);
      g_PostCode := pMsg.dwCode;
      g_ProjectName := pMsg.dwName;

      s := 'Select *from ' + g_Table_Project_Worksheet + ' Where Code ="' +  g_PostCode + '"';

      with Self.ADOWork do
      begin
        Close;
        SQL.Clear;
        SQL.Text := s;
        Open;
      end;

      with Self.qry do
      begin
        Close;
        SQL.Clear;
      //  SQL.Text := 'Select * from ' + g_Table_Maintain_Work ;
        SQL.Text := 'Select * from '     + g_Table_Maintain_Work +
                    ' AS PT left join '  + g_Table_CompanyManage +
                    ' AS PS on PT.SignName = PS.SignName WHERE ProjectName="' + g_ProjectName + '"';

        Open;
      end;

    end;
    WM_FrameDele :
    begin
      pMsg   := PProject(Message.LParam);
      szCodeList := pMsg.dwCodeList;

      szTableList[0].ATableName := g_Table_Project_Worksheet;
      szTableList[0].ADirectory := g_DirWork;

      DeleteTableFile(szTableList,szCodeList);
    //  DM.InDeleteData(g_Table_Project_Worksheet,szCodeList); //清除主目录表内关联
    end;
  end;
  // 一定要加上这句，否则编译通不过。因为绝大部分消息没人处理了
  inherited WndProc(Message); // 会一路向上调用，直到TControl.WndProc调用Dispatch来寻找消息处理函数
end;

procedure TfrmProjectWork.ADOWorkAfterInsert(DataSet: TDataSet);
var
  szColName : string;
  szCode : string;

begin
  with DataSet do
  begin
    if State <> dsInactive then
    begin
      FieldByName('Code').Value := g_PostCode;

      szColName := Self.tvWorkGridColumn3.DataBinding.FieldName;
      FieldByName(szColName).Value := g_ProjectName;

      szColName := Self.tvWorkGridColumn1.DataBinding.FieldName; //编号
      szCode := Prefix + GetRowCode(g_Table_Project_Worksheet,szColName,Suffix,40000);
      FieldByName(szColName).Value := szCode;

      szColName := Self.tvWorkGridColumn2.DataBinding.FieldName;
      FieldByName(szColName).Value := Date;

      FieldByName(Self.tvWorkGridColumn8.DataBinding.FieldName).AsVariant := 0;
      FieldByName(Self.tvWorkGridColumn11.DataBinding.FieldName).AsVariant:= 0;
      FieldByName(Self.tvWorkGridColumn7.DataBinding.FieldName).AsVariant := 0;
      FieldByName(Self.tvWorkGridColumn19.DataBinding.FieldName).Value := True;

    end;
  end;

end;

procedure TfrmProjectWork.ADOWorkAfterOpen(DataSet: TDataSet);
begin
  inherited;
  TADOQuery(DataSet).Properties.Get_Item('Update Criteria').Value :=0;
end;

procedure TfrmProjectWork.cxButton11Click(Sender: TObject);
var
  szStartTime : TDateTime;
  szEndTime   : TDateTime;
  szValue : Integer;
  m : Currency;
  Minute : Double;

begin //添加统计
  inherited;

  if Sender = Self.cxButton11 then
  begin
    m := Self.cxCurrencyEdit1.Value;
    szStartTime := Self.cxDateEdit11.Date;
    szEndTime   := Self.cxDateEdit12.Date;
    if CompareDateTime(szEndTime,szStartTime) <> -1 then
    begin

      if m <> 0 then
      begin
        Minute := MinuteSpan(szEndTime , szStartTime); //得到总分钟数
        m := m / 60;
        Label44.Caption := FloatToStr(Math.RoundTo( m * Minute , -2)) + ' 元';

      end;
      Label41.Caption := getTaskTimeStr(szEndTime,szStartTime,0);
      Label39.Caption := getDateTimeStr(szEndTime,szStartTime);
      if Length( Label39.Caption ) = 0 then Label39.Caption := '无';

    end
    else
    begin
      Application.MessageBox('开始时间不能大于结束时间,无法计算正确计算加班时间',m_title,MB_OK + MB_ICONHAND);
    end;
  end;

end;

procedure TfrmProjectWork.Excel1Click(Sender: TObject);
begin
  inherited;
  CxGridToExcel(Self.WorkGrid,'考勤明细表');
end;

procedure TfrmProjectWork.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmProjectWork.FormCreate(Sender: TObject);
var
  ChildFrame : TProjectFrame;
begin
  Prefix := 'KQ';
  Suffix := '00003';
  ChildFrame := TProjectFrame.Create(Self);
  ChildFrame.dwParentHandle := Handle;
  ChildFrame.Parent := Self.RzPanel2;
  ChildFrame.IsSystem := True;
  ChildFrame.FrameClick(Self);
  SetcxGrid(Self.tvWorkGrid,0,g_DirWork);
  Self.cxDateEdit1.Date := Date;
  Self.cxDateEdit2.Date := Date;
  Self.cxDateEdit11.Date:= Now;
  Self.cxDateEdit12.Date:= Now;
end;

procedure TfrmProjectWork.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 27 then Close;
  
end;

procedure TfrmProjectWork.N2Click(Sender: TObject);
begin
  inherited;
  DM.BasePrinterLink1.Component := Self.WorkGrid;
  DM.BasePrinterLink1.ReportTitleText := '项目名称：' + g_Projectname;
  DM.BasePrinter.Preview(True, nil);
end;

procedure TfrmProjectWork.RzToolButton1Click(Sender: TObject);
begin
  inherited;
  ADOIsEdit( tvWorkGridColumn19.EditValue , Self.ADOwork)
end;

procedure TfrmProjectWork.RzToolButton49Click(Sender: TObject);
begin
  inherited;
  with Self.ADOWork do
  begin
    if State = dsInactive then
    begin
      Application.MessageBox( '当前查询状态无效，单击项目管理在进行操作？', '提示:', MB_OKCANCEL + MB_ICONWARNING)
    end else
    begin
      First;
      Insert;
    //  Append;
      Self.tvWorkGridColumn5.FocusWithSelection;
      Self.WorkGrid.SetFocus;
      keybd_event(VK_RETURN,0,0,0);

    end;

  end;
end;

procedure TfrmProjectWork.RzToolButton50Click(Sender: TObject);
begin
  IsDeleteEmptyData(Self.ADOWork ,
                      g_Table_Project_Worksheet ,
                      Self.tvWorkGridColumn5.DataBinding.FieldName);
  IsSave := False;
end;

procedure TfrmProjectWork.RzToolButton51Click(Sender: TObject);
begin
  DeleteSelection(Self.tvWorkGrid,'',g_Table_Project_Worksheet,
  Self.tvWorkGridColumn1.DataBinding.FieldName);
  Self.ADOWork.Requery;
end;

procedure TfrmProjectWork.RzToolButton52Click(Sender: TObject);
var
  child : TfrmIsViewGrid;
begin
  //表格设置
  Child := TfrmIsViewGrid.Create(Application);
  try
    Child.g_fromName := Self.Name + g_DirWork;
    Child.ShowModal;
    SetcxGrid(Self.tvWorkGrid,0,g_DirWork);
  finally
    Child.Free;
  end;
end;

procedure TfrmProjectWork.RzToolButton54Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmProjectWork.RzToolButton55Click(Sender: TObject);
var
  sqltext : string;
  szColName : string;
begin
  inherited;
  szColName   := Self.tvWorkGridColumn2.DataBinding.FieldName;
  sqltext := 'select *from '  + g_Table_Project_Worksheet +
             ' where Code= "' + g_PostCode +
             ' " and ' + szColName + ' between :t1 and :t2';
  SearchDateRange(Self.cxDateEdit1.Text,Self.cxDateEdit2.Text,sqltext,Self.ADOWork);
end;

procedure TfrmProjectWork.RzToolButton56Click(Sender: TObject);
begin
  inherited;
  CreateOpenDir(Handle,Concat(g_Resources,'\' + g_DirWork + '\ColligateFile'),True);
end;

procedure TfrmProjectWork.RzToolButton57Click(Sender: TObject);
begin
  inherited;
  if RzPanel25.Visible then
  begin
    RzPanel25.Visible := False;
  end else
  begin
    RzPanel25.Visible := True;
  end;
end;

procedure TfrmProjectWork.tvWorkGridColumn10PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var
  szRowIndex : Integer;
  DaysCount  : Variant;
  UnitPrice  : Variant;
  Overtimer  : Variant;

begin
  inherited;
  szRowIndex := Self.tvWorkGrid.Controller.FocusedRowIndex;
  DaysCount  := Self.tvWorkGrid.DataController.Values[szRowIndex,Self.tvWorkGridColumn7.Index];
  UnitPrice  := Self.tvWorkGrid.DataController.Values[szRowIndex,Self.tvWorkGridColumn8.Index];
  Overtimer  := Self.tvWorkGrid.DataController.Values[szRowIndex,Self.tvWorkGridColumn9.Index];
//  OverPrice  := Self.tvWorkGrid.DataController.Values[szRowIndex,Self.tvWorkGridColumn10.Index];
  SumMoney(DaysCount,UnitPrice,Overtimer,DisplayValue);
end;

procedure TfrmProjectWork.tvWorkGridColumn16GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  AText := IntToStr( ARecord.Index + 1 );
end;

procedure TfrmProjectWork.tvWorkGridColumn17GetPropertiesForEdit(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AProperties: TcxCustomEditProperties);
begin
  inherited;
  GetCompanyInfo(AProperties);
end;

procedure TfrmProjectWork.tvWorkGridColumn19PropertiesChange(Sender: TObject);
var
  szRowIndex : Integer;

begin
  inherited;
  szRowIndex := Self.tvWorkGrid.DataController.FocusedRecordIndex;
  Self.ADOWork.UpdateBatch();
  Self.ADOWork.Requery();
  Self.tvWorkGrid.Controller.FocusedRowIndex := szRowIndex;
end;

procedure TfrmProjectWork.tvWorkGridColumn5PropertiesCloseUp(Sender: TObject);
var
  szColName : string;

  szCol1Name: Variant;
  szCol2Name: Variant;
  szCol3Name: Variant;
  szCol4Name: Variant;
  szCol5Name: Variant;
  szCol6Name: Variant;
  szCol7Name: Variant;
  szCol8Name: Variant;

  szRowIndex: Integer;

  DaysCount  : Variant;
  UnitPrice  : Variant;
  Overtimer  : Variant;
  OverPrice  : Variant;
  szDate : Variant;
  szRecordCount : Integer;

begin
  inherited;
  szRowIndex := Self.tvWorkGrid.Controller.FocusedRowIndex;
  szDate := Self.tvWorkGrid.DataController.Values[szRowIndex,Self.tvWorkGridColumn2.Index];

  with (Sender as TcxLookupComboBox).Properties.Grid do
  begin
    szRowIndex := FocusedRowIndex;
    if szRowIndex >= 0 then
    begin
      szColName  := Self.tvWorkGridColumn2.DataBinding.FieldName;
      szCol1Name := DataController.Values[szRowIndex, 0]; //往来单位
      ////////////////////////////////////////////////////////////////////////
      szCol2Name := DataController.Values[szRowIndex, 1]; //简码
      szCol3Name := DataController.Values[szRowIndex, 2]; //工种
      szCol4Name := DataController.Values[szRowIndex, 3]; //单价(工日)
      szCol5Name := DataController.Values[szRowIndex, 4]; //单价(加班)
      szCol6Name := DataController.Values[szRowIndex, 5]; //部门
      szCol7Name := DataController.Values[szRowIndex, 6]; //劳务公司
      szCol8Name := DataController.Values[szRowIndex, 7]; //工种

      with DM.Qry do
      begin
        Close;
        SQL.Text := 'Select * from ' + g_Table_Project_Worksheet +
                    ' Where ' +
                    Self.tvWorkGridColumn5.DataBinding.FieldName + '="' + VarToStr(szCol1Name) + '" AND ' +
                    Self.tvWorkGridColumn3.DataBinding.FieldName + '="' + g_ProjectName +  '" AND ' +
                    szColName + '=#' + VarToStr( szDate ) + '#';
        Open;
        szRecordCount := RecordCount;
        Close;
      end;

      if szRecordCount = 0 then
      begin
        ////////////////////////////////////////////////////////////////////////
      end else
      begin
        szCol1Name := null;
        szCol1Name := null; // 往来单位
        szCol3Name := null; //工种
        szCol4Name := null; //工日单价
        szCol5Name := null; //加班单价
        szCol6Name := null; //部门
        szCol7Name := null; //劳务公司
        szCol8Name := null; //工种
        ShowMessage('日期:' + vartostr(szDate) + ' 已有相同员工签到');
      end;

      /////////////////////////////////////////////////
      with Self.ADOWork do
      begin
        if State <> dsInactive then
        begin
          if State <> dsEdit then
            Edit;

          FieldByName(Self.tvWorkGridColumn5.DataBinding.FieldName).Value  := szCol1Name; // 往来单位
          FieldByName(Self.tvWorkGridColumn6.DataBinding.FieldName).Value  := szCol3Name; //工种
          FieldByName(Self.tvWorkGridColumn8.DataBinding.FieldName).Value  := szCol4Name; //工日单价
          FieldByName(Self.tvWorkGridColumn10.DataBinding.FieldName).Value := szCol5Name; //加班单价
          FieldByName(Self.tvWorkGridColumn20.DataBinding.FieldName).Value := szCol6Name; //部门
          FieldByName(Self.tvWorkGridColumn21.DataBinding.FieldName).Value := szCol7Name; //劳务公司
          FieldByName(Self.tvWorkGridColumn22.DataBinding.FieldName).Value := szCol8Name; //工种
        end;
      end;

      szRowIndex := Self.tvWorkGrid.Controller.FocusedRowIndex;
      DaysCount  := Self.tvWorkGrid.DataController.Values[szRowIndex,Self.tvWorkGridColumn7.Index];
    //  UnitPrice  := Self.tvWorkGrid.DataController.Values[szRowIndex,Self.tvWorkGridColumn8.Index];
      Overtimer  := Self.tvWorkGrid.DataController.Values[szRowIndex,Self.tvWorkGridColumn9.Index];
    //  OverPrice  := Self.tvWorkGrid.DataController.Values[szRowIndex,Self.tvWorkGridColumn10.Index];
      SumMoney(DaysCount,szCol4Name,Overtimer,szCol5Name);
      /////////////////////////////////////////////////
    end;

  end;

end;

procedure TfrmProjectWork.tvWorkGridColumn7PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var
  szRowIndex : Integer;
  DaysCount  : Variant;
  UnitPrice  : Variant;
  Overtimer  : Variant;
  OverPrice  : Variant;

begin
  inherited;
  szRowIndex := Self.tvWorkGrid.Controller.FocusedRowIndex;
//  DaysCount  := Self.tvWorkGrid.DataController.Values[szRowIndex,Self.tvWorkGridColumn7.Index];
  UnitPrice  := Self.tvWorkGrid.DataController.Values[szRowIndex,Self.tvWorkGridColumn8.Index];
  Overtimer  := Self.tvWorkGrid.DataController.Values[szRowIndex,Self.tvWorkGridColumn9.Index];
  OverPrice  := Self.tvWorkGrid.DataController.Values[szRowIndex,Self.tvWorkGridColumn10.Index];
  SumMoney(DisplayValue,UnitPrice,Overtimer,OverPrice);
end;

procedure TfrmProjectWork.tvWorkGridColumn8PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var
  szRowIndex : Integer;
  DaysCount  : Variant;
  UnitPrice  : Variant;
  Overtimer  : Variant;
  OverPrice  : Variant;

begin
  inherited;
  szRowIndex := Self.tvWorkGrid.Controller.FocusedRowIndex;
  DaysCount  := Self.tvWorkGrid.DataController.Values[szRowIndex,Self.tvWorkGridColumn7.Index];
//  UnitPrice  := Self.tvWorkGrid.DataController.Values[szRowIndex,Self.tvWorkGridColumn8.Index];
  Overtimer  := Self.tvWorkGrid.DataController.Values[szRowIndex,Self.tvWorkGridColumn9.Index];
  OverPrice  := Self.tvWorkGrid.DataController.Values[szRowIndex,Self.tvWorkGridColumn10.Index];

  SumMoney(DaysCount,DisplayValue,Overtimer,OverPrice);
end;

procedure TfrmProjectWork.tvWorkGridColumn9PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var
  szRowIndex : Integer;
  DaysCount  : Variant;
  UnitPrice  : Variant;
  Overtimer  : Variant;
  OverPrice  : Variant;

begin
  inherited;
  szRowIndex := Self.tvWorkGrid.Controller.FocusedRowIndex;
  DaysCount  := Self.tvWorkGrid.DataController.Values[szRowIndex,Self.tvWorkGridColumn7.Index];
  UnitPrice  := Self.tvWorkGrid.DataController.Values[szRowIndex,Self.tvWorkGridColumn8.Index];
//  Overtimer  := Self.tvWorkGrid.DataController.Values[szRowIndex,Self.tvWorkGridColumn9.Index];
  OverPrice  := Self.tvWorkGrid.DataController.Values[szRowIndex,Self.tvWorkGridColumn10.Index];

  SumMoney(DaysCount,UnitPrice,DisplayValue,OverPrice);
end;

procedure TfrmProjectWork.tvWorkGridColumnSizeChanged(Sender: TcxGridTableView;
  AColumn: TcxGridColumn);
begin
  inherited;
  SetcxGrid(Self.tvWorkGrid,1,g_DirWork);
end;

procedure TfrmProjectWork.tvWorkGridDblClick(Sender: TObject);
var
  szDir : string;
begin
  inherited;
  szDir := Concat(g_Resources, '\'+ g_DirWork);
  CreateEnclosure(Self.tvWorkGrid.DataController.DataSource.DataSet,Handle,szDir);
end;

procedure TfrmProjectWork.tvWorkGridEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
var
  szColName : string;

begin
  inherited;
  with Self.ADOWork do
  begin
    if State <> dsInactive then
    begin
      if (State <> dsEdit) and (State <> dsInsert) then
      begin
        AAllow := False;
      end else
      begin
        AAllow := True;
      end;

    end;
  end;

  {
  szColName := Self.tvWorkGridColumn19.DataBinding.FieldName;
  if AItem.Index <> Self.tvWorkGridColumn19.Index then
  begin

  end;
  }
  if (AItem.Index = Self.tvWorkGridColumn16.Index) or (AItem.Index = tvWorkGridColumn1.Index) then
  begin
    AAllow := False;
  end;
end;

function TfrmProjectWork.SumMoney(DaysCount , UnitPrice , Overtimer , OverPrice : Variant):Variant;
var
  szDaysSum : Currency;
  szOverSum : Currency;
  szRowIndex: Integer;
  szColName : string;

begin

  szDaysSum := StrToCurrDef( VarToStr( DaysCount ) , 0 ) * StrToCurrDef( VarToStr( UnitPrice ) , 0);
  szOverSum := StrToCurrDef( VarToStr( Overtimer ) , 0 ) * StrToCurrDef( VarToStr( OverPrice ) , 0);

  with Self.ADOWork do
  begin
    if State <> dsInactive then
    begin
      if (State <> dsInsert) or (State <> dsEdit) then
      begin

        szColName := Self.tvWorkGridColumn7.DataBinding.FieldName;
        FieldByName(szColName).Value := DaysCount; //天(工日)
        szColName := Self.tvWorkGridColumn8.DataBinding.FieldName;
        FieldByName(szColName).Value := UnitPrice; //天单价
        szColName := Self.tvWorkGridColumn9.DataBinding.FieldName;
        FieldByName(szColName).Value := Overtimer; //加班时间
        szColName := Self.tvWorkGridColumn10.DataBinding.FieldName;
        FieldByName(szColName).Value := OverPrice; //加班单价
        szColName := Self.tvWorkGridColumn11.DataBinding.FieldName;
        FieldByName(szColName).Value := szDaysSum +  szOverSum;

      end;

    end;

  end;

end;

procedure TfrmProjectWork.tvWorkGridEditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
var
  szTotal : Currency;
  szDaysCount : Single;
  szUnitPrice : Currency;
  szOvertimer : Single;

  szFieldNameUnitPrice : string;
  szFieldNameWorkTotal : string;
  szFieldNameWorkDays  : string;
  szFieldNameOvertimer : string;
  szFieldNameWorkUnitPrice:string;

  szEditValue : Variant;
begin
  inherited;

  if (Key = 13) then
  begin
      try
        if AItem.Index = Self.tvWorkGridColumn11.Index then
        begin
          if Self.tvWorkGrid.Controller.FocusedRow.IsLast and IsNewRow then
          begin
            //在最后一行新增

            with Self.ADOWork do
            begin
              if State <> dsInactive then
              begin
                Append;
                Self.tvWorkGridColumn2.FocusWithSelection;
              end;
            end;

          end;

        end;

        {
      //  Self.tvWorkGrid.DataController.UpdateData;
        szEditValue := AEdit.EditingValue;

        with Self.ADOWork do
        begin
          szFieldNameWorkTotal := Self.tvWorkGridColumn11.DataBinding.FieldName; //总额
          szFieldNameWorkDays  := Self.tvWorkGridColumn7.DataBinding.FieldName; //取出工日/天

          szDaysCount := FieldByName(szFieldNameWorkDays).AsFloat; //正常天数
          if (State = dsEdit) or (State = dsinsert) then
          begin

            if AItem.Name = Self.tvWorkGridColumn8.Name then
            begin
              FieldByName(szFieldNameWorkTotal).AsCurrency := szDaysCount * StrToIntDef(VarToStr( szEditValue ),0);
            end;

            if (AItem.Name = Self.tvWorkGridColumn9.Name) then
            begin
              szFieldNameUnitPrice     := Self.tvWorkGridColumn8.DataBinding.FieldName; //工日单价
              szFieldNameOvertimer     := Self.tvWorkGridColumn9.DataBinding.FieldName; //加班小时
              szFieldNameWorkUnitPrice := Self.tvWorkGridColumn10.DataBinding.FieldName;//加班单价

              szUnitPrice := FieldByName(szFieldNameUnitPrice).AsCurrency; //取出工日单价
              szOvertimer := FieldByName(szFieldNameOvertimer).AsFloat;    //取出加班小时

              FieldByName(szFieldNameWorkTotal).AsCurrency := (szDaysCount * szUnitPrice) +
                       (szOvertimer * FieldByName(szFieldNameWorkUnitPrice).AsCurrency );

            end;



          end;

        end;
        }
      except
        on e:Exception do
        begin
          ShowMessage('ClassName:' + e.ClassName + ' Msg:' + e.Message );
        end;

      end;

  end;

end;

procedure TfrmProjectWork.tvWorkGridKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = 46 then Self.RzToolButton51.Click;

end;

procedure TfrmProjectWork.tvWorkGridStylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
var
  szIndex : Integer;
  Value : Variant;
begin
  inherited;
  Value := ARecord.Values[Self.tvWorkGridColumn19.Index];
  if Value = False then
  begin
    AStyle := DM.cxStyle222;
  end;
end;

procedure TfrmProjectWork.tvWorkGridTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
  Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
  var AText: string);
var
  t : Currency;
  szCapital : string;

begin
  inherited;
  try
    szCapital := '';
    if AValue <> null then
    begin
      t := StrToCurr( VarToStr(AValue) );
      szCapital := MoneyConvert(t);
    end else
    begin
      szCapital := '';
    end;
    Self.tvWorkGridColumn12.Summary.FooterFormat := szCapital;
  except
  end;

end;

end.
