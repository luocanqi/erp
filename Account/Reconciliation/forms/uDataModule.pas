unit uDataModule;

interface

uses
  System.SysUtils, System.Classes, frxClass, frxDesgn;

type
  TDM = class(TDataModule)
    frxReport: TfrxReport;
    frxDesigner: TfrxDesigner;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DM: TDM;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

end.
